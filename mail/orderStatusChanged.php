<?php
/**
 * @var int $orderId
 * @var app\models\OrderStatus $Status
 */
$completed = in_array($Status->id, [$Status::STATUS_COMPLITED, $Status::STATUS_REJECTED, $Status::STATUS_WITHDRAWN]);
?>
<p>
    Статус заказа №<?= $orderId ?> изменился на: <i><?= $Status->name_ru ?></i>.
    <?= $completed ? '<br>Заказ перенесен в архив.' : '' ?>
</p>
<p>
    Посмотреть свои активные заказы можно на странице <a href="<?= \yii\helpers\Url::toRoute('/cabinet/orders/', true) ?>">Мои заказы</a>
</p>