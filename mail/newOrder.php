<?php
/**
 * @var int $orderId
 */
?>
<p>
    У вас новый заказ. Номер заказа: <?= $orderId ?>
</p>
<p>
    Посмотреть свои активные заказы можно на странице <a href="<?= \yii\helpers\Url::toRoute('/cabinet/orders/', true) ?>">Мои заказы</a>
</p>