<?php
/**
 * @var string $newPassword
 */
?>
<p>
    На сайте <a href="<?= \yii\helpers\Url::home(true) ?>">Tailor place</a>
    вы осуществили процедуру востановления пароля.
</p>
<p>
    Новый пароль: <?= $newPassword ?>
</p>
<p>
    После авторизации вы можете сменить пароль в личном кабинете.
</p>