<?php
/**
 * Шаблон уведомления пользователям о наличии непрочитанных сообщений.
 *
 * @var $userName string
 * @var $countOfMessages int
 */
?>
<p>Приветствуем, <?= $userName ?></p>
<p>У вас новые сообщения: <b><?= $countOfMessages ?></b> шт.</p>