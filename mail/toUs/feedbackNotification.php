<?php
/**
 * Уведомление о том, что пользователь воспользовался формой обратной связи
 *
 * @var app\models\Feedback $FeedbackObject
 */

use yii\helpers\Html;

?>
<p>
    Email: <b><?= Html::encode($FeedbackObject->email) ?></b><br>
    Пользователь был авторизован: <b><?= (isset($FeedbackObject->from_authorized_user) && $FeedbackObject->from_authorized_user > 0) ? 'да' : 'нет' ?></b><br>
    Пользователь с таким email'ом зарегистрирован: <b><?= (isset($FeedbackObject->email_registered_to_user) && $FeedbackObject->email_registered_to_user > 0) ? 'да' : 'нет' ?></b><br>
    Сообщение пользователя:
</p>
<p style="margin-left: 15px;">
    <?= Html::encode($FeedbackObject->message) ?>
</p>