<?php
/**
 * Шаблон письма в виде свитка (или модального окна)
 *
 * @var $this \yii\web\View view component instance
 * @var $content string main view render result
 */
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div style="background: #FDF8F0; border-top: solid 3px #E9DCD4; border-bottom: solid 3px #E9DCD4;">
        <div style="margin: 35px auto 35px; width: 500px; border: solid 1px #D0BAAD; border-radius: 20px; padding: 25px; background: #FFFFFF; box-shadow: 5px 5px 10px rgba(0,0,0,0.5)">
            <a href="http://tailor-place.com" style="display: inline-block; float: left; margin: 0 15px 0 0">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJwAAACBCAIAAAC6kkRfAAAMpUlEQVR42u2dCXRU1RmA/8wkk8nsIYAHFIGCsikYQJawVwR64DRFQKu2KAiHSjGiIKjYYFCUuLK0AU7DIhaVQ9WicAq4YghggECiUEQiVKwoGJNZM5ON/jOTTN4+895k3kyu9zsczsybt97v/f/9732PQ9LJ0uKrAElAIQSNRpt0orT4lsyseJ8JpdUoO3EoCSN1AJVKEFQqgVCpBEKlEgiVSiBUKoFQqQRCpRIIlUogVCqBUKkEQqUSCJVKIFQqgVCpBEKlEgiVSiBUKoFQqQRCpRIIlUogVCqBUKkEQqUSCJVKIFQqgUQhtfJtODZd3ibXXYR+1wn/9J974ds3/B+uvwx9OkR13IwSGHxr0+dTU+C7PYzfJsPg3ZAh91J9cOk4XNoKjo/BVxFYYgRdHzDkwLVjoXMX0Ehs+x0UdQGP5O5TBoMxB67/DXRqL/fMBEkMqY1fQ9GN4A1e4ToYu0CqmVSV6oOKArjwF6h3i66iGQadN0GfviLnHIHUEIYNMGgeGOQ1Kp/EkFqxFM690PzFCD0vQw/xK1NNqus0lI8EZ1VEK+uegsynwabl/SBHKpLyLAxbFqXXRJB6BQ51BCdjgf51GPMH5cdtFamuI1AyHOrkXJ1mGmTugPYcrzKlIpa9MHyinA24JIDUi2vg9EL2oi7Q9zx00QrvRAWpjd9CcVd5JoIIxJl8qdKXHwGtWv0eS4JK5vdnYeKycNt4oKQjVPF6rEju1kgOp0wqd6sAmonQdS106QppqVDnhP/th4tzwcNLzh2Ow8CBjO88qYZ/wqhpLV/rfoJT0+HHA6ydMO9L+cRb6o//gJN/FPohC24phmuiPpwCqb6D8NkoaGQvNG2GzFncrq6xCk6Ogytl7KX3wLDtYA19DSfVv5+v4LPeWJO1kLoZxs6KvOE5xFdqA5zoDpcvCv8Y9m6NkdRzj0LFq6wlmkdgxCsixct3UNwbXOxMw+plIpCqpOmkiKtU+z4omcSNiRY4t7yiw8mW2gDHrFDJltS5Am7+legW/JqA1Xf80qR+MQK+P9Ty1bwaGhayrv+aL+CWm6I6nGyp/LpmEPQ/Bp0kNimFDwaxbk1NPty+RHSHAum3HD4ZAPWMJcnr4LYFstqeSfykcruuwPAU8hgDVmydJTA6H1KjOJxsqUdh3xD2kly4PU/mnNEjMPaV5tOOoFAqy4LKr1m7TC+CISOVKAgQP6mhecEgyath3MMAvHtWYmZRJalhMyFfKvMov5whTeOX8OnNrKF9qN/i5OSgbMFAaaORGpZ4Tj7wiVwqa14QWDURPy13Ow+9hKb4E6RPbTwKHw2R0adKE+dpQj6RSuVdJ6sL4Y1zxGYN22j1K0HqCzD4UTApT7xB4iFVYF5Qmi7Q6wx04929MRqncrOI5DhVcEJRepwqRRb0/ww6tT2pDvi8M1S7I95pAPNuyJqs5HCxnlEqHcotXCOfUWI+cAwRdYcKcZAqOi8ojdAtnJhzv9yxteSQ5sJK+Oop9vZG6PE99LTIb6IWVJYqOS8oTajfFWxxJsxWC7tyiF5XoVvzZ0yqh/tyJ/8iIZKnNKxx6hU43B0c7AOl5MOIJaKj8whQV6p9DxyZonTvzUGmglTEcxSODInJ81TO5MOlLVA+m7urTmeh/w1KG0pVqQ1QPhouMcag0i858FNTxzLI7K+SVMTzFZwc3vpvPnCnCT1wrDdUcrKXsnepmlBRKr8AkZhY8PMNHOjBqiOCVeh5taQGTrr131Hiz/0KPtiwfQpDx8hofAYqSuVHnvT4D3hTiRBoereaUoO06tuEAk9p+DkMoglW+t4vgVCpBEKlEgiVSiBUKoFQqQRCpRIIlUogVCqBKJG6eOE8/sKXVm8Muw5drbVWk4ZGKoFQqQRCpRIIlUogVCqBUKkEQqUSCJVKIPGSWr5u6vSt3zCX6Aztr+05es7iRTP6mcS325UzKHcvwA33b9vz0AC5B93z+K2PfFALMOb5Q2unRfYKpvfMO8/lb9179r/VXjxFY4fuQ2fOXXjfuK560QZdN3l24dciP07KLVubHfvGTRypzdgm5b61NlvsHyRFI/XEy1Pv3/gtfjDNyC9eOT78Bt7SdXf+ufBMLWexrvecwvceFDk6lQpw24vHX/Ffpq/ybNEbK54rLLFjo2U9s3v9XcKvXEUhtfLNByasKgp87D3njfce7Bdug5JVk+du/gHAmrXs1WfvGpBRX/HBmieffO1crdQZNktVyZ8giSI1gHf/spFP7HYAjFr88aa7BdssCqkf5g5eussR/DxqefnqO/TS6xevGpnz5mXQjV66t/DODM4Zjl9WUnCHUAanUjlSseGXjVm00wUdZxQcfHyE0HbKpQaP2H3EKCguOt/+7i37hA8QovLNPw1/6TDAjTnbdi7o17LUH+5627AHCrbNFop1KpUntWzN9Jl/Ow8wbsXZlwSbRLHUyu3zf533OUbY8uSVeXthwNK3tz3QTcEZuiorUzIyxKssKlVFqcEcgGH3NKy4Z+053aTcg2uzpSpgkTMM26BUqmpSTxX8dubGM7qJrx5+IXX5iPm7XeIZXvoMwzYolSrSp3afuXXfw5lC2ymU6n0np//KA9D/4f1bZpuDnWXgczfZZxi2QalUTvX7Ye7opbuqQSI3KpRavGr8rJ1XLFNePpY3vqmsDUTtZPFNqFTZSI1TrdkF/14xXri/Uyb1wqZZ2fnlzSMl3zvzs/I+hK7z/v7eooGi20QplY+yWTAlxFsqD911M/IKV05o3RmlPYuGP/l+bchiU889cMGut2Z1C3eGVGrE8KWmWTv2yLzzoZyZ47pKFKWKpJaunjh3y3nGlG9wElh3+3NfrhJNwDT9ykZhkymSemHLzAl/LYOe8/61Y35f5pLrZ+14d2GmyFZUqmzUlCoQl01ThsOX799wr8i/66VSZaOi1NDDGT5Sj2uoVNmoJ5XxcIaPxH7oNKFs1JMqkml3LR7w2CcAQx8/XCD8PKj5DNkbBmem6IS+IKpJPb1+xu8Kz/JroqblpuyCAyuEE/CeJTfl7qsF09TnP86f0BSa4R8OUqmxl+p7e+GQJ4qEJpObIpj9ZI216f6lEx571xV4SL7uxfv6mVwV7z+T89S7PwB9SC5MlFKFENxV8cqJ81/7SehVh+a+VvRxN8ClXY9mr/iomrtYf/OcDdsfzBR+xk5fZ4m51Aubfz9tTalwldvku2lCWIRLR19fX7CTvnhGiTdUKoFQqQRCpRIIlUogVCqBUKkEQqUSCJVKIFQqgVCpBEKlEgiVSiBUKoFQqQRCpRIIlUogVCqBUKkE4pd6+tTxlBSdyWRNMxhNJotWmxzvs6IoweW0u1wOl8te43EnXb161e122u0/u5zVPp83WZtiNFnS0owms//veJ8qRQq/SLcD/0aDOl2q0Whpl9ERI9MvlbkeekW7Tqfd43HV1dUajWaT2ep3TIM4MQiJREGpqWnoxZbeHgVpNC3/OyRXKpPGxkbc0umodrsd3hqPNjkZd6HX0yBWm2BedbscqEOvT8OINFvSDQaTVqsVXF9KKhNcDQMX92t3VHlr3LW1PgzipkRNg7i1aWior6nxBEXiB0ytmC8tlnRsbTGRTCKVyjtqQ02N2+mowmzu89ZotFoaxFGCIjEi3f6gdNTWejG1mlGktZ1eb2Cm1khQKJVDXa3P4ah2+ksvV319HWYGGsSRwBbpw9RqNtvMFpsCkUxaRyqT+npMHa5gSYZBjD0xJmoaxCEEI9Jqy2jFxml9qRzwvLGWxmoLS626+trAYOkXV05zRGIgYkTabBmp+rRYHC7mUtnXhj0x1lrYEzt8Xm8giIkdE2M69VetTrvX6/H5avACMRyt1nZY9cT60KpK5YA9sd1e5XRWY83VNCZu4xNbIZF41+KAEGsLTK0msy01VZ+UlKTaacRTKgfM0nZ7pdfjrvF62tDEVlCk2+WfpUORaWkGHHtg1ZqSolNTJJMEkhoCTwlLaOyGHY6qYBDjLZ9QPTFTJJ6tP7Va21ms6cnJKfE+NT+JKJUD9sQeHIg7q/1Vhs8brzEx3l41NR4UiVU9XAV9mr/YsVhsKbHvI+XSBqQywfyGgRucnQ6W0zEdE6NIl9OBHaTH7cSbCatWTK04jsTUGu+WkKKNSeUQDGKH/WdsfQziJI0m+iAOicQ/Go0WbxpMrVjBJackRGqNhLYtlQMGcbAnxlFEsCeOMIg5IrEOt6W3x+FWJBOtCQhRUpk0NjZgB+hwBMbEPi+WMAaDmVlOt4h0oUiNwWjGcSSOQBKhEIsSYqVyQK9OR5UzMBWAQYxXHXiqbE5v1wFTa7zGHjHi/wcM0N0SWNCKAAAAAElFTkSuQmCC">
            </a>
            <div style="font-size: 17px; font-family:  Arial, sans-serif">
                <?= $content ?>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>