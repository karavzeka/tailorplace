<?php
/**
 * Виджет для отображения комментариев
 */

namespace app\widgets;

use app\models\Comment;
use app\modules\users\models\Photo as UserPhoto;
use yii\base\ErrorException;
use yii\base\Widget;
use yii\helpers\Html;

class CommentsWidget extends Widget
{
    /**
     * @var int Id комментируемого объекта
     */
    public $objectId;

    /**
     * @var string Тип комментируемого объекта (товар или студия)
     */
    public $objectType;

    /**
     * @var int Id пользователя - владельца комментируемого объекта
     */
    public $objectOwner;

    /**
     * @var array Список возможных типов комментируемых объектов
     */
    private $possibleTypes = [
        Comment::TYPE_GOOD,
        Comment::TYPE_STUDIO
    ];

    /**
     * @var Comment[] Список комментариев
     */
    private $Comments;

    public function init()
    {
        if (empty($this->objectId)) {
            throw new ErrorException('Не указан id комментируемого объекта');
        }
        if (!in_array($this->objectType, $this->possibleTypes)) {
            throw new ErrorException('Неверно указан тип комментируемого объекта');
        }

        // Получение комментариев
        $this->Comments = $Comments = Comment::getCommentsWithData($this->objectId, $this->objectType);
    }

    /**
     * Отображет все комментарии к объекту
     */
    public function makeCommentsBlock()
    {
        echo '<div id="comments">';
        foreach ($this->Comments as $Comment) {
            $this->makeComment($Comment);
        }
        echo '</div>';
    }

    private function makeComment(Comment $Comment)
    {
        /** @var UserPhoto[] $userPhotos */
        $userPhotos = $Comment->user->photo;

        $commentClass = ['comment'];
        if ($Comment->user->id == $this->objectOwner) {
            $commentClass[] = 'objectOwner';
        }
        echo Html::beginTag('div', [
            'class' => $commentClass,
            'data-id' => $Comment->id,
        ]);
            // Отрисовка отступов
            echo '<div class="comment-indent">';
                for ($i = 0; $i < $Comment->getIndent(); $i++) {
                    echo '<div></div>';
                    if ($i >= 1) {
                        echo '<div></div>';
                    }
                }
            echo '</div>';
            // Отрисовка самого комментария
            echo '<div class="comment-img-wrapper">';
                echo Html::img(isset($userPhotos[0]) ? $userPhotos[0]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                    'width' => 48,
                    'height' => 48,
                ]);
                echo '<div class="icon-circle ' . $Comment->user->getUserIconClass() . ' small"><i></i></div>';
            echo '</div>';
            echo '<div class="comment-data">';
                echo '<div class="comment-meta">';
                    echo Html::a($Comment->user->getName(), $Comment->user->getLinkToPage(), [
                        'class' => 'size15 red'
                    ]);
                    echo ' ' . Html::tag('time', mb_strtolower(strftime('%e %B %Y в %H:%M', strtotime($Comment->create_time))), [
                        'datetime' => $Comment->create_time
                    ]);
                    if (!\Yii::$app->user->isGuest) {
                        echo ' ' . Html::tag('span', 'ответить', [
                                'class' => 'dashed-underline comment-answer'
                            ]);
                    }
                echo '</div>';
                echo '<p>' . $Comment->text . '</p>';
            echo '</div>';
        echo Html::endTag('div');

        foreach ($Comment->getChilds() as $ChildComment) {
            $this->makeComment($ChildComment);
        }
    }

    /**
     * Выводит блок с textarea для основного уровня комментария
     */
    public function makeMainCommentInput()
    {
        if (!\Yii::$app->user->isGuest) {
            echo <<<LABEL
<div class="comment-form">
    <textarea class="comment-input" placeholder="Ваш комментарий" data-parent="0" data-object-id="{$this->objectId}" data-object-type="{$this->objectType}"></textarea>
    <div class="comment-status"></div>
    <div class="button yellow send-comment-button">
        <div class="low-layer"></div>
        <button type="button">Отправить комментарий</button>
    </div>
</div>
LABEL;
        }
    }
}