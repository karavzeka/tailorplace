<?php
/**
 * Created by PhpStorm.
 * User: computer
 * Date: 08.11.2014
 * Time: 14:03
 */

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class RatingBlock extends Widget
{
    private $color = '#BBDC78';
    private $lineColor = '#9FD245';
    private $width = '0%';
    public $reputation = 0;
    public $countOfDeals = 0;

    public function init()
    {
        $this->reputation = (int)$this->reputation;
        $this->width = $this->reputation . '%';
        if ($this->countOfDeals == 0) {
            $this->color = '#BBBBBB';
            $this->lineColor = '#AAAAAA';
        } else {
            if ($this->reputation < 30) {
                $this->color = '#D95E5E';
                $this->lineColor = '#C83737';
            } elseif ($this->reputation < 70) {
                $this->color = '#E5E05E';
                $this->lineColor = '#D1C617';
            }
        }

        parent::init();
    }

    public function run()
    {
        echo Html::beginTag('div', ['class' => 'rating-block']);
            echo Html::beginTag('div');
                echo Html::tag('span', $this->reputation, [
                    'class' => 'big-digit',
                    'style' => 'color:' . $this->color . '; display: inline-block; margin-left: 8px'
                ]);
                echo Html::tag('span', '%');
            echo Html::endTag('div');
            echo Html::beginTag('div', [
                'class' => 'line',
                'style' => 'border-color:' . $this->color
            ]);
                echo Html::tag('div', '', [
                    'style' => 'background-color:' . $this->lineColor . '; width:' . $this->width
                ]);
            echo Html::endTag('div');
        echo Html::endTag('div');
    }
} 