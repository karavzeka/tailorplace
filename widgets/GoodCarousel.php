<?php
/**
 * Виджет карусели товаров
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\good\models\Good;
use app\modules\good\models\Photo;
use app\assets\AppAsset;

class GoodCarousel extends Widget
{
    /**
     * @var array Роутинговые пути для которых разрешено отображение карусели товаров
     */
    private $permittedRoutes = [
        'site/index',
        'good/default/index',
        'good/default/good',
        'studio/default/index',
        'studio/default/studio',
        'search/search',
        'site/news-all',
        'site/news-item',
        'site/help',
    ];
    
    public function run()
    {
        if (!in_array(Yii::$app->controller->route, $this->permittedRoutes)) {
            return '';
        }
        Yii::$app->getView()->registerCssFile('@web/css/goodsCarouselWidget.css', [
            'depends' => [AppAsset::className()]
        ]);
        Yii::$app->getView()->registerJsFile('@web/js/goodsCarouselWidget.js', [
            'depends' => [AppAsset::className()]
        ]);

        $PromotedGoods = Good::find()
            ->with('mainPhoto')
            ->select('
                `goods`.`id`,
                `goods`.`name`')
            ->innerJoin('goods_promotion', '`goods_promotion`.`good_id` = `goods`.`id`')
            ->orderBy([
                '`goods_promotion`.`last_promotion`' => SORT_DESC,
            ])
            ->limit(15)
            ->all();
        if (count($PromotedGoods) == 0) {
            return '';
        }

        $content = '<div id="good-carousel">';
        $content .= '<div class="goods-wrapper">';
        foreach ($PromotedGoods as $Good) {
            $imgHtml = Html::img($Good->mainPhoto ? $Good->mainPhoto->getSrc('_middle') : Photo::getNoPhotoSrc('_middle'), [
                'width' => 148,
                'height' => 148,
                'alt' => Html::encode($Good->name)
            ]);
            $content .= Html::a($imgHtml, Url::toRoute('/goods/' . $Good->id));
        }
        $content .= '</div>'; // <div class="goods-wrapper">
        if (!Yii::$app->user->isGuest && Yii::$app->user->identity->hasStudio()) {
            $content .= '
                <div class="how-be-this">
                    <span class="darker"><span>↑</span> <a href="/cabinet/services/goods-promotion/">Как сюда попасть?</a></span>
                </div>';
        }
        $content .= '</div>'; // <div id="good-carousel">

        return $content;
    }
}