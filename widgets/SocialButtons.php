<?php
/**
 * Created by PhpStorm.
 * User: karavzeka
 * Date: 25.02.2016
 * Time: 21:42
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class SocialButtons extends Widget
{
    /**
     * @var string Ссылка на изображение
     */
    public $imageUrl;

    /**
     * @var string Заголовок сообщения
     */
    public $title;

    /**
     * @var string Описание сообщения
     */
    public $description;

    /**
     * @var bool Зарегистрирован ли уже js
     */
    private static $jsAlreadyRegistered = false;

    public function run()
    {
        $this->registerJs();

        echo '<div class="social-buttons-block"></div>';
    }

    private function registerJs()
    {
        if (self::$jsAlreadyRegistered === false) {
            $this->view->registerJsFile('https://yastatic.net/share2/share.js');

            $js = "
                var socialBlock = $('.social-buttons-block');
                for (var i = 0, len = socialBlock.length; i < len; i++) {
                    Ya.share2(socialBlock[i], {
                        content: {
                            url: '" . Yii::$app->request->getAbsoluteUrl() . "',
                            title: '" . ($this->title ? $this->title : 'Tailor place - торговая площадка портных') . "',
                            description: '" . ($this->description ? str_replace(["\n", "\r"], ' ', $this->description) : 'Портные создают виртуальные ателье и предлагают услуги по пошиву одежды. Продавцы сбывают материалы и фурнитуру для шитья портным. Пользователи имеют возможность заказать пошив одежды по индивидуальным меркам.') . "',
                            image: '" . ($this->imageUrl ? $this->imageUrl : Url::toRoute('/img/site-logo.png', true)) . "'
                        },
                        theme: {
                            services: 'vkontakte,facebook,twitter,gplus,lj,odnoklassniki,tumblr',
                            bare: false
                        }
                    });
                }
            ";

            $this->view->registerJs($js);
            self::$jsAlreadyRegistered = true;
        }
    }
}