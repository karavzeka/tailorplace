<?php
/**
 * Виджет рейтинга пользователя
 */

namespace app\widgets\panelWidgets;

use yii\base\Exception;
use yii\helpers\Html;
use app\widgets\RatingBlock;

class UserRating extends PanelWidget
{
    protected $size = 'small';
    /**
     * @var \app\modules\users\models\User;
     */
    public $User;
    /**
     * @var bool В футере виджета показать кол-во покупок, иначе будет ссылка на историю заказов
     */
    public $showCountOfPurchases = false;

    public function run()
    {
        if (!($this->User instanceof \app\modules\users\models\User)) {
            throw new Exception('User model not passed');
        }

        $this->startPanel();
        $this->startBody();

        echo Html::beginTag('div', ['class' => 'header icon-line']);
            echo Html::tag('i', '', ['class' => 'icon little-star-light']);
            echo Html::tag('span', 'Заказчик');
            echo Html::tag('i', '', ['class' => 'icon little-star-light']);
        echo Html::endTag('div');

        $countOfPurchases = $this->User->countOfPurchases();
        echo RatingBlock::widget(['reputation' => $this->User->buyerReputation(), 'countOfDeals' => $countOfPurchases]);

        $this->endBody();

        $this->startFooter();

        echo Html::beginTag('div', ['class' => 'icon-line']);
            if ($this->showCountOfPurchases) {
                echo '<span>Совершено заказов: ' . $countOfPurchases . '</span>';
            } else {
                echo Html::a('История заказов', '/cabinet/orders/archive/');
            }
        echo Html::endTag('div');

        $this->endFooter();
        $this->endPanel();
    }
} 