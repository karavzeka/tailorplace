<?php
/**
 * Виджет рейтинга студии
 */

namespace app\widgets\panelWidgets;

use Yii;
use yii\helpers\Html;
use app\widgets\RatingBlock;

class PersonalIndexStudioRating extends PanelWidget
{
    protected $size = 'small';

    public function run()
    {
        $this->startPanel();
        $this->startBody();

        echo Html::beginTag('div', ['class' => 'header icon-line']);
            echo Html::tag('i', '', ['class' => 'icon little-star-light']);
            echo Html::tag('span', 'Исполнитель');
            echo Html::tag('i', '', ['class' => 'icon little-star-light']);
        echo Html::endTag('div');

        $reputationParams = $this->getReputationParams();
        echo RatingBlock::widget([
            'reputation' => $reputationParams['sellerReputation'],
            'countOfDeals' => $reputationParams['countOfSells'],
        ]);

        $this->endBody();

        $this->startFooter();

        echo Html::beginTag('div', ['class' => 'icon-line']);
            echo Html::tag('i', '', ['class' => 'icon star']);
            echo Html::tag('span', 'Заказчик', ['class' => 'size15']);
            echo Html::tag('span', ' ' . $reputationParams['buyerReputation'] . '%', ['class' => 'digit']);
        echo Html::endTag('div');

        $this->endFooter();
        $this->endPanel();
    }

    /**
     * Возвращает данные о репутациях
     *
     * @return array
     */
    private function getReputationParams()
    {
        $User = Yii::$app->user->identity;
        $sellerReputation = $User->sellerReputation();
        $buyerReputation = $User->buyerReputation();
        $countOfSells = $User->studio->finishedOrders();

        return [
            'sellerReputation' => $sellerReputation,
            'buyerReputation' => $buyerReputation,
            'countOfSells' => $countOfSells,
        ];
    }
} 