<?php
/**
 * Created by PhpStorm.
 * User: computer
 * Date: 09.11.2014
 * Time: 13:55
 */

namespace app\widgets\panelWidgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\users\models\User;
use app\models\Order;
use yii\helpers\Url;

class PersonalIndexUserOrders extends PanelWidget
{
    /**
     * @var int Общая сумма активных заказов
     */
    protected $total = 0;

    public function run()
    {
        $this->startPanel();
        $this->startBody();

        $orders =$this->getOrders();
        if (count($orders) > 0) {
            echo Html::beginTag('ul');
            foreach ($orders as $order) {
                $this->createLine($order);
                $this->total += $order['order_price'];
            }
            echo Html::endTag('ul');
        } else {
            echo Html::tag('p', 'У Вас нет активных заказов');
        }

        $this->endBody();

        $this->startFooter();

        if ($this->total > 0) {
            $coinClass = 'icon rouble-gold';
        } else {
            $coinClass = 'icon rouble-light';
        }
        echo Html::beginTag('div', ['class' => 'icon-line']);
            echo Html::tag('span', 'Общая сумма заказов: ');
            echo Html::tag('i', '', ['class' => $coinClass]);
            echo Html::tag('span', Yii::$app->numberHelper->numberToPrice($this->total), ['class' => 'digit']);
            echo Html::tag('span', ' руб.');
        echo Html::endTag('div');

        $this->endFooter();
        $this->endPanel();
    }

    /**
     * Получение активных заказов (покупок) пользователя
     *
     * @return array
     */
    private function getOrders()
    {
        $User = Yii::$app->user->identity;
        $orders = $User->getOrder('buy')
            ->select('
                `orders`.`id` AS `order_id`,
                `orders`.`seller_id` AS `seller_id`,
                `order_status_list`.`name_ru` AS `status`,
                `order_status_list`.`css_class` AS `status_class`,
                SUM(`order_content`.`price` * `order_content`.`quantity`) AS `order_price`')
            ->innerJoin('order_status_list', '`orders`.`status_id` = `order_status_list`.`id`')
            ->innerJoin('order_content', '`order_content`.`order_id` = `orders`.`id`')
            ->andWhere('`orders`.`status_id` NOT IN (2, 3, 4)')
            ->groupBy('`orders`.`id`')
            ->asArray()
            ->all();
        if (count($orders) == 0) {
            return [];
        }

        // нужно получить типы студий у заказов
        $sellerIds = ArrayHelper::getColumn($orders, 'seller_id');
        $studioTypes = User::find()
            ->select('
                `users`.`id` AS `user_id`,
                `studio`.`type` AS `studio_type`')
            ->innerJoin('studio', '`studio`.`user_id` = `users`.`id`')
            ->where(['`users`.`id`' => $sellerIds])
            ->asArray()
            ->all();
        $studioTypes = ArrayHelper::index($studioTypes, 'user_id');
        foreach ($orders as $key => $order) {
            if (isset($studioTypes[$order['seller_id']])) {
                $orders[$key]['studio_type'] = $studioTypes[$order['seller_id']]['studio_type'];
            }
        }

        return $orders;
    }

    /**
     * Выводит строку списка
     *
     * @param array $order
     */
    protected function createLine(array $order)
    {
        $typeClass = 'icon-circle small';

        if ($order['studio_type'] == 'atelier') {
            $typeClass .= ' atelier';
        } else {
            $typeClass .= ' store';
        }
        echo Html::beginTag('li');
            echo Html::beginTag('div', ['class' => $typeClass]);
                 echo Html::tag('i', '');
            echo Html::endTag('div');
            echo Html::beginTag('div');
                echo Html::a('Заказ №' . $order['order_id'], Url::toRoute('/cabinet/order/' . $order['order_id']));
            echo Html::endTag('div');
            echo Html::beginTag('div', ['class' => 'order-status ' . $order['status_class']]);
                echo '<span>' . $order['status'] . '</span>';
            echo Html::endTag('div');
        // TODO обязательно в таблицу заказов добавить колонку валюты, т.к. до завершения заказа студия может поменять валюту
        // и у пользователя отобразится заказ в новой валюте
            echo Html::beginTag('div');
                echo Html::tag('span', Yii::$app->numberHelper->numberToPrice($order['order_price']), ['class' => 'digit darker']);
                echo Html::tag('span', ' руб.', ['class' => 'smaller']);
            echo Html::endTag('div');
        echo Html::endTag('li');
    }
} 