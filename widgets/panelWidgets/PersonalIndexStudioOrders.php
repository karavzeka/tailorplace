<?php
/**
 * Виджет активных заказов студии
 */

namespace app\widgets\panelWidgets;

use Yii;
use yii\helpers\Html;

class PersonalIndexStudioOrders extends PanelWidget
{
    protected $size = 'small';

    public function run()
    {
        $this->startPanel();
        $this->startBody();

        $orders = $this->getOrders();
        $orderIconClass = count($orders) > 0 ? 'order' : 'order-light';
        $coinIconClass = count($orders) > 0 ? 'rouble-gold' : 'rouble-light';
        echo Html::beginTag('div', ['class' => 'header icon-line']);
            echo Html::tag('i', '', ['class' => 'icon ' . $orderIconClass]);
            echo Html::tag('span', 'Активные');
        echo Html::endTag('div');
        echo Html::tag('div', count($orders), ['class' => 'big-digit']);

        $this->endBody();

        $this->startFooter();

        $totalPrice = 0;
        foreach ($orders as $order) {
            $totalPrice += $order['order_price'];
        }

        echo Html::tag('div', 'Общая сумма');
        echo Html::beginTag('div', ['class' => 'icon-line']);
            echo Html::tag('i', '', ['class' => 'icon ' . $coinIconClass]);
            echo Html::tag('span', Yii::$app->numberHelper->numberToPrice($totalPrice), ['class' => 'digit']);
            echo Html::tag('span', ' руб.', ['class' => 'smaller']);
        echo Html::endTag('div');

        $this->endFooter();
        $this->endPanel();
    }

    private function getOrders()
    {
        $User = Yii::$app->user->identity;
        $orders = $User->getOrder('seller')
            ->select('
                `orders`.`id`,
                SUM(`order_content`.`price` * `order_content`.`quantity`) AS `order_price`')
            ->innerJoin('order_content', '`order_content`.`order_id` = `orders`.`id`')
            ->andWhere('`status_id` NOT IN (2, 3, 4)')
            ->groupBy('`orders`.`id`')
            ->asArray()
            ->all();
        return $orders;
    }
} 