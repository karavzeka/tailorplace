<?php
/**
 * Виджет для отрисовки краткого содержания корзины в личном кабинете простого пользователя
 */

namespace app\widgets\panelWidgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class PersonalIndexUserBasket extends PanelWidget
{
    /**
     * @var int Общая сумма товаров в корзине
     */
    protected $total = 0;

    public function run()
    {
        $this->startPanel();
        $this->startBody();

        $goods = $this->getBasketGoods();
        if (count($goods)) {
            echo Html::beginTag('ul');
            foreach ($goods as $good) {
                $this->createLine($good);
                $this->total += $good['price'];
            }
            echo Html::endTag('ul');
        } else {
            echo Html::tag('p', 'Нет товаров в корзине');
        }

        $this->endBody();

        $this->startFooter();

        if ($this->total > 0) {
            $coinClass = 'icon rouble-gold';
        } else {
            $coinClass = 'icon rouble-light';
        }
        echo Html::beginTag('div', ['class' => 'icon-line']);
            echo Html::tag('span', 'Общая сумма заказов: ');
            echo Html::tag('i', '', ['class' => $coinClass]);
            echo Html::tag('span', Yii::$app->numberHelper->numberToPrice($this->total), ['class' => 'digit']);
            echo Html::tag('span', ' руб.');
        echo Html::endTag('div');

        $this->endFooter();
        $this->endPanel();
    }

    private function getBasketGoods()
    {
        $goods = Yii::$app->db->createCommand("
            SELECT
                `b`.`quantity` AS `quantity`,
                `g`.`id` AS `good_id`,
                `g`.`name` AS `good_name`,
                `g`.`price` AS `price`
            FROM
                `basket` AS `b`
            INNER JOIN
                `goods` AS `g`
            ON
                `b`.`good_id` = `g`.`id`
            WHERE
                `b`.`user_id` = :userId", [':userId' => Yii::$app->user->identity->id])
            ->queryAll();
        return $goods;
    }

    /**
     * Выводит строку списка
     *
     * @param array $good
     */
    protected function createLine(array $good)
    {

        echo Html::beginTag('li');
            echo Html::beginTag('div');
                echo Html::a(Html::encode($good['good_name']), Url::toRoute('/goods/' . $good['good_id']));
            echo Html::endTag('div');
            echo Html::beginTag('div');
                echo Html::tag('span', Yii::$app->numberHelper->numberToPrice($good['price']), ['class' => 'digit darker']);
                echo Html::tag('span', ' руб.', ['class' => 'smaller']);
            echo Html::endTag('div');
        echo Html::endTag('li');
    }
} 