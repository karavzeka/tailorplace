<?php
/**
 * Виджет для отображения рекламных блоков
 *
 * Пример вставки:
 * AdBlock::widget([
 *    'style' => 'width: 160px; margin: auto',
 *    'adDirName' => 'google',
 *    'adIds' => [1]
 * ]);
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class AdBlock extends Widget {
    /**
     * @var string стили для рекламного блока
     */
    public $style;

    /**
     * @var string Id для <div>
     */
    public $blockId;

    /**
     * @var string|null Директория с файлами рекламных кодов. Указывается относительно папки 'ad'
     */
    public $adDirName;

    /**
     * @var array Массив номеров файлов, которые отображать в данном блоке в ротации
     */
    public $adIds = [];

    /**
     * @var string|null Реальный путь к директории с файлами рекламных кодов
     */
    private $directoryPath;

    public function run()
    {
        if ($this->validateBlockSetting()) {
            $adCodeFiles = [];
            foreach ($this->adIds as $adId) {
                $filePath = $this->directoryPath . '/' . $adId . '.html';
                if (file_exists($filePath)) {
                    $adCodeFiles[] = $filePath;
                }
            }

            if (count($adCodeFiles) > 0) {
                $divParams = [];
                if (!empty($this->blockId)) {
                    $divParams['id'] = $this->blockId;
                }
                if (!empty($this->style)) {
                    $divParams['style'] = $this->style;
                }
                $codeFile = $adCodeFiles[array_rand($adCodeFiles)];
                echo Html::tag('div', file_get_contents($codeFile), $divParams);
            }
        }
    }

    /**
     * Валидирует настройки виджета
     *
     * @return bool
     */
    private function validateBlockSetting()
    {
        $result = true;
        if ($this->adDirName !== null) {
            $this->directoryPath = Yii::getAlias('@app') . '/ad/' . $this->adDirName;
            $result &= file_exists($this->directoryPath);

        } else {
            $result = false;
        }
        $result &= is_array($this->adIds) && count($this->adIds) > 0;

        return $result;
    }
}