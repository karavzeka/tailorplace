<?php
/**
 * Виджет блока новостей для боковой панели сайта
 */

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\News;

class NewsWidget extends Widget
{
    public function run()
    {
        $News = News::find()
            ->where(['show' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(5)
            ->all();

        if (count($News)) {
            echo '<section class="widget news">';
                echo '<h3>Новости</h3>';
                echo '<div class="flag-link">';
                    echo '<a href="/news/" class="white">Все</a>';
                echo '</div>';
                echo '<ul>';
                foreach($News as $NewsItem) {
                    $robotDate = date('Y-m-d', strtotime($NewsItem->create_time));
                    $userDate = date('d.m.Y', strtotime($NewsItem->create_time));
                    echo '<li>';
                        echo '<p class="news-date"><time datetime="' . $robotDate . '">' . $userDate . '</time></p>';
                        echo '<p><a href="' . Url::toRoute('/news/' . $NewsItem->id) . '" class="lighter">' . Html::encode($NewsItem->title) . '</a></p>';
                    echo '</li>';
                }
                echo '</ul>';
            echo '</section>';
        }
    }
}
