<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage' => 'ru-RU',
    'language' => 'ru',
    'charset' => 'utf-8',
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '_vdr3r1gma5jcNqxJmZQUnyY7rtCsBCW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'app\modules\users\models\User',
            'loginUrl' => ['/user/default/login'],
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => 'no-reply@tailor-place.com'
            ],
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    //'cachePath' => '@runtime/Twig/cache',
                    //'options' => [], /*  Array of twig options */
                    'globals' => ['html' => '\yii\helpers\Html'],
                    'uses' => ['yii\bootstrap'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'app\modules\rbac\AuthManager',
            'defaultRoles' => ['guest'],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'suffix' => '/',

            'rules' => [
            // Общее
                'captcha' => 'common/captcha',
                'feedback' => 'site/feedback',
                'news/<newsId:\d+>' => 'site/news-item',
                'news' => 'site/news-all',
                '404' => 'site/404',
                '403' => 'site/403',
                'get-country-list' => 'site/get-country-list',
                'get-city-list' => 'site/get-city-list',
                'get-news-ajax' => 'site/get-news-ajax',
                'help/<helpPage:\w+>' => 'site/help',
                'help' => 'site/help',
                'you-are-banned' => 'site/you-are-banned',
                'comment/add' => 'comment/add',
            // Поиск
                'search/<type:\w*>/' => 'search/search',
                'search' => 'search/search',
            // Модуль [[Users]]
                'pass-recovery/success/' => 'users/default/pass-recovery-success',
                'new-password/<hash:\w+>' => 'users/default/new-password',
                'registration-confirm/<hash:\w+>' => 'users/default/registration-confirm',
                '<_a:(login|logout|signup|activation|recovery|index|pass-recovery|new-password)>' => 'users/default/<_a>',
                'order-goods/<goodId:\d+>' => '/users/order/order-goods',
                'order-goods' => '/users/order/order-goods',
                'simple-user/<userId:\d+>' => '/users/default/simple-user',
            // Модуль [[good]]
                'goods/<goodId:\d+>' => 'good/default/good',
                'goods/order_by/<order:\w+>/<dir:\w*>' => 'good/default/index',
                'goods/order_by/<order:\w+>/' => 'good/default/index',
                'goods' => 'good/default/index',
                'only-goods' => 'good/default/only-goods',
            // Модуль [[studio]]
                'studio/<studioId:\d+>/order_by/<order:\w+>/<dir:\w*>' => 'studio/default/studio',
                'studio/<studioId:\d+>/order_by/<order:\w+>/' => 'studio/default/studio',
                'studio/<studioId:\d+>' => 'studio/default/studio',

                'studio/order_by/<order:\w+>/<dir:\w*>' => 'studio/default/index',
                'studio/order_by/<order:\w+>/' => 'studio/default/index',
                'studio' => 'studio/default/index',
                '/only-studios' => 'studio/default/only-studios',
            // Личный кабинет
                'cabinet' => 'cabinet/default/index/',
                'cabinet/orders/<type:(purchase)>/order_by/<order:\w+>/<dir:\w*>' => 'cabinet/default/orders',
                'cabinet/orders/<type:(purchase)>/order_by/<order:\w+>' => 'cabinet/default/orders',
                'cabinet/orders/<type:(purchase)>' => 'cabinet/default/orders',
                'cabinet/orders/order_by/<order:\w+>/<dir:\w*>' => 'cabinet/default/orders',
                'cabinet/orders/order_by/<order:\w+>' => 'cabinet/default/orders',
                'cabinet/orders' => 'cabinet/default/orders',
                'cabinet/orders/archive/<type:(purchase)>/order_by/<order:\w+>/<dir:\w*>' => 'cabinet/default/orders-archive',
                'cabinet/orders/archive/<type:(purchase)>/order_by/<order:\w+>' => 'cabinet/default/orders-archive',
                'cabinet/orders/archive/<type:(purchase)>' => 'cabinet/default/orders-archive',
                'cabinet/orders/archive/order_by/<order:\w+>/<dir:\w*>' => 'cabinet/default/orders-archive',
                'cabinet/orders/archive/order_by/<order:\w+>' => 'cabinet/default/orders-archive',
                'cabinet/orders/archive' => 'cabinet/default/orders-archive',
                'cabinet/order/<orderId:\d+>' => 'cabinet/default/order',
                'cabinet/<_a:(basket|place-order|end-place-order|paid-services|change-order-status|order-evaluation)>' => 'cabinet/default/<_a>',
                'cabinet/<_a:(messages)>' => 'cabinet/personal/<_a>',
                '/cabinet/get-interlocutors' => 'cabinet/personal/get-more-interlocutors',
                'cabinet/dialog/<user:\d+>' => 'cabinet/personal/dialog/',
                'cabinet/new-messages' => 'cabinet/personal/get-messages-ajax',
                'cabinet/old-messages' => 'cabinet/personal/get-old-messages-ajax',
                'cabinet/studio/<section:(all|vitrine|sale)>/' => 'cabinet/studio/index/',
                'cabinet/studio/order_by/<order:\w+>/<dir:\w*>' => 'cabinet/studio/index/',
                'cabinet/studio/order_by/<order:\w+>' => 'cabinet/studio/index/',
                'cabinet/studio/<section:(all|vitrine|sale)>/<order:\w+>/' => 'cabinet/studio/index/',
                'cabinet/studio/<_a:(edit-good|delete-good)>/<goodId:\d+>' => 'cabinet/studio/<_a>/',
                'cabinet/statistic' => 'cabinet/default/statistic/',
                'cabinet/services' => 'cabinet/default/paid-services/',
                'cabinet/services/studio-promotion' => 'cabinet/service/studio-promotion/',
                'cabinet/services/goods-promotion' => 'cabinet/service/goods-promotion/',
                'cabinet/repeat-activation-key' => 'cabinet/personal/repeat-activation-key/',
            // sitemap.xml
                [
                    'pattern' => 'sitemap.xml',
                    'route' => 'sitemap/index',
                    'suffix' => '',
                ],
                'sitemap/common-sitemap' => 'sitemap/common-sitemap',
                'sitemap/studio-sitemap' => 'sitemap/studio-sitemap',
                'sitemap/good-sitemap' => 'sitemap/good-sitemap',
            // Инициализация ролей
                'roles' => 'rbac/rbac/init/',
            // Админка
                'admin/users/ban/' => 'admin/user/ban',
                'admin/users' => 'admin/user/index',
                'admin/redact-news/<newsId:\d+>' => 'admin/default/redact-news',
                'admin/<_a>' => 'admin/default/<_a>',
            ],
        ],
        'numberHelper' => 'app\components\NumberHelper',
        'fileHelper' => 'app\components\FileHelper',
        'commonTools' => 'app\components\CommonTools',
        'jsGlobalVar' => 'app\components\JsGlobalVar',
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'gii' => 'yii\gii\Module',
        'users' => [
            'class' => 'app\modules\users\Users'
        ],
        'rbac' => [
            'class' => 'app\modules\rbac\Rbac'
        ],
        'cabinet' => [
            'class' => 'app\modules\cabinet\Cabinet'
        ],
        'good' => [
            'class' => 'app\modules\good\Good'
        ],
        'studio' => [
            'class' => 'app\modules\studio\Studio'
        ],
        'admin' => [
            'class' => 'app\modules\admin\Admin'
        ],
    ],
    'params' => $params,
];

Yii::setAlias('@root', realpath(dirname(__FILE__) . '/../'));

if (YII_ENV_DEV && isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] !== 'guest') {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
