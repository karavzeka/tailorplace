<?php

return [
    'tailor_db' => 'tailor',
    'adminEmail' => 'admin@example.com',
    // 'goodThresholds' определяет пороги кол-ва товаров пользователя. 10 => 15, например означает, что пока пользователь не совершил 10 продаж он может размещать не более 15 товаров
    'goodThresholds' => [
        10 => 15,
        30 => 30,
        50 => 45,
        70 => 65,
        100 => 100
    ],
    // правила изменения статусов заказов см. таблицу order_status_list
    'orderStatusRules' => [
        1 => [
            'buyer' => [4], // отозван
            'seller' => [
                'atelier' => [3, 5, 6], // отменен; принят, ожидает оплаты; в работе
                'store' => [3, 5]
            ]
        ],
        2 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [],
                'store' => []
            ],
            'needEvaluation' => true // при переводе в этот статус требуется оценка
        ],
        3 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [],
                'store' => []
            ]
        ],
        4 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [],
                'store' => []
            ]
        ],
        5 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [6], // отменен; принят, ожидает оплаты; в работе
                'store' => [8]
            ]
        ],
        6 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [7, 8], // готов, ожидает оплаты; комплектуется
                'store' => []
            ]
        ],
        7 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [8], // комплектуется
                'store' => []
            ]
        ],
        8 => [
            'buyer' => [],
            'seller' => [
                'atelier' => [9], // отправлен
                'store' => [9] // отправлен
            ]
        ],
        9 => [
            'buyer' => [2], // завершен
            'seller' => [
                'atelier' => [],
                'store' => []
            ]
        ]
    ],
    'evaluationThresholds' => ['0', '0.2', '0.5', '0.8', '1']
];
