<?php
/**
 * Менеджер зависимостей модуля magnific-popup
 * Этот модуль служит для отображения галереи фотографий (например на странице товара)
 */

namespace app\assets;

use yii\web\AssetBundle;

class MagnificPopupAsset extends AssetBundle
{
    public $sourcePath = '@vendor/dimsemenov/magnific-popup/dist';
    public $css = [
        'magnific-popup.css',
    ];
    public $js = [
        'jquery.magnific-popup.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}