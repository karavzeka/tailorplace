<?php
/**
 * Css и js модуля комментариев
 */

namespace app\assets;

use yii\web\AssetBundle;

class CommentAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/CommentAsset';
    public $js = [
        'comments.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}