<?php
/**
 * Плагин, заменяющий системный скролл на кастомный.
 * Использовать там, где внутри страницы имеется блок с прокруткой, например в диалогах.
 */

namespace app\assets;

use yii\web\AssetBundle;

class BaronAsset extends AssetBundle
{
    public $sourcePath = '@app/assets/BaronAsset';
    public $js = [
        'baron.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
