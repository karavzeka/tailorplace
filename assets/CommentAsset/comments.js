/**
 * Управление блоком комментариев
 */

$(function() {
    var commentsArea = $('#comments-area');
    var commentsBlock = $('#comments', commentsArea);

    // Нажатие на кнопку скрытия комментариев
    $(commentsArea).on('click', '.hide-comments .dashed-underline', function() {
        if (commentsBlock.is(':visible')) {
            $('.hide-comments', commentsArea).html('↓ <span class="dashed-underline">Показать</span>');
            commentsBlock.slideUp();
        } else {
            $('.hide-comments', commentsArea).html('↑ <span class="dashed-underline">Свернуть</span>');
            commentsBlock.slideDown();
        }
    });

    // Нажатие на кнопку отправки комментария
    commentsArea.on('click', '.send-comment-button', function(e) {
        var textarea = $(this).parent('.comment-form').children('.comment-input');
        if (textarea.val()) {
            sendComment({
                text: textarea.val(),
                objectId: textarea.data('object-id'),
                objectType: textarea.data('object-type'),
                parentId: textarea.data('parent')
            }, textarea);
        }
    });

    // Нажатие на кнопку "ответить" у комментария
    commentsArea.on('click', '.comment-answer', function(e) {
        var commentWrapper = $(this).closest('.comment');
        var commentId = commentWrapper.data('id');
        if (commentId) {
            // Существует ли textarea для текущего блока комментария (не самом деле следующий сиблинг)
            var existsInCurrent = $('textarea', commentWrapper.next()).length;
            removeCloneForms();

            // Если в текущем блоке еще не был добавлен textarea, то добавляем
            if (!existsInCurrent) {
                var form = $('.comment-form', commentsArea).clone().addClass('clone-form');
                var textarea = $('textarea', form);
                textarea.data('parent', commentId);

                // Рассчет ширины и сдвига формы для ответа
                var indentCoefficient = $('.comment-indent div', commentWrapper).length;
                console.log(indentCoefficient);
                var indent = indentCoefficient * 13;
                form.css('margin-left', indent);
                textarea.width(textarea.width() - indent);

                form.insertAfter(commentWrapper);

                $(this).text('скрыть');
            } else {
                $(this).text('ответить');
            }
        }
    });

    /**
     * Отправка комментария
     *
     * @param data
     * @param textareaElement
     */
    function sendComment(data, textareaElement) {
        $.ajax({
            url: '/comment/add/',
            data: data,
            type: 'post',
            dataType: 'json',
            success: function(data) {
                if (data.status == 'ok') {
                    location.reload();
                } else {
                    textareaElement.siblings('.comment-status').text(data.error);
                }
            },
            error: function(data) {
                if (data.responseJSON.error) {
                    textareaElement.siblings('.comment-status').text(data.responseJSON.error);
                }
            }
        });
    }

    function removeCloneForms()
    {
        $('.clone-form').remove();
    }
});