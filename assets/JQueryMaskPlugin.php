<?php
/**
 * jQuery плагин для отрисовки полей с маской
 */

namespace app\assets;

use yii\web\AssetBundle;

class JQueryMaskPlugin extends AssetBundle
{
    public $sourcePath = '@app/assets/JQueryMaskPlugin';
    public $js = [
        'jquery.mask.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}