<?php
/**
 * css иконок
 */

namespace app\assets;

use yii\web\AssetBundle;

class IconAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/icons.css'
    ];
}