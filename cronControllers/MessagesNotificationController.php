<?php

namespace app\cronControllers;

use Yii;
use yii\console\Controller;
use app\models\Message;

class MessagesNotificationController extends Controller
{
    /**
     * Вызывается каждый час. Рассылает уведомления пользователям о наличии непрочитанных сообщений.
     * Причем проверяет сообщения на час старше. Т.е. в 15:00 скрипт ищет непрочитанные сообщения,
     * отправленные в интервале 13:00 - 13:59
     */
    public function actionNotifyUnreadMessages()
    {
        $currentDate = date('Y-m-d');
        $needHour = date('H', strtotime('-2 hour'));
        $startTime = $currentDate . ' ' . $needHour . ':00:00';
        $endTime = $currentDate . ' ' . $needHour . ':59:59';

        $newMessages = Message::find()
            ->select([
                'to_id',
                'count' => 'COUNT(*)'
            ])
            ->with('to')
            ->where(['between', 'create_time', $startTime, $endTime])
            ->andWhere(['is_read' => 0])
            ->groupBy('to_id')
            ->asArray()
            ->all();

        $this->sendNotifications($newMessages);
    }

    /**
     * Осуществляет рассылку
     *
     * @param array $newMessages
     */
    private function sendNotifications(array $newMessages)
    {
        $Mailer = Yii::$app->mailer;
        $Mailer->htmlLayout = '@app/mail/layouts/htmlScrollStyle';

        foreach ($newMessages as $messageInfo) {
            $params = [
                'userName' => $messageInfo['to']['login'],
                'countOfMessages' => $messageInfo['count']
            ];
            $Mailer->compose('@app/mail/cron/newMessagesNotifier', $params)
                ->setTo($messageInfo['to']['email'])
                ->setSubject('У вас есть новые сообщения')
                ->send();
        }
    }
}