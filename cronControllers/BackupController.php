<?php
/**
 * Контроллер осуществляющий резервное копирование данных приложения.
 * В качестве хранилища резервных копий выбран Яндекс.Диск
 *
 * Для дешифровки зашифрованного файла вызвать:
 * exec('openssl enc -d -aes-256-cbc -k "' . self::CRYPT_PASSWORD . '" -in ' . $encryptFilePath . ' -out ' . $encryptFilePath.'.zip');
 *
 * Про шифрование почитать тут:
 * http://belgorod.lug.ru/wiki/index.php/%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5_%D1%88%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%BD%D0%BE%D0%B3%D0%BE_%D1%84%D0%B0%D0%B9%D0%BB%D0%B0_%D0%B2_Linux
 *
 * Документация к Api Яндекс.Диска:
 * https://github.com/jack-theripper/Mackey
 */

namespace app\cronControllers;

use Yii;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\FileHelper;

class BackupController extends Controller
{
    /**
     * Токен, выданный приложению для получения доступа к Яндекс.Диску пользователя karavzeka
     * https://tech.yandex.ru/oauth/doc/dg/reference/console-client-docpage/ - Здесь описано как
     * получить токен для приложения, он выдается на время и периодически его придется руками обновлять.
     */
    const YA_DISK_TOKEN = '11b47a967e2f4ff296832e12adfe1915';

    /**
     * Папка приложени на ЯД. Приложение имеет доступ только к данной директории.
     */
    const YA_DISK_APP_DIR = '/Приложения/Tailor-place';

    /**
     * Имя папки с бэкапами баз данных
     */
    const YA_DISK_DB_BACKUP_DIR = 'dbBackups';

    /**
     * Имя папки с бэкапами фотографий пользователей
     */
    const YA_DISK_PHOTO_BACKUP_DIR = 'photoBackups';

    /**
     * Пароль для шифрования архива с бэкапом.
     * Учесть, что слеш перед двойной кавычкой - это экранирование при вставке пароля в консоль.
     */
    const CRYPT_PASSWORD = '{oO4\'oXn1\"]nA#_1c~6H';

    /**
     * @var string БД, для которой будет выполнено резервное копирование
     */
    private $dbToBackup = 'tailor';

    /**
     * @var \Mackey\Yandex\Disk Класс взаимодействия с API Яндекс.Диска
     */
    private $YaDisk;

    /**
     * Резервное копирование БД
     */
    public function actionBackupDb()
    {
        $requestParams = Yii::$app->request->getParams();
        $totalStartTs = microtime(true);

        if (isset($requestParams[1]) && !empty($requestParams[1])) {
            $this->dbToBackup = $requestParams[1];
        }

        // Проверка подключения к БД, если попытка будет неудачной, то остановка скрипта
        $dsn = preg_replace('/(dbname=)(\w+)/' , '$1' . $this->dbToBackup, Yii::$app->db->dsn);

        $connection = new \yii\db\Connection([
            'dsn' => $dsn,
            'username' => Yii::$app->db->username,
            'password' => Yii::$app->db->password,
        ]);
        try {
            $connection->open();
        } catch (Exception $e) {
            self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Ошибка подключения к базе ' . $this->dbToBackup . '</p>
<pre>
' . $e->getCode() . '
' . $e->getMessage() . '
' . $e->getTrace() . '
</pre>
');
            exit;
        }
        // Освобождение соединения
        $connection->close();

        $backupDir = Yii::getAlias('@runtime') . '/backup';
        $backupFileName = $this->dbToBackup . '_' . date('Y-m-d') . '.sql';
        $backupFilePath = $backupDir . '/' . $backupFileName;

        if (!file_exists($backupDir)) {
            mkdir($backupDir, 0777, true);
        }

        // Инициализируем ЯД Api, делаем тестовый запрос, чтобы проверить доступность соединения
        $YaDisk = new \Mackey\Yandex\Disk();
        $YaDisk->token(self::YA_DISK_TOKEN);

        try {
            $AppDir = $YaDisk->resource(self::YA_DISK_APP_DIR);
            $AppDir->getContents();
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            switch ($errorCode) {
                case 401:
                case 403:
                    self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Ошибка подключения к Яндекс.Диску. Возможно просрочен токен, и требуется получить новый.</p>
<pre>
' . $e->getCode() . '
' . $e->getMessage() . '
' . $e->getTrace() . '
</pre>
');
                    break;
                default:
                    self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Ошибка подключения к базе ' . $this->dbToBackup . '</p>
<pre>
' . $e->getCode() . '
' . $e->getMessage() . '
' . $e->getTrace() . '
</pre>
');
            }
            exit;
        }

        // Если соединение есть, то продолжаем работу
        // Создание бэкапа БД
        $dumpStartTs = microtime(true);
        exec('mysqldump -u ' . Yii::$app->db->username . ' -p' . Yii::$app->db->password . ' tailor_dev > ' . $backupFilePath);
        if (!file_exists($backupFilePath)) {
            FileHelper::removeDirectory($backupDir);
            self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Не удалось создать бэкап базы в файл ' . $backupFilePath . '</p>
');
            exit;
        }
        $dumpEndTs = microtime(true);

        // Архивация дампа
        $zipStartTs = microtime(true);
        $Zip = new \ZipArchive();
        $zipPath = $backupFilePath . '.zip';
        $zipFlag = file_exists($zipPath) ? \ZipArchive::OVERWRITE : \ZipArchive::CREATE;
        if ($Zip->open($zipPath, $zipFlag) !== true) {
            FileHelper::removeDirectory($backupDir);
            self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Не удалось заархивировать бэкап базы в файл ' . $zipPath . '</p>
');
            exit;
        }
        $Zip->addFile($backupFilePath, $backupFileName);
        $Zip->close();
        $zipEndTs = microtime(true);

        // Шифрование архива
        $cryptStartTs = microtime(true);
        $encryptFilePath = $backupFilePath . '.zip.crypt';
        exec('openssl enc -e -aes-256-cbc -k "' . self::CRYPT_PASSWORD . '" -in ' . $zipPath . ' -out ' . $encryptFilePath);
        if (!file_exists($encryptFilePath)) {
            FileHelper::removeDirectory($backupDir);
            self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Не удалось зашифровать архив с бэкапом базы в файл ' . $encryptFilePath . '</p>
');
            exit;
        }
        $cryptEndTs = microtime(true);

        // Создание папки для бэкапов, если ее нет
        $uploadStartTs = microtime(true);
        $BackupDir = $YaDisk->resource(self::YA_DISK_APP_DIR . '/' . self::YA_DISK_DB_BACKUP_DIR);
        if (!$BackupDir->has()) {
            $BackupDir->create();
        }

        // Заливка файла бэкапа
        $BackupFile = $YaDisk->resource(self::YA_DISK_APP_DIR . '/' . self::YA_DISK_DB_BACKUP_DIR . '/' . $backupFileName . '.zip.crypt');
        $res = $BackupFile->upload($encryptFilePath, true);
        $uploadEndTs = microtime(true);

        // Если загрузка бэкапа прошла успешно, то удаляем ненужные бэкапы
        $cleanStartTs = microtime(true);
        if ($res) {
            // Оставляем бэкапы за последние 7 дней, и за 1-е число каждого месяца
            $firstMonthDay = date('Y-m-01');
            $last7Days = [];
            for ($i = 0; $i < 7; $i++) {
                $last7Days[] = date('Y-m-d', strtotime('-' . $i . ' day'));
            }

            $BackupDirInfo = $BackupDir->getContents();
            foreach ($BackupDirInfo['items'] as $key => $Item) {
                $fileName = $Item->get('name');
                // Если бэкап исследуемой базы
                if (mb_strpos($fileName, $this->dbToBackup) !== false) {
                    $inLast7Days = false;
                    foreach ($last7Days as $day) {
                        if (strpos($fileName, $day) !== false) {
                            $inLast7Days = true;
                            break;
                        }
                    }
                    if (strpos($fileName, $firstMonthDay) !== false || $inLast7Days) {
                        // Если файлу меньше 7-ми дней или он сделан в первый день текущего месяца, то оставляем его
                        continue;
                    }

                    // Удаляем файл
                    $Item->delete(true);
                }
            }
        } else {
            self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Не удалось загрузить файл на Яндекс.Диск</p>
');
        }
        $cleanEndTs = microtime(true);

        // Для очистки промежуточных файлов просто удаляем всю директоию backup
        FileHelper::removeDirectory($backupDir);

        $totalEndTs = microtime(true);
        self::sendReport('Database backup success', '
<h1>Резервное копирование базы завершено успешно</h1>
<p>
База: ' . $this->dbToBackup . '<br>
Загруженный файл: ' . $backupFileName . '.zip.crypt' . '
</p>
<p>
' . ($dumpEndTs - $dumpStartTs) . 'сек. - дамп базы<br>
' . ($zipEndTs - $zipStartTs) . 'сек. - архивирование базы<br>
' . ($cryptEndTs - $cryptStartTs) . 'сек. - шифрование архива<br>
' . ($uploadEndTs - $uploadStartTs) . 'сек. - загрузка файла на Яндекс.Диск<br>
' . ($cleanEndTs - $cleanStartTs) . 'сек. - удаление старых бэкапов на Яндекс.Диске<br>
<b>' . ($totalEndTs - $totalStartTs) . 'сек. - всего затрачено времени</b><br>
</p>');
    }

    /**
     * Резервное копирование фотографий пользователей
     */
    public function actionBackupPhotos()
    {
        // Инициализируем ЯД Api, делаем тестовый запрос, чтобы проверить доступность соединения
        $this->YaDisk = new \Mackey\Yandex\Disk();
        $this->YaDisk->token(self::YA_DISK_TOKEN);

        try {
            $AppDir = $this->YaDisk->resource(self::YA_DISK_APP_DIR);
            $AppDir->getContents();
        } catch (\Exception $e) {
            $errorCode = $e->getCode();
            switch ($errorCode) {
                case 401:
                case 403:
                    self::sendReport('Database backup fail', '
<h1>Ошибка резервного копирования базы</h1>
<p>Ошибка подключения к Яндекс.Диску. Возможно просрочен токен, и требуется получить новый.</p>
<pre>
' . $e->getCode() . '
' . $e->getMessage() . '
' . $e->getTrace() . '
</pre>
');
            }
        }

        // Создание папки для бэкапов, если ее нет
        $BackupDir = $this->YaDisk->resource(self::YA_DISK_APP_DIR . '/' . self::YA_DISK_PHOTO_BACKUP_DIR);
        if (!$BackupDir->has()) {
            $BackupDir->create();
        }

        // Бэкап директорий
        $photoDirs = ['good', 'user'];
        foreach ($photoDirs as $photoDir) {
            $this->backupPhotoDir($photoDir);
        }

    }

    /**
     * Выполняет резервное копирование
     *
     * @param $photoDir
     */
    private function backupPhotoDir($photoDir)
    {
        if (!($this->YaDisk instanceof \Mackey\Yandex\Disk)) {
            return;
        }

        $backupDirPath = Yii::getAlias('@webroot') . '/photos/' . $photoDir;
        if (!is_dir($backupDirPath)) {
            return;
        }

        $stats = [
            'startTs' => microtime(true),
            'uploaded' => 0,
            'deleted' => 0,
            'dirFullUploaded' => 0,
            'dirFullDeleted' => 0
        ];

        // Создание папки для бэкапов, если ее нет
        $remoteBackupDir = self::YA_DISK_APP_DIR . '/' . self::YA_DISK_PHOTO_BACKUP_DIR . '/' . $photoDir;
        $BackupDir = $this->YaDisk->resource($remoteBackupDir);
        if (!$BackupDir->has()) {
            $BackupDir->create();
        }

        // Получение списка папок непосредственно с фотографиями на ЯД. Надо опрашивать в цикле,
        // т.к. библиотека не позволяет получить список всех вложенных ресурсов
        $remoteDirsWithFiles = [];
        $portion = 50;
        $BackupDir->limit($portion);
        $offset = 0;
        while (true) {
            $BackupDir->offset($offset);
            $offset += $portion;
            $DirInfo = $BackupDir->getContents();
            if (count($DirInfo['items']) == 0) {
                break;
            }
            foreach ($DirInfo['items'] as $Item) {
                $itemInfo = $Item->getContents();
                $remoteDirsWithFiles[$itemInfo['name']] = $itemInfo;
            }
        }

        // Получение папок с фотографиями
        $directories = [];
        $objects = scandir($backupDirPath);
        foreach ($objects as $object) {
            if (in_array($object, ['.', '..'])) {
                continue;
            }
            $objectPath = $backupDirPath . '/' . $object;
            if (is_dir($objectPath) && !$this->isEmptyDir($objectPath)) {
                $directories[$object] = $objectPath;
            }
        }

        // Для удаления
        $dirsToDelete = array_diff(array_keys($remoteDirsWithFiles), array_keys($directories));
        // Для загрузки
        $dirsToFullUpload = array_diff(array_keys($directories), array_keys($remoteDirsWithFiles));

        // Удаление директорий
        foreach ($dirsToDelete as $dirName) {
            $dirPath = $remoteDirsWithFiles[$dirName]['path'];
            $this->YaDisk->resource($dirPath)->delete(true);
            $stats['dirFullDeleted']++;
        }

        // Загрузка директорий без проверки содержимого
        foreach ($dirsToFullUpload as $dirName) {
            $fileList = FileHelper::findFiles($directories[$dirName]);
            $RemoteDir = $this->YaDisk->resource($remoteBackupDir . '/' . $dirName);
            if (!$RemoteDir->has()) {
                $RemoteDir->create();
            }

            foreach ($fileList as $filePath) {
                $fileInfo = pathinfo($filePath);
                $FileResource = $this->YaDisk->resource($remoteBackupDir . '/' . $dirName . '/' . $fileInfo['basename']);
                $FileResource->upload($filePath, true);
                // Прикрепление к файлу lastModified оригинального файла
                $FileResource->set('originModified', filemtime($filePath));

                $stats['uploaded']++;
            }
            $stats['dirFullUploaded']++;
        }

        // Обработка остальных файлов
        foreach ($directories as $dirName => $dirPath) {
            // пропускаем уже обработанные директории
            if (in_array($dirName, $dirsToDelete) || in_array($dirName, $dirsToFullUpload)) {
                continue;
            }
            $fileList = FileHelper::findFiles($dirPath);

            $RemoteFileList = $this->YaDisk->resource($remoteBackupDir . '/' . $dirName)->limit(100)->getContents()['items'];
            $localMaxFileModifiedTime = null;
            foreach ($fileList as $filePath) {
                $fileInfo = pathinfo($filePath);
                // Ищем удаленный соответствующий файл
                $upload = true;
                foreach ($RemoteFileList as $RemoteFile) {
                    if (!$RemoteFile->has('custom_properties')) {
                        self::sendReport('Photo backup warning', '
<h1>Ошибка синхронизации файла</h1>
<p>У файла ' . $dirPath . '/' . $RemoteFile->get('name') . ' отсутствуют параметр originModified</p>');
                        break;
                    }
                    $fileProperties = $RemoteFile->get('custom_properties');
                    if (!isset($fileProperties['originModified']) || empty($fileProperties['originModified'])) {
                        self::sendReport('Photo backup warning', '
<h1>Ошибка синхронизации файла</h1>
<p>У файла ' . $dirPath . '/' . $RemoteFile->get('name') . ' отсутствуют параметр originModified</p>');
                        break;
                    }
                    if ($RemoteFile->get('name') == $fileInfo['basename']) {
                        if ($fileProperties['originModified'] == filemtime($filePath)) {
                            // Файлы идентичны, загрузка не требуется
                            $upload = false;
                        }
                        break;
                    }
                }
                if ($upload) {
                    $FileResource = $this->YaDisk->resource($remoteBackupDir . '/' . $dirName . '/' . $fileInfo['basename']);
                    $FileResource->upload($filePath, true);
                    $FileResource->set('originModified', filemtime($filePath));
                    $stats['uploaded']++;
                }
            }

            // Проверка, а был ли удален какой файл на локальном сервере
            foreach ($RemoteFileList as $RemoteFile) {
                $delete = true;
                foreach ($fileList as $filePath) {
                    $fileInfo = pathinfo($filePath);
                    if ($RemoteFile->get('name') == $fileInfo['basename']) {
                        $delete = false;
                        break;
                    }
                }
                if ($delete) {
                    $RemoteFile->delete(true);
                    $stats['deleted']++;
                }
            }
        }

        // Если были хоть какие-то изменения - шлем отчет
        if ($stats['uploaded'] || $stats['deleted'] || $stats['dirFullUploaded'] || $stats['dirFullDeleted']) {
            self::sendReport('Photo backup success', '
<h1>Резервное копирование фотографий из директории ' . $photoDir . '</h1>
<p>
' . $stats['uploaded'] . ' - загружено фотографий<br>
' . $stats['deleted'] . ' - удалено фотографий (при удалении целых директорий кол-во фотографий в них не подсчитывается)<br>
' . $stats['dirFullUploaded'] . ' - загружено новых директорий с фотографиями<br>
' . $stats['dirFullDeleted'] . ' - целиком удаленных директорий<br>
<b>' . (microtime(true) - $stats['startTs']) . 'сек. - всего затрачено времени</b><br>
</p>');
        }
    }

    /**
     * Отправляет сообщение по почте на support@tailor-place.com
     *
     * @param string $subject Тема сообщения
     * @param string $message Тело сообщения
     */
    private static function sendReport($subject, $message)
    {
        $Mailer = Yii::$app->mailer;
        $Mailer->compose()
            ->setTo('support@tailor-place.com')
            ->setSubject($subject)
            ->setHtmlBody($message)
            ->send();
    }

    /**
     * Проверяет, пуста ли директория
     *
     * @param string $dir Путь к директории
     * @return bool
     */
    private function isEmptyDir($dir)
    {
        if (($files = scandir($dir)) && count($files) <= 2) {
            return true;
        }
        return false;
    }
}