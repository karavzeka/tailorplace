<?php
/**
 * Действия, корректирующие ошибки БД
 */

namespace app\cronControllers;

use Yii;
use yii\console\Controller;
use app\modules\good\models\Photo as GoodPhoto;
use yii\helpers\Url;

class CorrectorController extends Controller
{
    /**
     * Поиск товаров у которых не выставилась главная фотография, несмотря на то,
     * что фотографии загрузились, и выставление одной из фотографий флага, что она главная
     */
    public function actionCorrectMainPhoto()
    {
        $goodsWithoutMainPhoto = Yii::$app->db->createCommand("
            SELECT
                `all`.`good_id` AS `good_without_main_photo`,
                `zeros`.`good_id`
            FROM
                (
                    SELECT DISTINCT
                        `good_id`
                    FROM
                        " . GoodPhoto::tableName() . "
                ) AS `all`
            LEFT JOIN
                (
                    SELECT DISTINCT
                        `good_id`
                    FROM
                        " . GoodPhoto::tableName() . "
                    WHERE
                        `main` = 1
                ) AS `zeros`
            ON
                `all`.`good_id` = `zeros`.`good_id`
            WHERE
                `zeros`.`good_id` IS NULL
        ")->queryColumn();

        $reportMessage = 'У следующих товаров отсутствовала главная фотография:<br><br>';

        foreach ($goodsWithoutMainPhoto as $goodId) {
            $Photo = GoodPhoto::find()
                ->where(['good_id' => $goodId])
                ->one();
            $Photo->main = 1;
            $Photo->save();

            $googUrl =  Url::toRoute('/goods/' . $goodId, true);
            $reportMessage .= '<a href="' . $googUrl . '">' . $googUrl . '</a><br>';
        }

        if (count($goodsWithoutMainPhoto) > 0) {
            $Mailer = Yii::$app->mailer;
            $Mailer->compose()
                ->setTo('support@tailor-place.com')
                ->setSubject('Ошибка выставления главной фотографии')
                ->setHtmlBody($reportMessage)
                ->send();
        }
    }
}