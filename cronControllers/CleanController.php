<?php
/**
 * Очищает систему от ненужных данных
 */

namespace app\cronControllers;

use Yii;
use yii\console\Controller;

class CleanController extends Controller
{
    public function actionRemoveOldTmpPhotos()
    {
        $tmpPhotoDir = Yii::getAlias('@webroot') . '/tempPhotos';
        if (!is_dir($tmpPhotoDir)) {
            return;
        }

        $this->removeOldFiles($tmpPhotoDir);
    }

    /**
     * Удаляет в директории устаревшие файлы. Рекурсивно удаляет файлы в поддиректориях.
     * Если нив директории, ни в поддиректориях не осталось файлов, то удаляется и сама директория,
     * если установлен флаг, разрешающий это сделать.
     *
     * @param string $scannedDir Очищаемая директория
     * @param bool $removeDirIfEmpty Удалить ли текущую директорию, если она стала пустой
     * @param int $lifeTime Интервал в секундах, определяющий, считать ли файл устаревшим.
     * @return bool True, если в директории или ее поддиректориях остались файлы
     */
    private function removeOldFiles($scannedDir, $removeDirIfEmpty = false, $lifeTime = 86400)
    {
        $hasFiles = false;
        $directories = [];
        $objects = scandir($scannedDir);
        foreach ($objects as $object) {
            if (in_array($object, ['.', '..'])) {
                continue;
            }
            $objectPath = $scannedDir . '/' . $object;
            if (is_dir($objectPath)) {
                $directories[] = $objectPath;
            } else {
                $lastModified = filemtime($objectPath);
                if (($lastModified + $lifeTime) < time() && is_writable($objectPath)) {
                    unlink($objectPath);
                } else {
                    $hasFiles = true;
                }
            }
        }

        // Рекурсивно очищаются внутренние директории
        foreach ($directories as $directory) {
            $hasFiles |= $this->removeOldFiles($directory, true, $lifeTime);
        }

        // Если разрешено удалять директорию и нив самой директории, ни в дочерних не осталось файлов, то директория удаляется
        if ($removeDirIfEmpty && !$hasFiles && is_writable($scannedDir)) {
            rmdir($scannedDir);
        }

        return $hasFiles;
    }
}