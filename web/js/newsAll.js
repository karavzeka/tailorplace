$(function() {
    // подгрузка новостей по нажатию кнопки "Показать еще"
    $('#show-more').on('click', '#show-more-button', function() {
        var offset = $(this).data('offset');
        var maxNews = $(this).data('max-news');
        getOldNews(offset, maxNews);
    });

    function getOldNews(offset, maxNews) {
        $.ajax({
            url: '/get-news-ajax/',
            method: 'post',
            data: {offset: offset},
            beforeSend: function(jqXHR) {
                var showMoreButton = $('#show-more-button');
                jqXHR.showMoreButton = showMoreButton;
                showMoreButton.hide(0);
                var preloader = getPreloader({size: 40, spaceWidth: 3});
                $('#show-more').append(preloader);
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == 'ok') {
                    var showMoreButton = jqXHR.showMoreButton;
                    var currentOffset = showMoreButton.data('offset');
                    showMoreButton.data('offset', currentOffset + 1);

                    jqXHR.preloader.remove();

                    var newsWrapper = $('#news-wrapper');
                    newsWrapper.append(data.html);

                    if ($('.news-item', newsWrapper).length < maxNews) {
                        showMoreButton.fadeIn();
                    }
                }
            }
        });
    }
});