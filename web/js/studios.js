$(function() {
    // сортировка студий
    $( "#sort-selector" ).selectmenu({
        change: function(event, data) {
            var currentUrl = $('#base-url').val();

            switch (data.item.value) {
                case 'date_asc':
                    document.location.replace(currentUrl + 'order_by/date/');
                    break;
                case 'date_desc':
                    document.location.replace(currentUrl + 'order_by/date/DESC/');
                    break;
                case 'name_asc':
                    document.location.replace(currentUrl + 'order_by/name/');
                    break;
                case 'name_desc':
                    document.location.replace(currentUrl + 'order_by/name/DESC/');
                    break;
            }
        }
    });

    // фильтрация студий
    var filters = {};

    var studioTypeWrapper = $('#studio-type');

    // установим фильтрацию по типу студии
    var activeStudioElement = $('.active', studioTypeWrapper);
    if (activeStudioElement.length) {
        var activeStudio = activeStudioElement.data('filter-studio');
        if (activeStudio == 'atelier') {
            filters.studioType = 'atelier';
        } else if (activeStudio == 'store') {
            filters.studioType = 'store';
        }
    }

    $('span', studioTypeWrapper).click(function() {
        if (!$(this).hasClass('active')) {
            resetFilters(false, true);
            filters.studioType = $(this).data('filter-studio');

            $('.active', studioTypeWrapper).removeClass('active red').addClass('dashed-underline');
            $(this).addClass('active red').removeClass('dashed-underline');

            getStudios(filters);
        }
    });

    // фильтрация по стране
    var countryInput = $('#filter-country');
    countryInput.autocomplete({
        source: '/get-country-list/',
        select: function(event, ui) {
            // если сменилась выбранная страна, сбрасываем город
            if (filters.country && filters.country.id && filters.country.id != ui.item.id) {
                filters.city = undefined;
            }
            filters.country = ui.item;
            getStudios(filters);

            // раздизейбливание инпута города, если у страны есть города
            cityDisableToggle(ui.item.id);
        },
        change: function(event, ui) {
            var inputVal = this.value;
            if (this.value.trim) {
                inputVal = inputVal.trim();
            }
            // если очистили поле страны
            if (!inputVal) {
                filters.country = undefined;
                filters.city = undefined;
                $('#filter-city').val('').attr('disabled', 'disabled');
                getStudios(filters);
            }
        }
    });

    // установка фильтра по стране
    if (countryInput.data('country-id')) {
        filters.country = {
            id: countryInput.data('country-id'),
            value: countryInput.val()
        }
    }

    // фильтрация по городу
    var cityInput = $('#filter-city');
    cityInput.autocomplete({
        source: function(request, response) {
            // запрос шлется только если id страны известен
            if (filters.country && filters.country.id) {
                $.ajax({
                    url: '/get-city-list/',
                    dataType: 'json',
                    data: {
                        countryId: filters.country.id,
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            } else {
                response([]);
            }
        },
        select: function(event, ui) {
            filters.city = ui.item;
            getStudios(filters);
        },
        change: function(event, ui) {
            var inputVal = this.value;
            if (this.value.trim) {
                inputVal = inputVal.trim();
            }
            // если очистили поле города
            if (!inputVal) {
                filters.city = undefined;
                getStudios(filters);
            }
        }
    });

    // установка фильтра по городу
    if (cityInput.data('city-id')) {
        filters.city = {
            id: cityInput.data('city-id'),
            value: cityInput.val()
        }
    } else if (countryInput.data('country-id')) {
        cityDisableToggle(countryInput.data('country-id'));
    }

    // кнопка сброса фильтров
    $('#filters-reset span').click(function() {
        resetFilters(true);
    });

    /**
     * Сброс фильтров кроме типа студии
     *
     * @param doRequest boolean Выполнить ли запрос?
     * @param saveLocation boolean Не сбрасывать фильтры страны и города? При смене типа студии например надо оставить эти фильтры
     */
    function resetFilters(doRequest, saveLocation)
    {
        if (filters.country || filters.city) {
            if (!saveLocation) {
                filters.country = undefined;
                filters.city = undefined;
                countryInput.val('');
                cityInput.val('').attr('disabled', 'disabled');
            }
            if (doRequest) {
                getStudios(filters);
            }
        }
    }

    /**
     * Отправка запроса на получение студий в соответствии с фильтрами
     *
     * @param filters
     * @param actionType
     */
    function getStudios(filters, actionType)
    {
        $.ajax({
            type: 'post',
            url: '/only-studios/',
            dataType: 'json',
            data: {filters: filters},
            beforeSend: function(jqXHR) {
                var preloader;
                if (actionType == 'add') {
                    var showMoreButton = $('#show-more-button');
                    jqXHR.showMoreButton = showMoreButton;
                    showMoreButton.hide(0);
                    preloader = getPreloader({size: 40, spaceWidth: 3});
                    $('#show-more').append(preloader);
                } else {
                    // перед заменой списка студий
                    var studioWrapper = $('#studio-wrapper');
                    studioWrapper.empty();
                    preloader = getPreloader({size: 60, borderWidth: 4, spaceWidth: 5});
                    studioWrapper.append(preloader);
                }
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                var studioWrapper = $('#studio-wrapper');
                var showMoreButton = $('#show-more-button');
                if (actionType == 'add') {
                    // добавление списка студий в конец существующего списка
                    jqXHR.preloader.remove();
                    studioWrapper.append(data.html);
                    jqXHR.showMoreButton.fadeIn();
                    var currentPart = showMoreButton.data('part');
                    showMoreButton.data('part', currentPart + 1);
                    showGoodButtonToggle(studioWrapper.data('count-of-studios'), data.studiosOnPage);
                } else {
                    // замена списка студий
                    $('#count-of-studios').text(data.countText);
                    jqXHR.preloader.remove();
                    studioWrapper.append(data.html);
                    studioWrapper.data('count-of-studios', data.countOfStudios);
                    showGoodButtonToggle(data.countOfStudios, data.studiosOnPage);
                }

                // после очередного успешного запроса сбросить смещение, чтобы рассчиталось общее кол-во студий в следующем запросе
                filters.part = undefined;
            }
        });
    }

    // подгрузка студий
    $('#show-more').on('click', '#show-more-button', function() {
        filters.part = $(this).data('part');
        getStudios(filters, 'add');
    });

    /**
     * Отображает или скрывает кнопку "Показать еще" в зависимости от кол-ва студий, удовлетворяющих
     * фильтрам и кол-ву студий, отображенных на странице
     *
     * @param countOfStudios int
     * @param studiosOnPage int
     */
    function showGoodButtonToggle(countOfStudios, studiosOnPage)
    {
        var showMoreBlock = $('#show-more');
        var showMoreButton = $('#show-more-button');
        if (countOfStudios > studiosOnPage) {
            // если по фильтрам всего больше студий чем отображено и кнопки нет, то добавить ее
            if (showMoreButton.length == 0) {
                var createdButton = $('<div id="show-more-button" data-part="1"><span class="plus-button"></span> <span>Показать еще</span> </div>');
                showMoreBlock.append(createdButton);
            }
        } else {
            // если кнопка есть и на странице отображены все возможные студии, то кнопка удаляется
            if (showMoreButton.length > 0) {
                showMoreButton.remove();
            }
        }
    }

    /**
     * Раздизейбливает инпут города, если у страны есть города, в противном случае задизейбливает
     *
     * @param countryId
     */
    function cityDisableToggle(countryId)
    {
        $.ajax({
            url: '/get-city-list/',
            type: 'post',
            dataType: 'json',
            data: {countryId: countryId},
            success: function(data) {
                if(data[0]) {
                    $('#filter-city').removeAttr('disabled');
                } else {
                    $('#filter-city').val('').attr('disabled', 'disabled');
                }
            }
        });
    }
});