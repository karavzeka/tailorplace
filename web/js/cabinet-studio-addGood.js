$(function(){
    // Добавление/удаление категории
    var categoryCount = $('.multyField').length;
    var addButton = $('#addCategory');
    var form = $('#good-form');

    addButtonState(addButton, categoryCount);

    form.on('click', '.deleteButton', function(){
        var li = $(this).parent('.multyField');
        li.fadeOut('slow', function(){
            categoryCount--;
            $(this).remove();
            addButtonState(addButton, categoryCount);
        });
    });

    addButton.click(function(){
    	categoryCount++;
    	var baseSelect = $('#goodform-categories');
    	var options = baseSelect.children().clone();
    	var newSelect = $('<select name="GoodForm[categories][]">');
    	newSelect.append(options);

    	var li = $('<li class="multyField">');
    	var leftCol = $('<div class="left-col">');
		var centerCol = $('<div class="center-col">');
		var selectWrapper = $('<div class="select category">');
		li.append(leftCol).append(centerCol);
		centerCol.append(selectWrapper);
		var selectButton = $('<div class="select-button"><span class="caret"></span></div>');
		selectWrapper.append(newSelect).append(selectButton);

		li.css('display', 'none');
		$(this).parents('#add-button-line').before(li);
		li.fadeIn('slow');

		newSelect.selectmenu();
		selectButton.click(function(){
			var parent = $(this).parent();
			$('select', parent).selectmenu( "open" );
		});

		var delButton = $('<button type="button" class="deleteButton" data-toggle="tooltip" data-placement="right" title="Убрать категорию">')
        delButton.tooltip();
        li.append(delButton);

		addButtonState(addButton, categoryCount);
    });

	function addButtonState(addButton, count)
	{
		if (count >= 4) {
			addButton.attr('disabled', 'disabled');
			addButton.css('background', '#EAEAEA');
            addButton.tooltip('close');
            addButton.tooltip('disable');
		} else {
			addButton.removeAttr('disabled');
			addButton.css('background', '#FFFFFF');
            addButton.tooltip('enable');
		}
	}

	// Выбор фотографий
	var dropBox = $('#dropbox');
    var ieVersion = IEversion();

    // В IE9 и ниже ошибка при попытке добавить фотографию
    if (ieVersion !== null && ieVersion <= 9) {
        dropBox.hide();
        $('<p>Вы пользуетесь устаревшим браузером, и он не поддерживает загрузку фотографий.</p>').insertAfter(dropBox);
    }

	dropBox.click(function(){
		$('#photo-input').trigger('click');
	});

	dropBox.on('dragenter', function (e)
	{
		e.stopPropagation();
		e.preventDefault();
		$(this).addClass('ondrag');
	});
	dropBox.on('dragover', function (e)
	{
		$(this).removeClass('ondrag');
		e.stopPropagation();
		e.preventDefault();
	});
	dropBox.on('drop', function (e)
	{
		$(this).removeClass('ondrag');
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		processFiles(files);
	});

	$('#photo-input').change(function(){
		processFiles(this.files)
	});

	var FormState = new function()
	{
		this.locked = false;
		this.photosInProcess = 0;
		this.submitButton = $('#add-action');

		this.incrementProcess = function()
		{
			this.photosInProcess++;
			this.locked = true;
			this.disableSubmitButton();
		}

		this.decrementProcess = function()
		{
			this.photosInProcess--;
			if (this.photosInProcess == 0) {
				this.locked = false;
				this.undisableSubmitButton();
			}
		}

		this.disableSubmitButton = function()
		{
			this.submitButton.removeClass('yellow');
			this.submitButton.addClass('disable');
			$('button', this.submitButton).attr('disabled', 'disabled');
		}

		this.undisableSubmitButton = function()
		{
			this.submitButton.removeClass('disable');
			this.submitButton.addClass('yellow');

			$('button', this.submitButton).removeAttr('disabled');
		}
	}

    var mainWrapper = $('#photos-to-upload');
    // навесим событие вызова окна настройки
    mainWrapper.on('click', '.setting', function(){
        $('#photo-settings').modal();
        configurePhotoModal($(this).parent('.photo-wrapper'));
    });

    // выставим data-атрибуты миниатюрам уже загруженных фоток
    var existThumbnails = $('.photo-wrapper', mainWrapper);
    for (var i = 0, len = existThumbnails.length; i < len; i++) {
        var existThumbnail = $(existThumbnails[i]);
        existThumbnail.data('height', existThumbnail.attr('data-height'));
        existThumbnail.data('width', existThumbnail.attr('data-width'));
        existThumbnail.data('top-shift-part', existThumbnail.attr('data-top-shift-part'));
        existThumbnail.data('left-shift-part', existThumbnail.attr('data-left-shift-part'));
        existThumbnail.data('existId', existThumbnail.attr('data-existId'));

        $('.remove', existThumbnail).click(function(){
            deletePhoto($(this).parent('.photo-wrapper'), null);
        });
    }

	var photoCounter = 0;
	var photoQuantity = existThumbnails.length;

	function processFiles(files)
	{
		var findImg = false;

		// Создадим миниатюры для файлов
		for (var i = 0, f; f = files[i]; i++) {

			// Only process image files.
			if (!f.type.match('image.*')) {
				files[i] = undefined;
				continue;
			}

			if (!findImg) {
				$('p', dropBox).text('Добавить еще');
				findImg = true;
			}

			FormState.incrementProcess();
			quantityIncrement();

			var urlReader = new FileReader();

			// Closure to capture the file information.
			urlReader.onload = (function(theFile) {
				return function(e) {
                    var Img = new Image();
                    var TmpImg = new Image();
                    TmpImg.src = e.target.result;
                    TmpImg.onload = function() {
                        EXIF.getData(TmpImg, function () {
                            var orientation = EXIF.getTag(this, 'Orientation');
                            if (orientation > 1) {
                                var rect = 0;
                                var flip = false;
                                switch (orientation) {
                                    case 2:
                                        rect = 0;
                                        flip = true;
                                        break;
                                    case 3:
                                        rect = 180;
                                        break;
                                    case 4:
                                        rect = 180;
                                        flip = true;
                                        break;
                                    case 5:
                                        rect = 90;
                                        flip = true;
                                        break;
                                    case 6:
                                        rect = 90;
                                        break;
                                    case 7:
                                        rect = -90;
                                        flip = true;
                                        break;
                                    case 8:
                                        rect = -90;
                                        break;
                                }
                                rotateBase64Image(e.target.result, rect, flip, function(base64data) {
                                    Img.src = base64data;
                                });

                            } else {
                                Img.src = e.target.result;
                            }
                        });
                    }

                    // Render thumbnail.
					Img.onload = function() {
						var margin = 0;
						var width = this.width;
						var height = this.height;
                        var leftShiftPart = 0;
                        var topShiftPart = 0;
						if (width > height) {
                            leftShiftPart = (width - height) / (height * 2);
							margin = leftShiftPart * 82;
							$(this).attr('height', '82').css('margin-left', -margin + 'px');
						} else {
                            topShiftPart = (height - width) / (width * 2);
							margin = topShiftPart * 82;
							$(this).attr('width', '82').css('margin-top', -margin + 'px');
						}

						var thumbnail = $('<div class="photo-wrapper">');
						var glass = $('<div class="glass">');
						var hiddenWrapper = $('<div class="hidden-wrapper">');

						hiddenWrapper.append(this);
						thumbnail.append(glass);
						thumbnail.append(hiddenWrapper);
						thumbnail.data('isNew', true);
						thumbnail.data('number', photoCounter);
						photoCounter++;

						mainWrapper.append(thumbnail);

						if (width < 378 || height < 378) {
							setError(thumbnail, 'Размер изображения должен быть не меньше 378px как в врсоту, так и в ширину');
							quantityDecrement();
							FormState.decrementProcess();
							return false;
						}

                        thumbnail.data('height', height);
                        thumbnail.data('width', width);
                        thumbnail.data('top-shift-part', topShiftPart);
                        thumbnail.data('left-shift-part', leftShiftPart);

						sendFileToServer(theFile, thumbnail);
					};
				};
			})(f);

			// Read in the image file as a data URL.
			urlReader.readAsDataURL(f);
			if (photoQuantity >= 9) {
				break;
			}
		}
	}

	function sendFileToServer(file, thumbnail)
	{
		var fd = new FormData();
        fd.append('file', file);
        fd.append('fileNumber', thumbnail.data('number'));
        var uploadURL = '/file/upload-temp-good-photo/';
        var deleteButton = $('<div class="icon-circle remove" data-toggle="tooltip" data-placement="right" title="Убрать фотографию"><i></i></div>');
        var ProgressBar = new createProgressBar(thumbnail);
        var jqXHR = $.ajax({
	        xhr: function() {
	            var xhrobj = $.ajaxSettings.xhr();
	            if (xhrobj.upload) {
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total;
                        if (event.lengthComputable) {
                            percent = Math.ceil(position / total * 100);
                        }
                        //Set progress
                        ProgressBar.setProgress(percent);
                    }, false);
                }
	            return xhrobj;
	        },
	        url: uploadURL,
	        type: 'POST',
	        contentType: false,
	        processData: false,
	        cache: false,
	        data: fd,
	        dataType: 'json',
	        beforeSend: function() {
	        	thumbnail.data('status', 'uploading');
				thumbnail.append(deleteButton);
				
				ProgressBar.startProgress();
	        },
	        success: function(data) {
	            ProgressBar.finishProgress();

	            if (!data.error) {
	            	if (data.success) {
	            		thumbnail.find('.glass').fadeOut();
	            		thumbnail.data('status', 'uploadedSuccess');

	            		var settingButton = $('<div class="icon-circle setting" data-toggle="tooltip" data-placement="right" title="Настроить фотографию"><i></i></div>');
						thumbnail.append(settingButton);
						settingButton.tooltip();

                        var radioMain = $('<div class="radio-input"><input type="radio" name="mainPhoto" id="new-' + thumbnail.data('number') + '"><label for="new-' + thumbnail.data('number') + '">Главная</label></div>');
                        thumbnail.append(radioMain);
	            	} else {
	            		thumbnail.data('status', 'uploadedError');
	            		// TODO создать интернациализованный вывод сообщений
	            		setError(thumbnail, 'Произошла неизвестная ошибка');
	            	}
	            } else {
	            	thumbnail.data('status', 'uploadedError');
	            	setError(thumbnail, data.error);
	            }
	        },
	        error: function(data) {
	        	ProgressBar.finishProgress();
	        	thumbnail.data('status', 'uploadedError');
            	setError(thumbnail, data.error);
	        },
	        complete: function() {
	        	FormState.decrementProcess();
	        }
    	});

		deleteButton.tooltip();
		deleteButton.click(function(){
			deletePhoto(thumbnail, jqXHR);
		});
	}

	function quantityIncrement() {
		photoQuantity++;
		if (photoQuantity >= 9) {
			dropBox.fadeOut();
		}
	}

	function quantityDecrement() {
		photoQuantity--;
		if (photoQuantity < 9) {
			dropBox.fadeIn();
		}
	}

	function createProgressBar(thumbnail)
	{
		this.thumbnail = thumbnail;

		this.startProgress = function()
		{
			this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.thumbnail);
		}

		this.setProgress = function(progress)
		{
			var progressBarWidth = progress + '%';
			// TODO анимация на ноуте вообще не видна (и тормозит сильнее), посмотреть на компе
	        this.progressBar.find('div').animate({ width: progressBarWidth }, 10);
	        // this.progressBar.find('div').width(progressBarWidth);
		}

		this.finishProgress = function()
		{
			this.progressBar.fadeOut();
		}
	}

	function deletePhoto(thumbnail, xhr)
	{
        var photoId = parseInt(thumbnail.data('existId'));
        if (photoId) {
            // удаление уже существующей фотографии (если создать не через new, то при вызове нескольких окон, закрыться сможет только одно)
            var confirmObj = new globalConfirm({
                message: 'Данное действие безвозвратно удалит фотографию. Продолжить?',
                ok: function() {
                    $.ajax({
                        url: '/file/delete-good-photo/',
                        type: 'POST',
                        data: {photoId: photoId},
                        dataType: 'json',
                        success: function(data) {
                            if (data.success) {
                                thumbnail.fadeOut('slow', function () {
                                    $(this).remove();
                                    quantityDecrement();
                                })
                            } else if (data.error) {
                                setError(thumbnail, data.error);
                            }
                        },
                        error: function() {
                            setError(thumbnail, 'Произошла ошибка на сервере');
                        }
                    });
                }
            });
        } else {
            if (xhr && xhr.readyState != 4) {
                xhr.abort();
            }

            if (thumbnail.data('status') == 'uploadedSuccess') {
                var fileNumber = thumbnail.data('number');
                $.ajax({
                    url: '/file/delete-temp-good-photo/',
                    type: 'POST',
                    data: {'fileNumber': fileNumber},
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            thumbnail.fadeOut('slow', function () {
                                $(this).remove();
                                quantityDecrement();
                            })
                        } else if (data.error) {
                            setError(thumbnail, data.error);
                        }
                    },
                    error: function() {
                        setError(thumbnail, 'Произошла ошибка на сервере');
                    }
                });
            }
        }
	}

	function setError(thumbnail, message)
	{
        thumbnail.data('error', 'error');
		$('.error', thumbnail).remove();	//удалим предыдущую ошибку
		var glass = $('.glass', thumbnail);
		var errIcon = $('<div class="icon attention-red error">');
		var messageTag = $('<p>').text(message);
		errIcon.append(messageTag);
		thumbnail.append(errIcon);
		glass.fadeIn('fast');
		errIcon.fadeIn('fast');
		messageWidth = messageTag.width();
		messageHeight = messageTag.height();
		if (messageWidth < messageHeight) {
			messageTag.width(messageHeight);
		}
		messageTag.css('left', (messageTag.width()/2 - 9) * (-1) + 'px');
		errIcon.mouseover(function(){
			messageTag.fadeIn('fast');
		});
		errIcon.mouseout(function(){
			messageTag.fadeOut('fast');
		})
	}

    // Манипуляции с фотографией в модальном окне

    var photoWrapper = $('#photo-wrapper');
    var selectPlace = $('#select-place');
    var preview = $('#preview');

    var leftTopMark = $('.left-top', photoWrapper);
    var rightTopMark = $('.right-top', photoWrapper);
    var rightBottomMark = $('.right-bottom', photoWrapper);
    var leftBottomMark = $('.left-bottom', photoWrapper);
    var photoData = {
        marginTopPart: false,
        marginLeftPart: false,
        sizePart: false
    };

    function configurePhotoModal(thumbnail)
    {
        var img = $('img', photoWrapper);
        if (img.length > 0) {
            img.remove();
            $('img', preview).remove(); // удаление миниатюрки
        }

        var imgWidth = thumbnail.data('width');
        var imgHeight = thumbnail.data('height');
        var topShift = 0;
        var leftShift = 0;
        var sizePart = thumbnail.data('size-part') ? parseFloat(thumbnail.data('size-part')) : 1;
        var smallImgleftShift = 0;
        var smallImgtopShift = 0;
        var smallSide = 0;	// если ширина меньше высоты, то здесь будет ширина, иначе - высота
        var minSquareSize = 0;	// размер ребра квадрата в пикслях, соответствующего 378px реального размера фотографии
        var widthHeightRatio = imgWidth / imgHeight;

        // добавим новые изображения
        img = new Image();
        if (widthHeightRatio > 1) {
            $(img).width(400);
            $(img).height(imgHeight/imgWidth * 400);    // IE не читает атрибуты, так что jQuery выставляет через стили
            photoWrapper.width(400);
            photoWrapper.height(imgHeight/imgWidth * 400);
            smallSide = photoWrapper.height();
            leftShift = thumbnail.data('left-shift-part') * smallSide;
            topShift = thumbnail.data('top-shift-part') * smallSide;
            smallImgleftShift = thumbnail.data('left-shift-part') * 82;
            smallImgtopShift = thumbnail.data('top-shift-part') * 82;
            minSquareSize = 378 / imgWidth * 400;
        } else {
            $(img).height(400);
            $(img).width(imgWidth/imgHeight * 400);
            photoWrapper.height(400);
            photoWrapper.width(imgWidth/imgHeight * 400);
            smallSide = photoWrapper.width();
            leftShift = thumbnail.data('left-shift-part') * smallSide;
            topShift = thumbnail.data('top-shift-part') * smallSide;
            smallImgleftShift = thumbnail.data('left-shift-part') * 82;
            smallImgtopShift = thumbnail.data('top-shift-part') * 82;
            minSquareSize = 378 / imgHeight * 400;
        }

        img.src = $('img', thumbnail).attr('src');
        photoWrapper.prepend(img);
        img = $(img).clone();

        selectPlace.css('top', topShift + 'px');
        selectPlace.css('left', leftShift + 'px');
        selectPlace.width(sizePart * smallSide);
        selectPlace.height(sizePart * smallSide);

        img.css('margin-top', -topShift + 'px');
        img.css('margin-left', -leftShift + 'px');

        // настройка маленького изображения
        var smallImg = new Image();
        smallImg.src = $('img', thumbnail).attr('src');
        smallImg = $(smallImg);
        resizePreview(
            smallImg,
            thumbnail.data('top-shift-part'),
            thumbnail.data('left-shift-part'),
            sizePart
        );

        selectPlace.append(img);
        preview.append(smallImg);

        // настроим маркеры
        setAllMarkers(topShift, leftShift, sizePart * smallSide, sizePart * smallSide);

        // обработка перетаскивания всей выделенной области
        selectPlace.draggable({
        	containment: photoWrapper,
        	scroll: false,
        	drag: function() {
        		var position = $(this).position();
        		img.css('margin-top', -position.top + 'px');
        		img.css('margin-left', -position.left + 'px');
        		setAllMarkers(
        			position.top,
        			position.left,
        			$(this).height(),
        			$(this).width()
    			);
                resizePreview(
                    smallImg,
                    position.top / smallSide,
                    position.left / smallSide,
                    $(this).height() / smallSide
                );
        	},
        	stop: function() {
        		var position = $(this).position();
        		img.css('margin-top', -position.top + 'px');
        		img.css('margin-left', -position.left + 'px');
        		setAllMarkers(
        			position.top,
        			position.left,
        			$(this).height(),
        			$(this).width()
    			);
                resizePreview(
                    smallImg,
                    position.top / smallSide,
                    position.left / smallSide,
                    $(this).height() / smallSide
                );
                photoData = {
                    marginTopPart: position.top / smallSide,
                    marginLeftPart: position.left / smallSide,
                    sizePart: $(this).height() / smallSide
                };
        	}
        });

        // обработка перетаскивания маркеров
        leftTopMark.draggable({
        	containment: photoWrapper,
        	scroll: false,
        	drag: function() {
        		var position = $(this).position();
        		var diagonalPosition = rightBottomMark.position();
                this.minSize = Math.min(diagonalPosition.left - position.left, diagonalPosition.top - position.top);
                this.marginTop = diagonalPosition.top - this.minSize;
                this.marginLeft = diagonalPosition.left - this.minSize;
        		if (this.minSize > minSquareSize) {
        			selectPlace.css('top', this.marginTop + 'px');
        			selectPlace.css('left', this.marginLeft + 'px');
        			selectPlace.height(this.minSize);
        			selectPlace.width(this.minSize);
        			rightTopMark.css('top', this.marginTop + 'px');
                    leftBottomMark.css('left', this.marginLeft + 'px');
        			img.css('margin-top', -this.marginTop + 'px');
        			img.css('margin-left', -this.marginLeft + 'px');
        		}
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
        	},
         	stop: function() {
                $(this).css({
                    top: this.marginTop + 'px',
                    left: this.marginLeft + 'px'
                });
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
                photoData = {
                    marginTopPart: this.marginTop / smallSide,
                    marginLeftPart: this.marginLeft / smallSide,
                    sizePart: this.minSize / smallSide
                };
        	}
        });

        rightTopMark.draggable({
        	containment: photoWrapper,
        	scroll: false,
        	drag: function() {
        		var position = $(this).position();
        		var diagonalPosition = leftBottomMark.position();
                this.minSize = Math.min(position.left - diagonalPosition.left, diagonalPosition.top - position.top);
                this.marginTop = diagonalPosition.top - this.minSize;
                this.marginLeft = diagonalPosition.left;
        		if (this.minSize > minSquareSize) {
        			selectPlace.css('top', this.marginTop + 'px');
        			selectPlace.height(this.minSize);
        			selectPlace.width(this.minSize);
        			leftTopMark.css('top', this.marginTop + 'px');
                    rightBottomMark.css('left', (this.marginLeft + this.minSize) + 'px');
        			img.css('margin-top', -this.marginTop + 'px');
        		}
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
        	},
         	stop: function() {
                $(this).css({
                    top: this.marginTop + 'px',
                    left: (this.marginLeft + this.minSize) + 'px'
                });
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
                photoData = {
                    marginTopPart: this.marginTop / smallSide,
                    marginLeftPart: this.marginLeft / smallSide,
                    sizePart: this.minSize / smallSide
                };
        	}
        });

        leftBottomMark.draggable({
        	containment: photoWrapper,
        	scroll: false,
        	drag: function() {
        		var position = $(this).position();
        		var diagonalPosition = rightTopMark.position();
                this.minSize = Math.min(diagonalPosition.left - position.left, position.top - diagonalPosition.top);
                this.marginTop = diagonalPosition.top;
                this.marginLeft = diagonalPosition.left - this.minSize;
        		if (this.minSize > minSquareSize) {
        			selectPlace.css('left', this.marginLeft + 'px');
        			selectPlace.height(this.minSize);
        			selectPlace.width(this.minSize);
        			leftTopMark.css('left', this.marginLeft + 'px');
                    rightBottomMark.css('top', (this.marginTop + this.minSize) + 'px');
        			img.css('margin-left', -this.marginLeft + 'px');
        		}
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
        	},
         	stop: function() {
                $(this).css({
                    top: (this.marginTop + this.minSize) + 'px',
                    left: this.marginLeft + 'px'
                });
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
                photoData = {
                    marginTopPart: this.marginTop / smallSide,
                    marginLeftPart: this.marginLeft / smallSide,
                    sizePart: this.minSize / smallSide
                };
        	}
        });

        rightBottomMark.draggable({
        	containment: photoWrapper,
        	scroll: false,
        	drag: function() {
        		var position = $(this).position();
        		var diagonalPosition = leftTopMark.position();
                this.minSize = Math.min(position.left - diagonalPosition.left, position.top - diagonalPosition.top);
                this.marginTop = diagonalPosition.top;
                this.marginLeft = diagonalPosition.left;
        		if (this.minSize > minSquareSize) {
        			selectPlace.height(this.minSize);
        			selectPlace.width(this.minSize);
        			rightTopMark.css('left', (this.marginLeft + this.minSize) + 'px');
                    leftBottomMark.css('top', (this.marginTop + this.minSize) + 'px');
        		}
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );

        	},
         	stop: function() {
                $(this).css({
                    top: (this.marginTop + this.minSize) + 'px',
                    left: (this.marginLeft + this.minSize) + 'px'
                });
                resizePreview(
                    smallImg,
                    this.marginTop / smallSide,
                    this.marginLeft / smallSide,
                    this.minSize / smallSide
                );
                photoData = {
                    marginTopPart: this.marginTop / smallSide,
                    marginLeftPart: this.marginLeft / smallSide,
                    sizePart: this.minSize / smallSide
                };
        	}
        });

        function resizePreview(img, marginTopPart, marginLeftPart, sizePart)
        {
            var size = 82/sizePart;
            var marginTop = size * marginTopPart;
            var marginLeft = size * marginLeftPart;
            var roundSize = Math.round(size);
            if (roundSize > size) {
                marginTop = Math.ceil(marginTop);
                marginLeft = Math.ceil(marginLeft);
            } else {
                marginTop = Math.floor(marginTop);
                marginLeft = Math.floor(marginLeft);
            }
            if (widthHeightRatio > 1) {
                img.attr('height', roundSize);
            } else {
                img.attr('width', roundSize);
            }

            img.css('margin-top', -marginTop + 'px');
            img.css('margin-left', -marginLeft + 'px');
        }

        // сохранение настроек
        var saveButton = $('#photo-settings-save button');
        saveButton.unbind('click');
        saveButton.click(function(){
            if (photoData.marginTopPart !== false && photoData.marginLeftPart !== false && photoData.sizePart !== false) {
                resizePreview(
                    $('img', thumbnail),
                    photoData.marginTopPart,
                    photoData.marginLeftPart,
                    photoData.sizePart
                );
                thumbnail.data('top-shift-part', photoData.marginTopPart);
                thumbnail.data('left-shift-part', photoData.marginLeftPart);
                thumbnail.data('size-part', photoData.sizePart);
            }
            $('#photo-settings').modal('hide');
        });
    }

    function setAllMarkers(topShift, leftShift, height, width)
    {
        leftTopMark.css('top', topShift + 'px');
        leftTopMark.css('left', leftShift + 'px');

        rightTopMark.css('top', topShift + 'px');
        rightTopMark.css('left', (leftShift + width) + 'px');

        rightBottomMark.css('top', (topShift + height) + 'px');
        rightBottomMark.css('left', (leftShift + width) + 'px');

        leftBottomMark.css('top', (topShift + height) + 'px');
        leftBottomMark.css('left', leftShift + 'px');
    }

    /**
     * Разворот изображения, закодированного в Base64
     *
     * @param base64data
     * @param rect Угол, на который нужно повернуть картинку
     * @param flip Отобразить ли зеркально фотографию после поворота
     * @param callback
     */
    function rotateBase64Image(base64data, rect, flip, callback) {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");

        var image = new Image();
        image.src = base64data;
        image.onload = function() {
            if (Math.abs(rect) == 90) {
                canvas.height = image.width;
                canvas.width = image.height;
            } else {
                canvas.height = image.height;
                canvas.width = image.width;
            }

            ctx.translate(canvas.width/2,canvas.height/2);
            // Зеркальное отображение почему-то надо применить перед поворотом
            if (flip) {
                ctx.scale(-1, 1);
            }
            ctx.rotate(rect * Math.PI / 180);
            ctx.drawImage(image,-image.width/2,-image.height/2);
            callback(canvas.toDataURL());

            // Если не удалить, то будет все время работать с одним и тем же изображением
            ctx = undefined;
            canvas = undefined;
        };
    }

    // Сбор свойств изображений перед сохранением
    form.submit(function(){
        if (FormState.locked) {
            return false;
        } else {
            $('#photo-input').remove(); // файлы уже на сервере, незачем еще раз отправлять

            var thumbnails = $('.photo-wrapper');
            var dataArray = {
            	mainPhoto: {
            		type: '',	//'new' or 'exist'
            		numOrId: 0
            	},
            	upload: [],
            	update: []
            };
            for (var i = 0, len = thumbnails.length; i < len; i++) {
                var thumbnail = $(thumbnails[i]);
                if (!thumbnail.data('error')) {
                    var isNew = thumbnail.data('isNew') ? thumbnail.data('isNew') : false;
                    var marginTopPart = thumbnail.data('top-shift-part') ? parseFloat(thumbnail.data('top-shift-part')) : 0;
                    var marginLeftPart = thumbnail.data('left-shift-part') ? parseFloat(thumbnail.data('left-shift-part')) : 0;
                    var sizePart = thumbnail.data('size-part') ? parseFloat(thumbnail.data('size-part')) : 1;
                    var existId = parseInt(thumbnail.data('existId'));
                    var mainPhoto = $('.radio-input input', thumbnail).prop('checked');
                    if (isNew) {
                        dataArray.upload.push({
                            fileNumber: parseInt(thumbnail.data('number')),
                            marginTopPart: marginTopPart,
                            marginLeftPart: marginLeftPart,
                            sizePart: sizePart,
                            mainPhoto: $('.radio-input input', thumbnail).prop('checked')
                        });
                    } else if (existId) {
                        dataArray.update.push({
                            existId: existId,
                            marginTopPart: marginTopPart,
                            marginLeftPart: marginLeftPart,
                            sizePart: sizePart
                        });
                    }
                    if (mainPhoto) {
                        dataArray.mainPhoto.type = (!isNew && existId) ? 'exist' : 'new';
                        dataArray.mainPhoto.numOrId = (!isNew && existId) ? existId : parseInt(thumbnail.data('number'));
                    }
                }
            }

            $('#photo-properties').val(JSON.stringify(dataArray));

            return true;
        }
    });

    /**
     * Проверка версии IE
     *
     * @return int|null
     * @constructor
     */
    function IEversion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0) {
            // If Internet Explorer, return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
        } else {
            return null;
        }
    }
});