$(function() {
    var promotionForm = $('#promotion-form');
    $('button[type=submit]', promotionForm).click(function() {
        $('button[type=submit]', promotionForm).removeAttr('clicked');
        $(this).attr('clicked', 'true');
    });

    promotionForm.submit(function(eventObject) {
        var clickedBtn = $('button[type=submit][clicked=true]', promotionForm);
        $('#good-id-input').val(clickedBtn.data('good-id'));
    });
});