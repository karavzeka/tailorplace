$(function() {
    // увеличение кол-ва товара
    $('.plusButton').click(function() {
        calculatePriceByButton('plus', this);

        var orderNode = $(this).parents('.order')[0];
        calculateTotalOrderPrice(orderNode);
        calculateFullTotalPrice();
    });

    // уменьшение кол-ва товара
    $('.minusButton').click(function() {
        calculatePriceByButton('minus', this);

        var orderNode = $(this).parents('.order')[0];
        calculateTotalOrderPrice(orderNode);
        calculateFullTotalPrice();
    });

    // ручное выставление кол-ва
    $('.order .counter input').change(function() {
        var newVal = parseInt($(this).val());
        if (isNaN(newVal) || newVal < 1) {
            newVal = 1;
            $(this).val(1)
        }

        var parentLi = $(this).parent().parent('li');

        var priceOfGood = $('.price-box span', parentLi).text();
        priceOfGood = priceOfGood.replace(/\s/g, '').replace(',', '.');
        var multiplePrice = priceOfGood * newVal;

        multiplePrice = formatPrice(multiplePrice);

        $('.end-price .price-bigger', parentLi).text(multiplePrice);

        var orderNode = $(this).parents('.order')[0];
        calculateTotalOrderPrice(orderNode);
        calculateFullTotalPrice();
    });

    // удаление товара
    $('.good-list .delete').click(function() {
        var parentLi = $(this).parents('li');
        $.ajax({
            type: 'post',
            url: '/users/order/delete-good-from-basket/',
            data: {goodId: parentLi.data('good-id')},
            dataType: 'json',
            success: function(answer) {
                if (answer.status == 'ok') {
                    parentLi.fadeOut(300, function () {
                        var orderNode = parentLi.parents('.order')[0];
                        parentLi.remove();
                        calculateTotalOrderPrice(orderNode);
                        calculateFullTotalPrice();
                    });
                }
            }
        });
        console.log(parentLi);
    });

    // удаление заказа целиком
    $('.delete-order span').click(function() {
        var orderNode = $(this).parents('.order');
        $.ajax({
            type: 'post',
            url: '/users/order/delete-order-from-basket/',
            data: {studioId: $(this).data('studio-id')},
            dataType: 'json',
            success: function(answer) {
                if (answer.status == 'ok') {
                    hideOrder(orderNode);
                }
            }
        });
    });

    // оформление одного заказа
    $('#orders-form').submit(function(event) {
        var button = $('button[type=submit][clicked=true]');
        if (button.length) {
            var studioId = button.data('studio-id');
            $('#order-with-studio').val(studioId);
        }
    });
    $('#orders-form .post-one-order').click(function() {
        $('post-one-order', $(this).parents('form')).removeAttr('clicked');
        $(this).attr('clicked', 'true');
    });

    // Функции

    /**
     * В зависимости от нажатой кнопки увеличивает или уменьшает счетчик кол-ва товара и подсчитывает сумму
     *
     * @param operation Операция (сложение или вычитаение)
     * @param buttonNode
     */
    function calculatePriceByButton(operation, buttonNode)
    {
        var counterInput = $(buttonNode).parent('.counter').children('input');
        var newVal = 1;
        if (operation == 'minus') {
            newVal = parseInt(counterInput.val()) - 1;
        } else {
            newVal = parseInt(counterInput.val()) + 1;
        }
        if (isNaN(newVal) || newVal < 1) {
            newVal = 1;
        }
        counterInput.val(newVal);

        // находит родительский элемент списка
        var parentLi = $(buttonNode).parent().parent('li');

        var priceOfGood = $('.price-box span', parentLi).text();
        priceOfGood = priceOfGood.replace(/\s/g, '').replace(',', '.');
        var multiplePrice = priceOfGood * newVal;

        multiplePrice = formatPrice(multiplePrice);

        $('.end-price .price-bigger', parentLi).text(multiplePrice);
    }

    /**
     * Форматирует число в цену
     *
     * @param price Цена товара
     * @returns {string} Цена за все товары
     */
    function formatPrice(price)
    {
        price = price.toFixed(2);
        var priceParts = price.split('.');

        var numArray = [];
        for (var i = 1, pos = priceParts[0].length - 1; pos >= 0; pos--, i++) {
            numArray.push(priceParts[0][pos]);
            if (i%3 == 0 && pos != 0) {
                numArray.push(' ');
            }
        }
        numArray.reverse();

        var newPrice = price;
        if (priceParts[1] == '00') {
            newPrice = numArray.join(''); // без десятичной части
        } else {
            newPrice = numArray.join('') + ',' + priceParts[1];
        }

        return newPrice;
    }

    /**
     * Подсчитывает цену внутри заказа
     *
     * @param orderNode
     */
    function calculateTotalOrderPrice(orderNode)
    {
        var orderNode = $(orderNode);
        var goodPrices = $('.end-price .price-bigger', orderNode);
        var totalPrice = 0;

        for (var i = 0, len = goodPrices.length; i < len; i++) {
            var price = $(goodPrices[i]).text().replace(/\s/g, '').replace(',', '.') * 1;
            totalPrice = totalPrice + price;
        }

        var formattedPrice = formatPrice(totalPrice);
        $('.order-summary .price-normal', orderNode).text(formattedPrice);
    }

    /**
     * Подсчитывает суммарную цену всех заказов
     */
    function calculateFullTotalPrice()
    {
        var goodPrices = $('.end-price .price-bigger');
        var totalPrice = 0;

        for (var i = 0, len = goodPrices.length; i < len; i++) {
            var price = $(goodPrices[i]).text().replace(/\s/g, '').replace(',', '.') * 1;
            totalPrice = totalPrice + price;
        }

        var formattedPrice = formatPrice(totalPrice);
        $('.sum-info .sum-price .price-bigger').text(formattedPrice);
    }

    function hideOrder(orderNode)
    {
        orderNode.fadeOut(300, function() {
            orderNode.remove();

            var orders = $('main .order');
            if (orders.length <= 0) {
                var mainContent = $('main');
                // если заказов не осталось
                // удаление нижнего блока суммарной информации
                var sumInfo = $('.sum-info', mainContent);
                $(sumInfo[1]).remove();

                // удаление нижней полосы с ножницами
                var scissorsLine = $('.scissor-left', mainContent);
                $(scissorsLine[1]).remove();

                // удаление кнопки "Оформить все заказы"
                $('.place-all-order', mainContent).remove();

                // изменение текста о кол-ве заказов
                $(sumInfo[0]).children('p.italic').text('У вас нет заказов');

                mainContent.append($('<p class="empty-basket">Корзина пуста</p>'));
            }
            calculateFullTotalPrice();
        });
    }
});