$(function() {
    var goodCarousel = $('#good-carousel');
    var goodsWrapper = $('.goods-wrapper', goodCarousel);
    var promotedGoods = $('a', goodsWrapper);
    var countOfpromotedGoods = promotedGoods.length;
    if (countOfpromotedGoods > 6) {
        var movedElementIndex = 0;
        var disableMoving = false;
        shiftCarousel();

        // при наведении на товар тормозим движение карусели
        promotedGoods.mouseover(function(e) {
            disableMoving = true;
            e.stopPropagation();
        });

        promotedGoods.mouseleave(function(e) {
            disableMoving = false;
            e.stopPropagation();
        });
    }

    /**
     * Функция сдвигает товары в карусели на один влево
     */
    function shiftCarousel() {
        setTimeout(function() {
            if (!disableMoving) {
                $(promotedGoods[movedElementIndex]).animate({marginLeft: '-154px'}, 1000, 'linear', function () {
                    var movedElement = $(this).detach();
                    movedElement.css('margin-left', 4);
                    goodsWrapper.append(movedElement);
                    // несмотря на перемещение элемента в DOM'е его индекс в переменной-массиве не меняется
                    movedElementIndex++;
                    if (movedElementIndex >= countOfpromotedGoods) {
                        movedElementIndex = 0;
                    }
                    shiftCarousel();
                });
            } else {
                shiftCarousel();
            }
        }, 7000);
    }
});