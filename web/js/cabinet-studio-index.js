$(function(){
    $('.delete-good').click(function(){
        var href = $(this).attr('href');
        var confirmObj = new globalConfirm({
            message: 'Удалить товар?',
            ok: function() {
                location.pathname = href;
            }
        });
        return false;
    });
});