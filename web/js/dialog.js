$(function() {
    // активация плагина Baron для замены системного скролла на кастомный
    var baronScroller = baron({
        root: '#dialog-wrapper',
        scroller: '.dialog-scroller',
        bar: '.scroller__bar',
        barOnCls: 'baron'
    });

    // прокрутка окна сообщений к последнему сообщению
    var dialogScroller = $('.dialog-scroller');
    console.log(dialogScroller[0].scrollHeight);
    console.log(dialogScroller.height());
    dialogScroller[0].scrollTop = dialogScroller[0].scrollHeight - dialogScroller.height() + 100;

    var dialogContainer = $('.dialog-container');

    // определение Id собеседника по url'у
    var matches = location.href.match(/dialog\/(\d+)\//);
    var interlocutorId = matches[1];

    // первый запрос новых сообщений через 10 сек. после загрузки страницы
    setTimeout(function() {
        getNewMessages();
    }, 10000);

    // подгрузка старых сообщений
    $('#show-more').on('click', '#show-more-button', function() {
        var offset = $(this).data('offset');
        getOldMessages(offset);
    });

    /**
     * Подгружает новые сообщения
     */
    function getNewMessages()
    {
        $.ajax({
            url: '/cabinet/new-messages/',
            method: 'post',
            data: {interlocutorId: interlocutorId},
            success: function(data) {
                if (data.status == 'ok' && data.html) {
                    var oldScrollOffset = dialogScroller[0].scrollTop;
                    var oldScrollHeight = dialogScroller[0].scrollHeight;
                    dialogContainer.children('ul').append(data.html);
                    var newScrollHeight = dialogScroller[0].scrollHeight;

                    // сдвигаем содержимое к той позиции, на которой оно было до подгрузки
                    baronScroller.update();
                    dialogScroller[0].scrollTop = newScrollHeight - oldScrollHeight + oldScrollOffset;
                }
            }
        });

        // запрос новых сообщений каждые 10 сек.
        setTimeout(function() {
            getNewMessages();
        }, 10000);
    }

    /**
     * Подгружает старые сообщения
     *
     * @param offset
     */
    function getOldMessages(offset)
    {
        $.ajax({
            url: '/cabinet/old-messages/',
            method: 'post',
            data: {
                interlocutorId: interlocutorId,
                offset: offset
            },
            beforeSend: function(jqXHR) {
                var showMoreButton = $('#show-more-button');
                jqXHR.showMoreButton = showMoreButton;
                showMoreButton.hide(0);
                var preloader = getPreloader({size: 40, spaceWidth: 3});
                $('#show-more').append(preloader);
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == 'ok') {
                    var showMoreButton = jqXHR.showMoreButton;
                    var currentOffset = showMoreButton.data('offset');
                    showMoreButton.data('offset', currentOffset + 1);

                    var oldScrollOffset = dialogScroller[0].scrollTop;
                    var oldScrollHeight = dialogScroller[0].scrollHeight;
                    jqXHR.preloader.remove();
                    dialogContainer.children('ul').prepend(data.html);
                    var newScrollHeight = dialogScroller[0].scrollHeight;

                    if (data.hideShowMoreButton !== true) {
                        showMoreButton.fadeIn();
                    }

                    // сдвигаем содержимое к той позиции, на которой оно было до подгрузки
                    baronScroller.update();
                    dialogScroller[0].scrollTop = newScrollHeight - oldScrollHeight + oldScrollOffset;
                }
            }
        });
    }
});