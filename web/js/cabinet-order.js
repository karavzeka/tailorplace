$(function() {
    var statusBox = $('.status-box');
    // обработка отмены заказа
    $('span.cancel', statusBox).click(function() {
        var orderId = statusBox.data('order-id');
        var statusId = $(this).data('status-id');
        new globalConfirm({
            message: 'Вы уверены, что хотите ' + $(this).text().toLowerCase() + ' №' + orderId + '?',
            ok: function() {
                $.ajax({
                    url: '/cabinet/change-order-status/',
                    type: 'post',
                    data: {orderId: orderId, statusId: statusId},
                    success: function(data) {
                        if (data.status == 'ok') {
                            $('.icon-line', statusBox).remove();

                            var statusLabel = statusBox.children('.order-status');
                            statusLabel.removeClass();
                            statusLabel.addClass('order-status ' + data.orderStatusClass);
                            $('span', statusLabel).text(data.orderStatusText);

                            new globalAlert({
                                message: 'Заказ №' + orderId + ' ' + data.orderStatusText.toLowerCase() + ' и перенесен в архив заказов.',
                                ignoreOtherModals: true
                            });
                        } else {
                            new globalAlert({
                                message: 'Произошла ошибка',
                                ignoreOtherModals: true
                            });
                        }
                    },
                    error: function() {
                        new globalAlert({
                            message: 'Произошла ошибка',
                            ignoreOtherModals: true
                        });
                    }
                });
            }
        });
    });

    //выпадающий список новых статусов
    $('.edit-status-button .dashed-underline').click(function(e) {
        var statusList = $(this).parent('.edit-status-button').children('.status-list');
        if (statusList.css('display') == 'none') {
            statusList.slideDown('fast');
        } else {
            statusList.slideUp('fast');
        }
        e.stopPropagation(); // если не сделать, то сработает событие клика вне выпадающего списка и оно сразу закроется
    });

    // скрыть список статусов при клике вне его
    $(document).click(function(e){
        var visibleLists = $(e.target).parents().filter('.status-list:visible');
        if (visibleLists.length == 0) {
            $('.status-list:visible').slideUp('fast');
        }
    });

    // изменение статуса заказа
    statusBox.on('click', '.status-list .new-status', function() {
        var orderId = statusBox.data('order-id');
        var statusId = $(this).data('status-id');

        if ($(this).data('need-evaluation')) {
            // это последнее изменение статуса пользователем, завершающее заказ. Его требуется сразу оценить

            var htmlEvaluationBlock = $('<form id="order-evaluation">');
            var htmlEvaluationList = $('<ul>').appendTo(htmlEvaluationBlock);
            $('<li><input type="radio" name="evaluation" value="0" id="evaluation-inp1"><label for="evaluation-inp1" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Обман</span></label></li>').appendTo(htmlEvaluationList);
            $('<li><input type="radio" name="evaluation" value="0.2" id="evaluation-inp2"><label for="evaluation-inp2" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Плохо</span></label></li>').appendTo(htmlEvaluationList);
            $('<li><input type="radio" name="evaluation" value="0.5" id="evaluation-inp3"><label for="evaluation-inp3" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Нормально</span></label></li>').appendTo(htmlEvaluationList);
            $('<li><input type="radio" name="evaluation" value="0.8" id="evaluation-inp4"><label for="evaluation-inp4" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><span>Хорошо</span></label></li>').appendTo(htmlEvaluationList);
            $('<li><input type="radio" name="evaluation" value="1" id="evaluation-inp5"><label for="evaluation-inp5" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><span>Отлично</span></label></li>').appendTo(htmlEvaluationList);

            new globalConfirm({
                message: 'Для завершения заказа пожалуйста оцените партнера сделки.',
                html: htmlEvaluationBlock,
                ok: function() {
                    var evaluation = $('#order-evaluation input[name=evaluation]:checked').val();
                    if (evaluation === undefined) {
                        new globalAlert({
                            message: 'Вы не оценили партнера.',
                            ignoreOtherModals: true
                        });
                        return;
                    }
                    // отправка данных
                    $.ajax({
                        url: '/cabinet/change-order-status/',
                        type: 'post',
                        data: {orderId: orderId, statusId: statusId, evaluation: evaluation},
                        success: function (data) {
                            if (data.status == 'ok') {
                                var statusLabel = statusBox.children('.order-status');
                                statusLabel.removeClass();
                                statusLabel.addClass('order-status ' + data.orderStatusClass);
                                $('span', statusLabel).text(data.orderStatusText);

                                // изменение списка новых статусов
                                var statusList = $('.status-list', statusBox);

                                if (data.possibleStatuses !== undefined && data.possibleStatuses.length > 0) {
                                    $('.new-status', statusList).remove();
                                    for (var i = 0, len = data.possibleStatuses.length; i < len; i++) {
                                        var NewStatus = data.possibleStatuses[i];
                                        var listItem = $('<li>').addClass('new-status').data('status-id', NewStatus.id).appendTo(statusList);
                                        var statusDiv = $('<div>').addClass('order-status ' + NewStatus.css_class).appendTo(listItem);
                                        $('<span>').text(NewStatus.name_ru).appendTo(statusDiv);
                                    }
                                } else {
                                    // заказ нельзя певодить в новый статус, удаляем кнопку изменения статуса и список статусов
                                    $('.edit-status-button', statusBox).remove();
                                    statusList.remove();
                                }

                                // отрисовка звезд оценки пользователя
                                $('<div class="evaluation-line"><span class="italic">Вас еще не оценили</span></div>').appendTo(statusBox);
                                var starsDiv = $('<div class="icon-line evaluation-line">').appendTo(statusBox);
                                $('<span class="italic">Ваша оценка:</span>').appendTo(starsDiv);
                                var evaluationInputs = $('#order-evaluation input[name=evaluation]');
                                for (var i = 0, len = evaluationInputs.length; i < len; i++) {
                                    if ($(evaluationInputs[i]).val() <= evaluation) {
                                        $('<i class="icon little-star">').appendTo(starsDiv);
                                    } else {
                                        $('<i class="icon little-star-empty">').appendTo(starsDiv);
                                    }
                                }

                                new globalAlert({
                                    message: 'Заказ №' + orderId + ' ' + data.orderStatusText.toLowerCase() + ' и перенесен в архив заказов.',
                                    ignoreOtherModals: true
                                });
                            } else {
                                new globalAlert({
                                    message: 'Произошла ошибка',
                                    ignoreOtherModals: true
                                });
                            }
                        },
                        error: function () {
                            new globalAlert({
                                message: 'Произошла ошибка',
                                ignoreOtherModals: true
                            });
                        }
                    });
                }
            });
        } else {
            // это не последний статус заказа
            new globalConfirm({
                message: 'Изменить статус заказа №' + orderId + ' на "' + $('span', this).text() + '"',
                ok: function () {
                    $.ajax({
                        url: '/cabinet/change-order-status/',
                        type: 'post',
                        data: {orderId: orderId, statusId: statusId},
                        success: function (data) {
                            if (data.status == 'ok') {
                                var statusLabel = statusBox.children('.order-status');
                                statusLabel.removeClass();
                                statusLabel.addClass('order-status ' + data.orderStatusClass);
                                $('span', statusLabel).text(data.orderStatusText);

                                // изменение списка новых статусов
                                var statusList = $('.status-list', statusBox);

                                if (data.possibleStatuses !== undefined && data.possibleStatuses.length > 0) {
                                    $('.new-status', statusList).remove();
                                    for (var i = 0, len = data.possibleStatuses.length; i < len; i++) {
                                        var NewStatus = data.possibleStatuses[i];
                                        var listItem = $('<li>').addClass('new-status').data('status-id', NewStatus.id).appendTo(statusList);
                                        var statusDiv = $('<div>').addClass('order-status ' + NewStatus.css_class).appendTo(listItem);
                                        $('<span>').text(NewStatus.name_ru).appendTo(statusDiv);
                                    }
                                } else {
                                    // заказ нельзя певодить в новый статус, удаляем кнопку изменения статуса и список статусов
                                    $('.edit-status-button', statusBox).remove();
                                    statusList.remove();
                                }

                                // удаление кнопки отмены заказа, если она есть
                                var canselSpan = $('.cancel', statusBox);
                                if (canselSpan.length > 0) {
                                    canselSpan.parent('.icon-line').remove();
                                }
                            } else {
                                new globalAlert({
                                    message: 'Произошла ошибка',
                                    ignoreOtherModals: true
                                });
                            }
                        },
                        error: function () {
                            new globalAlert({
                                message: 'Произошла ошибка',
                                ignoreOtherModals: true
                            });
                        }
                    });
                }
            });
        }
    });

    // оценка заказа прдавцом
    $('.evaluate-button span').click(function() {
        var orderId = statusBox.data('order-id');

        var htmlEvaluationBlock = $('<form id="order-evaluation">');
        var htmlEvaluationList = $('<ul>').appendTo(htmlEvaluationBlock);
        $('<li><input type="radio" name="evaluation" value="0" id="evaluation-inp1"><label for="evaluation-inp1" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Обман</span></label></li>').appendTo(htmlEvaluationList);
        $('<li><input type="radio" name="evaluation" value="0.2" id="evaluation-inp2"><label for="evaluation-inp2" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Плохо</span></label></li>').appendTo(htmlEvaluationList);
        $('<li><input type="radio" name="evaluation" value="0.5" id="evaluation-inp3"><label for="evaluation-inp3" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><i class="icon gold-star-empty"></i><span>Нормально</span></label></li>').appendTo(htmlEvaluationList);
        $('<li><input type="radio" name="evaluation" value="0.8" id="evaluation-inp4"><label for="evaluation-inp4" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star-empty"></i><span>Хорошо</span></label></li>').appendTo(htmlEvaluationList);
        $('<li><input type="radio" name="evaluation" value="1" id="evaluation-inp5"><label for="evaluation-inp5" class="icon-line"><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><i class="icon gold-star"></i><span>Отлично</span></label></li>').appendTo(htmlEvaluationList);


        new globalConfirm({
            message: 'Оцените действия заказчика.',
            html: htmlEvaluationBlock,
            ok: function() {
                var evaluation = $('#order-evaluation input[name=evaluation]:checked').val();
                if (evaluation === undefined) {
                    new globalAlert({
                        message: 'Вы не поставили оценку.',
                        ignoreOtherModals: true
                    });
                    return;
                }

                $.ajax({
                    url: '/cabinet/order-evaluation/',
                    type: 'post',
                    data: {orderId: orderId, evaluation: evaluation, getPartnerEvaluation: true},
                    success: function(data) {
                        if (data.status == 'ok') {
                            $('.evaluate-please', statusBox).remove();
                            $('.evaluate-button', statusBox).remove();

                            // отрисовка звезд оценки, поставленной покупателем
                            var evaluationInputs = $('#order-evaluation input[name=evaluation]');
                            if (data.partnerEvaluation !== undefined) {
                                var bigStarsDiv = $('<div class="icon-line evaluation-line">').appendTo(statusBox);
                                $('<span class="italic">Вас оценили:</span>').appendTo(bigStarsDiv);
                                for (var i = 0, len = evaluationInputs.length; i < len; i++) {
                                    if ($(evaluationInputs[i]).val() <= data.partnerEvaluation) {
                                        $('<i class="icon star">').appendTo(bigStarsDiv);
                                    } else {
                                        $('<i class="icon star-empty">').appendTo(bigStarsDiv);
                                    }
                                }
                            }

                            // отрисовка звезд оценки, поставленной продавцом
                            var starsDiv = $('<div class="icon-line evaluation-line">').appendTo(statusBox);
                            $('<span class="italic">Ваша оценка:</span>').appendTo(starsDiv);
                            for (var i = 0, len = evaluationInputs.length; i < len; i++) {
                                if ($(evaluationInputs[i]).val() <= evaluation) {
                                    $('<i class="icon little-star">').appendTo(starsDiv);
                                } else {
                                    $('<i class="icon little-star-empty">').appendTo(starsDiv);
                                }
                            }

                            new globalAlert({
                                message: 'Заказ №' + orderId + ' оценен и перенесен в архив заказов.',
                                ignoreOtherModals: true
                            });
                        } else {
                            new globalAlert({
                                message: 'Произошла ошибка',
                                ignoreOtherModals: true
                            });
                        }
                    },
                    error: function () {
                        new globalAlert({
                            message: 'Произошла ошибка',
                            ignoreOtherModals: true
                        });
                    }
                });
            }
        });
    });
});