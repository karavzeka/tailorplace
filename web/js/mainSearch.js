$(function() {
    var searchForm = $('#search');
    var requestTypeInput = $('input[name=request]', searchForm);
    $('ul.dropdown-menu span', searchForm).click(function() {
        requestTypeInput.val($(this).data('search-type'));
    });

    // обработка поискового запроса и переход по поисковой ссылке
    searchForm.submit(function() {
        var url = '/search/goods/';
        switch (requestTypeInput.val()) {
            case 'all':
                url = '/search/';
                break;
            case 'atelier':
                url = '/search/atelier/';
                break;
            case 'store':
                url = '/search/store/';
                break;
        }

        var searchPhrase = $('input[name=query]', searchForm).val();
        location.href = url + '?q=' + encodeURIComponent(searchPhrase);

        return false;
    });
});