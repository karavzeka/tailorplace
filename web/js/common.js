$(function(){
    // Включение тултипов
    $('*[data-toggle="tooltip"]').tooltip();

    $('select').selectmenu();

    // Активация кнопки для селекторов
    $('.select-button').click(function(){
        var parent = $(this).parent();
        $('select', parent).selectmenu( "open" );
    });
});

/**
 * Функция вызывает окно подтверждения
 */
function globalConfirm(settings)
{
    this.cancelFunction = (settings.cancel !== undefined) ? settings.cancel: function(){};
    this.okFunction = settings.ok;

    this.init = function (settings)
    {
        // если модальное окно уже вызвано, то не создавать еще
        if ($('.custom-modal').length > 0 && settings.ignoreOtherModals !== true) {
            return false;
        }
        // создадим html окошка
        this.modalWindow = $('<div class="confirm-message panel custom-modal">');
        var content = $('<div class="panel-body icon-line">');
        var icon = $('<span class="icon-circle little-help icon-circle-text">?</span>');
        var message = $('<p>').text(settings.message);
        var buttonDiv = $('<div class="button-line panel-footer">');
        var okButton = $('<span class="dashed-underline">').text('Да');
        var cancelButton = $('<span class="dashed-underline">').text('Отмена');

        this.modalWindow.append(content);
        this.modalWindow.append(buttonDiv);
        content.append(icon);
        content.append(message);

        if (settings.html !== undefined) {
            content.append(settings.html);
        }
        buttonDiv.append(okButton);
        buttonDiv.append(cancelButton);

        // навесим события на кнопки
        var self = this;
        okButton.click(function(){
            self.okAction();
        });
        cancelButton.click(function(){
            self.cancelAction(settings.cancel);
        });

        this.modalWindow.appendTo(document.body);

        var height = this.modalWindow.height();
        this.modalWindow.css('margin-top', -height / 2 - 20);
    };

    this.okAction = function()
    {
        this.okFunction();
        this.modalWindow.fadeOut('slow', function () {
            $(this).remove();
        })
    };

    this.cancelAction = function()
    {
        this.cancelFunction();
        this.modalWindow.fadeOut('slow', function () {
            $(this).remove();
        })
    };

    this.init(settings);
}

/**
 * Функция вызывает окно с предупреждением
 */
function globalAlert(settings)
{
    this.init = function (settings)
    {
        // если модальное окно уже вызвано, то не создавать еще
        if ($('.custom-modal').length > 0 && settings.ignoreOtherModals !== true) {
            return false;
        }
        // создадим html окошка
        this.modalWindow = $('<div class="confirm-message panel custom-modal">');
        var content = $('<div class="panel-body icon-line">');
        var icon = $('<span class="icon-circle little-help icon-circle-text">?</span>');
        var message = $('<p>').text(settings.message);
        var buttonDiv = $('<div class="button-line panel-footer">');
        var okButton = $('<span class="dashed-underline">').text('Ok');

        this.modalWindow.append(content);
        this.modalWindow.append(buttonDiv);
        content.append(icon);
        content.append(message);
        buttonDiv.append(okButton);

        // навесим события на кнопки
        var self = this;
        okButton.click(function(){
            self.okAction();
        });

        this.modalWindow.appendTo(document.body);

        var height = this.modalWindow.height();
        this.modalWindow.css('margin-top', -height / 2 - 20);
    };

    this.okAction = function()
    {
        this.modalWindow.fadeOut('slow', function () {
            $(this).remove();
        })
    };

    this.init(settings);
}

/**
 * Отдает HTML прелоадера
 * Настройки:
 * {
 *      size: 25,       // размер прелоадера
 *      borderWidth: 2, // толщина кольца
 *      spaceWidth: 2   // пространство между штрихами
 * }
 */
function getPreloader(settings)
{
    var preloader = $('<div class="preloader">');
    var preloaderInner = $('<div class="preloader-inner">');
    if (settings.size) {
        var borderWidth = 3;
        if (settings.borderWidth) {
            borderWidth = settings.borderWidth
            preloaderInner.css('border-width', borderWidth);
        }
        preloader.width(settings.size);
        preloader.height(settings.size);
        preloaderInner.width(settings.size - borderWidth * 2 - 2);
        preloaderInner.height(settings.size - borderWidth * 2 - 2);
    }
    preloader.append(preloaderInner);
    var deg = 0;
    for (var i = 0; i < 4; i++) {
        var line = $('<div class="line">');
        if (settings.spaceWidth) {
            line.width(settings.spaceWidth);
        }
        if (settings.background) {
            line.css('background', settings.background);
        }
        if (deg > 0) {
            line.css('transform', 'rotate(' + deg + 'deg)');
        }
        preloader.append(line);
        deg += 45;
    }
    return preloader;
}