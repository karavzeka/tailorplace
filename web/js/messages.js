$(function() {
    // подгрузка собеседников
    $('#show-more').on('click', '#show-more-button', function() {
        var offsetPart = $(this).data('offset-part');
        getInterlocutors(offsetPart);
    });

    function getInterlocutors(offsetPart)
    {
        $.ajax({
            url: '/cabinet/get-interlocutors/',
            type: 'post',
            data: {offsetPart: offsetPart},
            beforeSend: function(jqXHR) {
                var showMoreButton = $('#show-more-button');
                jqXHR.showMoreButton = showMoreButton;
                showMoreButton.hide(0);
                var preloader = getPreloader({size: 40, spaceWidth: 3});
                $('#show-more').append(preloader);
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                if (data.status == 'ok') {
                    var showMoreButton = jqXHR.showMoreButton;
                    var currentOffset = showMoreButton.data('offset-part');
                    showMoreButton.data('offset-part', currentOffset + 1);

                    var messagesWrapper = $('#messages');
                    jqXHR.preloader.remove();
                    messagesWrapper.append(data.html);

                    //скрытие кнопки подгрузки
                    if (messagesWrapper.children().length < showMoreButton.data('interlocutors-count')) {
                        showMoreButton.fadeIn();
                    }
                }
            }
        });
    }
});