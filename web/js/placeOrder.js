$(function() {
    $('.comment-button').click(function() {
        var footerBlock = $(this).parents('.panel-footer');
        var footerComment = $('.footer-comment', footerBlock);
        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $('button', $(this)).text('Добавить комментарий');
            $('textarea', footerComment).val('');
            footerComment.slideUp();
        } else {
            $(this).addClass('open');
            $('button', $(this)).text('Удалить комментарий');
            footerComment.slideDown();
        }
    });
});