$(function() {
    // карусель студий
    var studiosWrapper = $('#studio-carousel-wrapper');
    var helpTitle = $('#disable-moving-title');
    var promotedStudios = $('.studio-block', studiosWrapper);
    var countOfpromotedStudios = promotedStudios.length;
    if (countOfpromotedStudios > 4) {
        var movedElementIndex = 0;
        var disableMoving = false;
        shiftCarousel();

        // при наведении на студию тормозим движение карусели
        promotedStudios.mouseover(function(e) {
            disableMoving = true;
            helpTitle.fadeIn(200);
            e.stopPropagation();
        });

        promotedStudios.mouseleave(function(e) {
            disableMoving = false;
            helpTitle.fadeOut(200);
            e.stopPropagation();
        });
    }

    /**
     * Функция сдвигает студии в карусели на один вверх
     */
    function shiftCarousel() {
        setTimeout(function() {
            if (!disableMoving) {
                // надо двигать карусель
                $(promotedStudios[movedElementIndex]).animate({marginTop: '-134px'}, 500, 'linear', function () {
                    var movedElement = $(this).detach();
                    movedElement.css('margin-top', 0);
                    studiosWrapper.append(movedElement);
                    // несмотря на перемещение элемента в DOM'е его индекс в переменной-массиве не меняется
                    movedElementIndex++;
                    if (movedElementIndex >= countOfpromotedStudios) {
                        movedElementIndex = 0;
                    }
                    shiftCarousel();
                });
            } else {
                shiftCarousel();
            }
        }, 5000);
    }
});