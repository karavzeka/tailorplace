$(function() {
    // добавление товара в корзину
    var toBasket = $('#to-basket');
    var toBasketLink = $('a', toBasket);
    toBasketLink.click(function(){
        var goodId = $(this).data('good-id') * 1;

        $.ajax({
            type: 'post',
            url: '/users/order/to-basket/',
            dateType: 'json',
            data: {goodId: goodId},
            success: function(answer) {
                if (answer.status == 'ok') {
                    toBasket.fadeOut(300, function() {
                        var icon = $('#to-basket i').removeClass('basket-gold').addClass('basket');
                        toBasketLink.remove();
                        toBasket.append($('<span>В корзине</span>'));
                        toBasket.fadeIn(300);

                        // обновление кол-ва товаров в корзине в верхнем меню
                        var basketMenuItem = $('#personal li')[2];
                        var countSpan = $('span', basketMenuItem);
                        if (countSpan.length) {
                            $(countSpan).text('(' + answer.count + ')');
                        } else {
                            $(basketMenuItem).append($('<span class="red bolder">(' + answer.count + ')</span>'));
                        }

                    });
                } else {
                    var message = 'Ощибка на сервере. Не удалось добавить товар.';
                    if (answer.errorText !== undefined && answer.errorText != '') {
                        message = answer.errorText;
                    }
                    var alertObj = new globalAlert({
                        message: message
                    });
                }
            },
            error: function(answer) {
                var message = 'Ощибка на сервере. Не удалось добавить товар.';
                if (answer.responseJSON.errorText !== undefined && answer.responseJSON.errorText != '') {
                    message = answer.responseJSON.errorText;
                }
                var alertObj = new globalAlert({
                    message: message
                });
            }
        });

        return false;
    });

    // заказ товара
    var orderButton = $('#to-order');
    orderButton.click(function() {
        var goodId = $(this).data('good-id') * 1;

        $.ajax({
            type: 'post',
            url: '/order-goods/',
            dateType: 'json',
            data: {goodId: goodId},
            success: function(answer) {
                if (answer.status == 'ok') {
                    window.location.href = '/cabinet/basket/';
                } else {
                    var message = 'Ощибка на сервере. Не удалось добавить товар.';
                    if (answer.responseJSON.errorText !== undefined && answer.responseJSON.errorText != '') {
                        message = answer.responseJSON.errorText;
                    }
                    var alertObj = new globalAlert({
                        message: message
                    });
                }
            },
            error: function(answer) {
                var message = 'Ощибка на сервере. Не удалось добавить товар.';
                if (answer.responseJSON.errorText !== undefined && answer.responseJSON.errorText != '') {
                    message = answer.responseJSON.errorText;
                }
                var alertObj = new globalAlert({
                    message: message
                });
            }
        });

        return false;
    });

    // выбор фотографий
    var mainPhoto = $('#main-photo')[0];
    var thumbnailsBlock = $('.thumbnails-block');
    $('div', thumbnailsBlock).click(function() {
        if (!$(this).hasClass('active')) {
            $('div.active', thumbnailsBlock).removeClass('active');
            var thumbSrc = $(this).children()[0].src;
            var postfixStartPos = thumbSrc.lastIndexOf('_');
            var postfixEndPos = thumbSrc.lastIndexOf('.');
            var bigSrc = thumbSrc.substring(0, postfixStartPos) + '_big' + thumbSrc.substring(postfixEndPos);
            mainPhoto.src = bigSrc;
            $(this).addClass('active');
        }
    });

    // Галерея изображений (модуль magnific-popup)
    var galleryItems = [];
    var imgList = $('img', thumbnailsBlock);
    if (imgList.length > 0) {
        for (var i = 0, length = imgList.length; i < length; i++) {
            galleryItems[i] = {src: imgList[i].dataset.fullImg};
        }

        $('#main-photo-wrapper').magnificPopup({
            items: galleryItems,
            gallery: {
                enabled: true
            },
            type: 'image',
            callbacks: {
                beforeOpen: function() {
                    this.index = $('.active img', thumbnailsBlock).data('index');
                }
            }
        });

        $('#main-photo-wrapper').addClass('galleryAvailable');
    }
});