$(function() {
    var filters = {
        searchQuery: $('#search-query').text()
    };

    // подгрузка товаров
    $('#show-more').on('click', '#show-more-button', function() {
        filters.part = $(this).data('part');
        getGoods(filters, 'add');
    });

    /**
     * Отправка запроса на получение товаров
     *
     * @param filters
     */
    function getGoods(filters)
    {
        $.ajax({
            type: 'post',
            url: '/search/only-goods/',
            dataType: 'json',
            data: {filters: filters},
            beforeSend: function(jqXHR) {
                var showGoodButton = $('#show-more-button');
                jqXHR.showGoodButton = showGoodButton;
                showGoodButton.hide(0);
                var preloader = getPreloader({size: 40, spaceWidth: 3});
                $('#show-more').append(preloader);
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                var goodWrapper = $('#goods-wrapper');
                var showGoodButton = $('#show-more-button');

                // добавление списка товаров в конец существующего списка
                $('.empty', goodWrapper).remove();
                jqXHR.preloader.remove();
                goodWrapper.append(data.html);
                jqXHR.showGoodButton.fadeIn();
                var currentPart = showGoodButton.data('part');
                showGoodButton.data('part', currentPart + 1);
                showGoodButtonToggle(goodWrapper.data('count-of-goods'), data.goodsOnPage);

                // дополнение пустыми блоками товаров для коррекции text-align: justify
                var visibleGoodsCount = $('.good-instance', goodWrapper).length;
                var lastCol = visibleGoodsCount % 3;
                if (lastCol != 0) {
                    for (var i = 3 - lastCol; i > 0; i--) {
                        goodWrapper.append($('<div class="good-instance empty"></div>'));
                    }
                }
            }
        });
    }

    /**
     * Отображает или скрывает кнопку "Показать еще" в зависимости от кол-ва товаров, удовлетворяющих
     * фильтрам и кол-ву товаров, отображенных на странице
     *
     * @param countOfGoods int
     * @param goodsOnPage int
     */
    function showGoodButtonToggle(countOfGoods, goodsOnPage)
    {
        var showMoreBlock = $('#show-more');
        var showGoodButton = $('#show-more-button');
        if (countOfGoods > goodsOnPage) {
            // если по фильтрам всего больше товаров чем отображено и кнопки нет, то добавить ее
            if (showGoodButton.length == 0) {
                var createdButton = $('<div id="show-more-button" data-part="1"><span class="plus-button"></span> <span>Показать еще</span> </div>');
                showMoreBlock.append(createdButton);
            }
        } else {
            // если кнопка есть и на странице отображены все возможные товары, то кнопка удаляется
            if (showGoodButton.length > 0) {
                showGoodButton.remove();
            }
        }
    }
});