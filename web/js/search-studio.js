$(function() {
    var studioWrapper = $('#studio-wrapper');
    var filters = {
        searchQuery: $('#search-query').text(),
        studioType: studioWrapper.data('studio-type')
    };

    // подгрузка студий
    $('#show-more').on('click', '#show-more-button', function() {
        filters.part = $(this).data('part');
        getStudios(filters, 'add');
    });

    /**
     * Отправка запроса на получение студий
     *
     * @param filters
     * @param actionType
     */
    function getStudios(filters, actionType)
    {
        $.ajax({
            type: 'post',
            url: '/search/only-studios/',
            dataType: 'json',
            data: {filters: filters},
            beforeSend: function(jqXHR) {
                var showMoreButton = $('#show-more-button');
                jqXHR.showMoreButton = showMoreButton;
                showMoreButton.hide(0);
                var preloader = getPreloader({size: 40, spaceWidth: 3});
                $('#show-more').append(preloader);
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                var showMoreButton = $('#show-more-button');

                // добавление списка студий в конец существующего списка
                jqXHR.preloader.remove();
                studioWrapper.append(data.html);
                jqXHR.showMoreButton.fadeIn();
                var currentPart = showMoreButton.data('part');
                showMoreButton.data('part', currentPart + 1);
                showGoodButtonToggle(studioWrapper.data('count-of-studios'), data.studiosOnPage);
            }
        });
    }

    /**
     * Отображает или скрывает кнопку "Показать еще" в зависимости от кол-ва студий, удовлетворяющих
     * фильтрам и кол-ву студий, отображенных на странице
     *
     * @param countOfStudios int
     * @param studiosOnPage int
     */
    function showGoodButtonToggle(countOfStudios, studiosOnPage)
    {
        var showMoreBlock = $('#show-more');
        var showMoreButton = $('#show-more-button');
        if (countOfStudios > studiosOnPage) {
            // если по фильтрам всего больше студий чем отображено и кнопки нет, то добавить ее
            if (showMoreButton.length == 0) {
                var createdButton = $('<div id="show-more-button" data-part="1"><span class="plus-button"></span> <span>Показать еще</span> </div>');
                showMoreBlock.append(createdButton);
            }
        } else {
            // если кнопка есть и на странице отображены все возможные студии, то кнопка удаляется
            if (showMoreButton.length > 0) {
                showMoreButton.remove();
            }
        }
    }
});