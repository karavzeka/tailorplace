$(function(){
    // пока надо посмотреть как будут выглядеть страницы с полным описанем (без сокрытия)
    //var open = false;
    //var mainDescription = $('#description');
    //var descriptionButton = $('#read-all');
    //var description = $('p', mainDescription);
    //var innerWrapper = $('.inner-wrapper');
    //var outerWrapper = $('.outer-wrapper');
    //var descriptionFooter = $('.description-footer');
    //var startMaxHeight = parseInt(innerWrapper.css('max-height'));
    //var descriptionHeight = description.height();
    //
    //console.log(startMaxHeight);
    //console.log(descriptionHeight);
    //
    //if (descriptionHeight <= startMaxHeight) {
    //    descriptionFooter.remove();
    //} else {
    //    innerWrapper.height(startMaxHeight);
    //    innerWrapper.css('max-height', 'none');
    //    mainDescription.height(mainDescription.height());
    //
    //    $('a', descriptionButton).click(function(){
    //        if (open == false) {
    //            $('span',descriptionButton).text('↑');
    //            $(this).text('Свернуть');
    //            innerWrapper.animate({height: descriptionHeight});
    //            descriptionFooter.animate({
    //                backgroundColor: '#EFE5DC',
    //                paddingTop: '5px',
    //                paddingBottom: '13px'
    //            });
    //
    //            open = true;
    //        } else {
    //            $('span',descriptionButton).text('↓');
    //            $(this).text('Читать полностью');
    //            innerWrapper.animate({height: startMaxHeight});
    //            descriptionFooter.animate({
    //                backgroundColor: '#FDF8F0',
    //                padding: 0
    //            });
    //
    //            open = false;
    //        }
    //    });
    //}

    $( "#sort-selector" ).selectmenu({
        change: function(event, data) {
            var currentUrl = $('#base-url').val();

            switch (data.item.value) {
                case 'date_asc':
                    document.location.replace(currentUrl + 'order_by/date/');
                    break;
                case 'date_desc':
                    document.location.replace(currentUrl + 'order_by/date/DESC/');
                    break;
                case 'name_asc':
                    document.location.replace(currentUrl + 'order_by/name/');
                    break;
                case 'name_desc':
                    document.location.replace(currentUrl + 'order_by/name/DESC/');
                    break;
                case 'price_asc':
                    document.location.replace(currentUrl + 'order_by/price/');
                    break;
                case 'price_desc':
                    document.location.replace(currentUrl + 'order_by/price/DESC/');
                    break;
            }
        }
    });
});