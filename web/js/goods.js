$(function() {
    // сортировка товаров
    $( "#sort-selector" ).selectmenu({
        change: function(event, data) {
            var currentUrl = $('#base-url').val();

            switch (data.item.value) {
                case 'date_asc':
                    document.location.replace(currentUrl + 'order_by/date/');
                    break;
                case 'date_desc':
                    document.location.replace(currentUrl + 'order_by/date/DESC/');
                    break;
                case 'name_asc':
                    document.location.replace(currentUrl + 'order_by/name/');
                    break;
                case 'name_desc':
                    document.location.replace(currentUrl + 'order_by/name/DESC/');
                    break;
                case 'price_asc':
                    document.location.replace(currentUrl + 'order_by/price/');
                    break;
                case 'price_desc':
                    document.location.replace(currentUrl + 'order_by/price/DESC/');
                    break;
            }
        }
    });

    // фильтрация товаров
    var filters = {};

    var studioTypeWrapper = $('#studio-type');
    var categoryList = $('#category-list');

    // установим фильтрацию по типу студии
    var activeStudioElement = $('.active', studioTypeWrapper);
    if (activeStudioElement.length) {
        var activeStudio = activeStudioElement.data('filter-studio');
        if (activeStudio == 'atelier') {
            filters.studioType = 'atelier';
        } else if (activeStudio == 'store') {
            filters.studioType = 'store';
        }
    }

    $('span', studioTypeWrapper).click(function() {
        if (!$(this).hasClass('active')) {
            resetFilters(false);
            var studioType = $(this).data('filter-studio');
            filters.studioType = studioType;

            $('.active', studioTypeWrapper).removeClass('active red').addClass('dashed-underline');
            $(this).addClass('active red').removeClass('dashed-underline');

            switch (studioType) {
                case 'all':
                    $('li', categoryList).show(0);
                    break;
                case 'atelier':
                    $('li .category-wrapper[data-type=atelier]', categoryList).parent().show(0);
                    $('li .category-wrapper[data-type=store]', categoryList).parent().hide(0);
                    break;
                case 'store':
                    $('li .category-wrapper[data-type=store]', categoryList).parent().show(0);
                    $('li .category-wrapper[data-type=atelier]', categoryList).parent().hide(0);
                    break;
            }

            getGoods(filters);
        }
    });

    // фильтры цены
    var lowPriceInput = $('#low-price-limit');
    var highPriceInput = $('#high-price-limit');
    if (lowPriceInput.val()) {
        filters.priceLow = lowPriceInput.val();
    }
    if (highPriceInput.val()) {
        filters.priceHigh = highPriceInput.val();
    }
    lowPriceInput.change(function() {
        filters.priceLow = $(this).val();
        getGoods(filters);
    });
    highPriceInput.change(function() {
        filters.priceHigh = $(this).val();
        getGoods(filters);
    });

    // фильтры категории
    var maxActive = 4;
    var activeCategoryList = $('#active-category-list');
    var deleteIcon = $('<i class="icon delete-red" title="Убрать фильтр">');

    $('li .category-wrapper', activeCategoryList).each(function() {
        if (filters.category === undefined) {
            filters.category = [];
        }
        filters.category.push($(this).data('id'));
    });

    categoryList.on('click', '.category-wrapper', function() {
        if ($('li', activeCategoryList).length < maxActive) {
            if (filters.category === undefined) {
                filters.category = [];
            }
            filters.category.push($(this).data('id'));
            getGoods(filters);
            activateCategory($(this));
        }

        return false;
    });

    activeCategoryList.on('click', '.delete-red', function() {
        var parentLi = $(this).parent();
        var categoryId = parentLi.children('.category-wrapper').data('id');
        var indexOfCategory = filters.category.indexOf(categoryId);
        filters.category.splice(indexOfCategory, 1);    // удаление части массива (в данном случае одного значения)
        getGoods(filters);
        deactivateCategory($(this));
    });

    // кнопка сброса фильтров
    $('#filters-reset span').click(function() {
        resetFilters(true);
    });

    /**
     * Сброс фильтров кроме типа студии
     *
     * @param doRequest
     */
    function resetFilters(doRequest)
    {
        if (filters.priceLow || filters.priceHigh ||filters.category) {
            filters.priceLow = undefined;
            filters.priceHigh = undefined;
            filters.category = undefined;
            lowPriceInput.val(0);
            highPriceInput.val('');
            $('.delete-red', activeCategoryList).each(function() {
                deactivateCategory($(this));
            });
            if (doRequest) {
                getGoods(filters);
            }
        }
    }

    /**
     * Перемещает категорию из общего списка в список активных
     * @param categoryNode
     */
    function activateCategory(categoryNode)
    {
        if ($('li', activeCategoryList).length < maxActive) {
            var parentLi = categoryNode.parent();
            parentLi.detach();
            parentLi.append(deleteIcon.clone());

            var categoryPos = categoryNode.data('sort-pos');
            var activeList = $('li', activeCategoryList);
            if (activeList.length > 0) {
                // добавление элемента в нужной позиции
                for (var i = 0, len = activeList.length; i < len; i++) {
                    var elementPosition = $(activeList[i]).children('.category-wrapper').data('sort-pos');
                    if (elementPosition > categoryPos) {
                        $(activeList[i]).before(parentLi);
                        break;
                    }
                    if (i == len - 1) {
                        // если у категории порядок сортировки больше остальных, то добавляем в конец
                        activeCategoryList.append(parentLi)
                    }
                }
            } else {
                activeCategoryList.append(parentLi);
            }

            activeCategoryList.removeClass('hide');
        }
    }

    /**
     * Перемещает категорию из общего списка в список активных
     * @param deleteButtonNode
     */
    function deactivateCategory(deleteButtonNode)
    {
        var parentLi = deleteButtonNode.parent();

        deleteButtonNode.remove();
        parentLi.detach();
        var categoryPos = parentLi.children('.category-wrapper').data('sort-pos');
        var mainList = $('li', categoryList);
        for (var i = 0, len = mainList.length; i < len; i++) {
            var elementPosition = $(mainList[i]).children('.category-wrapper').data('sort-pos');
            if (elementPosition > categoryPos) {
                $(mainList[i]).before(parentLi);
                break;
            }
            if (i == (len - 1)) {
                // если у категории порядок сортировки больше остальных, то добавляем в конец
                categoryList.append(parentLi)
            }
        }
        if ($('li', activeCategoryList).length == 0) {
            activeCategoryList.addClass('hide');
        }
    }

    /**
     * Отправка запроса на получение товаров в соответствии с фильтрами
     *
     * @param filters
     * @param actionType
     */
    function getGoods(filters, actionType)
    {
        $.ajax({
            type: 'post',
            url: '/only-goods/',
            dataType: 'json',
            data: {filters: filters},
            beforeSend: function(jqXHR) {
                var preloader;
                if (actionType == 'add') {
                    var showGoodButton = $('#show-more-button');
                    jqXHR.showGoodButton = showGoodButton;
                    showGoodButton.hide(0);
                    preloader = getPreloader({size: 40, spaceWidth: 3});
                    $('#show-more').append(preloader);
                } else {
                    var goodWrapper = $('#goods-wrapper');
                    goodWrapper.empty();
                    preloader = getPreloader({size: 60, borderWidth: 4, spaceWidth: 5});
                    goodWrapper.append(preloader);
                }
                jqXHR.preloader = preloader;
            },
            success: function(data, textStatus, jqXHR) {
                var goodWrapper = $('#goods-wrapper');
                var showGoodButton = $('#show-more-button');
                if (actionType == 'add') {
                    // добавление списка товаров в конец существующего списка
                    $('.empty', goodWrapper).remove();
                    jqXHR.preloader.remove();
                    goodWrapper.append(data.html);
                    jqXHR.showGoodButton.fadeIn();
                    var currentPart = showGoodButton.data('part');
                    showGoodButton.data('part', currentPart + 1);
                    showGoodButtonToggle(goodWrapper.data('count-of-goods'), data.goodsOnPage);
                } else {
                    // замена списка товаров
                    $('#count-of-goods').text(data.countText);
                    jqXHR.preloader.remove();
                    goodWrapper.append(data.html);
                    goodWrapper.data('count-of-goods', data.countOfGoods);
                    showGoodButtonToggle(data.countOfGoods, data.goodsOnPage);
                }
                // дополнение пустыми блоками товаров для коррекции text-align: justify
                var visibleGoodsCount = $('.good-instance', goodWrapper).length;
                var lastCol = visibleGoodsCount % 3;
                if (lastCol != 0) {
                    for (var i = 3 - lastCol; i > 0; i--) {
                        goodWrapper.append($('<div class="good-instance empty"></div>'));
                    }
                }

                // после очередного успешного запроса сбросить смещение, чтобы рассчиталось общее кол-во товаров в следующем запросе
                filters.part = undefined;
            }
        });
    }

    // подгрузка товаров
    $('#show-more').on('click', '#show-more-button', function() {
        filters.part = $(this).data('part');
        getGoods(filters, 'add');
    });

    /**
     * Отображает или скрывает кнопку "Показать еще" в зависимости от кол-ва товаров, удовлетворяющих
     * фильтрам и кол-ву товаров, отображенных на странице
     *
     * @param countOfGoods int
     * @param goodsOnPage int
     */
    function showGoodButtonToggle(countOfGoods, goodsOnPage)
    {
        var showMoreBlock = $('#show-more');
        var showGoodButton = $('#show-more-button');
        if (countOfGoods > goodsOnPage) {
            // если по фильтрам всего больше товаров чем отображено и кнопки нет, то добавить ее
            if (showGoodButton.length == 0) {
                var createdButton = $('<div id="show-more-button" data-part="1"><span class="plus-button"></span> <span>Показать еще</span> </div>');
                showMoreBlock.append(createdButton);
            }
        } else {
            // если кнопка есть и на странице отображены все возможные товары, то кнопка удаляется
            if (showGoodButton.length > 0) {
                showGoodButton.remove();
            }
        }
    }
});