<?php
/**
 * Осуществляет операции с числами
 */

namespace app\components;

use yii\base\Component;

class NumberHelper extends Component
{
    /**
     * Форматирует число в валюту
     *
     * @param int|float $number Валюта, умноженная на 100 (чтобы в базе хранилась в int)
     * @param bool $divide Если false, то не будет осуществлять деление, а отформатирует поданное число
     * @return string Отформатированное число для денежного номинала
     */
    public function numberToPrice($number, $divide = true)
    {
        // TODO сделать форматирование для разных локалей
        if ($divide) {
            $decimalPart = $number % 100;
            $number /= 100;
            if ($decimalPart == 0) {
                return number_format($number, 0, ',', '&nbsp;');
            } else {
                return number_format($number, 2, ',', '&nbsp;');
            }
        }
        return number_format($number, 0, ',', '&nbsp;');
    }

    /**
     * Преобразует строковое представление цены в целое число увеличенное в 100 раз,
     * для хранения в БД
     *
     * @param string $price
     * @return int|float
     */
    public function priceToDBVal($price, $multiple = true)
    {
        $res = str_replace(
            [' ', ','],
            ['', '.'],
            trim($price)
        );
        $value = $multiple ? intval($res * 100) : floatval($res);
        return $value;
    }

    /**
     * Преобразует строковое представление IPv4 в беззнаковое целое число для хранения в БД
     *
     * @param string $ip строковое представление IPv4
     * @return string
     */
    public function ipToUnsignedNumber($ip)
    {
        return sprintf('%u', ip2long($ip));
    }

    /**
     * Форматирует 11-значное число в строку формата +7(911)345-67-89
     * Если длина числа не равна 11 символам, то вернет строку как есть
     *
     * @param string $phoneNumber
     * @return string
     */
    public function numberToPhone($phoneNumber)
    {
        if (strlen($phoneNumber) != 11) {
            return $phoneNumber;
        }

        return '+' . substr($phoneNumber, 0, 1) . '(' . substr($phoneNumber, 1, 3) . ')' . substr($phoneNumber, 4, 3) . '-' . substr($phoneNumber, 7, 2) . '-' . substr($phoneNumber, 9, 2);
    }
} 