<?php
/**
 * Класс позволяет собирать переменные для js и затем вывести их в виде js объекта в секции <head>
 */

namespace app\components;

use yii\base\Component;

class JsGlobalVar extends Component
{
    /**
     * @var array Массив с переменными
     */
    private $vars = [];

    /**
     * Добавляет переменную в массив
     *
     * @param string $key По сути имя переменной
     * @param mixed $value Значение переменной
     */
    public function addVar($key, $value)
    {
        $this->vars[$key] = $value;
    }

    /**
     * Преобразует переменные в Json объект и возвращает его.
     * По сути этот объект нужно присвоить js переменной на странице
     *
     * @return string
     */
    public function translateToJs()
    {
        return !empty($this->vars) ? json_encode($this->vars) : '{}';
    }
}