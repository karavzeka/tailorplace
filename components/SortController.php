<?php
/**
 * Сохраняет в сеесию, какие таблицы по какому полю сортировать
 */

namespace app\components;

use Yii;
use yii\base\Exception;

class SortController
{
    /**
     * @var string Ключ для хранения в сессии настроек пользователя по сортировке таблиц
     */
    private $sessionKey = 'pageSort';
    private $sortKey;
    private $sortFields = [];

    public function __construct($key, array $sortFields)
    {
        $this->sortKey = $key;
        $this->sortFields = $sortFields;
        return $this;
    }

    /**
     * Возвращает параметры сортировки и устанавливает новые если они переданы через GET параметры
     *
     * @return array
     * @throws Exception
     */
    public function getSort()
    {
        if (!$this->sortKey || !$this->sortFields) {
            throw new Exception('Unknown sort key');
        }

        $dirList = ['ASC', 'DESC'];

        $sortRequest = isset($this->sortFields[Yii::$app->request->get('order')]) ? Yii::$app->request->get('order') : '';
        $direction = in_array(mb_strtoupper(Yii::$app->request->get('dir')), $dirList) ? mb_strtoupper(Yii::$app->request->get('dir')) : '';
        $existOrder = $this->getCurrentOrder();

        if (!empty($sortRequest) && in_array($sortRequest, array_keys($this->sortFields))) {
            // отсортируем результат по полученной сортировке
            $direction = $direction == 'DESC' ? $direction : 'ASC';
            $sortParams = [
                'sortField' => $sortRequest,
                'sortDirection' => $direction,
                'sortQueryPart' => $this->sortFields[$sortRequest] . ' ' . $direction
            ];

            // сохраним сортировку для данной страницы
            $fieldOrder = [
                'fieldName' => $this->sortFields[$sortRequest],
                'direction' => $direction
            ];
            $this->setOrder($fieldOrder, 'mainGoodList');
        } elseif ($existOrder) {
            // отсортируем результат по сохраненной сортировке
            $sortField = array_search($existOrder['fieldName'], $this->sortFields);
            $sortParams = [
                'sortField' => $sortField ? $sortField : 'date',
                'sortDirection' => $existOrder['direction'],
                'sortQueryPart' => $existOrder['fieldName'] . ' ' . $existOrder['direction']
            ];
        } else {
            // сортировка по умолчанию
            if (isset($this->sortFields['default'])) {
                $sortParams = [
                    'sortField' => $this->sortFields['default'],
                    'sortDirection' => 'DESC',
                    'sortQueryPart' => $this->sortFields[$this->sortFields['default']] . ' DESC'
                ];
            } else {
                $sortParams = null;
            }
        }

        return $sortParams;
    }

    /**
     * Получить настройки сортировки таблицы
     *
     * @return array|null
     */
    private function getCurrentOrder()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        $tableOrders = $session->get($this->sessionKey);
        if (empty($tableOrders)) {
            return null;
        }

        return isset($tableOrders[$this->sortKey]) ? $tableOrders[$this->sortKey] : null;
    }

    /**
     * Установить настройки сортировки таблицы
     *
     * @param array $fieldParams
     * @param $tableKey
     * @return mixed
     * @throws Exception
     */
    private function setOrder(array $fieldParams, $tableKey)
    {
        if (!isset($fieldParams['fieldName']) || empty($fieldParams['fieldName']) || !isset($fieldParams['direction']) || empty($fieldParams['direction'])) {
            throw new Exception('В качестве параметра должен быть массив вида [\'fieldName\' => \'`db`.`tale`.`fieldName`\', \'direction\' => \'ASC|DESC\']');
        }

        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        $tableOrders = $session->get($this->sessionKey);
        if (!is_array($tableOrders)) {
            $tableOrders = [
                $this->sortKey => [
                    'fieldName' => $fieldParams['fieldName'],
                    'direction' => $fieldParams['direction']
                ]
            ];
        } else {
            $tableOrders[$this->sortKey] = [
                'fieldName' => $fieldParams['fieldName'],
                'direction' => $fieldParams['direction']
            ];
        }

        $session->set($this->sessionKey, $tableOrders);
        return $tableOrders[$tableKey];
    }
}