<?php
/**
 * Набор общих методов для разных целей
 */
// TODO если нигде не используется класс, то удалить.
namespace app\components;

use yii\base\Component;
use yii\web\Request;

class CommonTools extends Component
{
    /**
     * @var string|null Абсолютный путь на главную страницу сайта
     */
    private $absoluneRootPath = '';

    /**
     * Возвращает абсолютный путь на главную страницу сайта
     *
     * @return string
     */
    public function getAbsoluneRootPath()
    {
        if ($this->absoluneRootPath === '') {
            $Request = new Request();
            $Request->setUrl('/');
            $this->absoluneRootPath = $Request->getAbsoluteUrl();
        }
        return $this->absoluneRootPath;
    }

    /**
     * По определенному алгоритму обрезает description по определенному символу.
     *
     * @param string $description Текст для тега description
     * @param int $maxDescriptionLength Максимальная длина для тега description
     * @return string
     */
    public function metaDescriptionLengthOptimize($description, $maxDescriptionLength = 250)
    {
        if (mb_strlen($description) <= $maxDescriptionLength) {
            return $description;
        }
        $prioritetDotPosition = 150;
        $minDotPosition = 100;
        $minOtherSymbolsPosition = $prioritetDotPosition + 1;

        $lastDotPosition = 0;
        $lastOtherSymbolPosition = 0;
        $lastSpacePosition = 0;

        for ($i = 0; $i < 300; $i++) {
            $symbol = mb_substr($description, $i, 1);
            if (in_array($symbol, ['.', '?', '!'])) {
                $lastDotPosition = $i;
                continue;
            }
            if (in_array($symbol, [',', ';', ':'])) {
                $lastOtherSymbolPosition = $i;
                continue;
            }
            if (preg_match('/\s/', $symbol)) {
                $lastSpacePosition = $i;
            }
        }

        if ($lastDotPosition >= $prioritetDotPosition) {
            return mb_substr($description, 0, $lastDotPosition + 1);
        } elseif ($lastDotPosition >= $minDotPosition && $lastOtherSymbolPosition < $minOtherSymbolsPosition) {
            return mb_substr($description, 0, $lastDotPosition + 1);
        } elseif ($lastOtherSymbolPosition >= $minOtherSymbolsPosition) {
            return mb_substr($description, 0, $lastOtherSymbolPosition);
        } elseif ($lastSpacePosition > 0) {
            return mb_substr($description, 0, $lastSpacePosition);
        } else {
            return mb_substr($description, 0, $maxDescriptionLength);
        }
    }

    /**
     * Формирует отчет об ошибке и отправляет его на почту
     *
     * @param int $errorCode Код ошибки
     * @param string $errorText Текст ошибки
     * @param array $backtrace Результат вызова функции debug_backtrace()
     */
    public function sendErrorReport($errorCode, $errorText, array $backtrace)
    {
        $html = '<h2>Произошла ошибка</h2>';
        $html .= '<p>Время ошибки: ' . date('d.m.Y H:i:s') . '</p>';
        $html .= '<p>Ошибка: ' . $errorText . '</p>';
        $html .= '<p>Код ошибки: ' . $errorCode . '</p>';
        $html .= '<p>Url: ' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '</p>';
        $html .= '<p>IP: ' . $_SERVER['REMOTE_ADDR'] . '</p>';

        $User = \Yii::$app->user->identity;
        if ($User instanceof \app\modules\users\models\User) {
            $html .= '<p>Пользователь авторизован, id = ' . $User->id . '</p>';
        } else {
            $html .= '<p>Пользователь не авторизован</p>';
        }

        $html .= '<pre>';
        $html .= "\nBacktrace:\n";
        foreach ($backtrace as $key => $row) {
            $html .= "#" . $key . "[\n";
            if (isset($row['file'])) {
                $html .= "\tФайл: " . $row['file'] . "\n";
            }
            if (isset($row['line'])) {
                $html .= "\tСтрока: " . $row['line'] . "\n";
            }
            if (isset($row['class'])) {
                $html .= "\tКласс: " . $row['class'] . "\n";
            }
            if (isset($row['function'])) {
                $html .= "\tФункция/метод: " . $row['function'] . "\n";
            }
            $html .= "]\n\n";
        }
        $html .= '</pre>';

        $Mailer = \Yii::$app->mailer;
        $Mailer->compose()
            ->setTo('support@tailor-place.com')
            ->setSubject('На сервере произошла ошибка')
            ->setHtmlBody($html)
            ->send();
    }
}