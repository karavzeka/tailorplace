<?php
/**
 * Набор методов по управлению загрузкой, перемещением и удалением фотографий
 */

namespace app\components;

use Yii;
use yii\helpers\BaseFileHelper;
use app\modules\good\models\Photo;
use app\modules\users\models\Photo as UserPhoto;


class FileHelper
{
    /**
     * Загружает файл товара фо временную директорию
     *
     * @param int $userId Id пользователя
     * @param \yii\web\UploadedFile $file
     * @param int $fileNumber
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
	public function uploadTempGoodPhoto($userId, $file, $fileNumber)
	{
		$answer = [
			'success' => null,
			'error' => null,
		];

		$photoDir = self::getTempGoodPhotoDir($userId);
		$res = BaseFileHelper::createDirectory($photoDir);
		if (!$res) {
			$answer['error'] = 'Произошла ошибка на сервере';
			return $answer;
		}

		$fileList = BaseFileHelper::findFiles($photoDir);
		if (count($fileList) >= 9) {
			$answer['error'] = 'Превышен лимит загружаемых фотографий';
			return $answer;
		}

		$resultExtension = '';
		$legalExtensions = ['jpg', 'png', 'gif'];
		$mime = BaseFileHelper::getMimeType($file->tempName);
		$fileExtensions = BaseFileHelper::getExtensionsByMimeType($mime);
		foreach ($legalExtensions as $extension) {
			if (in_array($extension, $fileExtensions)) {
				$resultExtension = $extension;
				break;
			}
		}
		if (!$resultExtension) {
			$answer['error'] = 'Разрешено загружать файлы типов jpg, png или gif';
			return $answer;
		}

		$fileName = $photoDir . '/' . $fileNumber . '.' . $resultExtension;
        // Проверка, развернута ли фотография
        if ($resultExtension == 'jpg') {
            $exif = exif_read_data($file->tempName);
        } else {
            $exif = null;
        }
        if (!empty($exif['Orientation']) && $exif['Orientation'] != 1) {
            // фотография имеет метку положения угла
            $this->rotateAndSave($file->tempName, $fileName);
        } else {
            $file->saveAs($fileName);
        }

		$answer['success'] = $fileNumber;
		return $answer;
	}

    private function rotateAndSave($sourceFileName, $destinationFileName)
    {
        $Image = new \Imagick($sourceFileName);
        $orientation = $Image->getImageOrientation();

        switch($orientation) {
            case \Imagick::ORIENTATION_TOPLEFT:
                break;
            case \Imagick::ORIENTATION_TOPRIGHT:
                $Image->flopImage();
                break;
            case \Imagick::ORIENTATION_BOTTOMRIGHT:
                $Image->rotateimage("#000", 180); // rotate 180 degrees
                break;
            case \Imagick::ORIENTATION_BOTTOMLEFT:
                $Image->rotateimage("#000", 180); // rotate 180 degrees
                $Image->flopImage();
                break;
            case \Imagick::ORIENTATION_LEFTTOP:
                $Image->rotateimage("#000", 90); // rotate 90 degrees CW
                $Image->flopImage();
                break;
            case \Imagick::ORIENTATION_RIGHTTOP:
                $Image->rotateimage("#000", 90); // rotate 90 degrees CW
                break;
            case \Imagick::ORIENTATION_RIGHTBOTTOM:
                $Image->rotateimage("#000", -90); // rotate 90 degrees CCW
                $Image->flopImage();
                break;
            case \Imagick::ORIENTATION_LEFTBOTTOM:
                $Image->rotateimage("#000", -90); // rotate 90 degrees CCW
                break;
        }

        $Image->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
        $Image->writeImage($destinationFileName);
    }


	public function deleteTempGoodPhoto($userId, $fileNumber)
	{
		$dir = self::getTempGoodPhotoDir($userId);
		$files = BaseFileHelper::findFiles($dir);
		$result = false;
		foreach ($files as $file) {
			$pathInfo = pathinfo($file);
			if ($pathInfo['filename'] == $fileNumber) {
				$result = unlink($dir . '/' . $pathInfo['filename'] . '.' . $pathInfo['extension']);
				if (!$result) {
					$answer['error'] = 'Произошла ошибка на сервере';
					return $answer;
				}

				$answer['success'] = 'ok';
				return $answer;
			}
		}

		if (!$result) {
			$answer['error'] = 'Файл не найден на сервере';
			return $answer;
		}
	}

	/**
	 * Возвращает путь для временного хранения фотографий товара
	 * @param  int $userId Id пользователя
	 * @return string Путь к директории
	 */
	public function getTempGoodPhotoDir($userId)
	{
		$userId = (int)$userId;
		return Yii::getAlias('@app') . '/web/tempPhotos/' . $userId . '/goods';
	}

	/**
	 * Возвращает путь для временного хранения фотографий пользователя
	 * @param  int $sessionId Id сессии пользователя
	 * @return string Путь к директории
	 */
	public function getTempUserPhotoDir($sessionId)
	{
		return Yii::getAlias('@app') . '/web/tempPhotos/' . $sessionId . '/user';
	}

	/**
	 * Очищает директорию временного хранения фотографий товара
	 * @param  int $userId Id пользователя
	 */
	public function clearTempGoodPhotoDir($userId)
	{
		$userId = (int)$userId;
		$dir = self::getTempGoodPhotoDir($userId);
		BaseFileHelper::removeDirectory($dir);
	}

    /**
     * Очищает директорию временного хранения фотографий пользователя
     * @param  int $userId Id пользователя
     */
	public function clearTempUserPhotoDir($sessionId)
	{
		$dir = self::getTempUserPhotoDir($sessionId);
		BaseFileHelper::removeDirectory($dir);
	}

    public function uploadGoodPhotos($userId, $photoUploadSettings, $destinationDir, $startNumber)
    {
        if (!$userId || !is_numeric($startNumber)) {
            return false;
        }
        BaseFileHelper::createDirectory($destinationDir);
        $tmpPhotoDir = self::getTempGoodPhotoDir($userId);
        $fileList = BaseFileHelper::findFiles($tmpPhotoDir);
        $files = [];
        foreach ($fileList as $path) {
            $path = BaseFileHelper::normalizePath($path);
            $file = pathinfo($path);
            $files[$file['filename']] = $path;
        }
        $uploadedFiles = [];
        foreach ($photoUploadSettings as $Object) {
            $fileNumber = (int)$Object->fileNumber;
            // вызываем сохранение и обработку файла только для закаченных файлов
            if (isset($files[$fileNumber]) && file_exists($files[$fileNumber])) {
                $destinationPath = BaseFileHelper::normalizePath($destinationDir . '/' . $startNumber . '.jpg');
                $res = $this->makeFile($files[$fileNumber], $Object, $destinationPath);
                if ($res) {
                    $uploadedFiles[] = [
                        'fileNumber' => $fileNumber,
                        'numberName' => $startNumber,
                        'main' => boolval($Object->mainPhoto),
                        'marginTopPart' => $Object->marginTopPart,
                        'marginLeftPart' => $Object->marginLeftPart,
                        'sizePart' => $Object->sizePart,
                        'realWidth' => $res['width'],
                        'realHeight' => $res['height'],
                    ];
                }
                $startNumber++;
            }
        }
        return $uploadedFiles;
    }

    private function makeFile($path, $Settings, $destinationPath, $sizes = [])
    {
        $Image = new \Imagick($path);
        if ($Image->getImageHeight() > 2000 || $Image->getImageWidth() > 2000) {
            $Image->thumbnailImage(2000, 2000, true);
        }
        $Image->setInterlaceScheme(\Imagick::INTERLACE_PLANE);
        $realHeight = $Image->getImageHeight();
        $realWidth = $Image->getImageWidth();
        $res = $Image->writeImage($destinationPath);
        $Image->destroy();
        if ($res) {
            $Image = new \Imagick($path);
        } else {
            return false;
        }

        if (!$sizes) {
            $sizes = [
                'big' => 378,
                'middle' => 188,
                'small' => 88,
                'min' => 48,
            ];
        }

        $res = $this->makeThumbnails($Settings, $destinationPath, $sizes, $Image);
        if (!$res) {
            unlink($path);
        }
        return ['width' => $realWidth, 'height' => $realHeight];
    }

    private function makeThumbnails($Settings, $originalFile, $sizes, \Imagick $Image = null)
    {
        $marginTopPart = is_numeric($Settings->marginTopPart) ? $Settings->marginTopPart : 0;
        $marginLeftPart = is_numeric($Settings->marginLeftPart) ? $Settings->marginLeftPart : 0;
        $sizePart = is_numeric($Settings->sizePart) ? $Settings->sizePart : 1;

        $destinationDir = realpath(dirname($originalFile));
        if (!$destinationDir) {
            return false;
        }
        $fileName = pathinfo($originalFile, PATHINFO_FILENAME);

        $CropImage = !is_null($Image) ? $Image : new \Imagick($originalFile);

        $smallSide = min($CropImage->getImageHeight(), $CropImage->getImageWidth());
        $marginTop = $marginTopPart * $smallSide;
        $marginLeft = $marginLeftPart * $smallSide;
        $size = $sizePart * $smallSide;

        $croped = $CropImage->cropImage($size, $size, $marginLeft, $marginTop);
        if (!$croped) {
            return false;
        }

        $saved = [];
        foreach ($sizes as $postfix => $size) {
            $Thumbnail = clone $CropImage;
            $Thumbnail->thumbnailImage($size, $size);
            $filePath = BaseFileHelper::normalizePath($destinationDir . '/' . $fileName . '_' . $postfix . '.jpg');
            $res = $Thumbnail->writeImage($filePath);
            if ($res) {
                $saved[] = $filePath;
            } else {
                foreach ($saved as $path) {
                    unlink($path);
                }
                return false;
            }
            $Thumbnail->destroy();
        }

        $CropImage->destroy();
        return true;
    }

    public function updateGoodPhotos($Photos, $photoUpdateSettings)
    {
        foreach ($Photos as $Photo) {
            foreach ($photoUpdateSettings as $setting) {
                if ($Photo->id == $setting->existId) {
                    $marginTopPart = is_numeric($setting->marginTopPart) ? $setting->marginTopPart : 0;
                    $marginLeftPart = is_numeric($setting->marginLeftPart) ? $setting->marginLeftPart : 0;
                    $sizePart = is_numeric($setting->sizePart) ? $setting->sizePart : 1;
                    $photoUpdated = false;

                    if ($Photo->margin_top_part != round($marginTopPart, 3)) {
                        $Photo->margin_top_part = round($marginTopPart, 3);
                        $photoUpdated = true;
                    }
                    if ($Photo->margin_left_part != round($marginLeftPart, 3)) {
                        $Photo->margin_left_part = round($marginLeftPart, 3);
                        $photoUpdated = true;
                    }
                    if ($Photo->size_part != round($sizePart, 3)) {
                        $Photo->size_part = round($sizePart, 3);
                        $photoUpdated = true;
                    }

                    if ($photoUpdated) {
                        $originalFile = Photo::getPhotoDir($Photo->good_id) . '/' . $Photo->file_name . '.jpg';
                        $originalFile = BaseFileHelper::normalizePath($originalFile);
                        $sizes = [
                            'big' => 378,
                            'middle' => 188,
                            'small' => 88,
                            'min' => 48,
                        ];
                        $res = $this->makeThumbnails($setting, $originalFile, $sizes);
                        if ($res) {
                            $Photo->save();
                        }
                    }
                }
            }
        }
    }

    public function uploadTempUserPhoto($dirName, $file)
    {
        $answer = [
            'success' => null,
            'error' => null,
        ];

        $photoDir = self::getTempUserPhotoDir($dirName);
        $res = BaseFileHelper::createDirectory($photoDir);
        if (!$res) {
            $answer['error'] = 'Произошла ошибка на сервере';
            return $answer;
        }

        $fileList = BaseFileHelper::findFiles($photoDir);
        if (count($fileList) >= 1) {
            $answer['error'] = 'Можно загружать только одну фотографию';
            return $answer;
        }

        $resultExtension = '';
        $legalExtensions = ['jpg', 'png', 'gif'];
        $mime = BaseFileHelper::getMimeType($file->tempName);
        $fileExtensions = BaseFileHelper::getExtensionsByMimeType($mime);
        foreach ($legalExtensions as $extension) {
            if (in_array($extension, $fileExtensions)) {
                $resultExtension = $extension;
                break;
            }
        }
        if (!$resultExtension) {
            $answer['error'] = 'Разрешено загружать файлы типов jpg, png или gif';
            return $answer;
        }

        $fileName = $photoDir . '/avatar.' . $resultExtension;
        // Проверка, развернута ли фотография
        if ($resultExtension == 'jpg') {
            $exif = exif_read_data($file->tempName);
        } else {
            $exif = null;
        }
        if (!empty($exif['Orientation']) && $exif['Orientation'] != 1) {
            // фотография имеет метку положения угла
            $this->rotateAndSave($file->tempName, $fileName);
        } else {
            $file->saveAs($fileName);
        }
        $answer['success'] = 'ok';
        return $answer;
    }

    public function deleteTempUserPhoto($dirName)
    {
        $dir = self::getTempUserPhotoDir($dirName);
        $files = BaseFileHelper::findFiles($dir);
        $result = false;
        foreach ($files as $file) {
            $pathInfo = pathinfo($file);
            if ($pathInfo['filename'] == 'avatar') {
                $result = unlink($dir . '/' . $pathInfo['filename'] . '.' . $pathInfo['extension']);
                if (!$result) {
                    $answer['error'] = 'Произошла ошибка на сервере';
                    return $answer;
                }

                $answer['success'] = 'ok';
                return $answer;
            }
        }

        if (!$result) {
            $answer['error'] = 'Файл не найден на сервере';
            return $answer;
        }
    }

    public function uploadUserPhoto($photoUploadSettings, $destinationDir)
    {
        BaseFileHelper::createDirectory($destinationDir);
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        $tmpPhotoDir = self::getTempUserPhotoDir($session->id);
        $fileList = BaseFileHelper::findFiles($tmpPhotoDir);
        $files = [];
        foreach ($fileList as $path) {
            $path = BaseFileHelper::normalizePath($path);
            $file = pathinfo($path);
            $files[$file['filename']] = $path;
        }

        $sizes = [
            'big' => 218,
            'middle' => 98,
        ];

        $uploadedFiles = [];
        foreach ($photoUploadSettings as $Object) {
            // вызываем сохранение и обработку файла только для закаченных файлов
            if (isset($files['avatar']) && file_exists($files['avatar'])) {
                $destinationPath = BaseFileHelper::normalizePath($destinationDir . '/avatar.jpg');
                $res = $this->makeFile($files['avatar'], $Object, $destinationPath, $sizes);
                if ($res) {
                    $uploadedFiles[] = [
                        'fileNumber' => 'avatar',
                        'main' => boolval($Object->mainPhoto),
                        'marginTopPart' => $Object->marginTopPart,
                        'marginLeftPart' => $Object->marginLeftPart,
                        'sizePart' => $Object->sizePart,
                        'realWidth' => $res['width'],
                        'realHeight' => $res['height'],
                    ];
                }
            }
        }
        return $uploadedFiles;
    }

    public function updateUserPhotos($Photos, $photoUpdateSettings)
    {
        foreach ($Photos as $Photo) {
            foreach ($photoUpdateSettings as $setting) {
                if ($Photo->id == $setting->existId) {
                    $marginTopPart = is_numeric($setting->marginTopPart) ? $setting->marginTopPart : 0;
                    $marginLeftPart = is_numeric($setting->marginLeftPart) ? $setting->marginLeftPart : 0;
                    $sizePart = is_numeric($setting->sizePart) ? $setting->sizePart : 1;
                    $photoUpdated = false;

                    if ($Photo->margin_top_part != round($marginTopPart, 3)) {
                        $Photo->margin_top_part = round($marginTopPart, 3);
                        $photoUpdated = true;
                    }
                    if ($Photo->margin_left_part != round($marginLeftPart, 3)) {
                        $Photo->margin_left_part = round($marginLeftPart, 3);
                        $photoUpdated = true;
                    }
                    if ($Photo->size_part != round($sizePart, 3)) {
                        $Photo->size_part = round($sizePart, 3);
                        $photoUpdated = true;
                    }

                    if ($photoUpdated) {
                        $originalFile = UserPhoto::getPhotoDir($Photo->user_id) . '/avatar.jpg';
                        $originalFile = BaseFileHelper::normalizePath($originalFile);
                        $sizes = [
                            'big' => 218,
                            'middle' => 98,
                        ];
                        $res = $this->makeThumbnails($setting, $originalFile, $sizes);
                        if ($res) {
                            $Photo->save();
                        }
                    }
                }
            }
        }
    }
}