<?php
/**
 * Контроллер, реализующий работу с заказом
 */

namespace app\modules\users\controllers;

use Yii;
use yii\web\Response;
use app\controllers\CommonController;
use app\modules\users\models\Basket;
use app\modules\good\models\Good;

class OrderController extends CommonController
{
    /**
     * Добавление товара в корзину
     * Вызывается по Ajax
     */
    public function actionToBasket()
    {
        if (Yii::$app->request->isAjax && !Yii::$app->user->isGuest) {
            $response = [
                'status' => 'error',
                'count' => 0
            ];
            // проверка корректности значений
            $postParams = Yii::$app->request->post();
            if (!isset($postParams['goodId'])) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return false;
            }
            $goodId = (int)$postParams['goodId'];

            $User = Yii::$app->user->identity;
            if ($goodId <= 0 || !($User instanceof \app\modules\users\models\User)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return false;
            }

            if ($User->isUnconfirmed()) {
                Yii::$app->response->setStatusCode(403);
                $response['errorText'] = 'Вы не можете добавлять товары в корзину, пока не подтвердите регистрацию.';
                $this->sendJsonAnswer($response);
                return false;
            }

            // добавление в корзину
            $added = $this->addGoodToBasket($goodId, $User->id);
            if (!$added) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return false;
            }

            // товар успешно добавлен, формирование ответа
            $goodsInBasket = Basket::find()->where(['user_id' => $User->id])->count();
            $response = [
                'status' => 'ok',
                'count' => $goodsInBasket
            ];
            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Добавляет товар в корзину и сразу перенаправляет пользователя в корзину
     *
     * @return mixed
     */
    public function actionOrderGoods()
    {
        if (!Yii::$app->user->isGuest) {
            $response = [
                'status' => 'error'
            ];

            $goodId = Yii::$app->request->post('goodId');
            if (!$goodId) {
                $goodId = Yii::$app->request->get('goodId');
            }
            $goodId = (int)$goodId;

            $User = Yii::$app->user->identity;
            if ($goodId <= 0 || !($User instanceof \app\modules\users\models\User)) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return false;
                } else {
                    $SiteController = Yii::$app->createController('site');
                    return $SiteController[0]->runAction('400');
                }
            }

            if ($User->isUnconfirmed()) {
                Yii::$app->response->setStatusCode(403);
                $response['errorText'] = 'Вы не можете заказывать товары, пока не подтвердите регистрацию.';
                $this->sendJsonAnswer($response);
                return false;
            }

            // добавление в корзину
            $added = $this->addGoodToBasket($goodId, $User->id);
            if (!$added) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return false;
                } else {
                    $SiteController = Yii::$app->createController('site');
                    return $SiteController[0]->runAction('400');
                }
            }

            // товар успешно добавлен, формирование ответа
            if (Yii::$app->request->isAjax) {
                $response = [
                    'status' => 'ok'
                ];
                $this->sendJsonAnswer($response);
            } else {
                Yii::$app->response->redirect('/cabinet/basket/');
            }
        } else {
            // неавторизованным доступ запрещен
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->setStatusCode(403);
                $this->sendJsonAnswer(['status' => 'error']);
                return false;
            } else {
                $SiteController = Yii::$app->createController('site');
                return $SiteController[0]->runAction('403');
            }
        }
    }

    /**
     * Удаляет товар из корзины
     *
     * @return bool
     */
    public function actionDeleteGoodFromBasket()
    {
        if (Yii::$app->request->isAjax && !Yii::$app->user->isGuest) {
            $response = [
                'status' => 'error'
            ];

            $userId = Yii::$app->user->identity->id;
            $goodId = (int)Yii::$app->request->post('goodId');

            if ($userId <= 0 || $goodId <= 0) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return false;
            }

            Basket::deleteAll(['user_id' => $userId, 'good_id' => $goodId]);

            $response['status'] = 'ok';
            $this->sendJsonAnswer($response);
        } else {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }
    }

    /**
     * Удаляет заказ из корзины целиком
     *
     * @return bool
     */
    public function actionDeleteOrderFromBasket()
    {
        if (Yii::$app->request->isAjax && !Yii::$app->user->isGuest) {
            $response = [
                'status' => 'error'
            ];

            $userId = Yii::$app->user->identity->id;
            $studioId = (int)Yii::$app->request->post('studioId');

            if ($userId <= 0 || $studioId <= 0) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return false;
            }

            $dbName = Yii::$app->params['tailor_db'];
            $sql = "
                DELETE
                    `b`.*
                FROM
                    `" . $dbName . "`.`basket` AS `b`
                INNER JOIN
                    `" . $dbName . "`.`goods` AS `g`
                ON
                    `g`.`id` = `b`.`good_id`
                WHERE
                    `g`.`studio_id` = " . $studioId . "
                    ";
            Yii::$app->db->createCommand($sql)->execute();
            $response['status'] = 'ok';
            $this->sendJsonAnswer($response);
        } else {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }
    }

    /**
     * Добавляет товар в корзину
     *
     * @param int $goodId
     * @param int $userId
     * @return bool
     * @throws \yii\db\Exception
     */
    private function addGoodToBasket($goodId, $userId)
    {
        $Good = Good::findOne(['id' => $goodId]);
        if (!$Good) {
            return false;
        }
        if ($Good->studio->user_id == $userId) {
            // хозяин товара пытается сам у себя сделать заказ
            return false;
        }

        // добавление товара в корзину
        $dbName = Yii::$app->params['tailor_db'];
        $sql = "
                INSERT IGNORE INTO
                    `" . $dbName . "`.`basket` (
                        `user_id`,
                        `good_id`
                    )
                VALUES (
                    " . $userId . ",
                    " . $goodId . "
                )";
        Yii::$app->db->createCommand($sql)->execute();
        return true;
    }

    /**
     * Выставляет нужные заголовки и конвертирует данные в JSON
     *
     * @param array $data Массив с данными для отправки
     */
    protected function sendJsonAnswer(array $data)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->content = json_encode($data);
        Yii::$app->response->send();
    }
}