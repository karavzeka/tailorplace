<?php

namespace app\modules\users\controllers;

use Yii;
use app\controllers\CommonController;
use app\modules\users\models\User;
use app\modules\users\models\Photo as UserPhoto;
use app\modules\users\models\PassRecoveryForm;
use app\models\PasswordRecovery;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;

class DefaultController extends CommonController
{
    /**
	 * Создаём новую запись.
	 * В случае успеха, пользователь будет перенаправлен на view метод.
	 */
	public function actionSignup()
	{
        // авторизованному пользователю запрещен доступ на эту страницу
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

		$User = new User(['scenario' => 'signup']);
		// Добавляем обработчик события который отправляет сообщение с клюом активации на e-mail адрес что был указан при регистрации.
		if ($this->module->activeAfterRegistration === false) {
			$User->on(User::EVENT_AFTER_INSERT, [$this->module, 'onSignup']);
		}

		if ($User->load(Yii::$app->request->post())) {
            $saveResult = false;
            if ($User->validate()) {
                // Загрузка фотографии (если передана)
                $Settings = json_decode($User->photoSettings);
                $photoUploadSettings = $Settings->upload;

                $Transaction = Yii::$app->db->beginTransaction();
                try {
                    $User->save(false); // валидацию сделали выше, если ее повторить, то вылезет ошибка капчи, потому что при повторной валидации почему-то новый код генерируется

                    if (is_array($photoUploadSettings) && count($photoUploadSettings) > 0) {
                        $photoDir = UserPhoto::getPhotoDir($User->id);
                        $uploadedFiles = Yii::$app->fileHelper->uploadUserPhoto($photoUploadSettings, $photoDir);

                        if ($uploadedFiles) {
                            foreach ($uploadedFiles as $file) {
                                $Photo = new UserPhoto();
                                $Photo->margin_top_part = $file['marginTopPart'];
                                $Photo->margin_left_part = $file['marginLeftPart'];
                                $Photo->size_part = $file['sizePart'];
                                $Photo->real_width = $file['realWidth'];
                                $Photo->real_height = $file['realHeight'];
                                $User->link('photo', $Photo);
                            }
                        }
                    }

                    // Назначение пользователю роли
                    $User->assignRole('unconfirmed');

                    // Создание и отправка секретного ключа для подтверждения регистрации
                    $RegistrationActivation = new \app\models\RegistrationActivation();
                    $secretKey = Yii::$app->security->generateRandomString(10);
                    // Yii некорректно обрабатывает URL'ы по маске, если в URL'е в значениях GET параметров присутствует "-"
                    $secretKey = str_replace('-', '_', $secretKey);
                    $RegistrationActivation->user_id = $User->id;
                    $RegistrationActivation->secret_key = $secretKey;
                    $RegistrationActivation->save();

                    Yii::$app->mailer->compose('@app/mail/needConfirm', ['secretKey' => $secretKey])
                        ->setTo($User->email)
                        ->setSubject('Tailor place. Подтверждение регистрации')
                        ->send();

                    $Transaction->commit();
                    $saveResult = true;
                } catch (\Exception $e) {
                    // удаление загруженных фотографий
                    $photoDir = UserPhoto::getPhotoDir($User->id);
                    BaseFileHelper::removeDirectory($photoDir);

                    // откат изменений в базе
                    $Transaction->rollBack();

                    $User->addError(null, 'Произошла непредвиденная ошибка. Просьба сообщить о ней администрации сайта.');
                    $saveResult = false;
                }
            }

            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }
            Yii::$app->fileHelper->clearTempUserPhotoDir($session->id);

            $User->password = '';
            $User->repassword = '';

            if ($saveResult) {
                // Авторизуем сразу пользователя.
                Yii::$app->getUser()->login($User, User::LOGIN_DURATION);

                return $this->render('//other/simpleMessage', [
                    'message' => '
                        <p>Спасибо, что зарегистрировались на сайте Tailor place.
                        Проверьте свой почтовый ящик, вам было отправлено письмо, содержащее ссылку,
                        по которой надо пройти для завершения регистрации.</p>
                        <p>Пока вы не пройдете по отправленой вам ссылке, некоторые сервисы будут недоступны.</p>
                        <p><a href="' . Url::home() . '">На главную</a> или в <a href="' . Url::toRoute('/cabinet/') . '">Личный кабинет</a></p>'
                ]);
            }
		}
        if (Yii::$app->request->isGet) {
            Yii::$app->fileHelper->clearTempUserPhotoDir($session->id);
        }

		$User->captcha = '';

		// Рендерим представление.
		return $this->render('signup', [
			'User' => $User
		]);
	}

    /**
     * Страница подтверждения регистрации, на которую пользователь должен попасть, перейдя по ссылке из письма
     *
     * @return string
     */
    public function actionRegistrationConfirm()
    {
        $this->addBreadcrumbsItem('Завершение регистрации');

        $secretKey = Yii::$app->request->get('hash');

        if (empty($secretKey)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        }

        $RegistrationActivation = \app\models\RegistrationActivation::find()
            ->where([
                'secret_key' => $secretKey
            ])->one();
        if (!($RegistrationActivation instanceof \app\models\RegistrationActivation)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        }

        if ($RegistrationActivation->applied_time != '0000-00-00 00:00:00' && strtotime($RegistrationActivation->applied_time) > strtotime('-1 week')) {
            // если код использовался менее недели назад, то сообщим, что код уже использовался
            return $this->render('//other/simpleMessage', [
                'message' => '<p>По этой ссылке уже было проведено подтверждение регистации.</p>'
            ]);
        } elseif ($RegistrationActivation->applied_time != '0000-00-00 00:00:00') {
            // если код использовался более недели назад
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        } else {
            // если код еще не использовался
            $User = User::findOne($RegistrationActivation->user_id);
            if (!($User instanceof User)) {
                $SiteController = Yii::$app->createController('site');
                return $SiteController[0]->runAction('400');
            }
            $Auth = Yii::$app->authManager;
            $userRoles = $Auth->getRolesByUser($User->id);
            if (empty($userRoles) || isset($userRoles['unconfirmed'])) {
                $User->assignRole('user');
                $RegistrationActivation->applied_time = date('Y-m-d H:i:s');
                $RegistrationActivation->save();
            }

            // Авторизация пользователя
            Yii::$app->getUser()->login($User, User::LOGIN_DURATION);

            return $this->render('//other/simpleMessage', [
                'message' => '<p>Ваша учетная запись успешно активирована, теперь вам доступны все основные функции сайта.</p>'
            ]);
        }

    }

	/**
	 * Авторизуем пользователя.
	 */
	public function actionLogin()
	{
		// В случае если пользователь не гость, то мы перенаправляем его на главную страницу. В противном случае он бы увидел 403-ю ошибку.
		if (!Yii::$app->user->isGuest) {
			$this->goHome();
		}
		$model = new User(['scenario' => 'login']);
		if ($model->load(Yii::$app->request->post())) {
			$User = User::findByUsername($model->login);
			if ($User && $User->validatePassword($model->password)) {
                // Проверка, не забанен ли пользователь
                if ($User->additional_status == User::STATUS_BANNED) {
                    $model->addError('', 'Данный пользователь забанен. За подробностями обратитесь в службу поддержки.');
                    $model->password = '';
                    return $this->render('login', [
                        'model' => $model
                    ]);
                }

				Yii::$app->user->login($User, User::LOGIN_DURATION);

                // логирование действия авторизации
                $IpLog = new \app\models\IpLogCommon();
                $IpLog->action = $IpLog::ACTION_AUTHORIZATION;
                $IpLog->user_id = $this->id;
                $IpLog->ip = Yii::$app->numberHelper->ipToUnsignedNumber(Yii::$app->request->getUserIP());
                $IpLog->save();

				return $this->goBack();
			} else {
				$model->addError('', 'Неверно введен логин или пароль');
                $model->password = '';
				return $this->render('login', [
					'model' => $model
				]);
			}
		}
		// Рендерим представление.
		return $this->render('login', [
			'model' => $model
		]);
	}

	public function actionLogout()
    {
		Yii::$app->getUser()->logout();
		return $this->goHome();
	}

    /**
     * Страница восстановления пароля
     *
     * @return string
     */
	public function actionPassRecovery()
    {
        if (!Yii::$app->user->isGuest) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        $this->addBreadcrumbsItem('Восстановление пароля');
        $PassRecoveryForm = new PassRecoveryForm();

        $postData = Yii::$app->request->post();
        if (Yii::$app->request->isPost && $PassRecoveryForm->load($postData)) {
            if ($PassRecoveryForm->validate() === true) {
                $isEmail = (filter_var($PassRecoveryForm->email_login, FILTER_VALIDATE_EMAIL) !== false);

                $UserQuery = User::find();
                if ($isEmail) {
                    $UserQuery->where(['email' => $PassRecoveryForm->email_login]);
                } else {
                    $UserQuery->where(['login' => $PassRecoveryForm->email_login]);
                }
                $User = $UserQuery->one();
                if ($User instanceof User) {
                    // Пользователь найден, занесем его заявку в список, и отправим письмо
                    $secretKey = Yii::$app->security->generateRandomString(8);
                    $PasswordRecoveryRequest = new \app\models\PasswordRecovery();
                    $PasswordRecoveryRequest->user_id = $User->id;
                    $PasswordRecoveryRequest->secret_key = $secretKey;

                    if ($PasswordRecoveryRequest->save()) {
                        Yii::$app->mailer->compose('@app/mail/passRecover', ['secretKey' => $secretKey])
                            ->setTo($User->email)
                            ->setSubject('Восстановление пароля')
                            ->send();
                    }
                    $this->redirect('/pass-recovery/success/');
                }
            }
        }

		return $this->render('passRecovery', [
            'PassRecoveryForm' => $PassRecoveryForm
        ]);
	}

    /**
     * Страница, сообщающая, что форма восстановления пароля успешно отправлена
     */
    public function actionPassRecoverySuccess()
    {
        $this->addBreadcrumbsItem('Восстановление пароля');

        if (!Yii::$app->user->isGuest) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        return $this->render('passRecoverySuccess');
    }

    public function actionNewPassword()
    {
        $this->addBreadcrumbsItem('Новый пароль');

        if (!Yii::$app->user->isGuest) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        $secretKey = Yii::$app->request->get('hash');

        if (empty($secretKey)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        }

        $RecoveryRequest = PasswordRecovery::find()
            ->where([
                'secret_key' => $secretKey
            ])->one();
        if (!($RecoveryRequest instanceof PasswordRecovery)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        }

        $weekAgo = strtotime('-1 week');
        $showAppliedAlert = false;
        if ($RecoveryRequest->applied == 1 && strtotime($RecoveryRequest->applied_time) < $weekAgo) {
            // код применялся менее недели назад, говорим, что код уже использовался
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        } elseif ($RecoveryRequest->applied == 1) {
            // код применялся менее недели назад, отображаем ошибку
            $showAppliedAlert = true;
        } else {
            // код еще не использовался, восстанавливаем пароль пользователя
            $RecoveryRequest->applied = 1;
            $RecoveryRequest->applied_time = date('Y-m-d H:i:s');
            $RecoveryRequest->save();

            $User = User::findOne($RecoveryRequest->user_id);
            $User->setScenario('recovery');
            $newPassword = Yii::$app->security->generateRandomString(8);

            $User->password = Yii::$app->getSecurity()->generatePasswordHash($newPassword);
            $User->auth_key = Yii::$app->getSecurity()->generateRandomKey();
            $User->save();

            Yii::$app->mailer->compose('@app/mail/newPassword', ['newPassword' => $newPassword])
                ->setTo($User->email)
                ->setSubject('Ваш новый пароль')
                ->send();
        }

        return $this->render('newPassword', [
            'showAppliedAlert' => $showAppliedAlert
        ]);
    }

    /**
     * Страница простого пользователя, доступная всем
     */
    public function actionSimpleUser()
    {
        $userId = (int)Yii::$app->request->get('userId');
        if (!$userId) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
        $User = User::findOne($userId);
        if (!$User) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        // у пользователя есть студия, так что постоянный редирект на студию
        if ($User->hasStudio()) {
            $this->redirect(Url::toRoute('/studio/' . $User->studio->id), 301);
        }

        $this->addBreadcrumbsItem('Страница пользователя');

        return $this->render('simpleUser', [
            'User' => $User
        ]);
    }
}
