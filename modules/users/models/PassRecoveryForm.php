<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;

class PassRecoveryForm extends Model
{
    /**
     * @var string Email, на который будет выслана инструкция по восстановлению пароля
     */
    public $email_login;

    /**
     * Капча
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Email - логин
            ['email_login', 'required'],
            // Капча
            ['captcha', 'required'],
            ['captcha', 'captcha', 'captchaAction' => 'common/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email_login' => 'E-mail / Логин',
            'captcha' => 'Код-подтверждение',
        ];
    }
}