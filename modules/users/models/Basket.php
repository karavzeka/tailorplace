<?php

namespace app\modules\users\models;

use Yii;
use yii\db\ActiveRecord;

class Basket extends ActiveRecord
{
    /**
     * Получение названия таблицы
     */
    public static function tableName()
    {
        return '{{basket}}';
    }
}