<?php

namespace app\modules\users\models;

use Yii;
use yii\base\UserException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\rbac\Role;
use app\models\City;
use app\models\Country;
use app\models\Order;
use app\models\Message;
use app\modules\studio\models\Studio;
use app\modules\users\models\query\UserQuery;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * Статус пользователя 'ok', по сути означает, что дополнительных статусов нет
     */
    const STATUS_OK = 'ok';

    /**
     * Статус забаненого пользователя
     */
    const STATUS_BANNED = 'banned';

    /**
     * Время жизни куки авторизации (2592000 = 1 мес.)
     */
    const LOGIN_DURATION = 2592000;

    /**
     * Переменная используется для сбора пользовательской информации, но не сохраняется в базу.
     * @var string $repassword Повторный пароль
     */
    public $repassword;

    /**
     * Переменная используется для сбора пользовательской информации, но не сохраняется в базу.
     * @var boolean $acceptAgreement Согласился ли пользователь с соглашением при регистрации
     */
    public $acceptAgreement;

    /**
     * Капча
     * @var string
     */
    public $captcha;

    /**
     * @var string Настройки для обработки загруженных фотографий (JSON)
     */
    public $photoSettings;

    /**
     * @var array Репутация пользователя ['buyer' => xxx, 'seller' => xxx]
     */
    private $reputation = [];

    /**
     * @var bool Флаг, что пользователь не прошел подтверждение регистрации
     */
    private $unconfirmed;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{users}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * Выбор пользователя по [[id]]
     * @param integer $id
     * @return \yii\db\ActiveQuery
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Выбор пользователя по [[username]]
     * @param string $login
     * @return \yii\db\ActiveQuery
     */
    public static function findByUsername($login)
    {
        return static::find()->where('{{login}} = :login', [':login' => $login])->one();
    }

    /**
     * @return integer ID пользователя.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Получить фотографии пользователя
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasMany(Photo::className(), ['user_id' => 'id']);
    }

    /**
     * Получить заказы пользователя
     *
     * @param string $type ['', 'sell', 'buy'] Тип возвращаемых заказов (все, в качестве продавца, в качестве покупателя)
     * @return \yii\db\ActiveQuery
     */
    public function getOrder($type = '')
    {
        if ($type == 'sell') {
            // заказы в качестве продавца
            return $this->hasMany(Order::className(), ['seller_id' => 'id']);
        } elseif ($type == 'buy') {
            // заказы в качестве покупателя
            return $this->hasMany(Order::className(), ['buyer_id' => 'id']);
        } else {
            // все заказы
            return Order::find()
                ->where(
                    '{{seller_id}} = :seller_id OR {{buyer_id}} = :buyer_id',
                    [
                        ':seller_id' => $this->id,
                        ':buyer_id' => $this->id
                    ]
                );
        }
    }

    /**
     * Сколько покупок совершил пользователь
     *
     * @return int|string
     */
    public function countOfPurchases()
    {
        $count = $this->getOrder('buy')->andWhere(['status_id' => 2])->count();
        return $count;
    }

    /**
     * Получить товары в корзине
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBasket()
    {
        return $this->hasMany(Basket::className(), ['user_id' => 'id']);
    }

    public function getCountOfMarkOrders()
    {
        if ($this->id) {
            $countOfMarkOrders = Order::find()
                ->where('
                (
                    `seller_id` = :seller_id AND
                    `mark_for_seller` = 1
                ) OR
                (
                    `buyer_id` = :buyer_id AND
                    `mark_for_buyer` = 1
                )', [
                    ':seller_id' => $this->id,
                    ':buyer_id' => $this->id
                ])
                ->count();
        } else {
            $countOfMarkOrders = 0;
        }
        return $countOfMarkOrders;
    }

    /**
     * Возвращает репутацию пользователя как продавца (в процентах)
     * Для рассчета запрашиваются оценки покупателей
     *
     * @return float|int
     */
    public function sellerReputation()
    {
        if (!isset($this->reputation['seller'])) {
            $calcEvaluationRow = $this->getOrder('sell')
                ->select(['COUNT(*) AS {{count}}', 'SUM({{buyer_evaluation}}) AS {{sum}}'])
                ->andWhere(['status_id' => 2])
                ->asArray()
                ->one();
            $reputation = ($calcEvaluationRow['count'] > 0) ? $calcEvaluationRow['sum'] / $calcEvaluationRow['count'] * 100 : 0;
            $this->reputation['seller'] = $reputation;
        }
        return $this->reputation['seller'];
    }

    /**
     * Возвращает репутацию пользователя как покупателя (в процентах)
     * Для рассчета запрашиваются оценки продавцов
     *
     * @return float|int
     */
    public function buyerReputation()
    {
        if (!isset($this->reputation['buyer'])) {
            $calcEvaluationRow = $this->getOrder('buy')
                ->select(['COUNT(*) AS {{count}}', 'SUM({{seller_evaluation}}) AS {{sum}}'])
                ->andWhere(['status_id' => 2])
                ->asArray()
                ->one();
            $reputation = ($calcEvaluationRow['count'] > 0) ? $calcEvaluationRow['sum'] / $calcEvaluationRow['count'] * 100 : 0;
            $this->reputation['buyer'] = $reputation;
        }
        return $this->reputation['buyer'];
    }

    /**
     * @return string Ключ авторизации пользователя.
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Валидация ключа авторизации.
     * @param string $authKey
     * @return boolean
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Валидация пароля.
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * Валидация старого пароля.
     * В правилах модели метод назначен как валидатор атрибута модели.
     * @return boolean
     */
    public function validateOldPassword()
    {
        if (!$this->validatePassword($this->oldpassword)) {
            $this->addError('oldpassword', Yii::t('users', 'Неверный текущий пароль.'));
        }
    }

    /**
     * Возвращает true, если пользователь не подтвердил регистрацию
     *
     * @return bool
     */
    public function isUnconfirmed()
    {
        if (is_null($this->unconfirmed)) {
            /** @var \app\modules\rbac\AuthManager $Auth */
            $Auth = Yii::$app->authManager;
            $userRoles = $Auth->getRolesByUser($this->id);
            $this->unconfirmed = isset($userRoles[$Auth::ROLE_UNCONFIRMED]);
        }

        return $this->unconfirmed;
    }

    /**
     * Получить страну пользователя
     * @return Country
     */
    public function getCountry()
    {
        if ($this->country_id) {
            return $this->hasOne(Country::className(), ['id' => 'country_id']);
        } else {
            return new Country();
        }
    }

    /**
     * Получить город пользователя
     * @return City
     */
    public function getCity()
    {
        if ($this->city_id) {
            return $this->hasOne(City::className(), ['id' => 'city_id']);
        } else {
            return new City();
        }
    }

    /**
     * Получить студию пользователя
     * @return City
     */
    public function getStudio()
    {
        return $this->hasOne(Studio::className(), ['user_id' => 'id']);
    }

    /**
     * Есть ли у пользователя студия?
     * @return boolean
     */
    public function hasStudio()
    {
        return !empty($this->studio);
    }

    public function getNewMessages()
    {
        return $this->hasMany(Message::className(), ['to_id' => 'id'])->where(['is_read' => 0])->count();
    }

    /**
     * Получить класс для отображения иконки пользователя
     * @return string
     */
    public function getUserIconClass()
    {
        if ($this->hasStudio()) {
            $iconClass = $this->studio->type;
        } else {
            $iconClass = 'person-green';
        }

        return $iconClass;
    }

    /**
     * Если у пользователя есть студия, то возвращается название студии, иначе логин пользователя
     *
     * @return string
     */
    public function getName()
    {
        if ($this->hasStudio()) {
            $name = $this->studio->name;
        } else {
            $name = $this->login;
        }

        return $name;
    }

    /**
     * Если у пользователя есть студия, то возвращается ссылка на страницу студии, иначе на страницу пользователя
     *
     * @return string
     */
    public function getLinkToPage()
    {
        return $this->hasStudio() ? Url::toRoute('/studio/' . $this->studio->id) : Url::toRoute('/simple-user/' . $this->id);
    }

    /**
     * Назначить роль пользователю
     * @param  string $role Название роли
     * @return null
     * @throws UserException
     */
    public function assignRole($role)
    {
        $auth = Yii::$app->authManager;
        $Role = $auth->getRole($role);
        if (!($Role instanceof Role)) {
            throw new UserException("Роль \"" . $role . "\" не зарегистрирована.");
        }
        if ($this->getId()) {
            $auth->revokeAll($this->getId());
            $auth->assign($Role, $this->getId());
        } else {
            throw new UserException("Попытка назначить права пользователю, не зарегистрированному в базе.");
        }
    }

    public function validateAcceptAgreement($attribute, $params)
    {
        if ($this->$attribute == '0') {
            $this->addError($attribute, "Вы не подтвердили согласие.");
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            // Frontend scenarios
            'signup' => ['login', 'email', 'password', 'auth_key', 'repassword', 'acceptAgreement', 'captcha', 'photoSettings'],
            'login' => ['login', 'password'],
            'logout' => [],
            'recovery' => ['password', 'auth_key'],
            'userRedact' => ['password', 'address'],
            'ban' => ['additional_status'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //TODO поправить в каких сценариях применять фильтры
        return [
            // Логин [[login]]
            ['login', 'filter', 'filter' => 'trim', 'on' => ['signup','login']],
            ['login', 'required', 'on' => ['signup', 'login']],
            ['login', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/', 'on' => ['signup', 'login']],
            ['login', 'string', 'min' => 3, 'max' => 15, 'on' => ['signup', 'login']],
            ['login', 'unique', 'on' => ['signup']],

            // E-mail [[email]]
            ['email', 'filter', 'filter' => 'trim', 'on' => ['signup', 'recovery', 'admin-update', 'admin-create']],
            ['email', 'required', 'on' => ['signup', 'recovery', 'admin-update', 'admin-create']],
            ['email', 'email', 'on' => ['signup', 'recovery', 'admin-update', 'admin-create']],
            ['email', 'string', 'max' => 100, 'on' => ['signup', 'recovery', 'admin-update', 'admin-create']],
            ['email', 'unique', 'on' => ['signup', 'admin-update', 'admin-create']],
            ['email', 'exist', 'on' => ['resend', 'recovery'], 'message' => 'Пользователь с указанным адресом не существует.'],

            // Пароль [[password]]
            ['password', 'required', 'on' => ['signup', 'login', 'userRedact']],
            ['password', 'string', 'min' => 6, 'on' => ['signup', 'login', 'userRedact']],
            ['password', 'compare', 'compareAttribute' => 'oldpassword', 'operator' => '!==', 'on' => 'password'],

            // Подтверждение пароля [[repassword]]
            ['repassword', 'required', 'on' => ['signup', 'password', 'admin-create']],
            ['repassword', 'string', 'min' => 6, 'on' => ['signup', 'password', 'admin-update', 'admin-create']],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'on' => ['signup', 'password', 'admin-create']],

            // Подтверждение соглашения [[acceptAgreement]]
            ['acceptAgreement', 'boolean', 'on' => ['signup']],
            ['acceptAgreement', 'validateAcceptAgreement', 'on' => ['signup']],

            // Капча
            ['captcha', 'required'],
            ['captcha', 'captcha', 'captchaAction' => 'common/captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'acceptAgreement' => 'Я прочитал(а) и принимаю пользовательское соглашение',
            'password' => 'Пароль',
            'repassword' => 'Повторите пароль',
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
            'email' => 'E-mail',
            'captcha' => 'Код-подтверждение',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Проверяем если это новая запись
            if ($this->isNewRecord) {
                // Хэшируем пароль
                if (!empty($this->password)) {
                    $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                }
                // Устанавливаем время создания пользователя
                $this->create_time = date('Y-m-d H:i:s');
                // Генерируем уникальный ключ
                $this->auth_key = Yii::$app->getSecurity()->generateRandomKey();
                $this->create_time = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    public function afterSave()
    {
        // логирование действие регистрации пользователя
        if ($this->scenario == 'signup') {
            $IpLog = new \app\models\IpLogCommon();
            $IpLog->action = $IpLog::ACTION_REGISTRATION;
            $IpLog->user_id = $this->id;
            $IpLog->ip = Yii::$app->numberHelper->ipToUnsignedNumber(Yii::$app->request->getUserIP());
            $IpLog->save();
        }
    }
}
