<?php

namespace app\modules\users\models;

use Yii;
use yii\db\ActiveRecord;

class Photo extends ActiveRecord
{
    private static $photoDir = null;
    private static $userPhotoDir = '/photos/user';
    private static $otherPhotoDir = '/photos/other';

    /**
     * @var array Настройки сдвига для миниатюр при редактировании
     */
    private $calculatedMargins = [];

    /**
     * Получение названия таблицы
     */
    public static function tableName()
    {
        return '{{user_photos}}';
    }

    /**
     * Получить путь к папке с фотографиями товара, к которому относится данная фотография
     * @return bool|string
     */
    public function getParentDir()
    {
        return $this->getPhotoDir($this->good_id);
    }

    /**
     * Получить путь к папке с фотографиями
     * @param string $dirName Имя директории
     * @return bool|string
     */
    public static function getPhotoDir($dirName)
    {
        if (!empty($dirName)) {
            return self::getWebRootDir() . '/' . $dirName;
        } else {
            return false;
        }
    }

    /**
     * Получить путь к директории со всеми фотографиями товаров
     * @return string
     */
    public static function getWebRootDir()
    {
        if (self::$photoDir === null) {
            self::$photoDir = Yii::getAlias('@webroot') . self::$userPhotoDir;
        }
        return self::$photoDir;
    }

    /**
     * Получить ссылку на изображение
     * @param  string $postfix Постфикс, указывающий, какого размера изображение необходимо получить
     * @return string Ссылка на изображение относительно домена
     */
    public function getSrc($postfix = '')
    {
        return self::$userPhotoDir . '/' . $this->user_id . '/avatar' . $postfix . '.jpg';
    }

    public static function getNoPhotoSrc($postfix = '')
    {
        return self::$otherPhotoDir . '/no_profile_image' . $postfix . '.jpg';
    }

    /**
     * Получить сдвиги для атрибута style
     * @param int $sideSize размер иниатюры
     * @return string
     */
    public function getMargins($sideSize)
    {
        $this->calculateMargins($sideSize);
        return 'margin-top: ' . -$this->calculatedMargins[$sideSize]['marginTop'] . 'px; margin-left: ' . -$this->calculatedMargins[$sideSize]['marginLeft'] . 'px';
    }

    public function getSizeAttr($sideSize)
    {
        $this->calculateMargins($sideSize);
        return $this->calculatedMargins[$sideSize]['sizeAttr'];
    }

    /**
     * Рассчитывает размер и сдвиги для миниатюрки при редактировании
     * @param int $sideSize размер иниатюры
     */
    private function calculateMargins($sideSize)
    {
        if (!isset($this->calculatedMargins[$sideSize])) {
            $size = $sideSize / $this->size_part;
            $marginTop = $size * $this->margin_top_part;
            $marginLeft = $size * $this->margin_left_part;

            $roundSize = round($size);
            if ($roundSize > $size) {
                $marginTop = ceil($marginTop);
                $marginLeft = ceil($marginLeft);
            } else {
                $marginTop = floor($marginTop);
                $marginLeft = floor($marginLeft);
            }

            if ($this->real_width > $this->real_height) {
                $this->calculatedMargins[$sideSize]['sizeAttr'] = [
                    'side' => 'height',
                    'size' => $roundSize
                ];
            } else {
                $this->calculatedMargins[$sideSize]['sizeAttr'] = [
                    'side' => 'width',
                    'size' => $roundSize
                ];
            }

            $this->calculatedMargins[$sideSize]['marginTop'] = $marginTop;
            $this->calculatedMargins[$sideSize]['marginLeft'] = $marginLeft;
        }
    }
}
