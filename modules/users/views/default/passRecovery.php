<?php
/**
 * Страница восстановления пароля
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\users\models\PassRecoveryForm $PassRecoveryForm
 */

use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use app\assets\AppAsset;

$this->title = 'Восстановление пароля';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Восстановление пароля'
]);
$this->registerCssFile('@web/css/passRecovery.css', [
    'depends' => [AppAsset::className()]
]);

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
?>
<main>
    <div class="panel big-panel">
        <?php
        $form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => $fieldTemplate
            ]
        ]);
        ?>
        <div class="panel-header">
            <div class="icon-circle key"><i></i></div><h2>Восстановление пароля</h2>
        </div>
        <div class="panel-body">
            <p class="instruction italic">
                Вам на почту будет высланы инструкции по
                восстановлению пароля. Просто следуйте им.
            </p>
            <ul>
                <li>
                    <?= $form->errorSummary($PassRecoveryForm, [
                        'header' => ''
                    ])
                    ?>
                </li>
                <li>
                    <?= $form->field($PassRecoveryForm, 'email_login')->textInput(['placeholder' => 'E-mail или Логин от аккаунта']) ?>
                </li>
                <li class="captcha-line">
                    <?= $form->field($PassRecoveryForm, 'captcha', [
                        'template' => '<div class="left-col">{label}</div><div class="center-col">{input}</div>'
                    ])->widget(Captcha::className(), [
                        'captchaAction' => '/captcha',
                        'template' => '{input}{image}'
                    ])
                    ?>
                    <div class="right-col"><span>←</span><span>Введите код<br>с картинки</span></div>
                </li>
            </ul>
        </div>
        <div class="panel-footer">
            <div class="button yellow" id="repair-action">
                <div class="low-layer"></div>
                <button type="submit">Отправить запрос</button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</main>