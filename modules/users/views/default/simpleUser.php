<?php
/**
 * Страница простого пользователя, не имеющего студии
 * @var yii\web\View $this
 * @var app\modules\users\models\User $User
 */

use app\assets\AppAsset;
use app\modules\users\models\Photo AS UserPhoto;
use app\widgets\panelWidgets\UserRating;
use yii\helpers\Html;

$this->registerCssFile('@web/css/simpleUser.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Страница пользователя';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница пользователя'
]);
?>
<main>
    <aside class="float left">
        <div class="person-info">
            <img src="<?= count($User->photo) > 0 ? $User->photo[0]->getSrc('_big') : UserPhoto::getNoPhotoSrc() ?>">
            <div>
                <div>
                    <div class="icon-circle person-green"><i></i></div>
                    <p class="name darker">
                        <?= Html::encode($User->login) ?>
                    </p>
                </div>
            </div>
        </div>
    </aside>

    <div class="float right">
        <h2>Информация о пользователе</h2>
        <div class="widget-wrapper">
            <h4>Репутация</h4>
            <?= UserRating::widget(['User' => $User, 'showCountOfPurchases' => true]) ?>
        </div>
    </div>
    <div class="clear"></div>
</main>