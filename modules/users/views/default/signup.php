<?php
/**
 * Страница регистрации нового пользователя.
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\users\models\User $User
 */

use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;
use app\modules\assets\users\SignupAsset;

SignupAsset::register($this);

$this->title = 'Tailor place - Регистрация';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница регистрации на Tailor place'
]);

$this->params['breadcrumbs'][] = $this->title;
$this->params['page-id'] = 'signup';

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
?>

<main>
    <div class="panel big-panel">
        <?php
        $form = ActiveForm::begin([
            'id' => 'signup-form',
            'fieldConfig' => [
                'template' => $fieldTemplate
            ]
        ]);
        ?>
        <div class="panel-header">
            <div class="icon-circle person-green"><i></i></div><h2>Регистрации</h2>
        </div>
        <div class="panel-body">
            <ul>
                <li>
                    <?= $form->errorSummary($User, [
                        'header' => ''
                    ])
                    ?>
                </li>
                <li>
                    <?= $form->field($User, 'login') ?>
                    <div class="right-col">От 3 до 15 символов</div>
                </li>
                <li>
                    <?= $form->field($User, 'password')->passwordInput() ?>
                    <div class="right-col">Минимум 6 символов</div>
                </li>
                <li>
                    <?= $form->field($User, 'repassword')->passwordInput() ?>
                </li>
                <li>
                    <?= $form->field($User, 'email') ?>
                </li>
                <li class="captcha-line">
                    <?= $form->field($User, 'captcha', [
                        'template' => '<div class="left-col">{label}</div><div class="center-col">{input}</div>'
                    ])->widget(Captcha::className(), [
                        'captchaAction' => '/captcha',
                        'template' => '{input}{image}'
                    ])
                    ?>
                    <div class="right-col"><span>←</span><span>Введите код<br>с картинки</span></div>
                </li>
                <li class="photos">
                    <div class="left-col">Фотография</div><div class="center-col">
                        <div id="photos-to-upload"></div>
                        <div id="dropbox">
                            <p>Нажмите для выбора файла<br>или<br>перетащиет файл сюда</p>
                        </div>
                        <input type="file" id="photo-input" name="photos" accept="image/*">
                        <?= Html::activeHiddenInput($User, 'photoSettings', [
                            'id' => 'photo-properties'
                        ])
                        ?>
                        <div class="help-block"></div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="panel-footer">
            <?= $form->field($User, 'acceptAgreement', [
                'template' => '{input} {label}',
                'options' => ['id' => 'acceptAgreement']
            ])->checkbox([], false)
            ?>
            <div class="button yellow" id="signup-action">
                <div class="low-layer"></div>
                <button type="submit">Зарегистрироваться</button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
    <div class="dashed scissor-left"></div>
    <div id="agreement">
        <h4>Пользовательское соглашение</h4>
        <div id="agreement-text">
            <?= $this->renderFile('@app/views/parts/registration_agreement.php') ?>
        </div>
        <div class="how-be-this">
            <span class="darker"><span>↓</span> <a href="#agreement" class="dashed-underline">Развернуть</a></span>
        </div>
    </div>
    <div class="dashed scissor-right"></div>
</main>

<div class="modal fade" id="photo-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="darker">Выберите область фотографии</h4>
            </div>
            <div class="modal-body">
                <div id="photo-place">
                    <div id="photo-wrapper">
                        <div class="photo-shadow"></div>
                        <div id="select-place"></div>
                        <div class="marker left-top"></div>
                        <div class="marker right-top"></div>
                        <div class="marker right-bottom"></div>
                        <div class="marker left-bottom"></div>
                    </div>
                </div>
                <div id="preview-wrapper">
                    <figure>
                        <div id="preview"></div>
                        <figcaption class="darker">Превью</figcaption>
                    </figure>
                </div>
                <div class="clear"></div>
            </div>
            <div class="modal-footer">
                <div class="button" id="photo-settings-cancel">
                    <div class="low-layer"></div>
                    <button type="submit" data-dismiss="modal">Отмена</button>
                </div>
                <div class="button yellow" id="photo-settings-save">
                    <div class="low-layer"></div>
                    <button type="button">Применить</button>
                </div>
            </div>
        </div>
    </div>
</div>