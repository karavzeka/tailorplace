<?php
/**
 * Страница, сообщающая пользователю что ему выслан новый пароль
 * @var yii\web\View $this
 * @var bool $showAppliedAlert
 */

$this->title = 'Восстановление пароля';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Восстановление пароля'
]);
?>
<main>
    <div class="panel big-panel">
        <div class="panel-body">
            <?php if ($showAppliedAlert === true): ?>
            <p>По данной ссылке уже осуществлялось восстановление пароля ранее.</p>
            <?php else: ?>
            <p>Проверьте свою почту, вам был выслан новый пароль.</p>
            <p>Если письмо не пришло, проверьте, не попало ли оно в спам.</p>
            <?php endif ?>
        </div>
    </div>
</main>