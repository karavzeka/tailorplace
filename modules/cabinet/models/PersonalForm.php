<?php

namespace app\modules\cabinet\models;

use Yii;
use yii\base\Model;

/**
 * Class PersonalForm
 * Модель формы редактирования данных пользователя.
 * @package app\modules\cabinet\models
 */
class PersonalForm extends Model
{
    /**
     * @var string Старый пароль
     */
    public $oldPassword;

    /**
     * @var string Новый пароль
     */
    public $newPassword;

    /**
     * @var string Повторенный новый пароль
     */
    public $repeatNewPassword;

    /**
     * @var string Адрес доставки пользователя
     */
    public $deliveryAddress;

    /**
     * @var string Поле с настройками фотографий
     */
    public $photoSettings;

    public function rules()
    {
        return [
            ['oldPassword', 'validateOldPassword', 'skipOnEmpty' => false],
            ['newPassword', 'validateNewPassword', 'skipOnEmpty' => false],
            ['repeatNewPassword', 'validateRepeatNewPassword', 'skipOnEmpty' => false],

            ['deliveryAddress', 'string'],
            ['deliveryAddress', 'filter', 'filter' => 'trim'],
            ['deliveryAddress', 'filter', 'filter' => 'strip_tags'],

            ['photoSettings', 'string'],
        ];
    }

    public function validateOldPassword($attribute, $params)
    {
        if (empty($this->$attribute) && (!empty($this->newPassword) || !empty($this->repeatNewPassword))) {
            $this->addError($attribute, 'Для смены пароля введите страый пароль');
        } elseif (!empty($this->$attribute) || !empty($this->newPassword)) {
            $userPass = Yii::$app->user->identity->password;
            $compare = Yii::$app->getSecurity()->validatePassword($this->$attribute, $userPass);
            if (!$compare) {
                $this->addError($attribute, 'Старый пароль введен неверно.');
            }
        }
    }

    public function validateNewPassword($attribute, $params)
    {
        if ((!empty($this->$attribute) || !empty($this->oldPassword)) && mb_strlen($this->$attribute, 'UTF-8') < 6) {
            $this->addError($attribute, 'Новый пароль должен содержать минимум 6 символов');
        }
    }

    public function validateRepeatNewPassword($attribute, $params)
    {
        if (!empty($this->newPassword) && $this->newPassword !== $this->$attribute) {
            $this->addError($attribute, 'Вы неверно повторили новый пароль.');
        }
    }

    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
            'repeatNewPassword' => 'Повторите пароль',
            'deliveryAddress' => 'Адрес доставки',
        ];
    }
}