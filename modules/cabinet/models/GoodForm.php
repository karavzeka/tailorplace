<?php

namespace app\modules\cabinet\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use app\modules\studio\models\Studio;
use app\modules\good\models\Good;
use app\models\GoodCategory;

/**
 * Class GoodForm
 * Модель формы создания товара.
 * @package app\modules\cabinet\models
 */
class GoodForm extends Model
{
	/**
	 * @var string $name Название товара
	 */
	public $name;

	/**
	 * @var string Цена товара
	 */
	public $price;

	/**
	 * @var array Массив категорий товара
	 */
	public $categories = [];

	/**
	 * @var integer Кол-во товара (для ателье "готовых эксемпляров")
	 */
	public $quantity = 0;

	/**
	 * @var string Описание товара
	 */
	public $description;

	/**
	 * @var string Время изготовления
	 */
	public $craft_time;

	/**
	 * @var string Настройки для обработки загруженных фотографий (JSON)
	 */
	public $photoSettings;

	/**
	 * @var Studio Модель студии, к которой принадлежит товар
	 */
	private $Studio;

	public function __construct(Studio $Studio, Good $Good = null, $config = [])
    {
        $this->Studio = $Studio;
        if (!$this->Studio->type) {
        	throw new InvalidParamException('Не указан тип студии');
        }
        if ($Good) {
            $this->name = $Good->name;
            $this->price = Yii::$app->numberHelper->numberToPrice($Good->price);
            foreach ($Good->category as $Category) {
                $this->categories[] = $Category->id;
            }
            $this->quantity = $Good->quantity;
            $this->description = $Good->description;
            $this->craft_time = $Good->craft_time;
        }
        parent::__construct($config = []);
    }

    public function isStoreGood()
    {
    	return $this->Studio->type === 'store';
    }

    public function getCategoryList()
    {
        return GoodCategory::find()
            ->orWhere(['type' => $this->Studio->type])
            ->orWhere(['type' => 'all'])
            ->orderBy('sort_position')
            ->asArray()
            ->all();
    }

    public function rules()
    {
    	return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],

            ['price', 'required'],
            ['price', 'validatePrice'],

            ['categories', 'validateCategories'],

            ['quantity', 'integer'],

            ['description', 'string'],
            ['description', 'filter', 'filter' => 'strip_tags'],

			['craft_time', 'filter', 'filter' => 'trim'],
    		['craft_time', 'string', 'max' => 30],

    		['photoSettings', 'string'],
    	];
    }

    public function validatePrice($attribute, $params)
    {
        $value = Yii::$app->numberHelper->priceToDBVal($this->$attribute);
        if ($value <= 0) {
            $this->addError($attribute, 'Цена должна быть числом > 0');
        }
    }

    public function validateCategories($attribute, $params)
    {
        if (!is_array($this->$attribute) || count($this->$attribute) < 1) {
            $this->addError($attribute, 'Ошибка выбора категории, сообщите администрации');
            return;
        }
        $CategoryList = $this->categoryList;
        $categoryListIds = ArrayHelper::index($CategoryList, 'id');
        foreach ($this->$attribute as $key => $value) {
        	if ((int)$value <= 0) {
        		$this->addError($attribute, 'Ошибка выбора категории, сообщите администрации');
        		break;
        	}
            if (!isset($categoryListIds[$value])) {
                $this->addError($attribute, 'Ошибка. Выбранная категория не соответствует ' . $this->Studio->type == 'atelier' ? 'ателье' : 'магазину');
                break;
            }
        }
    }

    public function attributeLabels()
    {
        $labels = [
            'name' => 'Название',
            'price' => 'Цена',
            'description' => 'Описание',
            'craft_time' => 'Время изготовления',
        ];
        $labels['quantity'] = $this->isStoreGood() ? 'Количество товара' : 'Готовых экземпляров';
        return $labels;
    }
}