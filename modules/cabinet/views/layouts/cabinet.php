<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 * @var $User app\modules\users\models\User
 */

use app\modules\assets\cabinet\CommonAsset;
use app\modules\users\models\Photo as UserPhoto;
use yii\helpers\Html;

$this->title = empty($this->title) ? 'Tailor place - личный кабинет' : $this->title;

mb_internal_encoding('UTF-8'); // иначе mb_substr не верно работает

CommonAsset::register($this);

$User = Yii::$app->user->identity;

$this->beginContent('@app/views/layouts/main.php');
?>
<aside id="personal-menu" class="float left">
    <div class="person-info">
        <img src="<?= count($User->photo) > 0 ? $User->photo[0]->getSrc('_big') : UserPhoto::getNoPhotoSrc() ?>">
        <div>
            <div>
                <?php
                if ($User->hasStudio()) {
                    $iconType = $titleToTranslate = $User->studio->type;
                } else {
                    $iconType = 'person-green';
                    $titleToTranslate = 'user';
                }
                echo Html::tag('div', '<i></i>', [
                    'class' => 'icon-circle ' . $iconType,
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'right',
                    'title' => mb_strtoupper(mb_substr(Yii::t('app', $titleToTranslate), 0, 1)) . mb_substr(Yii::t('app', $titleToTranslate), 1)
                ]);
                echo '<p class="name darker">' . Html::encode($User->getName()) . '</p>';
                ?>
            </div>
            <?= $User->hasStudio() ? '<p class="city darker">' . $User->studio->city->name_ru . '</p>' : '' ?>
        </div>
    </div>
    <div class="menu">
        <?php if ($User->hasStudio()): ?>
            <p class="big-red-medium">Магазин</p>
            <menu>
                <li class="icon-line"><i class="icon store"></i><a href="/cabinet/studio/"><?= $User->studio->type == 'atelier' ? 'Мое ателье' : 'Мой магазин' ?></a></li>
                <li class="icon-line"><i class="icon statistic"></i><a href="/cabinet/statistic/">Статистика</a></li>
                <li class="icon-line"><i class="icon order"></i><a href="/cabinet/orders/">Заказы</a> <?= Yii::$app->user->identity->getCountOfMarkOrders() > 0 ? '<span class="red bolder">(' . Yii::$app->user->identity->getCountOfMarkOrders() . ')</span>' : '' ?></li>
                <li class="icon-line"><i class="icon archive"></i><a href="/cabinet/orders/archive/">Архив заказов</a></li>
                <li class="icon-line"><i class="icon arrow"></i><a href="/cabinet/services/">Дополнительные услуги</a></li>
            </menu>
            <p class="big-red-medium">Аккаунт</p>
            <menu>
                <li class="icon-line"><i class="icon person"></i><a href="/cabinet/personal/">Персональные данные</a></li>
                <li class="icon-line"><i class="icon basket"></i><a href="/cabinet/basket/">Корзина</a></li>
                <li class="icon-line"><i class="icon message"></i><a href="/cabinet/messages/">Сообщения</a></li>
            </menu>
        <?php else: ?>
            <p class="big-red-medium">Аккаунт</p>
            <menu>
                <li class="icon-line"><i class="icon person"></i><a href="/cabinet/personal/">Персональные данные</a></li>
                <li class="icon-line"><i class="icon basket"></i><a href="/cabinet/basket/">Корзина</a></li>
                <li class="icon-line"><i class="icon message"></i><a href="/cabinet/messages/">Сообщения</a></li>
                <li class="icon-line"><i class="icon order"></i><a href="/cabinet/orders/">Заказы</a> <?= Yii::$app->user->identity->getCountOfMarkOrders() > 0 ? '<span class="red bolder">(' . Yii::$app->user->identity->getCountOfMarkOrders() . ')</span>' : '' ?></li>
                <li class="icon-line"><i class="icon archive"></i><a href="/cabinet/orders/archive/">Архив заказов</a></li>
                <li class="icon-line"><i class="icon statistic"></i><a href="/cabinet/statistic/">Статистика</a></li>
            </menu>
        <?php endif ?>
    </div>

</aside>
<main class="float right">
    <?= $content ?>
</main>
<?php $this->endContent(); ?>
