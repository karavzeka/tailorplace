<?php
/**
 * Страница продвижения студии
 * @var yii\web\View $this
 * @var string $studioType
 * @var int|null $lastPromotion
 */

use app\assets\AppAsset;

$this->title = 'Продвижение ' . $studioType;

$this->registerCssFile('@web/css/cabinet-service-studioPromotion.css', [
    'depends' => [AppAsset::className()]
]);
?>

<h2 class="icon-line"><span>Продвижение <?= $studioType ?></span> <div class="icon-circle up"><i></i></div></h2>
<div id="services-wrapper">
    <div id="main-col">
        <?php
        $successMsg = Yii::$app->session->getFlash('studioPromotionSuccess');
        if ($successMsg) {
            echo '<p class="alert success">' . $successMsg . '</p>';
        }
        $errorMsg = Yii::$app->session->getFlash('studioPromotionError');
        if ($errorMsg) {
            echo '<p class="alert warning">' . $errorMsg . '</p>';
        }
        ?>
        <p>
            При активации данной услуги ваше ателье/магазин (далее по тексту применяется термин <span class="italic">студия</span>) окажется на главной странице сайта.
            Оно будет добавлено в карусель студий и пробудет там в течении определенного времени.
        </p>
        <p>
            <strong>В данный момент услуга продвижения работает в бесплатном пробном режиме.</strong>
        </p>
        <p>
            Текущие условия следующие:
        </p>
        <ul>
            <li>
                На главной странице в ротации крутится список из 10-ти последних продвинутых студий.
                При продвижении студии она попадает в этот список и будет показываться на главной странице.
                При этом студия, пробывшая в списке дольше всего вытесняется и больше не показывается на главной странице.
            </li>
            <li>
                Продвинуть свою студию вы можете один раз в течение месяца.
            </li>
        </ul>
        <p>
            Администрация сайта имеет право влюбое время на свое усмотрение менять условие сервиса.
        </p>
        <?php if ($lastPromotion >= strtotime('-1 month')): ?>
        <p class="italic">
            Вы продвигали свою студию менее месяца назад (<?= date('d.m.Y H:i', $lastPromotion) ?>).
        </p>
        <?php else: ?>
        <form method="post">
            <input type="hidden" name="doPromotion" value="1">
            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>">
            <div id="promote-button" class="button yellow">
                <div class="low-layer"></div>
                <button type="submit" class="icon-line"><i class="icon arrow-white"></i><span>Продвинуть</span></button>
            </div>
        </form>
        <?php endif ?>
    </div>
    <div id="right-col">
    </div>
</div>