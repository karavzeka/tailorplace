<?php
/**
 * Страница продвижения товаров
 * @var yii\web\View $this
 * @var array $goods
 * @var app\modules\good\models\Photo[] $GoodPhotos
 * @var int|null $lastPromotion
 */

use app\assets\AppAsset;
use app\modules\good\models\Photo;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Продвижение товаров';

$this->registerCssFile('@web/css/cabinet-service-goodsPromotion.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/cabinet-service-goodsPromotion.js', [
    'depends' => [AppAsset::className()]
]);

$deprecatePromotion = $lastPromotion >= strtotime('-1 month');
?>

<h2 class="icon-line"><span>Продвижение товаров</span> <div class="icon-circle good-green"><i></i></div></h2>
<div id="services-wrapper">
    <div id="main-col">
        <?php
        $successMsg = Yii::$app->session->getFlash('goodPromotionSuccess');
        if ($successMsg) {
            echo '<p class="alert success">' . $successMsg . '</p>';
        }
        $errorMsg = Yii::$app->session->getFlash('goodPromotionWarning');
        if ($errorMsg) {
            echo '<p class="alert warning">' . $errorMsg . '</p>';
        }
        $errorMsg = Yii::$app->session->getFlash('goodPromotionError');
        if ($errorMsg) {
            echo '<p class="alert error">' . $errorMsg . '</p>';
        }
        ?>
        <p>
            При активации данной услуги, продвинутый товар будет отображаться в каруселе товаров,
            которая показывается на основных страницах сайта. Таким образом товар привлечет внимание многих
            пользвателей, которые могут стать вашими клиентами.
        </p>
        <p>
            <strong>В данный момент услуга продвижения работает в бесплатном пробном режиме.</strong>
        </p>
        <p>
            Текущие условия следующие:
        </p>
        <ul>
            <li>
                В каруселе товаров в ротации крутится список из 15-ти последних продвинутых товаров.
                При продвижении товара он попадает в этот список и тоже начинает отображаться в каруселе товаров.
                При этом товар, пробывший в списке дольше всего вытесняется и больше не показывается.
            </li>
            <li>
                Вы можете продвинуть только один товар в течение месяца.
                По прошествии месяца с последнего продвижения товара вы еще раз сможете продвинуть
                любой из своих товаров и т.д.
            </li>
        </ul>
        <p>
            Администрация сайта имеет право влюбое время на свое усмотрение менять условие сервиса.
        </p>
        <?php if ($deprecatePromotion): ?>
            <p class="italic">
                Вы продвигали товар менее месяца назад (<?= date('d.m.Y H:i', $lastPromotion) ?>).
            </p>
        <?php endif ?>
        <div class="sloping-line"></div>
        <?php
        if (count($goods) > 0) {
            echo '<form method="post" id="promotion-form">';
            echo '<input type="hidden" name="_csrf" value="' . Yii::$app->request->getCsrfToken() . '">';
            echo '<input id="good-id-input" type="hidden" name="goodId" value="">';
            echo '<ul class="good-list">';
            foreach ($goods as $good) {
                echo '<li>';
                    echo '<div class="photo-name-wrapper">';
                        $imgHtml = Html::img(isset($GoodPhotos[$good['id']]) ? $GoodPhotos[$good['id']]->getSrc('_min') : Photo::getNoPhotoSrc('_min'));
                        echo Html::a($imgHtml, Url::toRoute('/goods/' . $good['id']), [
                            'class' => 'img-link'
                        ]);
                        echo Html::a(Html::encode($good['name']), Url::toRoute('/goods/' . $good['id']), [
                            'class' => 'good-name'
                        ]);
                    echo '</div>'; // <div class="photo-name-wrapper">
                    echo '<div class="last-promotion">';
                        echo '<span>' . ($good['last_promotion'] !== null ? ('Продвигался<br>' . date('d.m.Y H:i', strtotime($good['last_promotion']))) : 'Ни разу не продвигался') . '</span>';
                    echo '</div>';
                    echo '<div class="button promote-button ' . ($deprecatePromotion ? 'disable' : 'yellow') . '">';
                        echo '<div class="low-layer"></div>';
                        echo '<button type="submit" class="icon-line" data-good-id="' . $good['id'] . '"' . ($deprecatePromotion ? 'disabled' : '') . '><i class="icon ' . ($deprecatePromotion ? 'arrow' : 'arrow-white') . '"></i><span>Продвинуть</span></button>';
                    echo '</div>'; // <div class="button yellow promote-button">
                echo '</li>';
            }
            echo '</ul>';
            echo '</form>';
        } else {
            echo '<p id="no-goods">У вас еще нет товаров</p>';
        }
        ?>
    </div>
    <div id="right-col">
    </div>
</div>