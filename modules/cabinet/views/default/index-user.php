<?php
/**
 * Главная страница кабинета пользователя, не имеющего студии
 * @var yii\web\View $this
 * @var app\modules\users\models\User $User
 */

use yii\helpers\Url;
use app\widgets\panelWidgets\PersonalIndexUserOrders;
use app\widgets\panelWidgets\PersonalIndexUserBasket;
use app\widgets\panelWidgets\UserRating;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/cabinet-index.css', [
    'depends' => [AppAsset::className()]
]);
?>
<div class="float">
    <h4><a href="<?= Url::toRoute('/cabinet/orders/') ?>" class="big-red-medium">Заказы</a></h4>
    <?= PersonalIndexUserOrders::widget() ?>
    <h4><a href="<?= Url::toRoute('/cabinet/basket/') ?>" class="big-red-medium">Корзина</a></h4>
    <?= PersonalIndexUserBasket::widget() ?>
</div>
<div class="float">
    <div class="inline-vidget small-widget">
        <h4><span class="big-red-medium">Репутация</span></h4>
        <?= UserRating::widget(['User' => $User]) ?>
    </div>
    <div class="open-owner-studio">
        <p>
            Откройте свое собственное ателье или магазин
        </p>
        <div id="owner-studio-button" class="button">
            <div class="low-layer"></div>
            <a href="<?= Url::toRoute('studio/what-create/') ?>" data-type="button">
                <span>Открыть</span>
            </a>
        </div>
    </div>
</div>