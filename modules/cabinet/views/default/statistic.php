<?php
/**
 * Страница статистики.
 * @var yii\web\View $this
 * @var array $statisticArray
 */

use app\assets\AppAsset;

$this->registerCssFile('@web/css/statistic.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Статистика';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница пользователя'
]);
?>
<h2>Статистика</h2>
<ul class="statistic-list">
    <?php if ($statisticArray['hasStudio']): ?>
        <li>
            <div class="left-col">Репутация продавца</div>
            <div class="right-col"><?= $statisticArray['reputationAsSeller'] ?>%</div>
        </li>
        <li>
            <div class="left-col">Кол-во продаж</div>
            <div class="right-col"><?= $statisticArray['sellsCount'] ?></div>
        </li>
        <li>
            <div class="left-col">Репутация покупателя</div>
            <div class="right-col"><?= $statisticArray['reputationAsBuyer'] ?>%</div>
        </li>
        <li>
            <div class="left-col">Кол-во покупок</div>
            <div class="right-col"><?= $statisticArray['purchasesCount'] ?></div>
        </li>
        <li>
            <div class="left-col">Кол-во товаров</div>
            <div class="right-col"><?= $statisticArray['countOfGoods'] ?></div>
        </li>
        <li>
            <div class="left-col">Лимит товаров</div>
            <div class="right-col"><?= $statisticArray['goodsLimit'] ?></div>
        </li>
        <li>
            <div class="left-col">Требуется продаж для увеличения лимита</div>
            <div class="right-col"><?= $statisticArray['threshold'] ?></div>
        </li>
    <?php else: ?>
        <li>
            <div class="left-col">Репутация покупателя</div>
            <div class="right-col"><?= $statisticArray['reputationAsBuyer'] ?>%</div>
        </li>
        <li>
            <div class="left-col">Кол-во покупок</div>
            <div class="right-col"><?= $statisticArray['purchasesCount'] ?></div>
        </li>
    <?php endif ?>
</ul>