<?php
/**
 * Страница заказа.
 * @var yii\web\View $this
 * @var app\models\Order $Order
 * @var app\modules\users\models\User $Partner
 * @var bool $iAmBuyer
 * @var app\models\OrderStatus[] $StatusesList
 * @var array $orderGoods
 * @var app\modules\good\models\Photo[] $Photos
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\modules\good\models\Photo;
use app\models\Order;

$this->registerCssFile('@web/css/cabinet-order.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/cabinet-order.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Заказ №' . $Order->id;
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница заказа'
]);
?>

<h2>Заказ №<?= $Order->id ?></h2>

<div class="order-header">
    <div class="icon-circle <?= $Partner->getUserIconClass() ?>"><i></i></div><h3 class="order-studio">
        <?php
        if ($iAmBuyer) {
            echo 'Заказ у ' . Html::a(Html::encode($Partner->studio->name), Url::toRoute('/studio/' . $Partner->studio->id), ['class' => 'big-red-medium']);
        } else {
            if ($Partner->hasStudio()) {
                echo 'Заказчик ' . Html::a(Html::encode($Partner->studio->name), Url::toRoute('/studio/' . $Partner->studio->id), ['class' => 'big-red-medium']);
            } else {
                echo 'Заказчик ' . Html::a(Html::encode($Partner->login), Url::toRoute('/simple-user/' . $Partner->id), ['class' => 'big-red-medium']);
            }
        }
        ?>
    </h3>
    <div class="status-box" data-order-id="<?= $Order->id ?>">
        <div class="order-status <?= $Order->status->css_class ?>"><span><?= $Order->status->name_ru ?></span></div>
        <?php
        if ($Order->isCompleted()) {
            if (!$iAmBuyer && is_null($Order->seller_evaluation)) {
                echo '<p class="evaluate-please">Пожалуйста оцените действия заказчика.</p>';
                echo '<div class="icon-line evaluate-button"><i class="icon star"></i><span class="dashed-underline">Оценить заказчика</span></div>';
            } else {
                // отображение оценок на страницах архива заказов
                if (!$iAmBuyer && !is_null($Order->buyer_evaluation)) {
                    echo '<div class="icon-line evaluation-line"><span class="italic">Вас оценили:</span> ' . Order::paintEvalStars($Order->buyer_evaluation) . '</div>';
                } elseif ($iAmBuyer && !is_null($Order->seller_evaluation)) {
                    echo '<div class="icon-line evaluation-line"><span class="italic">Вас оценили:</span> ' . Order::paintEvalStars($Order->seller_evaluation) . '</div>';
                } else {
                    echo '<div class="evaluation-line"><span class="italic">Вас еще не оценили</span></div>';
                }

                if (!$iAmBuyer && !is_null($Order->seller_evaluation)) {
                    echo '<div class="icon-line evaluation-line"><span class="italic size">Ваша оценка:</span> ' . Order::paintEvalStars($Order->seller_evaluation, 'small') . '</div>';
                } elseif ($iAmBuyer && !is_null($Order->buyer_evaluation)) {
                    echo '<div class="icon-line evaluation-line"><span class="italic size">Ваша оценка:</span> ' . Order::paintEvalStars($Order->buyer_evaluation, 'small') . '</div>';
                }

            }
        } else {
            $possibleStatuses = $Order->getPossibleStatuses();
            if (is_array($possibleStatuses)) {
                $hasNotCancelStatus = false;
                $cancelStatus = 0;
                foreach ($possibleStatuses as $statusId) {
                    if ($statusId == 3 || $statusId == 4) {
                        $cancelStatus = $statusId;
                    } else {
                        $hasNotCancelStatus = true;
                    }
                }
                if ($hasNotCancelStatus) {
                    echo '<div class="icon-line edit-status-button"><i class="icon edit"></i><span class="dashed-underline">Изменить статус</span>';
                    echo Html::beginTag('ul', ['class' => 'status-list']);
                    echo '<li class="italic">Перевести заказ в:</li>';
                    foreach ($possibleStatuses as $statusId) {
                        if ($statusId == 3 || $statusId == 4) {
                            continue;
                        }
                        $needEvaluation = false;
                        $statusRules = Yii::$app->params['orderStatusRules'][$statusId];
                        if (isset($statusRules['needEvaluation']) && $statusRules['needEvaluation']) {
                            $needEvaluation = true;
                        }

                        $Status = $StatusesList[$statusId];
                        echo Html::beginTag('li', [
                            'class' => 'new-status',
                            'data-status-id' => $statusId,
                            'data-need-evaluation' => $needEvaluation ? 1 : 0
                        ]);
                        echo '<div class="order-status ' . $Status->css_class . '"><span>' . $Status->name_ru . '</span></div>';
                        echo Html::endTag('li');
                    }
                    echo Html::endTag('ul');
                    echo '</div>'; // <div class="icon-line">
                }

                // можно ли отозвать или отменить заказ
                if ($cancelStatus > 0) {
                    $cancelText = $cancelStatus == 3 ? 'Отклонить заказ' : 'Отозвать заказ';
                    echo '<div class="icon-line">';
                    echo '<i class="icon delete-red"></i>';
                    echo Html::tag('span', $cancelText, [
                        'class' => 'dashed-underline cancel',
                        'data-status-id' => $cancelStatus,
                    ]);
                    echo '</div>'; // <div class="icon-line">
                }
            }
        }
        ?>
    </div>
</div>
<div class="panel width-panel order-content">
    <div class="panel-body">
        <ul class="good-list">
            <?php
            $orderSummPrice = 0;
            foreach ($orderGoods as $goodId => $good) {
                echo '<li>';
                    if (!is_null($good['exists_good']) && (!$Order->isFinished() || mb_strtolower($good['good_name']) == mb_strtolower($good['current_good_name']))) {
                        // товар из заказа будет отображен со ссылкой если:
                        // товар существует в базе И (заказа еще не завершен ИЛИ название товара на момент заказа равно текущему названию)
                        $imgHtml = Html::img(isset($Photos[$goodId]) ? $Photos[$goodId]->getSrc('_min') : Photo::getNoPhotoSrc('_min'), [
                            'width' => 48,
                            'height' => 48
                        ]);
                        echo Html::a($imgHtml, Url::toRoute('/goods/' . $good['good_id']), [
                            'class' => 'img-link'
                        ]);
                        echo '<div class="name-box">';
                            echo Html::a(Html::encode($good['good_name']), Url::toRoute('/goods/' . $good['good_id']), [
                                'class' => 'size16'
                            ]);
                        echo '</div>'; // <div class="name-box">
                    } else {
                        // этот товар был удален из таблицы товаров либо его название сменилось
                        // TODO сделать отображение имени НЕ ссылкой и без фото
                        echo '<div class="without-photo">';
                            echo Html::img(Photo::getNoPhotoSrc('_min'), [
                                'width' => 48,
                                'height' => 48
                            ]);
                        echo '</div>';
                        echo '<div class="name-box">';
                            echo Html::tag('span', Html::encode($good['good_name']), [
                                'class' => 'size16'
                            ]);
                        echo '</div>'; // <div class="name-box">
                    }

                    echo '<div class="price-box">';
                        echo '<span class="price-normal">' . Yii::$app->numberHelper->numberToPrice($good['price']) . '</span> руб.';
                    echo '</div>'; //<div class="price-box">
                    echo '<div class="counter">';
                        echo '<span>Кол-во:</span> <span class="price-normal">' . number_format($good['quantity'], 0, ',', ' ') . '</span>';
                    echo '</div>'; //<div class="counter">
                    echo '<div class="end-price">';
                        echo '<span class="price-bigger">' . Yii::$app->numberHelper->numberToPrice($good['price'] * $good['quantity']) . '</span> руб.';
                    echo '</div>'; //<div class="end-price">
                echo '</li>';

                $orderSummPrice += $good['price'] * $good['quantity'];
            }
            ?>
        </ul>
    </div>
    <div class="panel-footer">
        <div>
            <div class="delivery icon-line">
                <?php
                if (!empty($Order->delivery->name_ru)) {
                    echo '<i class="icon ' . $Order->delivery->css_class . '"></i> <span>' . $Order->delivery->name_ru . '</span>';
                } elseif (!empty($Order->custom_delivery)) {
                    echo '<i class="icon other"></i> <span>' . $Order->custom_delivery . '</span>';
                } else {
                    echo '<span class="lighter">Способ доставки не определен</span>';
                }
                ?>
            </div>
            <div class="payment icon-line">
                <?php
                if (!empty($Order->payment->name_ru)) {
                    echo '<i class="icon ' . $Order->payment->css_class . '"></i> <span>' . $Order->payment->name_ru . '</span>';
                } elseif (!empty($Order->custom_payment)) {
                    echo '<i class="icon other"></i> <span>' . $Order->custom_payment . '</span>';
                } else {
                    echo '<span class="lighter">Способ оплаты не определен</span>';
                }
                ?>
            </div>
        </div><div class="order-summary">
            <span>Итого:</span> <i class="icon rouble-gold"></i> <span class="price-normal"><?= Yii::$app->numberHelper->numberToPrice($orderSummPrice) ?></span> <span>руб.</span>
        </div>
    </div>
</div>
<p class="order-date">
    Дата заказа: <span><?= date('d.m.Y', strtotime($Order->create_time)) ?></span>
</p>
<div class="dashed scissor-left"></div>


<?php
if ($Order->comment) {
    echo <<<LABEL
<div class="comment-header">
    <div class="icon-circle comment"><i></i></div><h3>Комментарий</h3>
</div>
LABEL;

    echo '<p class="panel width-panel comment-panel">' . Html::encode($Order->comment) . '</p>';
}
if ($Order->buyer_phone || $Order->buyer_index || $Order->buyer_address) {
    echo <<<LABEL
<div class="contact-header">
    <div class="icon-circle person"><i></i></div><h3>Контактная информация</h3>
</div>
LABEL;

    echo '<ul class="contact-info">';
        if ($Order->buyer_phone) {
            echo '<li>';
                echo '<div class="left-col">Номер телефона</div>';
                echo '<div class="right-col">' . Html::encode($Order->buyer_phone) . '</div>';
            echo '</li>';
        }
        if ($Order->buyer_index) {
            echo '<li>';
                echo '<div class="left-col">Почтовый индекс</div>';
                echo '<div class="right-col">' . Html::encode($Order->buyer_index) . '</div>';
            echo '</li>';
        }
        if ($Order->buyer_address) {
            echo '<li>';
                echo '<div class="left-col">Адрес доставки</div>';
                echo '<div class="right-col">' . Html::encode($Order->buyer_address) . '</div>';
            echo '</li>';
        }
    echo '</ul>';
}