<?php
/**
 * Страница со списком активных заказов.
 * @var yii\web\View $this
 * @var app\modules\users\models\User $User
 * @var app\models\Order[] $Orders
 * @var array $ordersArray
 * @var app\modules\users\models\User[] $Dealers
 * @var string $typeOfOrder
 * @var string $dealerIdColumn
 * @var array $ordersContentInfo
 * @var string $orderBy По какому столбцу сортировка
 * @var string $direction Направление сортировки
 * @var app\models\OrderStatus[] $StatusesList Все возможные статусы
 * @var string $baseUrl Url страницы
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\models\Order;

$this->registerCssFile('@web/css/cabinet-orders.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/cabinet-orders.js', [
    'depends' => [AppAsset::className()]
]);

if ($baseUrl == '/cabinet/orders/archive') {
    $this->title = 'Архив заказов';
    $this->registerMetaTag([
        'name' => 'description',
        'content' => 'Страница с архивом заказов'
    ]);
} else {
    $this->title = 'Заказы';
    $this->registerMetaTag([
        'name' => 'description',
        'content' => 'Страница с активными заказами'
    ]);
}

$tableHeader = [
    'number' => [
        'class' => 'number-header',
        'text' => 'Номер заказа',
        'dir' => '',
    ],
    'status' => [
        'class' => 'status-header',
        'text' => 'Статус',
        'dir' => '',
    ],
    'price' => [
        'class' => 'price-header',
        'text' => 'Цена',
        'dir' => '',
    ],
    'date' => [
        'class' => 'date-header',
        'text' => 'Дата',
        'dir' => '',
    ],
];
if (isset($tableHeader[$orderBy])) {
    $tableHeader[$orderBy]['dir'] = $direction;
}

?>
<h2><?= $this->title ?></h2>
<?php
$createSuccess = Yii::$app->session->getFlash('createOrderSuccess');
if ($createSuccess) {
    echo '<div class="alert success">' . $createSuccess . '</div>';
}

if ($User->hasStudio()) {
    echo '<menu class="' . $User->studio->type . ' over-table">'; // Класс - тип студии. От этого зависит цвет подчеркивания
    if ($typeOfOrder == 'sell') {
        echo '<li>';
            echo '<span class="bold-underline size16">У вас заказали</span>';
        echo '</li>';
        echo '<li>';
            echo Html::a('Вы заказали', Url::toRoute($baseUrl .'/purchase/'), ['class' => 'dashed-underline']);
        echo '</li>';
    } else {
        echo '<li>';
            echo Html::a('У вас заказали', Url::toRoute($baseUrl), ['class' => 'dashed-underline']);
        echo '</li>';
        echo '<li>';
            echo '<span class="bold-underline size16">Вы заказали</span>';
        echo '</li>';
    }
    echo '</menu>';
}
?>

<div class="table-header">
    <?php
    foreach ($tableHeader as $name => $col) {
        echo Html::beginTag('div', ['class' => $col['class']]);
            echo Html::beginTag('div');
                echo Html::tag('a', $col['text'], ['href' => Url::current(['order' => $name, 'dir' => $col['dir']]), 'class' => 'dashed-underline']);
                if ($orderBy == $name) {
                    $class = $col['dir'] == 'DESC' ? 'caret top' : 'caret';
                    echo ' ' . Html::tag('span', '', ['class' => $class]);
                }
            echo Html::endTag('div');
            echo Html::tag('div');
        echo Html::endTag('div');
    }
    ?>
</div>
<!-- TODO оценку партнера сделать в выпадающем меню (как смена статуса) -->
<!-- TODO сделать, что бы при нажатии на число заказов выезжал список заказов. -->
<!-- TODO сделать пагинатор. -->
<?php
if (count($Orders)) {
    echo '<ul class="order-list">';
    foreach ($Orders as $key => $Order) {
        $Dealer = $Dealers[$ordersArray[$key][$dealerIdColumn]];
        $dealerUrl = $Dealer->hasStudio() ? Url::toRoute('/studio/' . $Dealer->studio->id) : Url::toRoute('/simple-user/' . $Dealer->id);
        echo '<li data-order-id="' . $Order->id . '" class="order-item">';
            echo '<div class="main-info">';
                echo '<div class="order-number">';
                    echo '<p>';
                        echo Html::a('Заказ №' . $Order->id, Url::toRoute('/cabinet/order/' . $Order->id), ['class' => 'big-red']);
                    echo '</p>';
                    echo '<div class="studio-name">';
                        echo Html::tag('div', '<i></i>', ['class' => 'icon-circle small ' . $Dealer->getUserIconClass()]);
                        echo '<span>';
                            echo Html::a(Html::encode($Dealer->getName()), $dealerUrl);
                        echo '</span>';
                    echo '</div>'; // <div class="studio-name">
                echo '</div>'; // <div class="order-number">
//              <div class="photo-wrapper">
//              TODO сделать миниатюры фотографий
//              </div>
                echo '<div class="status-box">';
                    echo '<div class="order-status ' . $ordersArray[$key]['css_class'] . '"><span>' . $ordersArray[$key]['status'] . '</span></div>';
                    if ($Order->isCompleted()) {
                        if ($typeOfOrder == 'sell' && is_null($Order->seller_evaluation)) {
                            echo '<p class="evaluate-please">Пожалуйста оцените действия заказчика.</p>';
                            echo '<div class="icon-line evaluate-button"><i class="icon star"></i><span class="dashed-underline">Оценить заказчика</span></div>';
                        } else {
                            // отображение оценок на страницах архива заказов
                            if ($typeOfOrder == 'sell' && !is_null($Order->buyer_evaluation)) {
                                echo '<div class="icon-line evaluation-line"><span class="italic">Вас оценили:</span> ' . Order::paintEvalStars($Order->buyer_evaluation) . '</div>';
                            } elseif ($typeOfOrder == 'buy' && !is_null($Order->seller_evaluation)) {
                                echo '<div class="icon-line evaluation-line"><span class="italic">Вас оценили:</span> ' . Order::paintEvalStars($Order->seller_evaluation) . '</div>';
                            } else {
                                echo '<div class="evaluation-line"><span class="italic">Вас еще не оценили</span></div>';
                            }

                            if ($typeOfOrder == 'sell' && !is_null($Order->seller_evaluation)) {
                                echo '<div class="icon-line evaluation-line"><span class="italic size">Ваша оценка:</span> ' . Order::paintEvalStars($Order->seller_evaluation, 'small') . '</div>';
                            } elseif ($typeOfOrder == 'buy' && !is_null($Order->buyer_evaluation)) {
                                echo '<div class="icon-line evaluation-line"><span class="italic size">Ваша оценка:</span> ' . Order::paintEvalStars($Order->buyer_evaluation, 'small') . '</div>';
                            }

                        }
                    } else {
                        $possibleStatuses = $Order->getPossibleStatuses();
                        if (is_array($possibleStatuses)) {
                            $hasNotCancelStatus = false;
                            $cancelStatus = 0;
                            foreach ($possibleStatuses as $statusId) {
                                if ($statusId == 3 || $statusId == 4) {
                                    $cancelStatus = $statusId;
                                } else {
                                    $hasNotCancelStatus = true;
                                }
                            }
                            if ($hasNotCancelStatus) {
                                echo '<div class="icon-line edit-status-button"><i class="icon edit"></i><span class="dashed-underline">Изменить статус</span>';
                                    echo Html::beginTag('ul', ['class' => 'status-list']);
                                        echo '<li class="italic">Перевести заказ в:</li>';
                                        foreach ($possibleStatuses as $statusId) {
                                            if ($statusId == 3 || $statusId == 4) {
                                                continue;
                                            }
                                            $needEvaluation = false;
                                            $statusRules = Yii::$app->params['orderStatusRules'][$statusId];
                                            if (isset($statusRules['needEvaluation']) && $statusRules['needEvaluation']) {
                                                $needEvaluation = true;
                                            }

                                            $Status = $StatusesList[$statusId];
                                            echo Html::beginTag('li', [
                                                'class' => 'new-status',
                                                'data-status-id' => $statusId,
                                                'data-need-evaluation' => $needEvaluation ? 1 : 0
                                            ]);
                                                echo '<div class="order-status ' . $Status->css_class . '"><span>' . $Status->name_ru . '</span></div>';
                                            echo Html::endTag('li');
                                        }
                                    echo Html::endTag('ul');
                                echo '</div>'; // <div class="icon-line">
                            }

                            // можно ли отозвать или отменить заказ
                            if ($cancelStatus > 0) {
                                $cancelText = $cancelStatus == 3 ? 'Отклонить заказ' : 'Отозвать заказ';
                                echo '<div class="icon-line">';
                                    echo '<i class="icon delete-red"></i>';
                                    echo Html::tag('span', $cancelText, [
                                        'class' => 'dashed-underline cancel',
                                        'data-status-id' => $cancelStatus,
                                    ]);
                                echo '</div>'; // <div class="icon-line">
                            }
                        }
                    }
                echo '</div>'; // <div class="status-box">
                echo '<div class="price-box">';
                    echo '<span class="price-bigger">' . $ordersContentInfo[$Order->id]['order_price'] . '</span> <span>руб.</span>';
                echo '</div>'; // <div class="price-box">
                echo '<div class="date-box">' . date('d.m.Y', strtotime($Order->create_time)) . '</div>';
            echo '</div>'; // <div class="main-info">
            echo '<div class="sub-info">';
                echo '<div class="delivery icon-line">';
                    if (!empty($ordersArray[$key]['delivery'])) {
                        echo '<i class="icon ' . $ordersArray[$key]['delivery_css'] . '"></i> <span>' . $ordersArray[$key]['delivery'] . '</span>';
                    } else {
                        echo ' <span class="lighter">Способ доставки не определен</span>';
                    }
                echo '</div>'; // <div class="delivery icon-line">
                echo '<div class="payment icon-line">';
                    if (!empty($ordersArray[$key]['payment'])) {
                        echo '<i class="icon ' . $ordersArray[$key]['payment_css'] . '"></i> <span>' . $ordersArray[$key]['payment'] . '</span>';
                    } else {
                        echo ' <span class="lighter">Способ оплаты не определен</span>';
                    }
                echo '</div>'; // <div class="payment icon-line">
                echo '<div class="good-count icon-line">';
                    echo '<i class="icon order"></i><span class="dashed-underline size15">Товаров в заказе</span> <span class="red size15">(' . $ordersContentInfo[$Order->id]['count_of_goods'] . ')</span>';
                echo '</div>'; // <div class="good-count icon-line">
            echo '</div>'; // <div class="sub-info">
        echo '</li>';
    }
    echo '</ul>'; // <ul class="order-list">
} else {
    echo '<p class="no-orders">У вас нет активных заказов</p>';
}
?>
