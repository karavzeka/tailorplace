<?php
/**
 * Страница корзины.
 * @var yii\web\View $this
 * @var app\modules\studio\models\Studio[] $Studios
 * @var array $orderList
 * @var app\modules\good\models\Photo[] $goodPhotosArray
 * @var array $totalValues
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\modules\good\models\Photo;

$this->registerCssFile('@web/css/basket.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/basket.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Корзина товаров';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Корзина товаров'
]);

if (count($orderList) > 0) {
    echo Html::beginForm('/cabinet/place-order/', 'post', ['id' => 'orders-form']);
    echo '<input type="hidden" id="order-with-studio" name="studioId">';
}
?>
<h2>Корзина</h2>
<div class="sum-info">
    <p class="size16 italic">У Вас <?= Yii::t('app', '{n, plural, =0{нет заказов} one{# заказ} few{# заказов} many{# заказов} other{# заказа}}', ['n' => $totalValues['count']]) ?></p>
    <p class="sum-price">Общая сумма: <i class="icon rouble-gold"></i> <span class="price-bigger"><?= Yii::$app->numberHelper->numberToPrice($totalValues['price']) ?></span> руб.</p>
    <?php if (count($orderList) > 0) :?>
    <div class="button yellow place-all-order">
        <div class="low-layer"></div>
        <button type="submit" data-type="button" class="icon-line"><i class="icon order-white"></i>Оформить все заказы</button>
    </div>
    <?php endif ?>
</div>
<div class="dashed scissor-left"></div>
<?php if (count($orderList) > 0) :?>
<?php
foreach ($Studios as $Studio) {
    // блок заказа
    echo '<div class="order">';
        // иконка и название студии
        echo Html::beginTag('div', ['class' => 'icon-circle ' . $Studio->type]);
            echo '<i></i>';
        echo Html::endTag('div');
        $h3Content = 'Заказ у ' . Html::a(Html::encode($Studio->name), Url::toRoute('/studio/' . $Studio->id), ['class' => 'big-red-medium']);
        echo Html::tag('h3', $h3Content, ['class' => 'order-studio']);

        echo <<<LABEL
    <div class="panel width-panel" action="/cabinet/place-order/" method="post">
        <div class="panel-header">
            <ul class="good-list">
LABEL;

    // список товаров
    foreach ($orderList[$Studio->id]['goods'] as $good) {
        echo Html::beginTag('li', ['data-good-id' => $good['good_id']]);
            $photoImg = Html::img(isset($goodPhotosArray[$good['good_id']]) ? $goodPhotosArray[$good['good_id']]->getSrc('_min') : Photo::getNoPhotoSrc('_min'), ['width' => 48, 'height' => 48]);
            echo Html::a($photoImg, Url::toRoute('/goods/' . $good['good_id']), ['target' => '_blank', 'class' => 'img-link']);
            echo '<div class="name-box">';
                echo '<span>';
                    echo Html::a(Html::encode($good['good_name']), Url::toRoute('/goods/' . $good['good_id']), ['target' => '_blank', 'class' => 'size16']);
                echo '</span>';
                echo '<div class="icon-line"><i class="icon delete-red"></i>';
                    echo Html::tag('span', 'Удалить', [
                        'class' => 'dashed-underline delete',
                    ]);
                echo '</div>';
                echo '<div class="shadow"></div>';
            echo '</div>';

            echo '<div class="price-box">';
                echo '<span class="price-normal">' . Yii::$app->numberHelper->numberToPrice($good['price']) . '</span> руб.';
            echo '</div>';

            echo '<div class="counter">';
                echo '<button class="minusButton" type="button"></button>';
                echo Html::input('text', 'quantity[' . $good['good_id'] . ']', $good['quantity'], [
                    'class' => 'form-control'
                ]);
                echo '<button class="plusButton" type="button"></button>';
            echo '</div>';

            echo '<div class="end-price">';
                echo '<span class="price-bigger">' . Yii::$app->numberHelper->numberToPrice($good['price'] * $good['quantity']) . '</span> руб.';
            echo '</div>';
        echo Html::endTag('li');
    }
        echo <<<LABEL
            </ul>
        </div>
LABEL;
    // выбор способов доставки и оплаты
    echo '<div class="panel-body">';
        echo '<div class="delivery-type">';
            echo '<span class="size16">Доставка</span><div class="select">';
            if (count($Studio->delivery) > 0 || !empty($Studio->custom_delivery)) {
                echo Html::beginTag('select', ['name' => 'delivery[' . $Studio->id . ']']);
                foreach ($Studio->delivery as $Delivery) {
                    echo Html::tag('option', $Delivery->name_ru, [
                        'value' => $Delivery->id
                    ]);
                }
                if (!empty($Studio->custom_delivery)) {
                    echo Html::tag('option', Html::encode($Studio->custom_delivery), [
                        'value' => 'own'
                    ]);
                }
                echo Html::endTag('select');
                echo '<div class="select-button"><span class="caret"></span></div>';
            } else {
                echo '<span class="lighter size16 italic">нет вариантов</span>';
            }
            echo '</div>'; // <div class="select">
        echo '</div>'; // <div class="delivery-type">

        echo '<div class="payment-type">';
            echo '<span class="size16">Оплата</span><div class="select">';
            if (count($Studio->payment) > 0 || !empty($Studio->custom_payment)) {
                echo Html::beginTag('select', ['name' => 'payment[' . $Studio->id . ']']);
                foreach ($Studio->payment as $Payment) {
                    echo Html::tag('option', $Payment->name_ru, [
                        'value' => $Payment->id
                    ]);
                }
                if (!empty($Studio->custom_payment)) {
                    echo Html::tag('option', Html::encode($Studio->custom_payment), [
                        'value' => 'own'
                    ]);
                }
                echo Html::endTag('select');
                echo '<div class="select-button"><span class="caret"></span></div>';
            } else {
                echo '<span class="lighter size16 italic">нет вариантов</span>';
            }
            echo '</div>'; // <div class="select">
        echo '</div>'; // <div class="payment-type">
    echo '</div>'; // <div class="panel-body">

    // удаление/оформление заказа
    echo '<div class="panel-footer">';
        echo '<div class="icon-line delete-order">';
            echo '<i class="icon delete-red"></i>';
            echo Html::tag('span', 'Удалить заказ', [
                'class' => 'dashed-underline',
                'data-studio-id' => $Studio->id
            ]);
        echo '</div>'; // <div class="icon-line delete-order">
        echo '<div class="button-wrapper">';
            echo '<div class="button place-order">';
                echo '<div class="low-layer"></div>';
                echo Html::button('<i class="icon order-white"></i>Оформить', [
                    'data-type' => 'submit',
                    'type' => 'submit',
                    'class' => 'icon-line post-one-order',
                    'data-studio-id' => $Studio->id
                ]);
            echo '</div>'; // <div class="button place-order">
        echo '</div>'; // <div class="button-wrapper">
        echo '<div class="order-summary">';
            echo '<span>Итого:</span> <i class="icon rouble-gold"></i>';
            echo ' <span class="price-normal">' . Yii::$app->numberHelper->numberToPrice($orderList[$Studio->id]['total_price']) . '</span> <span>руб.</span>';
        echo '</div>'; // <div class="order-summary">
    echo '</div>'; // <div class="panel-footer">

    echo '</div>'; // <div class="panel width-panel">
    echo '</div>'; // <div class="order">
}
?>

<div class="dashed scissor-left"></div>
<div class="sum-info">
    <p class="size16 italic">У Вас <?= Yii::t('app', '{n, plural, =0{нет заказов} one{# заказ} few{# заказов} many{# заказов} other{# заказа}}', ['n' => $totalValues['count']]); ?></p>
    <p class="sum-price">Общая сумма: <i class="icon rouble-gold"></i> <span class="price-bigger"><?= Yii::$app->numberHelper->numberToPrice($totalValues['price']) ?></span> руб.</p>
    <div class="button yellow place-all-order">
        <div class="low-layer"></div>
        <button type="submit" data-type="button" class="icon-line"><i class="icon order-white"></i>Оформить все заказы</button>
    </div>
</div>
<?= Html::endForm() ?>
<?php else: ?>
<p class="empty-basket">Корзина пуста</p>
<?php endif ?>