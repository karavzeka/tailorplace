<?php
/**
 * Страница платных услуг.
 * @var yii\web\View $this
 */

use yii\helpers\Url;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/paidServices.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Дополнительные услуги';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница дополнительные услуг. Здесь предоставляются как платные, так и бесплатные услуги.'
]);
?>

<h2>Дополнительные услуги</h2>
<div id="services-wrapper">
    <div id="main-col">
        <p>
            Здесь представлены дополнительные услуги сайта.
        </p>
        <ul>
            <li>
                <div class="icon-circle up"><i></i></div>
                <div class="service-body">
                    <h3><a href="/cabinet/services/studio-promotion/">Продвижение ателье/магазина</a></h3>
                    <p>
                        Повысте популярность своего ателье/магазина, отобразав его на главной страни сайта.
                        Обращаясь в карусели ателье/магазинов, оно привлечет внимание многих пользователей, которые станут вашими клиентами.

<!--                        При активации данной услуги ваше ателье (магазин) окажется на главной странице сайта.-->
<!--                        Оно будет добавлено в карусель ателье/магазинов и пробудет там в течении определенного времени.-->
                    </p>
                </div>
            </li>
            <li>
                <div class="icon-circle good-green"><i></i></div>
                <div class="service-body">
                    <h3><a href="/cabinet/services/goods-promotion/">Продвижение товаров</a></h3>
                    <p>
                        Продвигайте свои товары, добавив их в карусель товаров, отображаемую на многих страницах сайта.
                    </p>
                </div>
            </li>
        </ul>
    </div>
    <div id="right-col">
<!--        <div class="panel small-panel" id="purse">-->
<!--            <div class="panel-body">-->
<!--                <div class="header icon-line"><i class="icon rouble-light"></i><span>Баланс</span></div>-->
<!--                <div class="big-digit">0</div>-->
<!--            </div>-->
<!--            <div class="panel-footer">-->
<!--                <div class="header icon-line"><a href="/cabinet/">История платежей</a></div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="button yellow" id="fill-up-button">-->
<!--            <div class="low-layer"></div>-->
<!--            <a href="" data-type="button" class="icon-line"><i class="icon rouble-white"></i>Пополнить</a>-->
<!--        </div>-->
    </div>
</div>