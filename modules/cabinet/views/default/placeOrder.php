<?php
/**
 * Страница оформления заказов.
 * @var yii\web\View $this
 * @var array $orderList
 * @var app\modules\good\models\Photo[] $goodPhotosArray
 * @var int $totalPrice
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use app\modules\good\models\Photo;

$this->registerCssFile('@web/css/placeOrder.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/placeOrder.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Оформление заказа';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница оформления заказа'
]);
?>

<h2>Оформление заказа</h2>
<?php
$createError = Yii::$app->session->getFlash('createOrderError');
if ($createError) {
    echo '<div class="alert error">' . $createError . '</div>';
}

if (count($orderList) > 0) {
    if (count($orderList) > 1) {
        echo '<p class="page-help size16 italic">С каждым ателье и/или магазином у вас будет оформлен отдельный заказ.</p>';
    }
    echo Html::beginForm('/cabinet/end-place-order/', 'post', ['id' => 'orders-form']);

    foreach ($orderList as $order) {
        echo '<div class="order">';
        // заголовок заказа
            echo Html::tag('div', '<i></i>', ['class' => 'icon-circle ' . $order['studioType']]);
            $studioLink = Html::a(Html::encode($order['studioName']), Url::toRoute('/studio/' . $order['studioId']), [
                'target' => '_blank',
                'class' => 'big-red-medium',
            ]);
            echo Html::tag('h3', 'Заказ у ' . $studioLink);
        // тело заказа
            echo '<div class="panel width-panel">';
            echo '<div class="panel-body">';
            echo '<ul class="good-list">';

            foreach ($order['goods'] as $good) {
                echo '<li>';
                    $imgHtml = Html::img(isset($goodPhotosArray[$good['good_id']]) ? $goodPhotosArray[$good['good_id']]->getSrc('_min') : Photo::getNoPhotoSrc('_min'), [
                        'width' => 48,
                        'height' => 48
                    ]);
                    echo Html::a($imgHtml, Url::toRoute('/goods/' . $good['good_id']), [
                        'target' => '_blank',
                        'class' => 'img-link',
                    ]);
                    echo '<div class="name-box">';
                        echo Html::a(Html::encode($good['good_name']), Url::toRoute('/goods/' . $good['good_id']), [
                            'target' => '_blank',
                            'class' => 'size16',
                        ]);
                    echo '</div>'; // <div class="name-box">

                    echo '<div class="price-box">';
                        echo '<span class="price-normal">' . Yii::$app->numberHelper->numberToPrice($good['price']) . '</span> руб.';
                    echo '</div>'; // <div class="price-box">

                    echo '<div class="counter">';
                        echo '<span>Кол-во:</span> <span class="price-normal">' . $good['quantity'] . '</span>';
                    echo '</div>'; // <div class="counter">

                    echo '<div class="end-price">';
                        echo '<span class="price-bigger">' . Yii::$app->numberHelper->numberToPrice($good['price'] * $good['quantity']) . '</span> руб.';
                    echo '</div>'; // <div class="end-price">
                echo '</li>';
            }

            echo '</ul>'; // <ul class="good-list">
            echo '</div>'; // <div class="panel-body">
            echo '<div class="panel-footer">';
            echo '<div class="footer-info">';

                echo '<div>';
                    echo '<div class="delivery icon-line">';
                    if (!empty($order['deliveryName'])) {
                        echo Html::tag('i', '', [
                            'class' => 'icon ' . $order['deliveryCss']
                        ]);
                        echo ' <span>' . $order['deliveryName'] . '</span>';
                    } else {
                        echo ' <span>Способ доставки не определен</span>';
                    }
                    echo '</div>'; // <div class="delivery icon-line">
                    echo '<div class="payment icon-line">';
                    if (!empty($order['paymentName'])) {
                        echo Html::tag('i', '', [
                            'class' => 'icon ' . $order['paymentCss']
                        ]);
                        echo ' <span>' . $order['paymentName'] . '</span>';
                    } else {
                        echo ' <span>Способ оплаты не определен</span>';
                    }
                    echo '</div>'; // <div class="payment icon-line">
                echo '</div>';

                echo '<div class="button-wrapper">
                          <div class="button comment-button">
                              <div class="low-layer"></div>
                              <button type="button">Добавить комментарий</button>
                          </div>
                      </div>'; // <div class="button-wrapper">

                echo '<div class="order-summary">';
                    echo '<span>Итого:</span> <i class="icon rouble-gold"></i> <span class="price-normal">' . Yii::$app->numberHelper->numberToPrice($order['totalPrice']) . '</span> <span>руб.</span>';
                echo '</div>'; // <div class="order-summary">

            echo '</div>'; // <div class="footer-info">
            echo '<div class="footer-comment">';
                echo Html::textarea('comment[' . $order['studioId'] . ']', '', [
                    'placeholder' => 'Комментарий будет прикркплен к заказу'
                ]);
            echo '</div>'; // <div class="footer-comment">

            echo '</div>'; // <div class="panel-footer">
            echo '</div>'; // <div class="panel width-panel">
        echo '</div>'; // <div class="order">
    }
?>

<div class="dashed scissor-left"></div>
<div class="sum-info">
    <p class="size16 italic">У Вас <?= Yii::t('app', '{n, plural, =0{нет заказов} one{# заказ} few{# заказов} many{# заказов} other{# заказа}}', ['n' => count($orderList)]) ?></p><p class="sum-price">Общая сумма: <i class="icon rouble-gold"></i> <span class="price-bigger"><?= Yii::$app->numberHelper->numberToPrice($totalPrice) ?></span> руб.</p>
</div>

<div class="panel width-panel" id="contacts">
    <div class="panel-header">
        <div class="icon-circle person"><i></i></div><h2>Контактная информация</h2>
    </div>
    <div class="panel-body">
        <ul>
            <li>
                <div class="form-group">
                    <div class="left-col">
                        <label class="control-label" for="phone">Номер телефона</label>
                    </div>
                    <div class="center-col">
                        <input id="phone" class="form-control" type="text" name="phone">
                        <div class="help-block"></div>
                    </div>
                </div>
            </li>
            <li>
                <div class="form-group">
                    <div class="left-col">
                        <label class="control-label" for="index">Почтовый индекс</label>
                    </div>
                    <div class="center-col">
                        <input id="index" class="form-control" type="text" name="post-index">
                        <div class="help-block"></div>
                    </div>
                </div>
            </li>
            <li>
                <div class="form-group">
                    <div class="left-col">
                        <label class="control-label" for="address">Адрес доставки</label>
                    </div>
                    <div class="center-col">
                        <textarea id="address" class="form-control" name="address"></textarea>
                        <div class="help-block"></div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="panel-footer">
        <div class="button yellow" id="place-orders">
            <div class="low-layer"></div>
            <button type="submit" class="icon-line"><i class="icon order-white"></i><span>Оформить заказы</span></button>
        </div>
    </div>
</div>

<?php
    echo Html::endForm();
} else {
    echo '<p class="empty-orders">Нечего заказывать</p>';
}
?>

<div id="back-to-basket">
    <a href="/cabinet/basket/">Вернуться в корзину</a>
</div>