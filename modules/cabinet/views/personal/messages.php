<?php
/**
 * Страница сообщений.
 * @var yii\web\View $this
 * @var string $messagesHtml
 * @var int $countOfInterlocutors Полное число собеседников
 */

use yii\helpers\Html;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/messages.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/messages.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Сообщения';
?>

<h2>Сообщения</h2>
<?php
    if ($messagesHtml) {
        echo '<ul id="messages">';
            echo $messagesHtml;
        echo '</ul>';
    } else {
        echo Html::tag('p', 'Вы еще ни с кем не общались', ['class' => 'not-messages']);
    }

    echo '<div id="show-more">';
    if ($countOfInterlocutors > $this->context->limitOfLastMessages) {
        echo <<<LABEL
            <div id="show-more-button" data-offset-part="1" data-interlocutors-count="{$countOfInterlocutors}">
                <span class="plus-button"></span>
                <span>Показать еще собеседников</span>
            </div>
LABEL;
    }
    echo '</div>'; // <div id="show-more">
?>