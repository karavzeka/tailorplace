<?php

/**
 * @var app\modules\users\models\User $User
 * @var app\modules\cabinet\models\PersonalForm $PersonalForm
 */

use yii\widgets\ActiveForm;
use app\assets\AppAsset;
use yii\helpers\Html;

$this->registerJsFile('@web/js/exif.js');
$this->registerCssFile('@web/css/cabinet-personal-redact.css', [
    'depends' => [AppAsset::className()]
]);

$this->registerJsFile('@web/js/cabinet-personal-redact.js', [
    'depends' => [AppAsset::className()]
]);

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
?>
<div class="panel width-panel">
    <?php
    $form = ActiveForm::begin([
        'id' => 'user-form','fieldConfig' => [
            'template' => $fieldTemplate,
        ],
    ]);
    ?>
    <div class="panel-header">
        <div class="icon-circle redact"><i></i></div><h2>Редактирование персональных данных</h2>
    </div>
    <div class="panel-body">
        <p class="pass-help">
            Для смены пароля введите старый пароль и новый пароль с повтором. Если пароль вы менять не хотите, оставьте эти поля пустыми.
        </p>
        <ul>
            <li>
                <?= $form->errorSummary($PersonalForm, [
                    'header' => ''
                ])
                ?>
            </li>
            <li class="pass-field">
                <?= $form->field($PersonalForm, 'oldPassword')->passwordInput() ?>
            </li>
            <li class="pass-field">
                <?= $form->field($PersonalForm, 'newPassword')->passwordInput() ?>
                <div class="right-col">Минимум 6 символов</div>
            </li>
            <li class="pass-field">
                <?= $form->field($PersonalForm, 'repeatNewPassword')->passwordInput() ?>
            </li>
            <li>
                <?= $form->field($PersonalForm, 'deliveryAddress') ?>
            </li>
            <li class="photos">
                <div class="left-col">Фотография</div><div class="center-col">
                    <?php
                    $dropboxAttributes = ['id' => 'dropbox'];
                    echo Html::beginTag('div', ['id' => 'photos-to-upload']);
                    if (is_array($User->photo) && count($User->photo) > 0) {
                        $dropboxAttributes['style'] = 'display: none';
                        $Photo = $User->photo[0];
                        echo Html::beginTag('div', [
                            'class' => 'photo-wrapper',
                            'data-existId' => $Photo->id,
                            'data-top-shift-part' => $Photo->margin_top_part,
                            'data-left-shift-part' => $Photo->margin_left_part,
                            'data-size-part' => $Photo->size_part,
                            'data-height' => $Photo->real_height,
                            'data-width' => $Photo->real_width,
                        ]);
                        echo Html::tag('div', '', ['class' => 'glass', 'style' => 'display: none;']);
                        echo Html::beginTag('div', ['class' => 'hidden-wrapper']);
                        $sizeAttr = $Photo->getSizeAttr(82);
                        echo Html::img($Photo->getSrc(), [
                            'style' => $Photo->getMargins(82),
                            $sizeAttr['side'] => $sizeAttr['size']
                        ]);
                        echo Html::endTag('div');
                        echo '<div class="icon-circle remove" title="Удалить фотографию" data-placement="right" data-toggle="tooltip"><i></i></div>';
                        echo '<div class="icon-circle setting" title="Настроить фотографию" data-placement="right" data-toggle="tooltip"><i></i></div>';

                        echo Html::endTag('div');
                    }
                    echo Html::endTag('div');
                    echo Html::tag('div', '<p>Нажмите для выбора файлов<br>или<br>перетащиет файл сюда</p>', $dropboxAttributes);
                    ?>
                    <input type="file" multiple id="photo-input" name="photos" accept="image/*">
                    <?= Html::activeHiddenInput($PersonalForm, 'photoSettings', [
                        'id' => 'photo-properties'
                    ])
                    ?>
                    <div class="help-block"></div>
                </div>
            </li>
        </ul>
    </div>
    <div class="panel-footer">
        <div class="button yellow" id="save-action">
            <div class="low-layer"></div>
            <button type="submit">Сохранить</button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="modal fade" id="photo-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="darker">Выберите область фотографии</h4>
            </div>
            <div class="modal-body">
                <div id="photo-place">
                    <div id="photo-wrapper">
                        <div class="photo-shadow"></div>
                        <div id="select-place"></div>
                        <div class="marker left-top"></div>
                        <div class="marker right-top"></div>
                        <div class="marker right-bottom"></div>
                        <div class="marker left-bottom"></div>
                    </div>
                </div>
                <div id="preview-wrapper">
                    <figure>
                        <div id="preview"></div>
                        <figcaption class="darker">Превью</figcaption>
                    </figure>
                </div>
                <div class="clear"></div>
            </div>
            <div class="modal-footer">
                <div class="button" id="photo-settings-cancel">
                    <div class="low-layer"></div>
                    <button type="submit" data-dismiss="modal">Отмена</button>
                </div>
                <div class="button yellow" id="photo-settings-save">
                    <div class="low-layer"></div>
                    <button type="button">Применить</button>
                </div>
            </div>
        </div>
    </div>
</div>