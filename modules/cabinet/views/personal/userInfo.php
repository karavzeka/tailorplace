<?php
/**
 * @var $hasStudio boolean
 * @var $User app\modules\users\models\User
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\users\models\Photo as UserPhoto;

$userSessionMessage = Yii::$app->session->getFlash('userRedactSuccess' , '');
?>
<?php
if ($userSessionMessage) {
    echo '<div class="alert success">' . $userSessionMessage . '</div>';
}
?>
<div id="user-data" class="info-wrapper">
	<h4 class="icon-line big-red-medium"><i class="icon person"></i><span>Аккаунт</span></h4>
    <div class="button">
        <div class="low-layer"></div>
        <a data-type="button" href="<?= Url::toRoute('user-redact') ?>">Редактировать</a>
    </div>
	<ul>
		<li>
			<div class="left-col">Логин</div>
			<div class="right-col"><?= Html::encode($User->login) ?></div>
		</li>
		<li>
			<div class="left-col">E-mail</div>
			<div class="right-col"><?= Html::encode($User->email) ?></div>
		</li>
		<li>
			<div class="left-col">Дата регистрации</div>
			<div class="right-col"><?= date('d.m.Y', strtotime($User->create_time)) ?></div>
		</li>
		<li>
			<div class="left-col">
				<span>Адрес доставки</span><br>
				<small>Никому не виден, подставляется<br>при оформлении заказа</small>
			</div>
			<div class="right-col">
				<?= $User->address ? Html::encode($User->address) : '—' ?>
			</div>
		</li>
		<li class="has-photo">
			<div class="left-col">
				<span>Фотография</span>
			</div>
			<div class="right-col">
				<img src="<?= count($User->photo) > 0 ? $User->photo[0]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle') ?>">
			</div>
		</li>
	</ul>
</div>