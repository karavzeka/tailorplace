<?php
/**
 * Страница диалога.
 * @var yii\web\View $this
 * @var app\modules\users\models\User $User
 * @var app\modules\users\models\User $Opponent
 * @var app\models\Message $NewMessage
 * @var string $messagesHtml
 * @var bool $showMoreButton
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/dialog.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/dialog.js', [
    'depends' => [
        AppAsset::className(),
        \app\assets\BaronAsset::className(),
    ]
]);

$this->title = 'Диалог';
?>

<h2>Диалог <span>(с пользователем <a href="<?= $Opponent->getLinkToPage() ?>"><?= Html::encode($Opponent->getName()) ?></a>)</span></h2>
<div class="sloping-line"></div>
<div id="dialog-wrapper">
    <div class="dialog-scroller">
        <div class="dialog-container">
            <?php
            if ($messagesHtml) {
                echo '<div id="show-more">';
                if ($showMoreButton) {
                    echo <<<LABEL
                    <div id="show-more-button" data-offset="1">
                        <span class="plus-button"></span>
                        <span>Показать еще {$this->context->limitOfMessagesInDialog} предыдущиx</span>
                    </div>
LABEL;
                }
                echo '</div>'; // <div id="show-more">
                echo '<ul>';
                    echo $messagesHtml;
                echo '</ul>';
            } else {
                echo Html::tag('p', 'С этим пользователем вы еще не общались', ['class' => 'not-messages']);
            }
            ?>
        </div>
    </div>
    <div class="scroller__track"><!-- Track is optional -->
        <div class="scroller__bar"></div>
    </div>
</div>
<div class="sloping-line"></div>
<?php
    $form = ActiveForm::begin([
        'id' => 'message-form',
        'fieldConfig' => [
            'template' => '{input}<div class="help-block"></div>',
        ],
    ]);
?>
<?= $form->field($NewMessage, 'text')
    ->textArea([
        'placeholder' => 'Введите сообщение'
    ])
?>
<div class="button yellow" id="send-button">
        <div class="low-layer"></div>
        <button type="submit">Отправить</button>
</div>
<?php ActiveForm::end(); ?>