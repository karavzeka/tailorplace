<?php
/**
 * @var $studioType string
 * @var $Studio app\modules\studio\models\Studio
 */

use yii\helpers\Url;
use yii\helpers\Html;

$studioSessionMessage = Yii::$app->session->getFlash('studioRedactSuccess' , '');
?>
<div class="dashed scissor-right"></div>
<?php
if ($studioSessionMessage) {
    echo '<div class="alert success">' . $studioSessionMessage . '</div>';
}
?>
<div id="studio-data" class="info-wrapper">
    <h4 class="icon-line big-red-medium"><i class="icon store"></i><span><?= $studioType ?></span></h4>
    <div class="watch-studio-link"><a href="<?= Url::toRoute('/studio/' . $Studio->id) ?>" class="lighter">Посмотреть на <?= $Studio->type == 'atelier' ? 'свое ателье' : 'свой магазин' ?></a></div>
    <div class="button">
        <div class="low-layer"></div>
        <a data-type="button" href="<?= Url::toRoute('/cabinet/studio/studio-redact') ?>">Редактировать</a>
    </div>
	<ul>
		<li>
			<div class="left-col">Название</div>
			<div class="right-col"><?= Html::encode($Studio->name) ?></div>
		</li>
		<li>
			<div class="left-col">Страна</div>
			<div class="right-col"><?= $Studio->country->name_ru ?></div>
		</li>
		<li>
			<div class="left-col">Город</div>
			<div class="right-col"><?= !empty($Studio->city->name_ru) ? $Studio->city->name_ru : '—' ?></div>
		</li>
        <li>
            <div class="left-col">Адрес</div>
            <div class="right-col"><?= !empty($Studio->address) ? Html::encode($Studio->address) : '—' ?></div>
        </li>
        <li>
            <div class="left-col">Телефон</div>
            <div class="right-col"><?= !empty($Studio->phone) ? Yii::$app->numberHelper->numberToPhone($Studio->phone) : '—' ?></div>
        </li>
		<li>
			<div class="left-col">Дата регистрации</div>
            <div class="right-col"><?= date('d.m.Y', strtotime($Studio->create_time)) ?></div>
		</li>
		<li>
			<div class="left-col">Слоган</div>
			<div class="right-col"><?= !empty($Studio->slogan) ? Html::encode($Studio->slogan) : '—' ?></div>
		</li>
		<li>
			<div class="left-col">Описание</div>
			<div class="right-col"><?= !empty($Studio->description) ? Html::encode($Studio->description) : '—' ?></div>
		</li>
        <li>
            <div class="left-col">Способ доставки</div>
            <div class="right-col">
                <?php
                if (count($Studio->delivery) > 0 || !empty($Studio->custom_delivery)) {
                    echo '<ul class="sublist icon-line">';
                    foreach ($Studio->delivery as $Delivery) {
                        echo Html::beginTag('li');
                            echo Html::tag('i', '', ['class' => 'icon ' . $Delivery->css_class]);
                            echo Html::tag('span', $Delivery->name_ru);
                        echo Html::endTag('li');
                    }
                    if (!empty($Studio->custom_delivery)) {
                        echo Html::beginTag('li');
                            echo Html::tag('i', '', ['class' => 'icon other']);
                            echo Html::tag('span', Html::encode($Studio->custom_delivery));
                        echo Html::endTag('li');
                    }
                    echo '</ul>';
                } else {
                    echo '—';
                }
                ?>
            </div>
        </li>
        <li>
            <div class="left-col">Способ оплаты</div>
            <div class="right-col">
                <?php
                    if (count($Studio->payment) > 0 || !empty($Studio->custom_payment)) {
                        echo '<ul class="sublist icon-line">';
                        foreach ($Studio->payment as $Payment) {
                            echo Html::beginTag('li');
                                echo Html::tag('i', '', ['class' => 'icon ' . $Payment->css_class]);
                                echo Html::tag('span', $Payment->name_ru);
                            echo Html::endTag('li');
                        }
                        if (!empty($Studio->custom_payment)) {
                            echo Html::beginTag('li');
                                echo Html::tag('i', '', ['class' => 'icon other']);
                                echo Html::tag('span', Html::encode($Studio->custom_payment));
                            echo Html::endTag('li');
                        }
                        echo '</ul>';
                    } else {
                        echo '—';
                    }
                ?>
            </div>
        </li>
	</ul>
</div>