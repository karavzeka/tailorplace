<?php
/**
 * Страница создания товара.
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\cabinet\models\GoodForm $GoodForm
 * @var app\modules\good\models\Photo[] $Photos
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/cabinet-studio-addGood.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/exif.js');
$this->registerJsFile('@web/js/cabinet-studio-addGood.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Добавление товара';

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
$requiredFieldTemplate = '<div class="left-col"><i class="icon asterisk"></i>{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
$selectTemplate = '<div class="left-col"><i class="icon asterisk"></i>{label}</div><div class="center-col"><div class="select category">{input}<div class="select-button"><span class="caret"></span></div></div><div class="help-block"></div></div>';
?>
<div class="panel width-panel">
    <?php
    $form = ActiveForm::begin([
        'id' => 'good-form',
        'fieldConfig' => [
            'template' => $fieldTemplate,
        ],
    ]);
    ?>
    <div class="panel-header">
        <div class="icon-circle good"><i></i></div><h2><?= $action == 'new' ? 'Добавление товара' : 'Редактирование товара' ?></h2>
    </div>
    <div class="panel-body">
        <ul>
            <li>
                <?= $form->errorSummary($GoodForm, [
                    'header' => ''
                ])
                ?>
            </li>
            <li>
                <?= $form->field($GoodForm, 'name', [
                    'template' => $requiredFieldTemplate,
                ])
                ?>
            </li>
            <li class="price">
                <?= $form->field($GoodForm, 'price', [
                    'template' => $requiredFieldTemplate,
                ])
                ?>
                <div class="right-col">Руб.</div>
            </li>
            <?php
                if (count($GoodForm->categories) > 0) {
                    $counter = 0;
                    foreach ($GoodForm->categories as $categoryId) {
                        $label = $counter == 0 ? 'Категория' : '';
                        echo '<li class="multyField">';
                            echo Html::Tag('div', $label, ['class' => 'left-col']);
                            echo Html::beginTag('div', ['class' => 'center-col']);
                                echo Html::beginTag('div', ['class' => 'select category']);
                                    echo Html::dropDownList('GoodForm[categories][]',
                                        $categoryId, //значение выбранной опции
                                        ArrayHelper::map($GoodForm->categoryList, 'id', 'name_ru'), [
                                            'id' => 'goodform-categories'
                                        ]
                                    );
                                    echo '<div class="select-button"><span class="caret"></span></div>';
                                echo Html::endTag('div');
                            echo Html::endTag('div');
                            if ($counter > 0) {
                                echo '<button class="deleteButton" title="Убрать категорию" data-placement="right" data-toggle="tooltip" type="button"></button>';
                            }
                        echo '</li>';
                        $counter++;
                    }
                } else {
                    echo '<li class="multyField">';
                    echo '<div class="left-col">Категория</div><div class="center-col">';
                    echo Html::beginTag('div', ['class' => 'select category']);
                    echo Html::dropDownList('GoodForm[categories][]',
                        null, //значение выбранной опции
                        ArrayHelper::map($GoodForm->categoryList, 'id', 'name_ru'), [
                            'id' => 'goodform-categories'
                        ]
                    );
                    echo '<div class="select-button"><span class="caret"></span></div>';
                    echo Html::endTag('div');
                    echo '</div>';
                    echo '</li>';
                }
            ?>
            <li id="add-button-line">
            	<div class="left-col"></div><div class="center-col">
                    <button type="button" class="addButton" id="addCategory" data-toggle="tooltip" data-placement="right" title="Добавить категорию"></button>
                </div>
            </li>
            <?php
            if ($GoodForm->isStoreGood()) {
                echo '<li class="quantity">';
                    echo $form->field($GoodForm, 'quantity');
                    echo '<div class="right-col">Если 0, то поле будет скрыто</div>';
                echo '</li>'; // <li class="quantity">
            }
            ?>
            <li class="description">
                <?= $form->field($GoodForm, 'description')->textArea([
                        'placeholder' => 'Чем подробнее вы опишете товар, тем больше он сможет заинтересовать покупателя. Так же это положительно скажется на поиске товара. Увеличится шанс показа товара поисковиками Яндекс и Google.',
                    ])
                ?>
            </li>
            <?php
            if (!$GoodForm->isStoreGood()) {
	            echo Html::beginTag('li');
	            echo $form->field($GoodForm, 'craft_time')->textInput([
		                'placeholder' => 'Пример: "3 недели"',
		            ]);
	            echo Html::endTag('li');
        	}
            ?>
            <li class="photos">
                <div class="left-col">Фотографии товара</div>
                <div class="center-col">
                    <?php
                    echo Html::beginTag('div', ['id' => 'photos-to-upload']);
                    foreach ($Photos as $Photo) {
                        echo Html::beginTag('div', [
                            'class' => 'photo-wrapper',
                            'data-existId' => $Photo->id,
                            'data-top-shift-part' => $Photo->margin_top_part,
                            'data-left-shift-part' => $Photo->margin_left_part,
                            'data-size-part' => $Photo->size_part,
                            'data-height' => $Photo->real_height,
                            'data-width' => $Photo->real_width,
                        ]);
                            echo Html::tag('div', '', ['class' => 'glass', 'style' => 'display: none;']);
                            echo Html::beginTag('div', ['class' => 'hidden-wrapper']);
                                $sizeAttr = $Photo->getSizeAttr(82);
                                echo Html::img($Photo->getSrc(), [
                                    'style' => $Photo->getMargins(82),
                                    $sizeAttr['side'] => $sizeAttr['size']
                                ]);
                            echo Html::endTag('div');
                            echo '<div class="icon-circle remove" title="Удалить фотографию" data-placement="right" data-toggle="tooltip"><i></i></div>';
                            echo '<div class="icon-circle setting" title="Настроить фотографию" data-placement="right" data-toggle="tooltip"><i></i></div>';

                            echo '<div class="radio-input">';
                                echo Html::radio('mainPhoto', $Photo->main, ['id' => 'old-' . $Photo->id]);
                                echo Html::beginTag('label', [
                                    'for' => 'old-' . $Photo->id,
                                ]);

                                    echo ' Главная';
                                echo Html::endTag('label');
                            echo '</div>';
                        echo Html::endTag('div');
                    }
                    echo Html::endTag('div');

                    $dropboxAttributes = ['id' => 'dropbox'];
                    if (count($Photos) >= 9) {
                        $dropboxAttributes['style'] = 'display: none';
                    }
                    echo Html::tag('div', '<p>Нажмите для выбора файлов<br>или<br>перетащиет файлы сюда</p>', $dropboxAttributes);
                    ?>
                    <input type="file" multiple id="photo-input" name="photos" accept="image/*">
                    <?= Html::activeHiddenInput($GoodForm, 'photoSettings', [
                        'id' => 'photo-properties'
                    ])
                    ?>
                    <div class="help-block"></div>
                </div>
            </li>
        </ul>
    </div>
    <div class="panel-footer">
        <div class="button yellow" id="add-action">
            <div class="low-layer"></div>
            <button type="submit"><?= $action == 'new' ? 'Добавить товар' : 'Сохранить' ?></button>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="modal fade" id="photo-settings" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="darker">Выберите область фотографии для превью</h4>
            </div>
            <div class="modal-body">
                <div id="photo-place">
                    <div id="photo-wrapper">
                        <div class="photo-shadow"></div>
                        <div id="select-place"></div>
                        <div class="marker left-top"></div>
                        <div class="marker right-top"></div>
                        <div class="marker right-bottom"></div>
                        <div class="marker left-bottom"></div>
                    </div>
                </div>
                <div id="preview-wrapper">
                    <figure>
                        <div id="preview"></div>
                        <figcaption class="darker">Превью</figcaption>
                    </figure>
                    <p class="italic">Изображение-превью
                        используется для
                        отображения товара
                        в общем каталоге
                        товаров.</p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="modal-footer">
                <div class="button" id="photo-settings-cancel">
                    <div class="low-layer"></div>
                    <button type="submit" data-dismiss="modal">Отмена</button>
                </div>
                <div class="button yellow" id="photo-settings-save">
                    <div class="low-layer"></div>
                    <button type="button">Применить</button>
                </div>
            </div>
        </div>
    </div>
</div>