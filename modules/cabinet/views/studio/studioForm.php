<?php
/**
 * Страница регистрации студии.
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\cabinet\models\StudioForm $studioForm
 * @var bool $isNew
 */
// TODO на этой странице почему-то ловается тултип на иконке выхода

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\modules\assets\studio\CreateStudioAsset;

CreateStudioAsset::register($this);
app\assets\JQueryMaskPlugin::register($this);

$phoneMaskJs = '
$(\'#studioform-phone\').mask(\'+0(000)000-00-00\');
';
$this->registerJs($phoneMaskJs);

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
$requiredFieldTemplate = '<div class="left-col"><i class="icon asterisk"></i>{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
$selectTemplate = '<div class="left-col"><i class="icon asterisk"></i>{label}</div><div class="center-col"><div class="select currency">{input}<div class="select-button"><span class="caret"></span></div></div><div class="help-block"></div></div>';
if ($studioForm->getType() === 'atelier') {
    $studioLabel = '<i class="icon asterisk"></i>Название ателье';
    $studioType = 'ателье';
} else {
    $studioLabel = '<i class="icon asterisk"></i>Название магазина';
    $studioType = 'магазина';
}

$action = $isNew ? 'Регистрация' : 'Редактирование';

$this->title = $action . ' ' . $studioType;
?>
<main>
    <div class="panel big-panel">
        <?php
        $form = ActiveForm::begin([
            'id' => 'studio-form',
            'fieldConfig' => [
                'template' => $fieldTemplate,
            ],
        ]);
        ?>
        <div class="panel-header">
            <?php
            $studioType = $studioForm->getType();
            $studioTypeLabel = $studioType === 'atelier' ? 'ателье' : 'магазина';
            echo Html::beginTag('div', ['class' => 'icon-circle ' . $studioType]);
                echo Html::tag('i');
            echo Html::endTag('div');
            echo Html::tag('h2', $action . ' ' . $studioTypeLabel);
            ?>
        </div>
        <div class="panel-body">
            <ul>
                <li>
                    <?= $form->errorSummary($studioForm, [
                        'header' => ''
                    ])
                    ?>
                </li>
                <?php
                if ($isNew) {
// TODO сделать справа область с названием, как в личном кабинете под фотографией,
// чтобы пользователь сразу видел, как будет выглядеть надпись. Заполнять с пом-ю Js.
                    echo Html::beginTag('li');
                    echo $form->field($studioForm, 'name')
                        ->label($studioLabel);
                    echo Html::endTag('li');
                }
                echo '<script>' . new JsExpression('var selectedCountry;') . '</script>';
                ?>
                <li>
                    <?= $form->field($studioForm, 'countryName', [
                            'template' => $requiredFieldTemplate,
                        ])->widget(AutoComplete::className(),
                        [
                            'model' => $studioForm,
                            'attribute' => 'countryName',
                            'options' => [
                                'class' => 'form-control',
                                'title' => 'Страна',
                                'placeholder' => 'Начните вводить название страны'
                            ],
                            'clientOptions' => [
                                'source' => Url::toRoute('/get-country-list'),
                                'select' => new JsExpression("function( event, ui ) {
                                        $('#studioform-countryid').val(ui.item.id);
                                        document.selectedCountryId = ui.item.id;
                                        successCountry = true;
                                        $('.form-group.field-studioform-countryname').removeClass('has-error').addClass('has-success');
                                        checkCities(ui.item.id, 'studioform-cityname', '".Url::toRoute('/get-city-list') . "?countryId=' + ui.item.id);
                                    }"),
                                'change' => new JsExpression("function( event, ui ) {
                                        successCountry = false
                                    }"),
                                'close' => new JsExpression("function( event, ui ) {
                                        if (successCountry == false) {
                                        $('.form-group.field-studioform-countryname').removeClass('has-success').addClass('has-error');
                                        }
                                    }"),
                            ]
                        ])
                    ?>
                    <?= Html::activeHiddenInput($studioForm, 'countryId') ?>
                </li>
                <li>
                    <?= $form->field($studioForm, 'cityName', [
                            'template' => $requiredFieldTemplate,
                        ])->widget(AutoComplete::className(),
                        [
                                'model' => $studioForm,
                                'attribute' => 'cityName',
                                'options' => [
                                    'class' => 'form-control',
                                    'title' => 'Город',
                                    'placeholder' => 'Начните вводить название города',
                                    'disabled' => 'disabled'
                                ],
                                'clientOptions' => [
                                    'source' => new JsExpression("function(request, response) {
                                        if ($('#studioform-countryid').val()) {
                                            $.ajax({
                                                url: '/get-city-list/',
                                                dataType: 'json',
                                                data: {
                                                    countryId: $('#studioform-countryid').val(),
                                                    term: request.term
                                                },
                                                success: function(data) {
                                                    response(data);
                                                }
                                            });
                                        } else {
                                            response([]);
                                        }
                                    }"),
                                    'select' => new JsExpression("function( event, ui ) {
                                        $('#studioform-cityid').val(ui.item.id);
                                        successCity = true;
                                        $('.form-group.field-studioform-cityname').removeClass('has-error').addClass('has-success');
                                    }"),
                                    'change' => new JsExpression("function( event, ui ) {
                                        successCity = false
                                    }"),
                                    'close' => new JsExpression("function( event, ui ) {
                                        if (successCity == false) {
                                        $('.form-group.field-studioform-cityname').removeClass('has-success').addClass('has-error');
                                        }
                                    }"),
                                ],
                        ])
                    ?>
                    <?= Html::activeHiddenInput($studioForm, 'cityId') ?>
                </li>
                <!-- TODO раскомментить, когда добавится поддержка отображения валюты на всех страницах, пока только рубли -->
<!--                <li>
                    //= $form->field($studioForm, 'currencyId', [
//                            'template' => $selectTemplate,
//                        ])->dropDownList(
//                            ArrayHelper::map($studioForm->getCurrencyList(), 'id', 'code')
//                        )
//                    
                </li>-->
                <li>
                    <?= $form->field($studioForm, 'address')->textInput([
//                        'placeholder' => 'требуется для отображения на карте'
                    ]) ?>
                </li>
                <li>
                    <?= $form->field($studioForm, 'phone')->textInput([
                        'placeholder' => '+7(921)633-45-45'
                    ]) ?>
                </li>
                <li class="heighCol checkboxList">
                    <div class="left-col">Способ доставки</div>
                    <div class="center-col">
                        <ul>
                        <?php
                        $deliveryList = $studioForm->getDeliveryList();
                        foreach ($deliveryList as $delivery) {
                            $id = 'studioform-deliveryList_' . $delivery['id'];
                            $checked = ($studioForm->deliveryList && in_array($delivery['id'], $studioForm->deliveryList));
                            echo Html::beginTag('li');
                                echo Html::beginTag('span', ['class' => 'icon-line']);
                                    echo Html::checkbox('StudioForm[deliveryList][]', $checked, [
                                        'id' => $id,
                                        'value' => $delivery['id']
                                    ]);
                                    echo Html::beginTag('label', ['for' => $id, 'class' => 'size16']);
                                        echo Html::tag('i', '', ['class' => 'icon ' . $delivery['css_class']]);
                                        echo Html::tag('span', $delivery['name_ru']);
                                    echo Html::endTag('label');
                                echo Html::endTag('span');
                            echo Html::endTag('li');
                        }
                        ?>
                        </ul>
                    </div>
<!-- TODO сделать добавление нескольких своих способов. При этом придется вручную сверстать инпуты. -->
                    <?= $form->field($studioForm, 'custom_delivery')->textInput([
                            'placeholder' => 'Другой способ доставки',
                            'class' => 'form-control addable',
                        ])
                        ->label('')
                    ?>
                </li>
                <li class="heighCol checkboxList">
                    <div class="left-col">Способ оплаты</div>
                    <div class="center-col">
                        <ul>
                            <?php
                            $paymentList = $studioForm->getPaymentList();
                            foreach ($paymentList as $payment) {
                                $iconClass = strtolower(str_replace(' ', '-', $payment['name_en']));
                                $id = 'studioform-paymentList_' . $payment['id'];
                                $checked = ($studioForm->paymentList && in_array($payment['id'], $studioForm->paymentList));
                                echo Html::beginTag('li');
                                echo Html::beginTag('span', ['class' => 'icon-line']);
                                echo Html::checkbox('StudioForm[paymentList][]', $checked, [
                                    'id' => $id,
                                    'value' => $payment['id']
                                ]);
                                echo Html::beginTag('label', ['for' => $id, 'class' => 'size16']);
                                echo Html::tag('i', '', ['class' => 'icon ' . $iconClass]);
                                echo Html::tag('span', $payment['name_ru']);
                                echo Html::endTag('label');
                                echo Html::endTag('span');
                                echo Html::endTag('li');
                            }
                            ?>
                        </ul>
                    </div>
                    <?= $form->field($studioForm, 'custom_payment')->textInput([
                            'placeholder' => 'Другой способ оплаты',
                            'class' => 'form-control addable'
                        ])
                        ->label('')
                    ?>
                </li>
                <li>
                    <?= $form->field($studioForm, 'slogan') ?>
                </li>
                <li class="heighCol">
                    <?= $form->field($studioForm, 'description')
                        ->textArea([
                            'rows' => '4',
                            'placeholder' => 'Здесь вы можете дать описание вашего ' . $studioTypeLabel . '. Чем подробнее описание, тем больше шанс, что вас найдут через Яндекс или Google.',
                        ])
                    ?>
                </li>
            </ul>
        </div>
        <div class="panel-footer">
            <div class="button yellow" id="register-action">
                <div class="low-layer"></div>
                <button type="submit"><?= $isNew ? 'Завершить регистрацию' : 'Сохранить' ?></button>
            </div>
            <?php
            if ($isNew) {
                echo $form->field($studioForm, 'acceptAgreement', [
                    'template' => '{input} {label}',
                    'options' => ['id' => 'acceptAgreement']
                ])->checkbox([], false);
            }
            ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <?php if ($isNew): ?>
    <div class="dashed scissor-left"></div>
    <div id="agreement-block">
        <h4>Пользовательское соглашение</h4>
        <div id="agreement-text">
            <?= $this->renderFile('@app/views/parts/registration_studio_agreement.php') ?>
        </div>
        <div class="how-be-this">
            <span class="darker"><span>↓</span> <a href="#agreement-block" class="dashed-underline">Развернуть</a></span>
        </div>
    </div>
    <div class="dashed scissor-right"></div>
    <?php endif ?>
</main>