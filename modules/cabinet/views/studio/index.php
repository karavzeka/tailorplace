<?php
/**
 * Страница списка товаров в кабинете студии
 *
 * @var $Studio app\modules\studio\models\Studio
 * @var $Goods app\modules\good\models\Good
 * @var $section string|boolean
 * @var $orderBy string
 * @var $direction string
 * @var $currentThreshold array
 */

use app\modules\good\models\Photo;
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/cabinet-studio-index.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/cabinet-studio-index.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = $studioType;

$tableHeader = [
    'name' => [
        'class' => 'name',
        'text' => 'Название',
        'dir' => '',
    ],
    'price' => [
        'class' => 'price',
        'text' => 'Цена',
        'dir' => '',
    ],
    'date' => [
        'class' => 'date',
        'text' => 'Дата',
        'dir' => '',
    ],
];

if (isset($tableHeader[$orderBy])) {
    $tableHeader[$orderBy]['dir'] = $direction;
}

$createAlert = Yii::$app->session->getFlash('studioCreated' , '');
?>
<?php
if ($createAlert) {
    echo '<div class="alert success">' . $createAlert . '</div>';
}
?>
<h2><?= $studioType ?></h2>
    <div><a href="<?= Url::toRoute('/studio/' . $Studio->id) ?>" class="lighter">Посмотреть на <?= $Studio->type == 'atelier' ? 'свое ателье' : 'свой магазин' ?></a></div>
<div class="over-table">
	<menu class="<?= $Studio->type ?>">
        <!-- TODO витрина -->
<!--		<li>-->
<!--			--><?php //if ($section != 'vitrine'): ?>
<!--				<a class="dashed-underline" href="--><?//= '/cabinet/studio/'  . 'vitrine/' . ($order ? $order . '/' : '') ?><!--">Витрина</a>-->
<!--			--><?php //else: ?>
<!--				<span class="bold-underline size16">Витрина</span>-->
<!--			--><?php //endif ?>
<!--		</li>-->
        <!-- TODO скидки добавить -->
<!--		<li>-->
<!--			--><?php //if ($section != 'sale'): ?>
<!--				<a href="--><?//= '/cabinet/studio/' . 'sale/' . ($order ? $order . '/' : '') ?><!--">Скидки</a>-->
<!--			--><?php //else: ?>
<!--				<span class="bold-underline">Скидки</span>-->
<!--			--><?php //endif ?>
<!--		</li>-->
		<li>
			<?php if ($section && $section != 'all'): ?>
				<a class="dashed-underline" href="">Все товары</a>
			<?php else: ?>
				<span class="bold-underline size16">Все товары</span>
			<?php endif ?>
		</li>
	</menu>
    <div class="add-button-wrapper">
        <?php
            if (count($Goods) < $currentThreshold['goodsLimit']) {
                // Нормальная кнопка
                echo Html::beginTag('div', [
                    'id' => 'add-good-button',
                    'class' => 'button yellow'
                ]);
                    echo '<div class="low-layer"></div>';
                    echo Html::a('<i class="icon good-white"></i><span>Добавить товар</span>', Url::toRoute('add-good'), [
                        'data-type' => 'button',
                        'class' => 'icon-line'
                    ]);
                echo Html::endTag('div');
            } else {
                // Задизейбленная кнопка
                echo Html::beginTag('div', [
                    'id' => 'add-good-button',
                    'class' => 'button disable'
                ]);
                    echo '<div class="low-layer"></div>';
                    echo Html::button('<i class="icon good"></i><span>Добавить товар</span>', [
                        'disabled' => 'disabled',
                        'class' => 'icon-line'
                    ]);
                echo Html::endTag('div');
                echo '<div id="need-more" class="icon-line">';
                    echo '<span class="italic">Нужно еще ' . ($currentThreshold['threshold'] - $currentThreshold['countOfDeals']) . ' продаж</span> ';
                // TODO поправить ссылку на раздел помощи (если надо)
                    echo Html::a('?', Url::toRoute('/help/goods_limit/'), ['class' => 'icon-circle icon-circle-text little-help']);
                echo '</div>'; //<div id="need-more" class="icon-line">
            }
        ?>
    </div>
</div>
<div class="table-header">
    <?php
    foreach ($tableHeader as $name => $col) {
        echo Html::beginTag('div', ['class' => $col['class']]);
            echo Html::beginTag('div');
                echo Html::tag('a', $col['text'], ['href' => Url::current(['order' => $name, 'dir' => $col['dir']]), 'class' => 'dashed-underline']);
                if ($orderBy == $name) {
                    $class = $col['dir'] == 'DESC' ? 'caret top' : 'caret';
                    echo ' ' . Html::tag('span', '', ['class' => $class]);
                }
            echo Html::endTag('div');
            echo Html::tag('div');
        echo Html::endTag('div');
    }
    ?>
<!-- TODO добавить кол-во продаж -->
<!--	<div>-->
<!--		<div><i class="icon icon-price"></div>-->
<!--		<div></div>-->
<!--	</div>-->
<!-- TODO скидки добавить -->
<!--	<div>-->
<!--		<div><i class="icon icon-sale"></div>-->
<!--		<div></div>-->
<!--	</div>-->
</div>

<?php if (count($Goods) == 0): ?>
    <p class="not-goods">У вас еще нет товаров</p>
<?php else: ?>
    <ul class="good-list">
        <?php foreach ($Goods as $Good): ?>
            <li>
                <div class="main-info">
                    <a href="<?= '/goods/' . $Good->id ?>/"><img class="img-link" src="<?= count($Good->photo) > 0 ? $Good->photo[0]->getSrc('_min') : Photo::getNoPhotoSrc('_min') ?>" width="48" height="48"></a>
                    <div class="name-box">
                        <span><a href="<?= '/goods/' . $Good->id ?>/" class="size16"><?= Html::encode($Good->name) ?></a></span>
                        <!-- TODO добавить возможность добавлять товар на витрину -->
                        <!-- <div class="vitrine-button on-vitrine">На витрине</div> -->
                    </div><div class="price-box">
                        <span class="price-normal"><?= Yii::$app->numberHelper->numberToPrice($Good->price) ?></span> <span>руб.</span>
                    </div><div class="date-box"><?= date('d.m.Y', strtotime($Good->create_time)) ?></div>
                </div>
                <div class="sub-info">
                    <ul>
                        <?php
                        foreach ($Good->category as $Category) {
                            echo Html::beginTag('li', ['class' => 'icon-line']);
                                echo Html::tag('i', '',['class' => 'icon ' . $Category->css_class]);
                                echo Html::tag('span', $Category->name_ru);
                            echo Html::endTag('li');
                        }
                        ?>
                    </ul>
                    <div class="redact">
                        <span class="icon-line"><i class="icon edit"></i><a href="/cabinet/studio/edit-good/<?= $Good->id ?>/" class="dashed-underline">Редактировать</a></span>
                        <span class="icon-line"><i class="icon delete-red"></i><a class="delete-good" href="/cabinet/studio/delete-good/<?= $Good->id ?>/" class="dashed-underline">Удалить</a></span>
                    </div>
                </div>
            </li>
        <?php endforeach?>
    </ul>
    <!-- TODO если пагинатор здесь не нужен, то и совсем убрать комментарии -->
    <!--<div class="pagination">
        <div class="dashed scissor-right"></div>
        <ul class="pages size16">
            <li class="link active">1</li><li class="link"><a href="" class="size16">2</a></li><li class="link"><a href="" class="size16">3</a></li><li>...</li><li class="link"><a href="" class="size16">9</a></li><li class="link"><a href="" class="size16">11</a></li><li class="link"><a href="" class="size16">12</a></li>
        </ul>
        <form method="get" class="page-selector">
            <div class="form-group">
                <label>Перейти <input type="text" name="page" class="form-control"></label>
                <div id="pagination-button" class="button yellow">
                    <div class="low-layer"></div>
                    <button type="submit">Ok</button>
                </div>
            </div>
        </form>
        <div class="show-word size16">Показывать:</div>
        <ul class="count-on-page size16 darker">
            <li><a href="">15</a></li>
            <li><a href="">30</a></li>
            <li><a href="">60</a></li>
        </ul>
    </div>-->
<?php endif ?>