<?php

namespace app\modules\cabinet\controllers;

use app\models\OrderStatus;
use app\modules\good\models\Good;
use Yii;
use app\controllers\CommonController;
use app\models\Order;
use app\models\OrderContent;
use app\modules\users\models\User;
use app\modules\users\models\Basket;
use app\modules\studio\models\Studio;
use app\modules\good\models\Photo;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class DefaultController extends CommonController
{
    public $breadcrumbItems = [
        'index' => 'not add',
        'basket' => 'Корзина',
        'place-order' => 'Оформление заказа',
        'orders' => 'Заказы',
        'orders-archive' => 'Архив заказов',
        'order' => 'not add',
        'paid-services' => 'Дополнительные услуги',
        'statistic' => 'Статистика',
    ];

	public $layout = 'cabinet';

	public function actionIndex()
	{
        $User = Yii::$app->user->identity;
        if ($User->hasStudio()) {
            $action = 'index-studio';
        } else {
            $action = 'index-user';
        }
		return $this->render($action, [
            'User' => $User
        ]);
	}

    /**
     * Страница корзины
     *
     * @return string
     */
    public function actionBasket()
    {
        $User = Yii::$app->user->identity;

        if ($User->isUnconfirmed()) {
            Yii::$app->response->setStatusCode(403);
            return $this->render('//other/simpleMessage', [
                'message' => '
<p>Корзина для вас пока закрыта. Вы не подтвердили регистрацию.</p>
<p>Письмо с инструкцией так и не пришло? <a href="/cabinet/repeat-activation-key/">Отправить письмо еще раз.</a></p>'
            ]);
        }

        $basketGoods = $User->getBasket()
            ->select('
                `basket`.`quantity` AS `quantity`,
                `goods`.`id` AS `good_id`,
                `goods`.`name` AS `good_name`,
                `goods`.`studio_id` AS `studio_id`,
                `goods`.`price` AS `price`,
            ')
            ->innerJoin('goods', '`goods`.`id` = `basket`.`good_id`')
            ->asArray()
            ->all();

        $orderList = [];
        $goodIds = [];
        $totalPrice = 0;
        foreach ($basketGoods as $good) {
            $goodIds[] = (int)$good['good_id'];
            $orderList[$good['studio_id']]['goods'][] = $good;
            $orderList[$good['studio_id']]['total_price'] = isset($orderList[$good['studio_id']]['total_price']) ? ($orderList[$good['studio_id']]['total_price'] + $good['price'] * $good['quantity']) : $good['price'] * $good['quantity'];
            $totalPrice += $good['price'] * $good['quantity'];
        }

        $studioIds = array_keys($orderList);

        $Studios = Studio::findAll($studioIds);
        $GoodPhotos = Photo::findAll([
                'good_id' => $goodIds,
                'main' => 1
            ]);
        $goodPhotosArray = [];
        foreach ($GoodPhotos as $Photo) {
            $goodPhotosArray[$Photo->good_id] = $Photo;
        }

        $totalValues = [
            'count' => count($studioIds),
            'price' => $totalPrice
        ];
        return $this->render('basket', [
            'Studios' => $Studios,
            'orderList' => $orderList,
            'goodPhotosArray' => $goodPhotosArray,
            'totalValues' => $totalValues,
        ]);
    }

    /**
     * Страница с отображением данных для заказа непосрадственно перед оформлением заказа
     *
     * @return string
     */
    public function actionPlaceOrder()
    {
        $User = Yii::$app->user->identity;
        $userId = $User->id;
        if (!$userId) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        if ($User->isUnconfirmed()) {
            Yii::$app->response->setStatusCode(403);
            return $this->render('//other/simpleMessage', [
                'message' => '
<p>Данный функционал для вас пока закрыт. Вы не подтвердили регистрацию.</p>
<p>Письмо с инструкцией так и не пришло? <a href="/cabinet/repeat-activation-key/">Отправить письмо еще раз.</a></p>'
            ]);
        }

        // в случае POST запроса обновить данные
        if (Yii::$app->request->isPost && Yii::$app->request->validateCsrfToken()) {
            $answer = $this->prepareOrders($userId);
            if ($answer['status'] == 'error') {
                $SiteController = Yii::$app->createController('site');
                return $SiteController[0]->runAction(strval($answer['code']));
            }
        }

        // Отображение заказов перед подтверждением
        $dbName = Yii::$app->params['tailor_db'];
        $sql = "
            SELECT
                `st`.`name` AS `studio_name`,
                `st`.`type` AS `studio_type`,
                `po`.`user_id` AS `user_id`,
                `po`.`studio_id` AS `studio_id`,
                `po`.`delivery_id` AS `delivery_id`,
                `po`.`payment_id` AS `payment_id`,
                `po`.`custom_delivery` AS `custom_delivery`,
                `po`.`custom_payment` AS `custom_payment`,
                `dl`.`name_ru` AS `delivery_name`,
                `dl`.`css_class` AS `delivery_css`,
                `pl`.`name_ru` AS `payment_name`,
                `pl`.`css_class` AS `payment_css`
            FROM
                `" . $dbName . "`.`prepared_orders` AS `po`
            INNER JOIN
                `" . $dbName . "`.`studio` AS `st`
            ON
                `po`.`studio_id` = `st`.`id`
            LEFT JOIN
                `" . $dbName . "`.`delivery_list` AS `dl`
            ON
                `po`.`delivery_id` = `dl`.`id`
            LEFT JOIN
                `" . $dbName . "`.`payment_list` AS `pl`
            ON
                `po`.`payment_id` = `pl`.`id`
            WHERE
                `po`.`user_id` = " . $userId;
        $preparedOrders = Yii::$app->db->createCommand($sql)->queryAll();
        $orderStudios = ArrayHelper::getColumn($preparedOrders, 'studio_id');

        // получение товаров из корзины в соответствии с какими студиями будет заказ
        if (count($orderStudios) > 0) {
            $basketGoods = $User->getBasket()
                ->select('
                `basket`.`quantity` AS `quantity`,
                `goods`.`id` AS `good_id`,
                `goods`.`name` AS `good_name`,
                `goods`.`studio_id` AS `studio_id`,
                `goods`.`price` AS `price`,
            ')
                ->innerJoin('goods', '`goods`.`id` = `basket`.`good_id`')
                ->where(['`studio_id`' => $orderStudios])
                ->asArray()
                ->all();
        } else {
            $basketGoods = [];
        }

        // подготовка массива заказов с вложенными массивами товаров
        $orderList = [];
        $goodIds = [];
        $totalPrice = 0;
        foreach ($preparedOrders as $order) {
            $goodsInOrder = [];
            $orderPrice = 0;
            foreach ($basketGoods as $good) {
                if ($good['studio_id'] == $order['studio_id']) {
                    $goodsInOrder[] = $good;
                    $goodIds[] = $good['good_id'];
                    $totalPrice += $good['price'] * $good['quantity'];
                    $orderPrice += $good['price'] * $good['quantity'];
                }
            }

            // определение способа доставки
            if (!empty($order['delivery_name'])) {
                $deliveryName = $order['delivery_name'];
                $deliveryCss = $order['delivery_css'];
            } elseif (!empty($order['custom_delivery'])) {
                $deliveryName = $order['custom_delivery'];
                $deliveryCss = 'other';
            } else {
                $deliveryName = '';
                $deliveryCss = '';
            }

            // определение способа оплаты
            if (!empty($order['payment_name'])) {
                $paymentName = $order['payment_name'];
                $paymentCss = $order['payment_css'];
            } elseif (!empty($order['custom_payment'])) {
                $paymentName = $order['custom_payment'];
                $paymentCss = 'other';
            } else {
                $paymentName = '';
                $paymentCss = '';
            }

            $orderList[] = [
                'studioId' => $order['studio_id'],
                'studioName' => $order['studio_name'],
                'studioType' => $order['studio_type'],
                'deliveryName' => $deliveryName,
                'paymentName' => $paymentName,
                'deliveryCss' => $deliveryCss,
                'paymentCss' => $paymentCss,
                'totalPrice' => $orderPrice,
                'goods' => $goodsInOrder
            ];
        }

        // получение фотографий товаров
        $GoodPhotos = Photo::findAll([
            'good_id' => $goodIds,
            'main' => 1
        ]);
        $goodPhotosArray = [];
        foreach ($GoodPhotos as $Photo) {
            $goodPhotosArray[$Photo->good_id] = $Photo;
        }

        return $this->render('placeOrder', [
            'orderList' => $orderList,
            'goodPhotosArray' => $goodPhotosArray,
            'totalPrice' => $totalPrice
        ]);
    }

    /**
     * Конечное подтверждение создания заказа
     */
    public function actionEndPlaceOrder()
    {
        $User = Yii::$app->user->identity;
        $userId = $User->id;
        $Auth = Yii::$app->getAuthManager();
        if (!$userId || !$Auth->checkAccess($userId, $Auth::PERMISSION_CREATE_ORDER)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        // обработка только POST запроса пришедшего со страницы оформления заказа
        if (Yii::$app->request->isPost && preg_match('/place-order\/?$/', Yii::$app->request->referrer)) {
            $phone = Yii::$app->request->post('phone');
            $phone = trim($phone);
            $postIndex = Yii::$app->request->post('post-index');
            $postIndex = trim($postIndex);
            $address = Yii::$app->request->post('address');
            $address = trim($address);
            $orderComments = Yii::$app->request->post('comment');

            $dbName = Yii::$app->params['tailor_db'];
            $sql = "
                SELECT
                    `st`.`user_id` AS `seller_id`,
                    `po`.`user_id` AS `user_id`,
                    `po`.`studio_id` AS `studio_id`,
                    `po`.`delivery_id` AS `delivery_id`,
                    `po`.`payment_id` AS `payment_id`,
                    `po`.`custom_delivery` AS `custom_delivery`,
                    `po`.`custom_payment` AS `custom_payment`
                FROM
                    `" . $dbName . "`.`prepared_orders` AS `po`
                INNER JOIN
                    `" . $dbName . "`.`studio` AS `st`
                ON
                    `po`.`studio_id` = `st`.`id`
                WHERE
                    `po`.`user_id` = " . $userId;
            $preparedOrders = Yii::$app->db->createCommand($sql)->queryAll();
            if (count($preparedOrders) == 0) {
                // подготовленных заказов нет
                $SiteController = Yii::$app->createController('site');
                return $SiteController[0]->runAction('400');
            }
            $orderStudios = ArrayHelper::getColumn($preparedOrders, 'studio_id');

            // выбираем товары из корзины для подготовленных заказов
            $basketGoods = $User->getBasket()
                ->select('
                    `basket`.`quantity` AS `quantity`,
                    `goods`.`id` AS `good_id`,
                    `goods`.`name` AS `good_name`,
                    `goods`.`studio_id` AS `studio_id`,
                    `goods`.`price` AS `price`,
                ')
                ->innerJoin('goods', '`goods`.`id` = `basket`.`good_id`')
                ->where(['`studio_id`' => $orderStudios])
                ->asArray()
                ->all();

            $createdOrders = [];
            $Transaction = Yii::$app->db->beginTransaction();
            try {
                // создание заказов
                $orderContentRows = [];
                $goodsIdToDeleteFromBasket = [];
                foreach ($preparedOrders as $order) {
                    $Order = new Order();
                    $Order->seller_id = $order['seller_id'];
                    $Order->buyer_id = $order['user_id'];
                    $Order->status_id = 1;  // order_status_list: В ожидании
                    $Order->delivery_id = $order['delivery_id'];
                    $Order->payment_id = $order['payment_id'];
                    $Order->custom_delivery = $order['custom_delivery'];
                    $Order->custom_payment = $order['custom_payment'];
                    $Order->mark_for_seller = 1;
                    if ($phone) {
                        $Order->buyer_phone = $phone;
                    }
                    if ($postIndex) {
                        $Order->buyer_index = $postIndex;
                    }
                    if ($address) {
                        $Order->buyer_address = $address;
                    }
                    if (isset($orderComments[$order['studio_id']]) && !empty($orderComments[$order['studio_id']])) {
                        $Order->comment = trim($orderComments[$order['studio_id']]);
                    }
                    $Order->save();
                    $createdOrders[] = [
                        'order_id' => $Order->id,
                        'seller_id' => $Order->seller_id,
                    ];

                    // создание записей для содержимого заказов
                    foreach ($basketGoods as $basketGood) {
                        if ($basketGood['studio_id'] == $order['studio_id']) {
                            $orderContentRows[] = [
                                'order_id' => $Order->id,
                                'good_id' => $basketGood['good_id'],
                                'good_name' => $basketGood['good_name'],
                                'price' => $basketGood['price'],
                                'quantity' => $basketGood['quantity'],
                            ];
                            $goodsIdToDeleteFromBasket[] = $basketGood['good_id'];
                        }
                    }
                }

                // пакетное сохранение содержимого заказов
                Yii::$app->db->createCommand()
                    ->batchInsert('`' . $dbName . '`.`order_content`', [
                        '`order_id`',
                        '`good_id`',
                        '`good_name`',
                        '`price`',
                        '`quantity`',
                    ], $orderContentRows)
                    ->execute();

                // очистка таблицы подготовленных заказов
                Yii::$app->db->createCommand()->delete('`' . $dbName . '`.`prepared_orders`', [
                    'user_id' => $userId
                ])->execute();

                // очистка корзины для оформленных заказов
                Yii::$app->db->createCommand()->delete('`' . $dbName . '`.`basket`', [
                    'user_id' => $userId,
                    'good_id' => $goodsIdToDeleteFromBasket
                ])->execute();

                $Transaction->commit();

                // Отправка уведомления исполнителям
                $sellerIds = ArrayHelper::getColumn($createdOrders, 'seller_id');
                $sellers = User::find()
                    ->select(['id', 'email'])
                    ->where(['id' => $sellerIds])
                    ->asArray()
                    ->all();
                $sellers = ArrayHelper::index($sellers, 'id');
                foreach ($createdOrders as $createdOrder) {
                    Yii::$app->mailer->compose('@app/mail/newOrder', ['orderId' => $createdOrder['order_id']])
                        ->setTo($sellers[$createdOrder['seller_id']]['email'])
                        ->setSubject('Вам поступил новый заказ')
                        ->send();
                }

                Yii::$app->session->setFlash('createOrderSuccess', 'Заказ успешно оформлен');
                $redirectUrl = $User->hasStudio() ? '/cabinet/orders/purchase/' : '/cabinet/orders/';
                return $this->redirect($redirectUrl);
            } catch (\Exception $e) {
                $Transaction->rollBack();
                Yii::$app->session->setFlash('createOrderError', 'При сохранении заказа произошла ошибка');
                return $this->runAction('place-order');
            }
        } else {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('400');
        }
    }

    /**
     * Страница активных заказов
     *
     * @return string
     */
    public function actionOrders()
    {
        $User = Yii::$app->user->identity;
        if ($User->hasStudio()) {
            // для студии необходимо показать завершенные, но не оцененные студией заказы
            $orderStatusesCondidion = '`orders`.`status_id` NOT IN (2, 3, 4) OR (`orders`.`status_id` = 2 AND `orders`.`seller_evaluation` IS NULL AND seller_id = ' . (int)$User->id . ')';
        } else {
            $orderStatusesCondidion = '`orders`.`status_id` NOT IN (2, 3, 4)';
        }
        $pageHtml = $this->generateOrdersPage($orderStatusesCondidion, '/cabinet/orders');
        return $pageHtml;
    }

    /**
     * Архив заказов
     *
     * @return string
     */
    public function actionOrdersArchive()
    {
        $User = Yii::$app->user->identity;
        if ($User->hasStudio()) {
            // для студии необходимо показать завершенные, но не оцененные студией заказы
            $orderStatusesCondidion = '`orders`.`status_id` IN (3, 4) OR (`orders`.`status_id` = 2 AND (seller_id <> ' . (int)$User->id . ' OR `orders`.`seller_evaluation` IS NOT NULL))';
        } else {
            $orderStatusesCondidion = '`orders`.`status_id` IN (2, 3, 4)';
        }
        $pageHtml = $this->generateOrdersPage($orderStatusesCondidion, '/cabinet/orders/archive');

        return $pageHtml;
    }

    /**
     * Генерация страницы со списком заказов (как активных, так и архива)
     *
     * @param $orderStatusesCondidion
     * @return string
     * @throws Exception
     */
    private function generateOrdersPage($orderStatusesCondidion, $baseUrl)
    {
        $dbName = Yii::$app->params['tailor_db'];
        $sortFields = [
            'default' => 'number',
            'number' => '`' . $dbName . '`.`orders`.`id`',
            'status' => '`' . $dbName . '`.`order_status_list`.`name_ru`',
            'price' => '`' . $dbName . '`.`orders`.`id`',  // сартировка будет производится на уровне php
            'date' => '`' . $dbName . '`.`orders`.`id`',   // id и create_time дадут одинаковую последовательность
        ];

        $User = Yii::$app->user->identity;
        if ($User->hasStudio()) {
            $typeOfOrder = Yii::$app->request->get('type');
            $typeOfOrder = $typeOfOrder == 'purchase' ? 'buy' : 'sell';
        } else {
            $typeOfOrder = 'buy';
        }

        $Sorter = new \app\components\SortController('orderList_' . $typeOfOrder, $sortFields);
        $sortParams = $Sorter->getSort();
        $newDirection = $sortParams['sortDirection'] == 'DESC' ? 'ASC' : 'DESC';

        $ordersArray = $User->getOrder($typeOfOrder)
            ->select('
                `orders`.`id` AS `id`,
                `orders`.`seller_id` AS `seller_id`,
                `orders`.`buyer_id` AS `buyer_id`,
                `orders`.`seller_evaluation` AS `seller_evaluation`,
                `orders`.`buyer_evaluation` AS `buyer_evaluation`,
                `orders`.`status_id` AS `status_id`,
                `orders`.`create_time` AS `create_time`,
                `orders`.`custom_delivery` AS `custom_delivery`,
                `orders`.`custom_payment` AS `custom_payment`,
                `delivery_list`.`name_ru` AS `delivery`,
                `delivery_list`.`css_class` AS `delivery_css`,
                `payment_list`.`name_ru` AS `payment`,
                `payment_list`.`css_class` AS `payment_css`,
                `order_status_list`.`name_ru` AS `status`,
                `order_status_list`.`css_class` AS `css_class`
                ')
            ->leftJoin('delivery_list', '`delivery_list`.`id` = `orders`.`delivery_id`')
            ->leftJoin('payment_list', '`payment_list`.`id` = `orders`.`payment_id`')
            ->innerJoin('order_status_list', '`order_status_list`.`id` = `orders`.`status_id`')
            ->andWhere($orderStatusesCondidion)
            ->orderBy($sortParams['sortQueryPart'])
            ->asArray()
            ->all();
        $ordersArray = ArrayHelper::index($ordersArray, 'id');
        $Orders = [];
        $ordersIds = [];
        foreach ($ordersArray as $key => $orderData) {
            // если не определен общий способ доставки, то подставляется кастомный
            if (empty($orderData['delivery'])) {
                $ordersArray[$key]['delivery'] = $orderData['custom_delivery'];
                $ordersArray[$key]['delivery_css'] = 'other';
            }
            // если не определен общий способ оплаты, то подставляется кастомный
            if (empty($orderData['payment'])) {
                $ordersArray[$key]['payment'] = $orderData['custom_payment'];
                $ordersArray[$key]['payment_css'] = 'other';
            }

            // в шаблоне понядобятся методы модели заказа
            $OrederObj = new Order();
            $OrederObj->setAttributes($orderData, false);
            $Orders[$orderData['id']] = $OrederObj;
            $ordersIds[] = $orderData['id'];
        }

        $ordersContentInfo = [];
        if (count($ordersIds) > 0) {
            // снимим пометки с заказов, т.к. порльзователь их просмотрел
            if ($typeOfOrder == 'sell') {
                Order::updateAll(['mark_for_seller' => 0], ['id' => $ordersIds]);
            } else {
                Order::updateAll(['mark_for_buyer' => 0], ['id' => $ordersIds]);
            }

            // получение цен заказов
            $ordersContentInfoArray = Yii::$app->db->createCommand('
            SELECT
                `order_id`,
                SUM(`price` * `quantity`) AS `order_price`,
                COUNT(*) AS `count_of_goods`
            FROM
                `order_content`
            WHERE
                `order_id` IN (' . implode(',', $ordersIds) . ')
            GROUP BY
                `order_id`')
                ->queryAll();
            $ordersContentInfoArray = ArrayHelper::index($ordersContentInfoArray, 'order_id');
            foreach ($ordersContentInfoArray as $orderContentData) {
                $ordersContentInfo[$orderContentData['order_id']]['order_id'] = $orderContentData['order_id'];
                $ordersContentInfo[$orderContentData['order_id']]['order_price'] = Yii::$app->numberHelper->numberToPrice($orderContentData['order_price']);
                $ordersContentInfo[$orderContentData['order_id']]['count_of_goods'] = $orderContentData['count_of_goods'];
            }
        }

        // сортировка по цене
        if ($sortParams['sortField'] == 'price') {
            $tmpArrForSort = $ordersContentInfo;
            $tmpOrdersArr = $Orders;
            $Orders = [];
            ArrayHelper::multisort($tmpArrForSort, 'order_price', $sortParams['sortDirection'] == 'DESC' ? SORT_DESC : SORT_ASC, SORT_NUMERIC);

            foreach ($tmpArrForSort as $orderParams) {
                $Orders[$orderParams['order_id']] = $tmpOrdersArr[$orderParams['order_id']];
            }
            unset($tmpArrForSort);
            unset($tmpOrdersArr);
        }

        $dealerIdColumn = $typeOfOrder == 'buy' ? 'seller_id' : 'buyer_id'; // если мы покупаем, значит оппонент продает
        $dealerId = ArrayHelper::getColumn($ordersArray, $dealerIdColumn);

        $Dealers = User::find()->with('studio')->where(['id' => $dealerId])->all();
        $Dealers = ArrayHelper::index($Dealers, 'id');

        $StatusesList = OrderStatus::find()->all();
        $StatusesList = ArrayHelper::index($StatusesList, 'id');

        return $this->render('orders', [
            'User' => $User,
            'Orders' => $Orders,
            'ordersArray' => $ordersArray,
            'Dealers' => $Dealers,
            'typeOfOrder' => $typeOfOrder,
            'dealerIdColumn' => $dealerIdColumn,
            'ordersContentInfo' => $ordersContentInfo,
            'orderBy' => $sortParams['sortField'],
            'direction' => $newDirection,
            'StatusesList' => $StatusesList,
            'baseUrl' => $baseUrl,
        ]);
    }

    /**
     * Смена статуса заказа
     */
    public function actionChangeOrderStatus()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $orderId = (int)Yii::$app->request->post('orderId');
            $statusId = (int)Yii::$app->request->post('statusId');

            if (!$orderId || !$statusId) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $userId = Yii::$app->user->identity->id;

            $Order = Order::find()
                ->where('`id` = :id AND (`seller_id` = :seller_id OR `buyer_id` = :buyer_id)', [
                    ':id' => $orderId,
                    ':seller_id' => $userId,
                    ':buyer_id' => $userId
                ])
                ->one();

            if (!$Order) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $possibleStatuses = $Order->getPossibleStatuses();
            if (!in_array($statusId, $possibleStatuses)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            // проверка, должна ли с этим статусом прийти оценка
            $statusRules = Yii::$app->params['orderStatusRules'][$statusId];
            if (isset($statusRules['needEvaluation']) && $statusRules['needEvaluation']) {
                $evaluation = Yii::$app->request->post('evaluation');
                if (!in_array($evaluation, ['0', '0.2', '0.5', '0.8', '1'])) {
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return;
                }
                // выставление оценки
                if ($Order->seller_id == $userId) {
                    $Order->seller_evaluation = floatval($evaluation);
                } else {
                    $Order->buyer_evaluation = floatval($evaluation);
                }
            }

            // изменим статус заказа
            $Order->status_id = $statusId;

            // пометим заказ для партнера, если статус не "отозван" или "отклонен"
            if (!in_array($statusId, [OrderStatus::STATUS_REJECTED, OrderStatus::STATUS_WITHDRAWN])) {
                if ($Order->seller_id == $userId) {
                    $Order->mark_for_buyer = 1;
                } else {
                    $Order->mark_for_seller = 1;
                }
            }

            $result = $Order->save();
            if (!$result) {
                Yii::$app->response->setStatusCode(500);
                $this->sendJsonAnswer($response);
                return;
            }

            // сохранение статуса в истории
            $StatusHistoryItem = new \app\models\OrderStatusHistory();
            $StatusHistoryItem->order_id = $orderId;
            $StatusHistoryItem->status_id = $statusId;
            $StatusHistoryItem->save();

            $CurrentOrderStatus = OrderStatus::findOne($statusId);

            // Отправка уведомления о смене статуса
            $Seller = User::findOne($Order->seller_id);
            Yii::$app->mailer->compose('@app/mail/orderStatusChanged', [
                'orderId' => $Order->id,
                'Status' => $CurrentOrderStatus,
            ])
                ->setTo($Seller->email)
                ->setSubject('Статус заказа изменен')
                ->send();

            // получение новых возможных статусов
            $newPossibleStatuseIds = $Order->getPossibleStatuses();
            if ($newPossibleStatuseIds) {
                $newPossibleStatuses = OrderStatus::find()
                    ->select(['id', 'name_ru', 'css_class'])
                    ->where(['id' => $newPossibleStatuseIds])
                    ->asArray()
                    ->all();
                foreach ($newPossibleStatuses as $key => $status) {
                    $newStatusRules = Yii::$app->params['orderStatusRules'][$status['id']];
                    if (isset($newStatusRules['needEvaluation']) && $newStatusRules['needEvaluation']) {
                        $newPossibleStatuses[$key]['needEvaluation'] = true;
                    }
                }
            } else {
                $newPossibleStatuses = [];
            }

            $response = [
                'status' => 'ok',
                'orderStatusText' => $CurrentOrderStatus->name_ru,
                'orderStatusClass' => $CurrentOrderStatus->css_class,
                'possibleStatuses' => $newPossibleStatuses
            ];
            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Оценка заказа
     */
    public function actionOrderEvaluation()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $orderId = (int)Yii::$app->request->post('orderId');
            $evaluation = Yii::$app->request->post('evaluation');

            if (!$orderId || !in_array($evaluation, ['0', '0.2', '0.5', '0.8', '1'])) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $userId = Yii::$app->user->identity->id;

            $Order = Order::find()
                ->where('`id` = :id AND (`seller_id` = :seller_id OR `buyer_id` = :buyer_id)', [
                    ':id' => $orderId,
                    ':seller_id' => $userId,
                    ':buyer_id' => $userId
                ])
                ->one();

            if (!$Order) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            // проверка можно ли на этом статусе заказа ставить оценку
            $statusRules = Yii::$app->params['orderStatusRules'][$Order->status_id];
            if (!isset($statusRules['needEvaluation']) || !$statusRules['needEvaluation']) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            // выставление оценки
            if ($Order->seller_id == $userId) {
                if (!is_null($Order->seller_evaluation)) {
                    // уже проставлена оценка
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return;
                }

                $Order->seller_evaluation = floatval($evaluation);
                $partnerEvaluation = $Order->buyer_evaluation;
            } else {
                if (!is_null($Order->buyer_evaluation)) {
                    // уже проставлена оценка
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return;
                }

                $Order->buyer_evaluation = floatval($evaluation);
                $partnerEvaluation = $Order->seller_evaluation;
            }

            // сохранение оценки
            $result = $Order->save();
            if (!$result) {
                Yii::$app->response->setStatusCode(500);
                $this->sendJsonAnswer($response);
                return;
            }

            $response = [
                'status' => 'ok',
            ];
            if (Yii::$app->request->post('getPartnerEvaluation')) {
                $response['partnerEvaluation'] = $partnerEvaluation;
            }

            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Страница конкретного заказа
     *
     * @return string
     */
    public function actionOrder()
    {
        $orderId = (int)Yii::$app->request->get('orderId');

        $Order = Order::find()
            ->where(['id' => $orderId])
            ->with('delivery')
            ->with('payment')
        ->one();
        if (!$Order) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        $this->addBreadcrumbsItem(['label' => 'Заказы', 'url' => '/cabinet/orders/']);
        $this->addBreadcrumbsItem(['label' => 'Заказ №' . $orderId]);

        $User = Yii::$app->user->identity;
        if ($Order->seller_id == $User->id) {
            $iAmBuyer = false;
            $Partner = User::findOne($Order->buyer_id);
        } else {
            $iAmBuyer = true;
            $Partner = User::findOne($Order->seller_id);
        }

        $StatusesList = OrderStatus::find()->all();
        $StatusesList = ArrayHelper::index($StatusesList, 'id');

        // содержимое заказа
        $orderGoods = OrderContent::find()
            ->select('
                `order_content`.`good_id` AS `good_id`,
                `order_content`.`good_name` AS `good_name`,
                `order_content`.`price` AS `price`,
                `order_content`.`quantity` AS `quantity`,
                `goods`.`id` AS `exists_good`,
                `goods`.`name` AS `current_good_name`
            ')
            ->leftJoin('goods', '`order_content`.`good_id` = `goods`.`id`')
            ->where(['`order_content`.`order_id`' => $Order->id])
            ->asArray()
            ->all();
        $orderGoods = ArrayHelper::index($orderGoods, 'good_id');

        // получение фотографий товаров
        $Photos = Photo::find()->where([
            'good_id' => array_keys($orderGoods),
            'main' => 1
        ])->all();
        $Photos = ArrayHelper::index($Photos, 'good_id');

        return $this->render('order', [
            'Order' => $Order,
            'Partner' => $Partner,
            'iAmBuyer' => $iAmBuyer,
            'StatusesList' => $StatusesList,
            'orderGoods' => $orderGoods,
            'Photos' => $Photos,
        ]);
    }

    public function actionPaidServices()
    {
        return $this->render('paidServices');
    }

    /**
     * Страница статистики
     *
     * @return string
     */
    public function actionStatistic()
    {
        $User = Yii::$app->user->identity;
        $Orders = $User->getOrder()->andWhere(['status_id' => 2])->all();

        $sellsCount = 0;
        $purchasesCount = 0;
        $evaluationAsSeller = 0;
        $evaluationAsBuyer = 0;
        foreach ($Orders as $Order) {
            if ($Order->seller_id == $User->id) {
                $sellsCount++;
                $evaluationAsSeller += (float)$Order->buyer_evaluation;
            } else {
                $purchasesCount++;
                $evaluationAsBuyer += (float)$Order->seller_evaluation;
            }
        }
        $reputationAsSeller = $sellsCount > 0 ? $evaluationAsSeller / $sellsCount * 100 : 0;
        $reputationAsBuyer = $purchasesCount > 0 ? $evaluationAsBuyer / $purchasesCount * 100 : 0;
        $statisticArray = [
            'hasStudio' => false,
            'purchasesCount' => $purchasesCount,
            'reputationAsBuyer' => $reputationAsBuyer,
        ];

        if ($User->hasStudio()) {
            $currentThreshold = $User->studio->getCurrentThreshold($sellsCount);
            $statisticArray['hasStudio'] = true;
            $statisticArray['sellsCount'] = $sellsCount;
            $statisticArray['reputationAsSeller'] = $reputationAsSeller;
            $statisticArray['threshold'] = $currentThreshold['threshold'];
            $statisticArray['goodsLimit'] = $currentThreshold['goodsLimit'];
            $statisticArray['countOfGoods'] = $User->studio->getGood()->count();
        }


        return $this->render('statistic', [
            'statisticArray' => $statisticArray
        ]);
    }

    /**
     * Подготавливает некоторые данные для оформления заказа
     *
     * @param int $userId
     * @return array
     * @throws \yii\db\Exception
     */
    private function prepareOrders($userId)
    {
        // раз был новый пост из корзины, то надо удалить старые подготовленные заказы и добавить те, которые пришли из корзины
        $dbName = Yii::$app->params['tailor_db'];
        $sql = "DELETE FROM `" . $dbName . "`.`prepared_orders` WHERE `user_id` = " . $userId;
        Yii::$app->db->createCommand($sql)->execute();

        $GoodsInBasket = Basket::find()->where([
            'user_id' => $userId,
        ])->all();
        if (count($GoodsInBasket) == 0) {
            // каким-то образом отправлен POST при пустой корзине. Ошибку не выдавать, просто не обрабатывать запрос.
            return ['status' => 'ok'];
        }

        $studioId = (int)Yii::$app->request->post('studioId', 0);
        $deliveryList = Yii::$app->request->post('delivery', []);
        $paymentList = Yii::$app->request->post('payment', []);
        if ($studioId) {
            // передан id студии и подготовить заказ требуется только для нее
            $insertData = [
                'user_id' => $userId,
                'studio_id' => $studioId,
            ];

            // Если способ доставки - число, значит выбран один из общих способов, если "own" - значит выбран способ продавца
            if (isset($deliveryList[$studioId]) && is_numeric($deliveryList[$studioId]) && $deliveryList[$studioId] > 0) {
                $insertData['delivery_id'] = $deliveryList[$studioId];
            } elseif (isset($deliveryList[$studioId]) && $deliveryList[$studioId] == 'own') {
                $Studio = Studio::findOne($studioId);
                if ($Studio instanceof Studio) {
                    $insertData['custom_delivery'] = $Studio->custom_delivery;
                }
            }

            // Если способ оплаты - число, значит выбран один из общих способов, если "own" - значит выбран способ продавца
            if (isset($paymentList[$studioId]) && is_numeric($paymentList[$studioId]) && $paymentList[$studioId] > 0) {
                $insertData['payment_id'] = $paymentList[$studioId];
            } elseif (isset($paymentList[$studioId]) && $paymentList[$studioId] == 'own') {
                $Studio = Studio::findOne($studioId);
                if ($Studio instanceof Studio) {
                    $insertData['custom_payment'] = $Studio->custom_payment;
                }
            }

            Yii::$app->db->createCommand()->insert('prepared_orders', $insertData)->execute();
        } else {
            // переданы все заказы
            // получим список студий переданных товаров
            $goodIds = [];
            foreach ($GoodsInBasket as $GoodInBasket) {
                $goodIds[] = $GoodInBasket->good_id;
            }
            $studiosByGoods = Good::find()
                ->select(['studio_id'])
                ->distinct()
                ->where(['id' => $goodIds])
                ->asArray()
                ->all();

            $studioIds = ArrayHelper::getColumn($studiosByGoods, 'studio_id');

            $insertData = [];
            foreach ($studioIds as $studioId) {
                $studioId = (int)$studioId;

                // значения по умолчанию
                $deliveryId = null;
                $paymentId = null;
                $customDelivery = '';
                $customPayment = '';
                $StudioInstance = null; // если и доставка и оплата выбраны пользовательские, то при определении доставки будет запрос, а при определении оплаты объект возьмется из массива
                // определение способа доставки для студии
                if (isset($deliveryList[$studioId]) && is_numeric($deliveryList[$studioId]) && $deliveryList[$studioId] > 0) {
                    $deliveryId = $deliveryList[$studioId];
                } elseif (isset($deliveryList[$studioId]) && $deliveryList[$studioId] == 'own') {
                    $Studio = Studio::findOne($studioId);
                    if ($Studio instanceof Studio) {
                        $customDelivery = $Studio->custom_delivery;
                        $StudioInstance = $Studio;
                    }
                }

                // определение способа оплаты для студии
                if (isset($paymentList[$studioId]) && is_numeric($paymentList[$studioId]) && $paymentList[$studioId] > 0) {
                    $paymentId = $paymentList[$studioId];
                } elseif (isset($paymentList[$studioId]) && $paymentList[$studioId] == 'own') {
                    if ($StudioInstance === null) {
                        $Studio = Studio::findOne($studioId);
                    } else {
                        $Studio = $StudioInstance;
                    }
                    if ($Studio instanceof Studio) {
                        $customPayment = $Studio->custom_payment;
                    }
                }

//                $deliveryId = (int)$deliveryId;
//                $paymentId = (isset($paymentList[$studioId]) && (int)$paymentList[$studioId] > 0) ? (int)$paymentList[$studioId] : 0;
                if (!$studioId) {
                    // ошибка в данных
                    return ['status' => 'error', 'code' => 400];
                }
                $insertData[] = [
                    $userId,
                    $studioId,
                    $deliveryId,
                    $paymentId,
                    $customDelivery,
                    $customPayment
                ];
            }
            if ($insertData) {
                Yii::$app->db->createCommand()->batchInsert('prepared_orders', [
                    'user_id',
                    'studio_id',
                    'delivery_id',
                    'payment_id',
                    'custom_delivery',
                    'custom_payment',
                ], $insertData)->execute();
            }
        }

        // сохранение количества товаров в корзине
        $goodsQuantities = Yii::$app->request->post('quantity', []);
        foreach ($GoodsInBasket as $GoodInBasket) {
            $goodId = $GoodInBasket->good_id;
            if (isset($goodsQuantities[$goodId]) && (int)$goodsQuantities[$goodId] > 0) {
                $GoodInBasket->quantity = (int)$goodsQuantities[$goodId];
                $GoodInBasket->save();
            }
        }

        return ['status' => 'ok'];
    }
}