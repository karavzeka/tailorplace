<?php
/**
 * Контроллер по управлению платными и иными сервисами
 */

namespace app\modules\cabinet\controllers;

use app\controllers\CommonController;
use app\modules\good\models\Photo;
use app\modules\studio\models\Studio;
use app\modules\studio\models\StudioPromotion;
use app\modules\good\models\GoodPromotion;
use Yii;
use yii\helpers\ArrayHelper;

class ServiceController extends CommonController
{
    public $layout = 'cabinet';

    public $breadcrumbItems = [
        'studio-promotion' => [
            ['label' => 'Дополнительные услуги', 'url' => '/cabinet/services/'],
            ['label' => 'Продвижение ателье/магазина']
        ],
        'goods-promotion' => [
            ['label' => 'Дополнительные услуги', 'url' => '/cabinet/services/'],
            ['label' => 'Продвижение товаров']
        ],
    ];

    /**
     * Страница продвижения студии
     */
    public function actionStudioPromotion()
    {
        $Studio = Yii::$app->user->identity->studio;
        if (!($Studio instanceof Studio)) {
            $SiteController = Yii::$app->createController('site');
            echo $SiteController[0]->runAction('403');
            return false;
        }

        $lastPromotion = $Studio->studioPromotion instanceof StudioPromotion ? strtotime($Studio->studioPromotion->last_promotion) : null;

        if (Yii::$app->request->isPost) {
            if (Yii::$app->request->post('doPromotion') && ($lastPromotion === null || $lastPromotion < strtotime('-1 month'))) {
                $StudioPromotion = StudioPromotion::findOne(['studio_id' => $Studio->id]);
                if (!$StudioPromotion) {
                    $StudioPromotion = new StudioPromotion();
                }
                $StudioPromotion->studio_id = $Studio->id;
                $StudioPromotion->last_promotion = date('Y-m-d H:i:s');
                $StudioPromotion->save();

                $lastPromotion = time();
                Yii::$app->session->setFlash('studioPromotionSuccess', 'Студия успешно продвинута');

                Yii::$app->cache->delete('promotedStudios');    // Очистка кэша продвинутых товаров на главной
            } else {
                Yii::$app->session->setFlash('studioPromotionError', 'Прошлое продвижение студии было меньше месяца назад');
            }
        }

        echo $this->render('studioPromotion', [
            'studioType' => $Studio->type == 'atelier' ? 'ателье' : 'магазина',
            'lastPromotion' => $lastPromotion
        ]);
    }

    /**
     * Страница продвижения товаров
     */
    public function actionGoodsPromotion()
    {
        $Studio = Yii::$app->user->identity->studio;
        if (!($Studio instanceof Studio)) {
            $SiteController = Yii::$app->createController('site');
            echo $SiteController[0]->runAction('403');
            return false;
        }

        $goods = $Studio->getGood()
            ->select('
                `goods`.`id` AS `id`,
                `goods`.`name` AS `name`,
                `goods_promotion`.`last_promotion` AS `last_promotion`,
                `goods_promotion`.`end_of_promotion` AS `end_of_promotion`')
            ->leftJoin('goods_promotion', '`goods`.`id` = `goods_promotion`.`good_id`')
            ->orderBy([
                'last_promotion' => SORT_DESC,
                '`goods`.`id`' => SORT_ASC
            ])
            ->asArray()
            ->all();
        $goodIds = ArrayHelper::getColumn($goods, 'id');

        $lastPromotion = (isset($goods[0]) && $goods[0]['last_promotion'] !== null) ? strtotime($goods[0]['last_promotion']) : null;

        if (Yii::$app->request->isPost) {
            $goodToPromote = (int)Yii::$app->request->post('goodId');
            if ($goodToPromote && in_array($goodToPromote, $goodIds)) {
                if ($lastPromotion === null || $lastPromotion < strtotime('-1 month')) {
                    $GoodPromotion = GoodPromotion::findOne(['good_id' => $goodToPromote]);
                    if (!$GoodPromotion) {
                        $GoodPromotion = new GoodPromotion();
                    }
                    $GoodPromotion->good_id = $goodToPromote;
                    $GoodPromotion->last_promotion = date('Y-m-d H:i:s');
                    $GoodPromotion->save();

                    $lastPromotion = time();
                    Yii::$app->session->setFlash('goodPromotionSuccess', 'Товар успешно продвинут');
                } else {
                    Yii::$app->session->setFlash('goodPromotionWarning', 'Прошлое продвижение товара было меньше месяца назад');
                }
            } else {
                Yii::$app->session->setFlash('goodPromotionError', 'Произошла ошибка');
            }
        }

        $GoodPhotos = Photo::find()->where([
            'good_id' => $goodIds,
            'main' => 1
        ])->all();
        $GoodPhotos = ArrayHelper::index($GoodPhotos, 'good_id');

        echo $this->render('goodsPromotion', [
            'goods' => $goods,
            'GoodPhotos' => $GoodPhotos,
            'lastPromotion' => $lastPromotion
        ]);
    }
}