<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\controllers\CommonController;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\BaseFileHelper;
use yii\web\ForbiddenHttpException;
use app\modules\users\models\User;
use app\modules\studio\models\Studio;
use app\modules\cabinet\models\StudioForm;
use app\modules\cabinet\models\GoodForm;
use app\models\Delivery;
use app\models\Payment;
use app\models\GoodCategory;
use app\modules\good\models\Good;
use app\modules\good\models\Photo;

class StudioController extends CommonController
{
	public $layout = 'cabinet';

	/**
	 * Элементы хлебных крошек
	 * @var array
	 */
	public $breadcrumbItems = [
		'index' => 'not add',
		'what-create' => 'Регистрация ателье/магазина',
		'create-atelier' => [
            ['label' => 'Регистрация ателье/магазина', 'url' => '/cabinet/studio/what-create/'],
            ['label' => 'Регистрация ателье']
        ],
		'create-store' => [
            ['label' => 'Регистрация ателье/магазина', 'url' => '/cabinet/studio/what-create/'],
            ['label' => 'Регистрация магазина']
        ],
        'studio-redact' => 'not add',
		'add-good' =>  'not add',
		'edit-good' => [
            ['label' => 'Мой магазин', 'url' => '/cabinet/studio/'],
            ['label' => 'Редактирование товара']
        ]
	];

    /**
     * Страница студии в личном кабинете
     * @return string
     */
	public function actionIndex()
	{
        $User = Yii::$app->user->identity;
        if (!$User->hasStudio()) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

		$sectionList = ['all', 'sale', 'vitrine'];
        $dbName = Yii::$app->params['tailor_db'];
        $sortFields = [
            'default' => 'date',
            'name' => '`' . $dbName . '`.`goods`.`name`',
            'price' => '`' . $dbName . '`.`goods`.`price`',
            'date' => '`' . $dbName . '`.`goods`.`id`',    // id и create_time дадут одинаковую последовательность
        ];

		$Studio = $User->studio;
		if ($Studio->type == 'atelier') {
			$studioType = 'Мое ателье';
		} else {
			$studioType = 'Мой магазин';
		}
		$this->addBreadcrumbsItem(['label' => $studioType]);

		$section = in_array(Yii::$app->request->get('section'), $sectionList) ? Yii::$app->request->get('section') : '';

		$GoodsSelector = $Studio->getGood(true);

        $Sorter = new \app\components\SortController('mainGoodList', $sortFields);
        $sortParams = $Sorter->getSort();
        $newDirection = $sortParams['sortDirection'] == 'DESC' ? 'ASC' : 'DESC';

        $GoodsSelector->orderBy($sortParams['sortQueryPart']);

        $Goods = $GoodsSelector->all();

        // получение лимита товаров для студии
        $currentThreshold = $Studio->getCurrentThreshold();

		return $this->render('index', [
            'studioType' => $studioType,
            'Studio' => $Studio,
            'section' => $section,
            'orderBy' => $sortParams['sortField'],
            'direction' => $newDirection,
            'Goods' => $Goods,
            'currentThreshold' => $currentThreshold
        ]);
	}

    /**
     * Страница с выбором создания ателье или магазина
     * @return string
     */
	public function actionWhatCreate()
	{
        $this->layout = '@app/views/layouts/main';
		return $this->render('whatCreate');
	}

    /**
     * Страница с формой создания ателье
     * @return string|\yii\web\Response
     */
	public function actionCreateAtelier()
	{
        $this->layout = '@app/views/layouts/main';

        $User = Yii::$app->user->identity;

        if ($User->isUnconfirmed()) {
            Yii::$app->response->setStatusCode(403);
            return $this->render('//other/simpleMessage', [
                'message' => '
<p>Данный функционал для вас пока закрыт. Вы не подтвердили регистрацию.</p>
<p>Письмо с инструкцией так и не пришло? <a href="/cabinet/repeat-activation-key/">Отправить письмо еще раз.</a></p>'
            ]);
        }

        // TODO по хорошему должна быть проверка прав, разрешено ли пользователю создавать студию
		if ($User->studio !== null) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
		}

        $StudioForm = new StudioForm('atelier', null);
        $StudioForm->setScenario('create-atelier');

        if ($StudioForm->load(Yii::$app->request->post()) && $StudioForm->validate()) {
            $Studio = new Studio();
            $saveResult = $this->saveStudio($Studio, $StudioForm, $User);
            if ($saveResult) {
                Yii::$app->session->setFlash('studioCreated', 'Ателье успешно создано.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('studioForm', [
            'studioForm' => $StudioForm,
            'isNew' => true,
        ]);
	}

    /**
     * Страница с формой создания магазина
     * @return string|\yii\web\Response
     */
	public function actionCreateStore()
	{
        $this->layout = '@app/views/layouts/main';

        $User = Yii::$app->user->identity;

        if ($User->isUnconfirmed()) {
            Yii::$app->response->setStatusCode(403);
            return $this->render('//other/simpleMessage', [
                'message' => '
<p>Данный функционал для вас пока закрыт. Вы не подтвердили регистрацию.</p>
<p>Письмо с инструкцией так и не пришло? <a href="/cabinet/repeat-activation-key/">Отправить письмо еще раз.</a></p>'
            ]);
        }

        // TODO по хорошему должна быть проверка прав, разрешено ли пользователю создавать студию
		if ($User->studio !== null) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
		}

        $StudioForm = new StudioForm('store', null);
        $StudioForm->setScenario('create-store');

        if ($StudioForm->load(Yii::$app->request->post()) && $StudioForm->validate()) {
            $Studio = new Studio();
            $saveResult = $this->saveStudio($Studio, $StudioForm, $User);
            if ($saveResult) {
                Yii::$app->session->setFlash('studioCreated', 'Магазин успешно создан.');
                return $this->redirect(['index']);
            }
        }

        return $this->render('studioForm', [
            'studioForm' => $StudioForm,
            'isNew' => true,
        ]);
	}

    /**
     * Страница с формой редактирования студии
     * @return string
     */
    public function actionStudioRedact()
    {
        $User = Yii::$app->getUser()->identity;
        $Studio = $User->studio;
        if (!($Studio instanceof Studio)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }
        $this->addBreadcrumbsItem([
            'label' => 'Персональные данные',
            'url' => '/cabinet/personal/'
        ]);
        if ($Studio->type == 'atelier') {
            $this->addBreadcrumbsItem(['label' => 'Редактирование ателье']);
        } else {
            $this->addBreadcrumbsItem(['label' => 'Редактирование магазина']);
        }

        $StudioForm = new StudioForm($Studio->type, $Studio);
        $post = Yii::$app->request->post();
        if ($StudioForm->load($post) && $StudioForm->validate()) {
            if (!isset($post['StudioForm']['deliveryList'])) {
                $StudioForm->deliveryList = [];
            }
            $saveResult = $this->saveStudio($Studio, $StudioForm, $User);
            if ($saveResult) {
                $studioType = $Studio->type == 'atelier' ? 'ателье' : 'магазина';
                Yii::$app->session->setFlash('studioRedactSuccess', 'Данные '. $studioType . ' сохранены.');
                return $this->redirect('/cabinet/personal/');
            }
        }

        return $this->render('studioForm', [
            'studioForm' => $StudioForm,
            'isNew' => false,
        ]);
    }

    /**
     * Метод сохранения ателье или магазина
     * @param $StudioForm
     * @param User $User
     * @return string|\yii\web\Response
     * @throws \yii\base\UserException
     */
	private function saveStudio(Studio $Studio, StudioForm $StudioForm, User $User)
    {
        $isNewRecord = $Studio->isNewRecord;
        if ($isNewRecord) {
            $Studio->type = $StudioForm->type;
            $Studio->name = $StudioForm->name;
        }
        $Studio->country_id = $StudioForm->countryId;
        $Studio->city_id = $StudioForm->cityId;
        $Studio->address = $StudioForm->address;
        $Studio->phone = $StudioForm->phone > 0 ? $StudioForm->phone : 0;
//        	$Studio->currency_id = $StudioForm->currencyId;
        $Studio->custom_delivery = $StudioForm->custom_delivery;
        $Studio->custom_payment = $StudioForm->custom_payment;
        $Studio->slogan = $StudioForm->slogan;
        $Studio->description = $StudioForm->description;
        $Studio->update_time = date('Y-m-d H:i:s');

        $Transaction = Yii::$app->db->beginTransaction();
        try {
            // Сохраним студию назначив ее пользователю
            $User->link('studio', $Studio);

            // Когда у Студии будет id (после записи), назначим связи многие ко многим
            Yii::$app->db->createCommand()
                ->delete('studio_delivery', ['studio_id' => $Studio->id])
                ->execute();
            $deliveryObjects = Delivery::findAll($StudioForm->deliveryList);
            foreach ($deliveryObjects as $delivery) {
                $Studio->link('delivery', $delivery);
            }

            Yii::$app->db->createCommand()
                ->delete('studio_payment', ['studio_id' => $Studio->id])
                ->execute();
            $paymentObjects = Payment::findAll($StudioForm->paymentList);
            foreach ($paymentObjects as $payment) {
                $Studio->link('payment', $payment);
            }

            // Назначим новую роль пользователю (atelierOwner или storeOwner)
            if ($isNewRecord) {
                $User->assignRole($Studio->type . 'Owner');
            }

            $Transaction->commit();
            $saveResult = true;

        } catch(\Exception $e) {
            $Transaction->rollBack();
            $StudioForm->addError(null, 'Произошла непредвиденная ошибка. Просьба сообщить о ней администрации сайта.');
            $saveResult = false;
        }

        // логирование созданиея студии
        $IpLog = new \app\models\IpLogCommon();
        $IpLog->action = $IpLog::ACTION_CREATE_STUDIO;
        $IpLog->user_id = $this->id;
        $IpLog->ip = Yii::$app->numberHelper->ipToUnsignedNumber(Yii::$app->request->getUserIP());
        $IpLog->save();

        return $saveResult;
	}

    /**
     * Страница с формой создания нового товара
     * @return string|\yii\web\Response
     */
	public function actionAddGood()
	{
		$Studio = Yii::$app->user->identity->studio;
        if (!($Studio instanceof Studio)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        $studioTypeLabel = $Studio->type == 'atelier' ? 'Мое ателье' : 'Мой магазин';
        $this->addBreadcrumbsItem(['label' => $studioTypeLabel, 'url' => '/cabinet/studio/']);
        $this->addBreadcrumbsItem(['label' => 'Добавление товара']);

        if (!$Studio->mayAddGood()) {
            return $this->renderContent('<p style="margin-top: 100px; font-family: RobotoBlack, sans-serif; font-size: 28px;color: #95938E; text-align: center;">Достигнут предел кол-ва товаров</p>');
        }

        $userId = Yii::$app->user->identity->id;
		$GoodForm = new GoodForm($Studio);
        if (Yii::$app->request->isPost) {
            $Good = new Good();
            $post = Yii::$app->request->post();
            $GoodForm->categories = $post['GoodForm']['categories'];
            if ($GoodForm->load($post) && $GoodForm->validate()) {
                $result = $this->saveGood($GoodForm, $Good, $Studio, $userId);
                if ($result) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isGet) {
            Yii::$app->fileHelper->clearTempGoodPhotoDir($userId);
        }
		return $this->render('addGood', [
			'GoodForm' => $GoodForm,
            'Photos' => [],
            'action' => 'new'
		]);
	}

    /**
     * Страница с формой редактирования товара
     * @return string|\yii\web\Response
     */
	public function actionEditGood()
	{
        $goodId = (int)Yii::$app->request->get('goodId');
        if ($goodId == 0) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
        $userId = Yii::$app->user->identity->id;
        $Studio = Yii::$app->user->identity->studio;
        $Good = Good::findOne([
            'id' => $goodId,
            'studio_id' => $Studio->id
        ]);
        if (!$Good) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
		$GoodForm = new GoodForm($Studio, $Good);
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $GoodForm->categories = $post['GoodForm']['categories'];
            if ($GoodForm->load($post) && $GoodForm->validate()) {
                $result = $this->saveGood($GoodForm, $Good, $Studio, $userId);
                if ($result) {
                    return $this->redirect(['index']);
                }
            }
        }
        if (Yii::$app->request->isGet) {
            Yii::$app->fileHelper->clearTempGoodPhotoDir($userId);
        }
		return $this->render('addGood', [
			'GoodForm' => $GoodForm,
            'Photos' => $Good->photo,
            'action' => 'edit'
		]);
	}

    /**
     * Метод сохранения товара и связанных с ним категорий и фотографий
     * @param GoodForm $GoodForm
     * @param Good $Good
     * @param Studio $Studio
     * @param $userId
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveGood(GoodForm $GoodForm, Good $Good, Studio $Studio, $userId)
    {
        $Good->name = $GoodForm->name;
        $Good->price = Yii::$app->numberHelper->priceToDBVal($GoodForm->price);
        $Good->description = $GoodForm->description;
        $Good->quantity = $GoodForm->quantity;
        $Good->craft_time = $GoodForm->craft_time;
        $Good->update_time = date('Y-m-d H:i:s');

        $Transaction = Yii::$app->db->beginTransaction();
        try {
            $Studio->link('good', $Good);

            // обработка категорий
            $categories = array_unique(array_slice($GoodForm->categories, 0, 4));
            $GoodCategories = $Good->category;
            $goodCategoryIds = [];
            foreach ($GoodCategories as $CategoryOfGood) {
                $goodCategoryIds[] = $CategoryOfGood->id;
            }
            $toInsert = array_diff($categories, $goodCategoryIds);
            $toDelete = array_diff($goodCategoryIds, $categories);
            $CategoryToInsert = GoodCategory::findAll($toInsert);
            foreach ($CategoryToInsert as $Category) {
                $Good->link('category', $Category);
            }
            foreach ($toDelete as $categoryId) {
                foreach ($GoodCategories as $CategoryOfGood) {
                    if ($CategoryOfGood->id == $categoryId) {
                        $Good->unlink('category', $CategoryOfGood, true);
                    }
                }
            }

            // обработка фотографий
            $Settings = json_decode($GoodForm->photoSettings);
            $photoUploadSettings = $Settings->upload;
            $photoUpdateSettings = $Settings->update;

            $mainWasSet = false;
            // если главная среди существующих, то обновим главную
            if ($Settings->mainPhoto->type == 'exist' && $Settings->mainPhoto->numOrId) {
                $newMainPhoto = Photo::find()->where([
                    'id' => $Settings->mainPhoto->numOrId,
                    'good_id' => $Good->id,
                ])->one();
                $oldMainPhoto = $Good->mainPhoto;
                if ($oldMainPhoto) {
                    $mainWasSet = true;
                }
                if ($newMainPhoto && $newMainPhoto->id != $oldMainPhoto->id) {
                    $oldMainPhoto->main = '0';
                    $oldMainPhoto->save();
                    $newMainPhoto->main = '1';
                    $newMainPhoto->save();
                }
            }

            $existPhotos = $Good->photo;
            if ($existPhotos) {
                Yii::$app->fileHelper->updateGoodPhotos($existPhotos, $photoUpdateSettings);
            }

            $photoDir = Photo::getPhotoDir($Good->id);

            $uploadedFiles = [];
            if (is_array($photoUploadSettings) && count($photoUploadSettings) > 0) {
                // Если оказалось, что уже есть фотографии и заливается столько новых, что в сумме окажется больше допустимого,
                // то отсечем часть фотографий на закачку
                $sliceLength = 9 - count($existPhotos);
                $photoUploadSettings = array_slice($photoUploadSettings, 0, $sliceLength);

                $maxPhotoName = Photo::find()->where(['good_id' => $Good->id])->max('file_name');
                $continueName = !is_null($maxPhotoName) ? $maxPhotoName + 1 : 0;
                $uploadedFiles = Yii::$app->fileHelper->uploadGoodPhotos($userId, $photoUploadSettings, $photoDir, $continueName);
            }

            $issetMain = $mainWasSet;
            $mainPhotoNumber = (int)$Settings->mainPhoto->numOrId;

            foreach ($uploadedFiles as $file) {
                $Photo = new Photo();
                // если главная фотка не установлена, то она среди новых
                if (!$issetMain && $file['fileNumber'] == $mainPhotoNumber) {
                    $Photo->main = 1;
                    Yii::$app->db->createCommand()
                        ->update(
                            Photo::getTableSchema()->name,
                            ['main' => 0],
                            ['main' => 1, 'good_id' => $Good->id]
                        )->execute();
                }
                $Photo->file_name = $file['numberName'];
                $Photo->margin_top_part = $file['marginTopPart'];
                $Photo->margin_left_part = $file['marginLeftPart'];
                $Photo->size_part = $file['sizePart'];
                $Photo->real_width = $file['realWidth'];
                $Photo->real_height = $file['realHeight'];
                $Good->link('photo', $Photo);
            }

            $Transaction->commit();
            $saveResult = true;
        } catch(\Exception $e) {
            $Transaction->rollBack();
            $GoodForm->addError(null, 'Произошла непредвиденная ошибка. Просьба сообщить о ней администрации сайта.');
            $saveResult = false;
        }
        Yii::$app->fileHelper->clearTempGoodPhotoDir($userId);
        return $saveResult;
    }

    public function actionDeleteGood()
    {
        $goodId = (int)Yii::$app->request->get('goodId');
        if ($goodId == 0) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        $Studio = Yii::$app->user->identity->studio;
        $Good = Good::findOne([
            'id' => $goodId,
            'studio_id' => $Studio->id
        ]);
        if (!$Good) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        // удалим фотографии товара (сразу всю папку)
        $photoDir = Photo::getPhotoDir($goodId);
        BaseFileHelper::removeDirectory($photoDir);
        // удалим товар, все связанные с товаром записи удалятся каскадно в БД
        $result = $Good->delete();

        if (Yii::$app->request->isAjax) {
            $actionStatus = ($result > 0) ? 'ok' : 'error';
            $answer = ['status' => $actionStatus];
            echo json_encode($answer);
            exit;
        } else {
            return $this->redirect(['index']);
        }
    }
}