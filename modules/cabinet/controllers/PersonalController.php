<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\controllers\CommonController;
use app\models\Message;
use app\modules\users\models\User;
use app\modules\users\models\Photo as UserPhoto;
use app\modules\studio\models\Studio;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;

class PersonalController extends CommonController
{
    public $layout = 'cabinet';
    /**
     * @var int По сколько сообщений подгружать на странице диалога
     */
    public $limitOfMessagesInDialog = 20;
    /**
     * @var int По сколько пользователей и последних сообщений с ними выводить на странице сообщений
     */
    public $limitOfLastMessages = 20;

	/**
	 * Элементы хлебных крошек
	 * @var array
	 */
	public $breadcrumbItems = [
		'index' => 'Персональные данные',
		'studio-redact' => [
            ['label' => 'Персональные данные', 'url' => '/cabinet/personal/']
        ],
		'user-redact' => [
            ['label' => 'Персональные данные', 'url' => '/cabinet/personal/']
        ],
        'messages' => 'Сообщения',
        'dialog' => [
            ['label' => 'Сообщения', 'url' => '/cabinet/messages/'],
            ['label' => 'Диалог']
        ],
        'repeat-activation-key' => 'Повторная отправка кода активации',
	];

	public function actionIndex()
	{
		$User = Yii::$app->getUser()->identity;

		$Studio = $User->studio;
        if ($Studio) {
            $hasStudio = true;
            $studioType = ($Studio->type == 'atelier') ? 'Ателье' : 'Магазин';
            $studioView = $this->generateStudioView($Studio, $studioType);
        } else {
            $hasStudio = false;
            $studioView = '';
        }

        $userView = $this->generateUserView($User, $hasStudio);

		return $this->render('index', [
			'userView' => $userView,
			'studioView' => $studioView
		]);
	}

	private function generateUserView(User $User, $hasStudio = false)
	{
		return $this->renderPartial('userInfo', [
			'User' => $User,
            'hasStudio' => $hasStudio,
        ]);
	}

	private function generateStudioView(Studio $Studio, $studioType)
	{
		return $this->renderPartial('studioInfo', [
			'Studio' => $Studio,
            'studioType' => $studioType,
        ]);
	}

    public function actionUserRedact()
    {
        $this->addBreadcrumbsItem(['label' => 'Редактирование персональных данных']);
        $User = Yii::$app->getUser()->identity;
        $User->scenario = 'userRedact';
        $PersonalForm = new \app\modules\cabinet\models\PersonalForm();
        $PersonalForm->deliveryAddress = $User->address;
        if ($PersonalForm->load(Yii::$app->request->post()) && $PersonalForm->validate()) {
            if (!empty($PersonalForm->newPassword)) {
                $User->password = Yii::$app->getSecurity()->generatePasswordHash($PersonalForm->newPassword);
            }
            $User->address = $PersonalForm->deliveryAddress;

            $Transaction = Yii::$app->db->beginTransaction();
            try {
                $User->save();

                // обработка фотографий
                $Settings = json_decode($PersonalForm->photoSettings);
                $photoUploadSettings = $Settings->upload;
                $photoUpdateSettings = $Settings->update;

                $existPhotos = $User->photo;

                if ($existPhotos) {
                    Yii::$app->fileHelper->updateUserPhotos($existPhotos, $photoUpdateSettings);
                } elseif (is_array($photoUploadSettings) && count($photoUploadSettings) > 0) {
                    // защита, чтобы не было загружено больше одной фотографии
                    $sliceLength = 1 - count($existPhotos);
                    $photoUploadSettings = array_slice($photoUploadSettings, 0, $sliceLength);

                    $photoDir = UserPhoto::getPhotoDir($User->id);
                    $uploadedFiles = Yii::$app->fileHelper->uploadUserPhoto($photoUploadSettings, $photoDir);

                    if ($uploadedFiles) {
                        foreach ($uploadedFiles as $file) {
                            $Photo = new UserPhoto();
                            $Photo->margin_top_part = $file['marginTopPart'];
                            $Photo->margin_left_part = $file['marginLeftPart'];
                            $Photo->size_part = $file['sizePart'];
                            $Photo->real_width = $file['realWidth'];
                            $Photo->real_height = $file['realHeight'];
                            $User->link('photo', $Photo);
                        }
                    }
                }
                $Transaction->commit();

                Yii::$app->session->setFlash('userRedactSuccess', 'Данные успешно изменены');
                Yii::$app->response->redirect('/cabinet/personal/');
            } catch (\Exception $e) {
                // удаление загруженных фотографий
                $photoDir = UserPhoto::getPhotoDir($User->id);
                BaseFileHelper::removeDirectory($photoDir);

                // откат изменений в базе
                $Transaction->rollBack();

                $PersonalForm->addError(null, 'Произошла непредвиденная ошибка.');
                $PersonalForm->oldPassword = '';
                $PersonalForm->newPassword = '';
                $PersonalForm->repeatNewPassword = '';
            }
        }

        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }
        Yii::$app->fileHelper->clearTempUserPhotoDir($session->id);

        return $this->render('userRedact', [
            'User' => $User,
            'PersonalForm' => $PersonalForm,
        ]);
    }

    /**
     * Страница сообщений, где выводится список собеседников
     *
     * @return string
     */
    public function actionMessages()
    {
        // получение сообщений и пользователей c фотографиями
        $lastMsgUserPairs = $this->getLastMsgUserPairs(0, true);

        $messagesHtml = $this->renderPartial('@app/views/parts/messages', [
            'lastMsgUserPairs' => $lastMsgUserPairs,
        ]);

        return $this->render('messages', [
            'messagesHtml' => $messagesHtml,
            'countOfInterlocutors' => $lastMsgUserPairs['countOfInterlocutors'],
        ]);
    }

    /**
     * Получить еще партицию собеседников по запросу пользователя
     */
    public function actionGetMoreInterlocutors()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $offsetPart = (int)Yii::$app->request->post('offsetPart');

            $msgUserPairs = $this->getLastMsgUserPairs($offsetPart, false);
            $messagesHtml = $this->renderPartial('@app/views/parts/messages', [
                'lastMsgUserPairs' => $msgUserPairs,
            ]);

            $response = [
                'status' => 'ok',
                'html' => $messagesHtml
            ];
            $this->sendJsonAnswer($response);
        }
    }


    /**
     * Страница диалога пользователей
     *
     * @return string|\yii\web\Response
     */
    public function actionDialog()
    {
        $opponentId = (int)Yii::$app->request->get('user');
        if ($opponentId == 0) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
        $Opponent = User::findOne($opponentId);
        if (!$Opponent) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
        $User = Yii::$app->getUser()->identity;
        if ($User->isUnconfirmed()) {
            Yii::$app->response->setStatusCode(403);
            return $this->render('//other/simpleMessage', [
                'message' => '
<p>Общение с пользователями для вас пока закрыто. Вы не подтвердили регистрацию.</p>
<p>Письмо с инструкцией так и не пришло? <a href="/cabinet/repeat-activation-key/">Отправить письмо еще раз.</a></p>'
            ]);
        }

        $NewMessage = new Message();
        $NewMessage->from_id = $User->id;
        $NewMessage->to_id = $opponentId;

        if ($NewMessage->load(Yii::$app->request->post()) && $NewMessage->validate()) {
            $NewMessage->save();

            return $this->redirect('/cabinet/dialog/' . $opponentId .'/');
        }


        $messagesData = $this->getOnlyMessages($Opponent, null, null, false, true);
        $Messages = $messagesData['Messages'];
        // восстанавливаем порядок сортировки
        $Messages = array_reverse($Messages);

        // новые входящие помечаются как прочитанные
        $inputMessageIds = [];
        foreach ($Messages as $Message) {
            if ($Message->to_id == $User->id && $Message->is_read == 0) {
                $inputMessageIds[] = $Message->id;
            }
        }
        if ($inputMessageIds) {
            Message::updateAll(['is_read' => 1], ['id' => $inputMessageIds]);
        }

        $messagesHtml = $this->renderPartial('@app/views/parts/dialog', [
            'User' => $User,
            'Interlocutor' => $Opponent,
            'Messages' => $Messages,
        ]);

        // если всего товаров, соответствующих фильтрам, больше чем ограничение в одной выборке, то отображаем кнопку "Показать еще"
        $showMoreButton = $messagesData['count'] > $this->limitOfMessagesInDialog;

        return $this->render('dialog', [
            'User' => $User,
            'Opponent' => $Opponent,
            'messagesHtml' => $messagesHtml,
            'NewMessage' => $NewMessage,
            'showMoreButton' => $showMoreButton
        ]);
    }

    /**
     * Получение новых сообщений, пока пользователь находится на странице диалога
     */
    public function actionGetMessagesAjax()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $interlocutorId = (int)Yii::$app->request->post('interlocutorId');
            if (!$interlocutorId) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $Interlocutor = User::findOne($interlocutorId);
            if (!$Interlocutor) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $User = Yii::$app->user->identity;

            // новые сообщения
            $messagesData = $this->getOnlyMessages($Interlocutor, ['is_read' => 0], null, true);
            $Messages = $messagesData['Messages'];
            // восстанавливаем порядок сортировки
            $Messages = array_reverse($Messages);

            if (count($Messages)) {
                $html = $this->renderPartial('@app/views/parts/dialog', [
                    'User' => $User,
                    'Interlocutor' => $Interlocutor,
                    'Messages' => $Messages,
                ]);

                // выставление флага о прочитанности сообщений
                $messageIds = [];

                foreach ($Messages as $Message) {
                    $messageIds[] = $Message->id;
                }
                Message::updateAll(['is_read' => 1], ['id' => $messageIds]);
            } else {
                $html = '';
            }

            $response = [
                'status' => 'ok',
                'html' => $html
            ];
            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Выборка более старых сообщений по запросу пользователя
     */
    public function actionGetOldMessagesAjax()
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $interlocutorId = (int)Yii::$app->request->post('interlocutorId');
            $offset = (int)Yii::$app->request->post('offset') * $this->limitOfMessagesInDialog;
            if (!$interlocutorId) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $Interlocutor = User::findOne($interlocutorId);
            if (!$Interlocutor) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $User = Yii::$app->user->identity;

            // выборка сообщений
            $messagesData = $this->getOnlyMessages($Interlocutor, null, $offset, false, true);
            $Messages = $messagesData['Messages'];
            // восстанавливаем порядок сортировки
            $Messages = array_reverse($Messages);

            if (count($Messages)) {
                $html = $this->renderPartial('@app/views/parts/dialog', [
                    'User' => $User,
                    'Interlocutor' => $Interlocutor,
                    'Messages' => $Messages,
                ]);
            } else {
                $html = '';
            }
            $countOfMessages = $messagesData['count'];
            $hideShowMoreButton = ($offset + $this->limitOfMessagesInDialog >= $countOfMessages);

            $response = [
                'status' => 'ok',
                'hideShowMoreButton' => $hideShowMoreButton,
                'html' => $html
            ];
            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Ищет последние сообщения от пользователей
     *
     * @param int $offsetPart Номер партиции
     * @param bool $calcCountOfInterlocutors Рассчитать полное кол-во собеседников
     * @return array
     */
    private function getLastMsgUserPairs($offsetPart = 0, $calcCountOfInterlocutors = false)
    {
        $userId = Yii::$app->user->identity->id;
        $offset = $this->limitOfLastMessages * $offsetPart;
        $sql = "
            SELECT
                `sub`.`interlocutor_id` AS `interlocutor_id`,
                MAX(`sub`.`msg_id`) AS `last_msg_id`
            FROM (
                (
                    SELECT
                        `id` AS `msg_id`,
                        `from_id` AS `interlocutor_id`
                    FROM
                        `messages`
                    WHERE
                        `to_id` = " . $userId . "
                )
                UNION
                (
                    SELECT
                        `id` AS `msg_id`,
                        `to_id` AS `interlocutor_id`
                    FROM
                        `messages`
                    WHERE
                        `from_id` = " . $userId . "
                )
            ) AS `sub`
            GROUP BY
                `interlocutor_id`
            ORDER BY
                `last_msg_id` DESC
            LIMIT " . $offset . ", " . $this->limitOfLastMessages;
        $lastUserMessageIds = Yii::$app->db->createCommand($sql)->queryAll();

        if ($calcCountOfInterlocutors) {
            $sql = "
                (
                    SELECT
                        `from_id` AS `interlocutor_id`
                    FROM
                        `messages`
                    WHERE
                        `to_id` = " . $userId . "
                )
                UNION
                (
                    SELECT
                        `to_id` AS `interlocutor_id`
                    FROM
                        `messages`
                    WHERE
                        `from_id` = " . $userId . "
                )";
            $interlocutorsList = Yii::$app->db->createCommand($sql)->queryAll();
            $countOfInterlocutors = count($interlocutorsList);
        } else {
            $countOfInterlocutors = null;
        }

        $messageIds = ArrayHelper::getColumn($lastUserMessageIds, 'last_msg_id');
        $interlocutorIds = ArrayHelper::getColumn($lastUserMessageIds, 'interlocutor_id');

        $Messages = Message::findAll($messageIds);
        $Messages = ArrayHelper::index($Messages, 'id');

        $Interlocutors = User::find()->with('studio')->where(['id' => $interlocutorIds])->all();
        $Interlocutors = ArrayHelper::index($Interlocutors, 'id');

        // получение фотографий собеседников
        $Photos = UserPhoto::find()->where(['user_id' => $interlocutorIds])->all();
        $Photos = ArrayHelper::index($Photos, 'user_id');

        $lastUserMessages = [
            'users' => [],
            'userPhotos' => $Photos,
            'countOfInterlocutors' => $countOfInterlocutors
        ];
        foreach ($lastUserMessageIds as $messageParams) {
            $lastUserMessages['users'][] = [
                'message' => $Messages[$messageParams['last_msg_id']],
                'user' => $Interlocutors[$messageParams['interlocutor_id']]
            ];
        }

        return $lastUserMessages;
    }

    /**
     * Получает коллекцию сообщений польльзователя и собеседника.
     * Сортирует в обратном порядке.
     *
     * @param User $Interlocutor Собеседник
     * @param array|string|null $whereCondition Часть WHERE запроса
     * @param int|null $offset Смешение
     * @param bool $onlyInterlocutor Брать только сообщения собеседника
     * @param bool $calcCountOfMessages Подсчитать кол-во сообщений
     * @return array|\app\models\Message[]
     */
    private function getOnlyMessages(User $Interlocutor, $whereCondition = null, $offset = null, $onlyInterlocutor = false, $calcCountOfMessages = false) {
        $User = Yii::$app->getUser()->identity;

        $MessagesQuery = Message::find()
            ->orWhere(['to_id' => $User->id, 'from_id' => $Interlocutor->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit($this->limitOfMessagesInDialog);

        if (!$onlyInterlocutor) {
            // берем и свои сообщения
            $MessagesQuery->orWhere(['to_id' => $Interlocutor->id, 'from_id' => $User->id]);
        }
        if ($whereCondition) {
            $MessagesQuery->andWhere($whereCondition);
        }
        if ($offset) {
            $MessagesQuery->offset($offset);
        }

        $Messages = $MessagesQuery->all();

        if ($calcCountOfMessages) {
            $countOfMessages = $MessagesQuery->orderBy(null)->count();
        } else {
            $countOfMessages = null;
        }

        return ['Messages' => $Messages, 'count' => $countOfMessages];
    }

    /**
     * Заного отправляет письмо со ссылкой подтверждения регистрации
     *
     * @return string
     * @throws Exception
     */
    public function actionRepeatActivationKey()
    {
        $User = Yii::$app->user->identity;
        if (!$User->isUnconfirmed()) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('403');
        }

        $RegistrationActivation = \app\models\RegistrationActivation::find()
            ->where(['user_id' => $User->id])
            ->one();
        if (!$RegistrationActivation) {
            $RegistrationActivation = new \app\models\RegistrationActivation();

            $secretKey = Yii::$app->security->generateRandomString(10);
            // Yii некорректно обрабатывает URL'ы по маске, если в URL'е в значениях GET параметров присутствует "-"
            $secretKey = str_replace('-', '_', $secretKey);
            $RegistrationActivation->user_id = $User->id;
            $RegistrationActivation->secret_key = $secretKey;
        } else {
            // Если с момента прошедшей отправки прошло меньше часа, то выводим сообщение, чтобы попробовали позже
            if ($RegistrationActivation->was_repeated && (time() - strtotime($RegistrationActivation->update_time) < 3600)) {
                return $this->render('//other/simpleMessage', [
                    'message' => '
                        <p>С момента предыдущей отправки письма прошло мало времени, при необходимости повторите попытку позже.</p>
                        <p>Если письмо не приходит, проверьте, не попало ли оно в спам. Если и там его нет, воспользуйтесь формой
                        обратной связи и напишите обращение в службу поддержки.</p>'
                ]);
            }

            $RegistrationActivation->update_time = date('Y-m-d H:i:s');
            $RegistrationActivation->was_repeated = 1;
        }
        // Сохранение и отправка письма
        if ($RegistrationActivation->save()) {
            Yii::$app->mailer->compose('@app/mail/needConfirm', ['secretKey' => $RegistrationActivation->secret_key])
                ->setTo($User->email)
                ->setSubject('Tailor place. Подтверждение регистрации')
                ->send();
        } else {
            throw new Exception('При повторной отправке письма со ссылкой подтверждения регистрации прозошла ошибка.');
        }

        return $this->render('//other/simpleMessage', [
            'message' => '
                <p>Вам повторно было выслано письмо с инструкцией по подтверждению регистрации.</p>'
        ]);
    }
}