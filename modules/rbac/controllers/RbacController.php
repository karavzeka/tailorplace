<?php

namespace app\modules\rbac\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RbacController extends Controller
{
    public function actionInit()
	{
        // TODO по хорошему надо перенести вызов этого контроллера в админку
        if (!defined('YII_DEBUG') || \YII_DEBUG !== true) {
            throw new NotFoundHttpException();
        }

		// TODO доделать права
		$Auth = Yii::$app->authManager;

        // Очистка таблицы с дочерними ролями и правами
        Yii::$app->db->createCommand()
            ->delete($Auth->itemChildTable)
            ->execute();

        // Очистка основной таблицы с ролями и правами
        Yii::$app->db->createCommand()
            ->delete($Auth->itemTable)
            ->execute();

        // Очистка таблицы с правилами
        Yii::$app->db->createCommand()
            ->delete($Auth->ruleTable)
            ->execute();

		// Создание прав
		// Право входа в личный кабинет
		$viewCabinet = $Auth->createPermission($Auth::PERMISSION_VIEW_CABINET);
		$viewCabinet->description = 'Просмотр личного кабинета';
		$Auth->add($viewCabinet);

		// Право отправлять сообщения
		$sendMessages = $Auth->createPermission($Auth::PERMISSION_SEND_MESSAGE);
		$sendMessages->description = 'Отправка сообщений';
		$Auth->add($sendMessages);

        // Право создавать студию
        $createStudio = $Auth->createPermission($Auth::PERMISSION_CREATE_STUDIO);
        $createStudio->description = 'Создание ателье/магазина';
        $Auth->add($createStudio);

		// Право оформлять заказ
		$createOrder = $Auth->createPermission($Auth::PERMISSION_CREATE_ORDER);
		$createOrder->description = 'Оформление заказа';
		$Auth->add($createOrder);

        // Право заходить в админку
        $viewAdminSection = $Auth->createPermission($Auth::PERMISSION_VIEW_ADMIN_SECTION);
        $viewAdminSection->description = 'Просмотр раздела админки';
        $Auth->add($viewAdminSection);

        // Право просматривать новости, у которых не стоит флаг "Показывать на сайте"
        $viewHideNews = $Auth->createPermission($Auth::PERMISSION_VIEW_HIDE_NEWS);
        $viewHideNews->description = 'Просмотр скрытых новостей';
        $Auth->add($viewHideNews);

		// Роли и права
        // Роль unconfirmed (пользователь не подтвердивший свою регистрацию)
        $unconfirmed = $Auth->createRole($Auth::ROLE_UNCONFIRMED);
        $unconfirmed->description = 'Пользователь еще не подтвердивший свою регистрацию';
        $Auth->add($unconfirmed);
        $Auth->addChild($unconfirmed, $viewCabinet);

		// Роль user
		$user = $Auth->createRole($Auth::ROLE_USER);
        $user->description = 'Обычный зарегистрированный пользователь';
		$Auth->add($user);
		// Добавление прав и потомков для  $atelierOwner
		$Auth->addChild($user, $viewCabinet);
		$Auth->addChild($user, $sendMessages);
		$Auth->addChild($user, $createOrder);
		$Auth->addChild($user, $createStudio);

//		// Роль atelierOwner
//		$atelierOwner = $Auth->createRole('atelierOwner');
//		$Auth->add($atelierOwner);
//		// Добавление прав и потомков для  $atelierOwner
//        $Auth->addChild($atelierOwner, $user);
//
//		// Роль storeOwner
//		$storeOwner = $Auth->createRole('storeOwner');
//		$Auth->add($storeOwner);
//		// Добавление прав и потомков для  $storeOwner
//        $Auth->addChild($storeOwner, $user);

		// Роль admin
		$admin = $Auth->createRole('admin');
        $admin->description = 'Пользователь, обладающий правами администратора';
		$Auth->add($admin);
		$Auth->addChild($admin, $user);
		$Auth->addChild($admin, $viewAdminSection);
		$Auth->addChild($admin, $viewHideNews);

		return $this->goHome();
	}
}