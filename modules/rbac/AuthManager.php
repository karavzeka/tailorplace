<?php
/**
 * Менеджер прав пользователей
 */

namespace app\modules\rbac;

class AuthManager extends \yii\rbac\DbManager
{
    /**
     * Разрешение просматривать личный кабинет
     */
    const PERMISSION_VIEW_CABINET = 'viewCabinet';

    /**
     * Разрешение отправлять сообщения
     */
    const PERMISSION_SEND_MESSAGE = 'sendMessage';

    /**
     * Разрешение создавать ателье/магазин
     */
    const PERMISSION_CREATE_STUDIO = 'createStudio';

    /**
     * Разрешение создавать заказ
     */
    const PERMISSION_CREATE_ORDER = 'createOrder';

    /**
     * Разрешение просматривать админку
     */
    const PERMISSION_VIEW_ADMIN_SECTION = 'viewAdminSection';

    /**
     * Разрешение просматривать новости на сайте, у которых не стоит флаг "Показывать на сайте"
     */
    const PERMISSION_VIEW_HIDE_NEWS = 'viewHideNews';

    /**
     * Роль пользователя: еще не подтвердивший свою регистрацию
     */
    const ROLE_UNCONFIRMED = 'unconfirmed';

    /**
     * Роль пользователя: обычный зарегистрированный пользователь
     */
    const ROLE_USER = 'user';
}