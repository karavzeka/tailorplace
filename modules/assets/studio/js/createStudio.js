var successCountry = false;
var successCity = false;

function checkCities(countryId, fieldId, url)
{
	if (!countryId) {
		return 'error';
	}

    $.ajax({
    	url: url,
    	type: 'post',
    	dataType: 'json',
    	data: {countryId: countryId},
    	success: function(data){
    		if(data[0]) {
    			$('#studioform-cityname').removeAttr('disabled');
    		} else {
                $('#studioform-cityname').val('').attr('disabled', 'disabled');
                $('.form-group.field-studioform-cityname').removeClass('has-error').removeClass('has-success');
                $('#studioform-cityid').val('');
            }
    	}
    });
}

$(function(){
    var id = $('#studioform-countryid').val();
    if (id > 0) {
        checkCities(id, 'studioform-cityname', '/get-city-list/');
    }

    // развертывание/сворачивание соглашения
    var opened = false;
    var agreementText = $('#agreement-text');
    $('#agreement-block .how-be-this a').click(function(){
        if (opened === false) {
            $('#agreement-text').animate({height: agreementText[0].scrollHeight}, 300);
            $(this).prev('span').text('↑');
            $(this).text('Свернуть');
            opened = true;
        } else {
            $('#agreement-text').animate({height: 100}, 300);
            $(this).prev('span').text('↓');
            $(this).text('Развернуть');
            opened = false;
        }
        return false;
    });
});
