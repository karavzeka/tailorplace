$(function() {
    // Настройки при вызове модального окна бана пользователя
    $('#ban-user-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);

        var placeholderText = '';
        if (button.data('user-banned') == 0) {
            placeholderText = 'Забанить пользователя #' + button.data('user-id') + '?';
        } else {
            placeholderText = 'Снять бан с пользователя #' + button.data('user-id') + '?';
        }

        $('#ban-user-modal-placeholder').text(placeholderText);
        $('#user-ban-accept').data('user-id', button.data('user-id'));
    });

    // Подтверждение бана/анбана пользователя
    $('#user-ban-accept').click(function() {
        var userId = $(this).data('user-id');
        if (!(parseInt(userId) > 0)) {
            $('#ban-user-modal .modal-body').append($('<p class="text-danger">Ошибка! Невозможно забанить/разбанить.</p>'));
        }

        $.ajax({
            url: '/admin/users/ban/',
            type: 'post',
            dataType: 'json',
            data: {userId: userId},
            success: function(data) {
                if (data.status !== undefined && data.status == 'ok') {
                    var banned = parseInt(data.ban);
                    var buttonWrapper = $('.user-item-' + userId + ' span.ban-user');
                    var buttonWrapperTitle = '';
                    var button = $('button', buttonWrapper);
                    var icon = $('span.glyphicon', button);
                    if (banned) {
                        buttonWrapperTitle = 'Снять бан c пользователя';
                        button.data('user-banned', 1);
                        button.removeClass('btn-danger');
                        button.addClass('btn-warning');
                        icon.removeClass('glyphicon-ban-circle');
                        icon.addClass('glyphicon-ok-circle');

                        $('.user-item-' + userId + ' td.params').append($('<span class="glyphicon glyphicon-ban-circle text-danger" aria-hidden="true" title="Забанен"></span>')).tooltip();
                    } else {
                        buttonWrapperTitle = 'Забанить пользователя';
                        button.data('user-banned', 0);
                        button.removeClass('btn-warning');
                        button.addClass('btn-danger');
                        icon.removeClass('glyphicon-ok-circle');
                        icon.addClass('glyphicon-ban-circle');

                        $('.user-item-' + userId + ' td.params .glyphicon-ban-circle').remove();
                    }
                    buttonWrapper.tooltip('hide')
                        .attr('data-original-title', buttonWrapperTitle)
                        .tooltip('fixTitle')
                        .tooltip('show');

                    $('#ban-user-modal').modal('hide');
                } else {
                    $('#ban-user-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке забанить/разбанить. Возможно у вас не хватает прав.</p>'));
                }
            },
            error: function() {
                $('#ban-user-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке забанить/разбанить. Возможно у вас не хватает прав.</p>'));
            }
        });
    });
});