$(function() {
    $('.delete-news').modal({show: false});

    // Настройки при вызове модального окна удаления новости
    $('#delete-news-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#news-id-modal-placeholder').text(button.data('news-id'));
        $('#news-delete-accept').data('news-id', button.data('news-id'));
    });

    // Подтверждение удаления новости
    $('#news-delete-accept').click(function() {
        var newsId = $(this).data('news-id');
        if (!(parseInt(newsId) > 0)) {
            $('#delete-news-modal .modal-body').append($('<p class="text-danger">Ошибка! Невозможно удалить.</p>'));
        }

        $.ajax({
            url: '/admin/delete-news-item/',
            type: 'post',
            dataType: 'json',
            data: {newsId: newsId},
            success: function(data) {
                if (data.status !== undefined && data.status == 'ok') {
                    $('#news-item-' + newsId).fadeOut();
                    $('#delete-news-modal').modal('hide');
                } else {
                    $('#delete-news-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке удаления.</p>'));
                }
            },
            error: function() {
                $('#delete-news-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке удаления.</p>'));
            }
        });
    });

    // Настройки при вызове модального окна удаления изображения новости
    $('#delete-news-img-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $('#news-id-img-modal-placeholder').text(button.data('news-id'));
        $('#news-img-delete-accept').data('news-id', button.data('news-id'));
    });

    // Подтверждение удаления изображения новости
    $('#news-img-delete-accept').click(function() {
        var newsId = $(this).data('news-id');
        if (!(parseInt(newsId) > 0)) {
            $('#delete-news-img-modal .modal-body').append($('<p class="text-danger">Ошибка! Невозможно удалить.</p>'));
        }

        $.ajax({
            url: '/admin/delete-news-img/',
            type: 'post',
            dataType: 'json',
            data: {newsId: newsId},
            success: function(data) {
                if (data.status !== undefined && data.status == 'ok') {
                    var newsRow = $('#news-item-' + newsId);
                    var picIcons = $('.glyphicon-picture', newsRow);
                    for (var i = 0; i < picIcons.length; i++) {
                        var btnParent = $(picIcons[i]).parent('.btn');
                        if (btnParent.length > 0) {
                            btnParent.remove();
                        } else {
                            $(picIcons[i]).remove();
                        }
                    }
                    $('#delete-news-img-modal').modal('hide');
                } else {
                    $('#delete-news-img-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке удаления.</p>'));
                }
            },
            error: function() {
                $('#delete-news-img-modal .modal-body').append($('<p class="text-danger">Ошибка при попытке удаления.</p>'));
            }
        });
    });
});