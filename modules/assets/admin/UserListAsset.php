<?php

namespace app\modules\assets\admin;

use yii\web\AssetBundle;

/**
 * Cтили для страницы со списком пользователей
 *
 * @package app\modules\assets\admin
 */
class UserListAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/assets/admin';
    public $js = [
        'js/user_list.js',
    ];
    public $depends = [
        'app\modules\assets\admin\AdminAsset'
    ];
}