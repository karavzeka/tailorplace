<?php

namespace app\modules\assets\admin;

use yii\web\AssetBundle;

/**
 * Общие стили для админки
 * Здесь подключается тема https://bootswatch.com/slate/
 * @package app\modules\assets\admin
 */
class AdminAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/assets/admin';
    public $css = [
        'css/bootstrap-theme.min.css',
        'css/adminCommon.css'
    ];
    public $js = [
        'js/bootstrap.file-input.js',
        'js/adminCommon.js'
    ];
    public $depends = [
        'app\assets\IconAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}