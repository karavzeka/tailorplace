<?php

namespace app\modules\assets\admin;

use yii\web\AssetBundle;

/**
 * Cтили для страницы со списком новостей
 *
 * @package app\modules\assets\admin
 */
class NewsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/assets/admin';
    public $css = [
        'css/news.css',
    ];
    public $js = [
        'js/news.js',
    ];
    public $depends = [
        'app\modules\assets\admin\AdminAsset'
    ];
}