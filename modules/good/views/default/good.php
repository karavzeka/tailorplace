<?php
/**
 * Страница товара
 * @var yii\web\View $this
 * @var app\modules\good\models\Good $Good
 * @var bool $inBasket
 * @var app\modules\good\models\Photo[] $Photos
 */

use app\assets\AppAsset;
use app\assets\MagnificPopupAsset;
use app\assets\CommentAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\good\models\Photo;

$this->registerCssFile('@web/css/good.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/good.js', [
    'depends' => [
        AppAsset::className(),
        MagnificPopupAsset::className(),
    ]
]);
CommentAsset::register($this);

$this->title = $Good->name . ($Good->studio->type == 'atelier' ? ' - пошив на заказ' : '');
$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::$app->commonTools->metaDescriptionLengthOptimize('Цена ' . str_replace(' ', '', Yii::$app->numberHelper->numberToPrice($Good->price)) . 'руб. ' . $Good->description)
]);

$Photos = $Good->photo;
$MainPhoto = null;
$mainPhotoKey = 0;
foreach ($Photos as $key => $Photo) {
    if ($Photo->main) {
        $MainPhoto = $Photo;
        $mainPhotoKey = $key;
    }
}
// главную фотографию на первое место
if ($mainPhotoKey > 0) {
    unset($Photos[$mainPhotoKey]);
    array_unshift($Photos, $MainPhoto);
}
?>
<main>
    <h2><?= Html::encode($Good->name) ?></h2>
    <div>
        <div id="photo-col">
            <div id="main-photo-wrapper">
                <img src="<?= $MainPhoto ? $MainPhoto->getSrc('_big') : Photo::getNoPhotoSrc() ?>" width="378" height="378" id="main-photo" alt="<?= Html::encode($Good->name) ?>">
            </div>
            <?php
            if (count($Photos) > 0) {
                echo Html::beginTag('div', ['class' => 'thumbnails-block']);
                foreach ($Photos as $key => $Photo) {
                    echo Html::beginTag('div', $Photo->main ? ['class' => 'active'] : []);
                        echo Html::img($Photo->getSrc('_small'), [
                            'width' => 88,
                            'height' => 88,
                            'data-full-img' => $Photo->getSrc(),
                            'data-index' => $key,
                            'alt' => Html::encode($Good->name)
                        ]);
                    echo Html::endTag('div');
                }
                echo Html::endTag('div');
            }
            ?>
        </div>
        <div id="main-info">
            <h3 id="price-label">Цена:</h3>
            <div>
                <div id="price">
                    <span><?= Yii::$app->numberHelper->numberToPrice($Good->price) ?></span> <span>руб.</span>
                </div>
                <div id="order-block">
                    <?php
                    if (Yii::$app->user->id != $Good->studio->user->id) {
                        $buttonOptions = ['id' => 'to-order'];
                        if (Yii::$app->user->isGuest) {
                            $buttonOptions['class'] = 'button disable';
                            $insideButton = Html::button('<i class="icon rouble-light"></i><span>Заказать</span>', [
                                'class' => 'icon-line',
                                'disabled' => 'disabled',
                            ]);
                            $underOrder =
                                Html::beginTag('span', ['id' => 'need-authorization', 'class' => 'icon-line']) .
                                Html::tag('i', '', ['class' => 'icon attention']) .
                                Html::a('Для заказа <br> товара требуется авторизация', '/login/') .
                                Html::endTag('span');
                        } else {
                            $buttonOptions['class'] = 'button yellow';
                            $buttonOptions['data-good-id'] = $Good->id;

                            $insideButton = Html::a('<i class="icon rouble-white"></i><span>Заказать</span>', '', [
                                'data-type' => 'button',
                                'class' => 'icon-line'
                            ]);

                            $underOrder = Html::beginTag('span', ['id' => 'to-basket', 'class' => 'icon-line']);
                            if ($inBasket) {
                                $underOrder .= Html::tag('i', '', ['class' => 'icon basket']) .
                                    Html::tag('span', 'В корзине');
                            } else {
                                $underOrder .= Html::tag('i', '', ['class' => 'icon basket-gold']) .
                                    Html::a('В корзину', '#to-basket', ['class' => 'dashed-underline', 'data-good-id' => $Good->id]);
                            }
                            $underOrder .= Html::endTag('span');
                        }
                        echo Html::beginTag('div', $buttonOptions);
                        echo Html::tag('div', '', ['class' => 'low-layer']);
                        echo $insideButton;
                        echo Html::endTag('div');
                        echo $underOrder;
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($Good->craft_time || $Good->quantity) {
                echo Html::beginTag('div', ['class' => 'additional-params']);
                    echo Html::beginTag('ul', ['class' => 'italic']);
                        if ($Good->craft_time) {
                            echo '<li><span>Срок изготовления:</span> ' . Html::encode($Good->craft_time) . '</li>';
                        }
                        if ($Good->quantity) {
                            if ($Good->studio->type == 'atelier') {
                                echo '<li><span>Готовых экземпляров:</span> ' . $Good->quantity . ' шт.</li>';
                            } else {
                                echo '<li><span>В наличии:</span> ' . $Good->quantity . ' шт.</li>';
                            }
                        }
                    echo Html::endTag('ul');
                echo Html::endTag('div');
            }

            ?>
            <div id="studio-block">
                <div class="name">
                    <div class="icon-circle <?= $Good->studio->type ?>"><i></i></div><span><a href="<?= Url::toRoute('/studio/' . $Good->studio->id) ?>"><?= $Good->studio->name ?></a></span>
                </div>
                <div class="city italic"><?= $Good->studio->country->name_ru . ($Good->studio->city->id ? ', ' . $Good->studio->city->name_ru : '') ?></div>
                <div id="rating" class="icon-line">
                    <i class="icon green-star"></i><span>Репутация</span> <span><?= $Good->studio->user->sellerReputation() ?>%</span>
                </div>
                <div id="order-count" class="icon-line">
                    <i class="icon rouble"></i><span>Выполнено заказов</span> <span><?= $Good->studio->finishedOrders() ?></span>
                </div>
                <?php
                if (Yii::$app->user->id != $Good->studio->user->id) {
                    $linkText = ($Good->studio->type == 'atelier') ? 'Написать мастеру' : 'Написать продавцу';
                    $buttonOptions = [];

                    if (Yii::$app->user->isGuest) {
                        $buttonOptions['class'] = 'button disable';
                        $insideButton = Html::button($linkText, [
                            'class' => 'icon-line',
                            'disabled' => 'disabled',
                        ]);
                    } else {
                        $buttonOptions['class'] = 'button';
                        $insideButton = Html::a($linkText, Url::toRoute('/cabinet/dialog/' . $Good->studio->user->id), [
                            'data-type' => 'button',
                            'target' => '_blank'
                        ]);
                    }
                    echo Html::beginTag('div', $buttonOptions);
                    echo Html::tag('div', '', ['class' => 'low-layer']);
                    echo $insideButton;
                    echo Html::endTag('div');
                }
                ?>
            </div>
            <div id="social-block">
                <?= \app\widgets\SocialButtons::widget([
                    'title' => $Good->name,
                    'description' => $Good->description ? $Good->description : null,
                    'imageUrl' => $MainPhoto ? Url::to($MainPhoto->getSrc('_big'), true) : null
                ]) ?>
            </div>
            <h3 class="description-label">Описание</h3>
            <p id="good-description"><?= Html::encode($Good->description) ?></p>
        </div>
        <aside id="additional-info">
            <h3 class="big-red-medium">Категории товара</h3>
            <ul>
                <?php
                foreach ($Good->category as $Category) {
                    echo Html::beginTag('li', ['class' => 'icon-line']);
                        echo Html::tag('i', '', ['class' => 'icon ' . $Category->css_class]);
                        echo $Category->name_ru;
                    echo Html::endTag('li');
                }
                ?>
            </ul>
            <h3 class="big-red-medium">Доставка</h3>
            <ul>
                <?php
                if (count($Good->studio->delivery) > 0 || !empty($Good->studio->custom_delivery)) {
                    foreach ($Good->studio->delivery as $Delivery) {
                        echo Html::beginTag('li', ['class' => 'icon-line']);
                            echo Html::tag('i', '', ['class' => 'icon ' . $Delivery->css_class]);
                            echo $Delivery->name_ru;
                        echo Html::endTag('li');
                    }
                    if (isset($Good->studio->custom_delivery) && !empty($Good->studio->custom_delivery)) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon other']);
                            echo Html::tag('span', Html::encode($Good->studio->custom_delivery));
                        echo '</li>';
                    }
                } else {
                    echo '<li class="lighter">Не указано</li>';
                }
                ?>
            </ul>
            <h3 class="big-red-medium">Оплата</h3>
            <ul>
                <?php
                if (count($Good->studio->payment) > 0 || !empty($Good->studio->custom_payment)) {
                    foreach ($Good->studio->payment as $Payment) {
                        echo Html::beginTag('li', ['class' => 'icon-line']);
                            echo Html::tag('i', '', ['class' => 'icon ' . $Payment->css_class]);
                            echo $Payment->name_ru;
                        echo Html::endTag('li');
                    }
                    if (isset($Good->studio->custom_payment) && !empty($Good->studio->custom_payment)) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon other']);
                            echo Html::tag('span', Html::encode($Good->studio->custom_payment));
                        echo '</li>';
                    }
                } else {
                    echo '<li class="lighter">Не указано</li>';
                }
                ?>
            </ul>
        </aside>
        <div class="clear"></div>
    </div>
    <!-- TODO реализовать возможность пожаловаться на товар -->
<!--    <div id="complain" class="icon-line">-->
<!--        <i class="icon attention-red"></i><span><a href="">Пожаловаться на товар</a></span>-->
<!--    </div>-->
</main>
<div id="comments-area">
    <div>
        <div class="comments-caption">Комментарии</div>
        <div class="hide-comments">↑ <span class="dashed-underline">Свернуть</span></div>
        <div class="clear"></div>
    </div>

    <?php
    $CommentWidget = \app\widgets\CommentsWidget::begin([
        'objectId' => $Good->id,
        'objectType' => \app\models\Comment::TYPE_GOOD,
        'objectOwner' => $Good->studio->user->id,
    ]);
    $CommentWidget->makeCommentsBlock();
    ?>

    <div class="sloping-line"></div>

    <?php
    $CommentWidget->makeMainCommentInput();
    $CommentWidget->end();
    ?>
</div>
