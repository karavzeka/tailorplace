<?php
/**
 * Страница товаров
 * @var yii\web\View $this
 * @var int $countOfGoods
 * @var string $goodsList сформированный HTML списка товаров
 * @var array $CategoryList Список категорий товаров
 * @var string $baseUrl Url страницы товаров без GET параметров
 * @var string $orderBy По какому полю сортировать товары
 * @var string $direction Направление сортировки товаров
 * @var bool $showMoreButton Отображать ли кнопку "Показать еще"
 */

use app\assets\AppAsset;
use yii\helpers\Html;

$this->registerCssFile('@web/css/goods.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/goods.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Товары';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Пошив на заказ. Подберите себе уникальный наряд, неповторимое платье или просто удобную одежду. Портные создадут ее для вас.'
]);

$studioTypes = [
    'all' => [
        'name' => 'Все',
        'icon-class' => 'rouble',
    ],
    'atelier' => [
        'name' => 'Ателье',
        'icon-class' => 'atelier',
    ],
    'store' => [
        'name' => 'Магазин',
        'icon-class' => 'store',
    ],
];
if (isset($filterConditions['studioType'])) {
    $studioTypes[$filterConditions['studioType']]['active'] = true;
} else {
    $studioTypes['all']['active'] = true;
}
?>
<main>
    <aside class="float left">
        <section id="studio-section">
            <h3 class="big-red-medium">Категория продавца</h3>
            <ul id="studio-type">
                <?php
                foreach ($studioTypes as $key => $type) {
                    echo '<li class="icon-line">';
                        echo Html::tag('div', '<i></i>', ['class' => 'icon-circle small ' . $type['icon-class']]);
                        $spanClass = $type['active'] ? 'red active' : 'dashed-underline';
                        echo Html::tag('span', $type['name'], [
                            'class' => $spanClass,
                            'data-filter-studio' => $key
                        ]);
                    echo '</li>';
                }
                ?>
            </ul>
        </section>

        <div id="filters-reset">
             <span class="dashed-underline size15">Сбросить фильтры ↓</span>
        </div>

        <!-- TODO слайдер добавить и запрограммировать уже после запуска сайта, ибо может быть сложно рассчитывать максимальную цену при уже выбранных категориях -->
        <section id="price-section">
            <h3 class="big-red-medium">Диапазон цен</h3>
            <div class="price-inputs">
                <label for="low-price-limit">От</label>
                <input type="text" id="low-price-limit" class="form-control" value="<?= $filterConditions['priceLow'] ? $filterConditions['priceLow'] : '0' ?>">
                <label for="high-price-limit">до</label>
                <input type="text" id="high-price-limit" class="form-control" value="<?= $filterConditions['priceHigh'] ? $filterConditions['priceHigh'] : '' ?>">
            </div>
            <!--
            <div id="price-slider">
                <div class="line">
                    <div class="left-control"></div>
                    <div class="right-control"></div>
                </div>
                <div class="limits">
                    <span class="left">0</span>
                    <span class="right">10 000 (max)</span>
                </div>
            </div>
            -->
        </section>

        <section id="category-section">
            <h3 class="big-red-medium">Категория товаров</h3>
            <?php
            echo Html::beginTag('ul', [
                'id' => 'active-category-list',
                'class' => 'icon-line ' . ((!isset($filterConditions['category']) || count($filterConditions['category']) == 0) ? ' hide' : '')]
            );
            if (isset($filterConditions['category']) && is_array($filterConditions['category'])) {
                foreach ($filterConditions['category'] as $categoryId) {
                    echo '<li>';
                        echo Html::beginTag('div', [
                            'class' => 'category-wrapper',
                            'data-type' => $CategoryList[$categoryId]->type,
                            'data-id' => $categoryId,
                            'data-sort-pos' => $CategoryList[$categoryId]->sort_position
                        ]);
                            echo Html::tag('i', '', ['class' => 'icon ' . $CategoryList[$categoryId]->css_class]);
                            echo ' ';
                            echo Html::tag('span', $CategoryList[$categoryId]->name_ru);
                        echo Html::endTag('div');
                        echo Html::tag('i', '', ['class' => 'icon delete-red']);
                    echo '</li>';
                }
            }
            echo Html::endTag('ul');
            echo '<ul class="icon-line size15" id="category-list">';
                foreach ($CategoryList as $Category) {
                    if (isset($filterConditions['category']) && is_array($filterConditions['category']) && in_array($Category->id, $filterConditions['category'])) {
                        continue;
                    }
                    $liClass = (isset($filterConditions['studioType']) && $filterConditions['studioType'] && $Category->type != $filterConditions['studioType'] && $Category->type != 'all') ? ' style="display: none;"' : '';
                    echo '<li' . $liClass .'>';
                        echo Html::beginTag('div', [
                            'class' => 'category-wrapper',
                            'data-type' => $Category->type,
                            'data-id' => $Category->id,
                            'data-sort-pos' => $Category->sort_position
                        ]);
                            echo Html::tag('i', '', ['class' => 'icon ' . $Category->css_class]);
                            echo ' ';
                            echo Html::tag('span', $Category->name_ru);
                        echo Html::endTag('div');
                    echo '</li>';
                }
            echo '</ul>';
            ?>
        </section>
    </aside>

    <div class="float right">
        <h2>Все товары</h2>
        <!-- TODO сделать оформление товаров списком и раскомментировать кнопки -->
<!--        <div id="view-style">
            <div class="icon-square tile active"><i></i></div><div class="icon-square list"><i></i></div>
        </div>
        <div class="clear"></div>-->
        <div id="goods-header">
            <ul class="size16">
                <li class="bold-underline">Все товары</li>
<!--                <li>Скидки</li>-->
<!--                <li><a href="" class="size16">Новинки</a></li>-->
            </ul>
            <div class="sort-type">
                <input type="hidden" id="base-url" value="<?= $baseUrl ?>">
                <span class="size16">Сортировать по</span>
                <div class="select">
                    <select id="sort-selector">
                        <option value="date_desc" <?= ($orderBy == 'date' && $direction == 'DESC') ? 'selected' : '' ?>>Дате (сначала новые)</option>
                        <option value="date_asc" <?= ($orderBy == 'date' && $direction != 'DESC') ? 'selected' : '' ?>>Дате (сначала старые)</option>
                        <option value="name_asc" <?= ($orderBy == 'name' && $direction != 'DESC') ? 'selected' : '' ?>>Названию А-Я</option>
                        <option value="name_desc" <?= ($orderBy == 'name' && $direction == 'DESC') ? 'selected' : '' ?>>Названию Я-А</option>
                        <option value="price_asc" <?= ($orderBy == 'price' && $direction != 'DESC') ? 'selected' : '' ?>>Возрастанию цены</option>
                        <option value="price_desc" <?= ($orderBy == 'price' && $direction == 'DESC') ? 'selected' : '' ?>>Убыванию цены</option>
                    </select>
                    <div class="select-button">
                        <span class="caret"></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="count-of-goods" class="size15"><?= Yii::t('app', '{n, plural, =0{Товаров не найдено} one{Найден # товар} few{Найдено # товара} many{Найдено # товаров} other{Найдено # товаров}}', ['n' => $countOfGoods]) ?></div>
        <div id="goods-wrapper" class="good-tile" data-count-of-goods="<?= $countOfGoods ?>">
            <?php
            echo $goodsList;
            ?>
        </div>
        <div id="show-more">
            <?php if ($showMoreButton) :?>
            <div id="show-more-button" data-part="1">
                <span class="plus-button"></span>
                <span>Показать еще</span>
            </div>
            <?php endif ?>
        </div>
    </div>
    <div class="clear"></div>
</main>