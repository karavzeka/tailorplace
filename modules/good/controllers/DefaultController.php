<?php

namespace app\modules\good\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\controllers\CommonController;
use app\modules\good\models\Good;
use app\modules\good\models\Photo AS GoodPhoto;
use app\models\GoodCategory;
use app\modules\users\models\Basket;


class DefaultController extends CommonController
{
    /**
     * @var int По сколько товаров выводить в запросе
     */
    private $goodsLimit = 15;
    /**
     * @var string Название сессионной переменной для хранения фильров для товаров на главной странице товаров
     */
    private $sessionGoodFilterName = 'filter-good-main';
    /**
     * @var null|array состояние фильтров товаров
     */
    private $filterConditions;

	public function actionIndex()
	{
        // если пользователь пришел с другой страницы, то фильтры сбрасываются
        // если пользователь обновил страницу (или например изменил сортировку), то фильтры сохраняются
        $referrer = Url::previous('commonReferrer');
        $ReferrerRequest = new \yii\web\Request();
        $ReferrerRequest->setUrl($referrer);
        $referrerUrlRoute = Yii::$app->urlManager->parseRequest($ReferrerRequest);
        if (!is_array($referrerUrlRoute) || ($referrerUrlRoute[0] != $this->route)) {
            Yii::$app->session->remove($this->sessionGoodFilterName);
        } else {
            $this->filterConditions = Yii::$app->session->get($this->sessionGoodFilterName);
        }

        $this->addBreadcrumbsItem(['label' => 'Товары']);

        $baseUrl = Yii::$app->request->hostInfo . '/goods/';

        // параметры сортировки
        $dbName = Yii::$app->params['tailor_db'];
        $sortFields = [
            'default' => 'date',
            'date' => '`' . $dbName . '`.`goods`.`create_time`',
            'name' => '`' . $dbName . '`.`goods`.`name`',
            'price' => '`' . $dbName . '`.`goods`.`price`',
        ];
        $Sorter = new \app\components\SortController('mainGoodList', $sortFields);
        $sortParams = $Sorter->getSort();

        // преобразование значений фильтров в часть WHERE запроса
        $whereCondition = $this->makeWhere($this->filterConditions);
        $goodsData = $this->getGoods($whereCondition, null, $sortParams['sortQueryPart']);

        $goodsList = $this->renderPartial('@app/views/parts/goodsList', [
            'goodsData' => $goodsData,
            'fillByEmptyGoods' => true
        ]);

        // если всего товаров, соответствующих фильтрам, больше чем ограничение в одной выборке, то отображаем кнопку "Показать еще"
        $showMoreButton = $goodsData['countOfGoods'] > $this->goodsLimit;

        $CategoryList = GoodCategory::find()->orderBy('sort_position')->all();
        $CategoryList = ArrayHelper::index($CategoryList, 'id');

		return $this->render('index', [
            'countOfGoods' => $goodsData['countOfGoods'],
            'goodsList' => $goodsList,
            'CategoryList' => $CategoryList,
            'baseUrl' => $baseUrl,
            'orderBy' => $sortParams['sortField'],
            'direction' => $sortParams['sortDirection'],
            'filterConditions' => $this->filterConditions,
            'showMoreButton' => $showMoreButton
        ]);
	}

    /**
     * Страница товаров
     *
     * @return string
     */
    public function actionGood()
    {
        $goodId = (int)Yii::$app->request->get('goodId');
        if (!$goodId) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
        $Good = Good::findOne($goodId);
        if (!$Good) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        $this->addBreadcrumbsItem(['label' => 'Товары', 'url' => '/goods/']);
        $this->addBreadcrumbsItem(['label' => $Good->name]);

        $inBasket = (bool)Basket::findOne([
            'user_id' => Yii::$app->user->identity->id,
            'good_id' => $goodId
        ]);

        return $this->render('good', [
            'Good' => $Good,
            'inBasket' => $inBasket
        ]);
    }

    /**
     * Делает выборку товаров в соответствии с заданными условиями
     * В добавок подтягивает фотографии к товарам
     *
     * @param string|null $wherePart Часть WHERE запроса
     * @param int|null $offset Смещение выборки
     * @param string|null $orderBy Поле для сортировки
     * @param bool $calcRows Подсчитывать ли общее кол-во строк, уодовлетворяющее запросу без учета LIMIT
     * @return array Массив с товарами и фотографиями
     */
    public function getGoods($wherePart = null, $offset = null, $orderBy = null, $calcRows = true)
    {
        //выборка товаров
        $GoodsSelector = Good::find()
            ->select('DISTINCT
                `goods`.`id` AS `good_id`,
                `goods`.`name` AS `good_name`,
                `goods`.`price` AS `price`,
                `studio`.`id` AS `studio_id`,
                `studio`.`name` AS `studio_name`,
                `studio`.`type` AS `studio_type`')
            ->innerJoin('studio', '`goods`.`studio_id` = `studio`.`id`')
            ->limit($this->goodsLimit);
        if (isset($this->filterConditions['category'])) {
            // добавляем JOIN только если есть фильтрация по категории
            $GoodsSelector->innerJoin('good_category', '`goods`.`id` = `good_category`.`good_id`');
        }
        if ($wherePart) {
            $GoodsSelector->where($wherePart);
        }
        if ($offset) {
            $GoodsSelector->offset($offset);
        }
        if ($orderBy) {
            $GoodsSelector->orderBy($orderBy);
        }
        $goods = $GoodsSelector->asArray()->all();

        if ($calcRows) {
            $countOfGoods = $GoodsSelector->orderBy(null)->count('DISTINCT `goods`.`id`');
        } else {
            $countOfGoods = null;
        }
        $goodIds = ArrayHelper::getColumn($goods, 'good_id');

        //выборка фотографий товаров
        $Photos = GoodPhoto::findAll([
            'good_id' => $goodIds,
            'main' => 1,
        ]);
        $photoArray = [];
        foreach ($Photos as $Photo) {
            $photoArray[$Photo->good_id] = $Photo;
        }

        return ['goods' => $goods, 'photos' => $photoArray, 'countOfGoods' => $countOfGoods];
    }

    /**
     * Формирование только контента товаров для блока товаров.
     * Вызывается тоьлко по Ajax
     */
    public function actionOnlyGoods()
    {
        if (Yii::$app->request->isAjax) {
            $requestFilters = Yii::$app->request->post('filters', null);
            $filterConditions = [];

            if (isset($requestFilters['studioType'])) {
                $filterConditions['studioType'] = in_array($requestFilters['studioType'], ['atelier', 'store']) ? $requestFilters['studioType'] : null;
            }

            if (isset($requestFilters['priceLow'])) {
                $priceVal = Yii::$app->numberHelper->priceToDBVal($requestFilters['priceLow'], false);
                $filterConditions['priceLow'] = $priceVal ? $priceVal : null;
            }

            if (isset($requestFilters['priceHigh'])) {
                $priceVal = Yii::$app->numberHelper->priceToDBVal($requestFilters['priceHigh'], false);
                $filterConditions['priceHigh'] = $priceVal ? $priceVal : null;
            }

            if (isset($requestFilters['category']) && is_array($requestFilters['category']) && count($requestFilters['category'])) {
                foreach ($requestFilters['category'] as $categoryId) {
                    if ((int)$categoryId > 0) {
                        $filterConditions['category'][] = (int)$categoryId;
                    }
                }
            }

            // смещение выборки
            $offset = 0;
            if (isset($requestFilters['part'])) {
                $offset = (int)$requestFilters['part'] * $this->goodsLimit;
            }
            // если смещение 0, то это новый фильтр и надо подсчитать общее кол-вот товаров, удовлетворяющих фильтрам
            $calcCountOfGoods = $offset > 0 ? false : true;

            // параметры сортировки
            $dbName = Yii::$app->params['tailor_db'];
            $sortFields = [
                'default' => 'date',
                'date' => '`' . $dbName . '`.`goods`.`create_time`',
                'name' => '`' . $dbName . '`.`goods`.`name`',
                'price' => '`' . $dbName . '`.`goods`.`price`',
            ];
            $Sorter = new \app\components\SortController('mainGoodList', $sortFields);
            $sortParams = $Sorter->getSort();

            $this->filterConditions = $filterConditions;
            Yii::$app->session->set($this->sessionGoodFilterName, $filterConditions);

            $whereCondition = $this->makeWhere($filterConditions);
            $goodsData = $this->getGoods($whereCondition, $offset, $sortParams['sortQueryPart'], $calcCountOfGoods);

            $countText = Yii::t('app', '{n, plural, =0{Товаров не найдено} one{Найден # товар} few{Найдено # товара} many{Найдено # товаров} other{Найдено # товаров}}', ['n' => $goodsData['countOfGoods']]);

            $html = $this->renderPartial('@app/views/parts/goodsList', [
                'goodsData' => $goodsData,
                'fillByEmptyGoods' => false
            ]);
            return json_encode([
                'html' => $html,
                'countText' => $countText,
                'countOfGoods' => $goodsData['countOfGoods'],
                'goodsOnPage' => count($goodsData['goods']) + $offset
            ]);
        } else {
            return '';
        }
    }

    /**
     * Преобразует массив с состоянием фильтров в WHERE запрос
     *
     * @param array $conditions
     * @return string
     */
    private function makeWhere($conditions)
    {
        if (!$conditions) {
            return '';
        } else {
            $where = '1';

            if (isset($conditions['studioType']) && in_array($conditions['studioType'], ['atelier', 'store'])) {
                $where .= " AND `studio`.`type` = '" . $conditions['studioType'] . "'";
            }

            if (isset($conditions['priceLow']) && $conditions['priceLow'] > 0) {
                $where .= " AND `goods`.`price` >= " . Yii::$app->numberHelper->priceToDBVal($conditions['priceLow']);
            }

            if (isset($conditions['priceHigh']) && $conditions['priceHigh'] > 0) {
                $where .= " AND `goods`.`price` <= " . Yii::$app->numberHelper->priceToDBVal($conditions['priceHigh']);
            }

            if (isset($conditions['category']) && count($conditions['category']) > 0) {
                $where .= " AND `good_category`.`category_id` IN (" . implode(',', $conditions['category']) . ")";
            }

            return $where;
        }
    }

    /**
     * Установить лимит выборки товаров
     *
     * @param $limit
     */
    public function setGoodsLimit($limit)
    {
        $this->goodsLimit = $limit;
    }

    /**
     * Получить лимит выборки товаров
     *
     * @return int
     */
    public function getGoodsLimit()
    {
        return $this->goodsLimit;
    }
}