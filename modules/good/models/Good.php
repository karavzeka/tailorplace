<?php

namespace app\modules\good\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\Delivery;
use app\models\Payment;
use app\models\GoodCategory;
use app\modules\studio\models\Studio;

class Good extends ActiveRecord
{

    /**
     * @var boolean Вызывался ли метод получения фотографий из базы
     */
    private $alreadyGetPhotos = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%goods}}';
    }

    /**
     * Получить фотографии товара
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasMany(Photo::className(), ['good_id' => 'id']);
    }

    public function getMainPhoto()
    {
        return $this->hasOne(Photo::className(), ['good_id' => 'id'])
            ->where('main=1');
    }

    /**
     * Получить связанную с товаром студию
     * @return \yii\db\ActiveQuery
     */
    public function getStudio()
    {
        return $this->hasOne(Studio::className(), ['id' => 'studio_id']);
    }

    /**
     * Получить категории, связанные с товаром
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasMany(GoodCategory::className(), ['id' => 'category_id'])
            ->viaTable('good_category', ['good_id' => 'id']);
    }
}
