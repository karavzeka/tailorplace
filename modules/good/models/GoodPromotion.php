<?php
/**
 * Модель продвинутого товара
 */

namespace app\modules\good\models;

use Yii;
use yii\db\ActiveRecord;

class GoodPromotion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{goods_promotion}}';
    }

    /**
     * Возвращает экземпляр товара
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['id' => 'good_id']);
    }
}