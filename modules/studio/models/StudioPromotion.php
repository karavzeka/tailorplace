<?php
/**
 * Модель продвинутой студии
 */

namespace app\modules\studio\models;

use Yii;
use yii\db\ActiveRecord;

class StudioPromotion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{studio_promotion}}';
    }

    /**
     * Возвращает экземпляр студии
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudio()
    {
        return $this->hasOne(Studio::className(), ['id' => 'studio_id']);
    }
}