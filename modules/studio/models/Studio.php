<?php

namespace app\modules\studio\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\City;
use app\models\Country;
use app\models\Delivery;
use app\models\Order;
use app\models\Payment;
use app\modules\good\models\Good;
use app\modules\users\models\User;

class Studio extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{studio}}';
    }

    /**
     * Получить связанные со студией способы доставки
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Получить связанные со студией способы доставки
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasMany(Delivery::className(), ['id' => 'delivery_id'])
            ->viaTable('studio_delivery', ['studio_id' => 'id']);
    }

    /**
     * Получить связанные со студией способы оплаты
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasMany(Payment::className(), ['id' => 'payment_id'])
            ->viaTable('studio_payment', ['studio_id' => 'id']);
    }

    /**
     * Получить коллекцию товаров студии
     * @param  boolean $withMainPhotos Подтянуть вместе с товароми их глвные фотографии?
     * @return \yii\db\ActiveQuery
     */
    public function getGood($withMainPhotos = false)
    {
        if ($withMainPhotos) {
            return $this->hasMany(Good::className(), ['studio_id' => 'id'])
                ->with(['photo' => function($query) {
                    $query->andWhere('main = 1');
                }]);
        } else {
            return $this->hasMany(Good::className(), ['studio_id' => 'id']);
        }
    }

    /**
     * Получить страну студии
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        if ($this->country_id) {
            return $this->hasOne(Country::className(), ['id' => 'country_id']);
        } else {
            return new Country();
        }
    }

    /**
     * Получить город студии
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        if ($this->city_id) {
            return $this->hasOne(City::className(), ['id' => 'city_id']);
        } else {
            return new City();
        }
    }

    /**
     * Возвращает связанный экземпляр продвинутости студии
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStudioPromotion()
    {
        return $this->hasOne(StudioPromotion::className(), ['studio_id' => 'id']);
    }

    /**
     * Сколько завершенных продаж имеется у студии
     */
    public function finishedOrders()
    {
        $Orders = $this->user->getOrder('sell')->andWhere(['status_id' => 2])->count();
        return $Orders;
    }

    /**
     * Возвращает список категорий товаров, и кол-ва товаров с данной категорией
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function categoriesOfGoods()
    {
        $dbName = Yii::$app->params['tailor_db'];
        $sql = "
            SELECT
                `cl`.`name_ru` AS `name_ru`,
                `cl`.`css_class` AS `css_class`,
                COUNT(*) AS `count`
            FROM
                `" . $dbName . "`.`goods` AS `g`
            INNER JOIN
                `" . $dbName . "`.`good_category` AS `gc`
            ON
                `gc`.`good_id` = `g`.`id`
            INNER JOIN
                `" . $dbName . "`.`category_list` AS `cl`
            ON
                `gc`.`category_id` = `cl`.`id`
            WHERE
                `g`.`studio_id` = " . $this->id . "
            GROUP BY
                `cl`.`id`
            ORDER BY
                `cl`.`name_ru` ";
        $rows = Yii::$app->db->createCommand($sql)->queryAll();
        return $rows;
    }

    /**
     * Возвращает текущий порог товаров
     *
     * @var int $countOfDeals Кол-во завершенных продаж студии
     * @return array
     */
    public function getCurrentThreshold($countOfDeals = null)
    {
        if ($countOfDeals === null) {
            $countOfDeals = $this->finishedOrders();
        }
        $currentThreshold = [
            'threshold' => 0,
            'goodsLimit' => 0,
            'countOfDeals' => $countOfDeals,
        ];
        foreach (Yii::$app->params['goodThresholds'] as $threshold => $goodsLimit) {
            $currentThreshold['threshold'] = $threshold;
            $currentThreshold['goodsLimit'] = $goodsLimit;

            if ($countOfDeals < $threshold) {
                break;
            }
        }

        return $currentThreshold;
    }

    /**
     * Проверка, не превышен ли лимит товаров студии
     *
     * @return bool
     */
    public function mayAddGood()
    {
        $currentThreshold = $this->getCurrentThreshold();
        $countOfGoods = $this->getGood()->count();

        return ($countOfGoods < $currentThreshold['goodsLimit']);
    }

    /**
     * Продвинута ли студия в настоящий момент?
     *
     * @return bool
     */
    public function isPromoted()
    {
        $StudioPromoted = $this->studioPromotion;
        if ($StudioPromoted && strtotime($StudioPromoted->end_of_promotion) >= time()) {
            return true;
        } else {
            return false;
        }
    }
}
