<?php

namespace app\modules\studio\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\controllers\CommonController;
use app\modules\studio\models\Studio;
use app\modules\users\models\Photo as UserPhoto;

class DefaultController extends CommonController
{
    /**
     * @var int По сколько студий выводить в запросе
     */
    private $queryLimit = 15;
    /**
     * @var string Название сессионной переменной для хранения фильров для студий на главной странице студий
     */
    private $sessionStudioFilterName = 'filter-studio-main';
    /**
     * @var null|array состояние фильтров студий
     */
    private $filterConditions;

    /**
     * Страница со списком студий
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->addBreadcrumbsItem(['label' => 'Ателье и магазины']);

        // если пользователь пришел с другой страницы, то фильтры сбрасываются
        // если пользователь обновил страницу (или например изменил сортировку), то фильтры сохраняются
        $referrer = Url::previous('commonReferrer');
        $ReferrerRequest = new \yii\web\Request();
        $ReferrerRequest->setUrl($referrer);
        $referrerUrlRoute = Yii::$app->urlManager->parseRequest($ReferrerRequest);
        if (!is_array($referrerUrlRoute) || ($referrerUrlRoute[0] != $this->route)) {
            Yii::$app->session->remove($this->sessionStudioFilterName);
        } else {
            $this->filterConditions = Yii::$app->session->get($this->sessionStudioFilterName);
        }

        $baseUrl = Yii::$app->request->hostInfo . '/studio/';

        // параметры сортировки
        $dbName = Yii::$app->params['tailor_db'];
        $sortFields = [
            'default' => 'date',
            'date' => '`' . $dbName . '`.`studio`.`create_time`',
            'name' => '`' . $dbName . '`.`studio`.`name`',
        ];
        $Sorter = new \app\components\SortController('mainStudioList', $sortFields);
        $sortParams = $Sorter->getSort();

        $whereCondition = $this->makeWhere($this->filterConditions);
        $studioData = $this->getStudios($whereCondition, null, $sortParams['sortQueryPart']);

        $studioList = $this->renderPartial('@app/views/parts/studioList', [
            'studioData' => $studioData
        ]);

        // если всего товаров, соответствующих фильтрам, больше чем ограничение в одной выборке, то отображаем кнопку "Показать еще"
        $showMoreButton = $studioData['countOfStudios'] > $this->queryLimit;

        return $this->render('index', [
            'countOfStudios' => $studioData['countOfStudios'],
            'studioList' => $studioList,
            'baseUrl' => $baseUrl,
            'orderBy' => $sortParams['sortField'],
            'direction' => $sortParams['sortDirection'],
            'filterConditions' => $this->filterConditions,
            'showMoreButton' => $showMoreButton
        ]);
    }

    /**
     * Формирование только контента студий для блока со списком студий.
     * Вызывается тоьлко по Ajax
     */
    public function actionOnlyStudios()
    {
        if (Yii::$app->request->isAjax) {
            $requestFilters = Yii::$app->request->post('filters', null);
            $filterConditions = [];

            if (isset($requestFilters['studioType'])) {
                $filterConditions['studioType'] = in_array($requestFilters['studioType'], ['atelier', 'store']) ? $requestFilters['studioType'] : null;
            }

            if (isset($requestFilters['country']) && is_array($requestFilters['country'])) {
                $filterConditions['country'] = $requestFilters['country'];
            }
            // фильтр по городу выставляется только если передана страна
            if (isset($requestFilters['city']) && is_array($requestFilters['city']) && isset($requestFilters['country'])) {
                $filterConditions['city'] = $requestFilters['city'];
            }

            // смещение выборки
            $offset = 0;
            if (isset($requestFilters['part'])) {
                $offset = (int)$requestFilters['part'] * $this->queryLimit;
            }
            // если смещение 0, то это новый фильтр и надо подсчитать общее кол-вот товаров, удовлетворяющих фильтрам
            $calcCountOfStudios = $offset > 0 ? false : true;

            // параметры сортировки
            $dbName = Yii::$app->params['tailor_db'];
            $sortFields = [
                'default' => 'date',
                'date' => '`' . $dbName . '`.`studio`.`create_time`',
                'name' => '`' . $dbName . '`.`studio`.`name`',
            ];

            $Sorter = new \app\components\SortController('mainStudioList', $sortFields);
            $sortParams = $Sorter->getSort();

            // сохранение состояния фильтров
            $this->filterConditions = $filterConditions;
            Yii::$app->session->set($this->sessionStudioFilterName, $filterConditions);

            $whereCondition = $this->makeWhere($filterConditions);
            $studiosData = $this->getStudios($whereCondition, $offset, $sortParams['sortQueryPart'], $calcCountOfStudios);

            if (isset($filterConditions['studioType']) && $filterConditions['studioType'] == 'atelier') {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного ателье не найдено} one{Найдено # ателье} few{Найдено # ателье} many{Найдено # ателье} other{Найдено # ателье}}', ['n' => $studiosData['countOfStudios']]);
            } elseif (isset($filterConditions['studioType']) && $filterConditions['studioType'] == 'store') {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного магазина не найдено} one{Найден # магазин} few{Найдено # магазина} many{Найдено # магазинов} other{Найдено # магазинов}}', ['n' => $studiosData['countOfStudios']]);
            } else {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного ателье или магазина не найдено} one{Найден # ателье/магазин} few{Найдено # ателье/магазина} many{Найдено # ателье/магазинов} other{Найдено # ателье/магазинов}}', ['n' => $studiosData['countOfStudios']]);
            }

            $html = $this->renderPartial('@app/views/parts/studioList', [
                'studioData' => $studiosData
            ]);

            return json_encode([
                'html' => $html,
                'countText' => $countText,
                'countOfStudios' => $studiosData['countOfStudios'],
                'studiosOnPage' => count($studiosData['studios']) + $offset
            ]);
        }
    }

    /**
     * Страница студии доступная всем
     *
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionStudio()
    {
        $studioId = (int)Yii::$app->request->get('studioId');
        if (!$studioId) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        $Studio = Studio::findOne($studioId);
        if (!($Studio instanceof Studio)) {
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }

        $baseUrl = Yii::$app->request->hostInfo . '/studio/' . $Studio->id .'/';

        $this->addBreadcrumbsItem(['label' => 'Ателье и магазины', 'url' => '/studio/']);
        $this->addBreadcrumbsItem(['label' => $Studio->name]);

        // список сортируемых полей
        $dbName = Yii::$app->params['tailor_db'];
        $sortFields = [
            'default' => 'date',
            'date' => '`' . $dbName . '`.`goods`.`create_time`',
            'name' => '`' . $dbName . '`.`goods`.`name`',
            'price' => '`' . $dbName . '`.`goods`.`price`',
        ];

        $GoodsSelector = $Studio->getGood(true);

        $Sorter = new \app\components\SortController('studioGoodList', $sortFields);
        $sortParams = $Sorter->getSort();
        $GoodsSelector->orderBy($sortParams['sortQueryPart']);

        $Goods = $GoodsSelector->all();

        return $this->render('studio', [
            'Studio' => $Studio,
            'Goods' => $Goods,
            'baseUrl' => $baseUrl,
            'orderBy' => $sortParams['sortField'],
            'direction' => $sortParams['sortDirection'],
        ]);
    }

    /**
     * Делает выборку студий в соответствии с заданными условиями
     * В добавок подтягивает фотографии к ним
     *
     * @param string|null $wherePart Часть WHERE запроса
     * @param int|null $offset Смещение выборки
     * @param string|null $orderBy Поле для сортировки
     * @param bool $calcRows Подсчитывать ли общее кол-во строк, уодовлетворяющее запросу без учета LIMIT
     * @return array Массив со студиями и фотографиями
     */
    public function getStudios($wherePart = null, $offset = null, $orderBy = null, $calcRows = true)
    {
        //выборка товаров
        $StudiosSelector = Studio::find()
            ->select('
                `studio`.`id` AS `studio_id`,
                `studio`.`user_id` AS `user_id`,
                `studio`.`name` AS `studio_name`,
                `studio`.`type` AS `studio_type`,
                `studio`.`slogan` AS `slogan`,
                `country`.`name_ru` AS `country_name`,
                `city`.`name_ru` AS `city_name`')
            ->limit($this->queryLimit)
            ->leftJoin('country', '`studio`.`country_id` = `country`.`id`')
            ->leftJoin('city', '`studio`.`city_id` = `city`.`id`');
        if ($wherePart) {
            $StudiosSelector->where($wherePart);
        }
        if ($offset) {
            $StudiosSelector->offset($offset);
        }
        if ($orderBy) {
            $StudiosSelector->orderBy($orderBy);
        }
        $studios = $StudiosSelector->asArray()->all();

        if ($calcRows) {
            $countOfStudios = $StudiosSelector->orderBy(null)->count('DISTINCT `studio`.`id`');
        } else {
            $countOfStudios = null;
        }

        $userIds = ArrayHelper::getColumn($studios, 'user_id');

        //выборка фотографий владельцев студий
        $Photos = UserPhoto::findAll([
            'user_id' => $userIds
        ]);
        $photoArray = [];
        foreach ($Photos as $Photo) {
            $photoArray[$Photo->user_id] = $Photo;
        }

        // выборка количества товаров
        $goodCounts = [];
        if ($studios) {
            $studioIds = ArrayHelper::getColumn($studios, 'studio_id');
            $dbName = Yii::$app->params['tailor_db'];
            $goodCounts = Yii::$app->db->createCommand("
                SELECT
                    `studio_id`,
                    COUNT(*) AS `count`
                FROM
                    `" . $dbName . "`.`goods`
                WHERE
                    `studio_id` IN (" . implode(',', $studioIds) . ")
                GROUP BY
                    `studio_id`
            ")->queryAll();
            $goodCounts = ArrayHelper::index($goodCounts, 'studio_id');
        }
        foreach ($studios as $key => $studio) {
            if (isset($goodCounts[$studio['studio_id']])) {
                $studios[$key]['count_of_goods'] = $goodCounts[$studio['studio_id']]['count'];
            } else {
                $studios[$key]['count_of_goods'] = 0;
            }
        }

        return ['studios' => $studios, 'photos' => $photoArray, 'countOfStudios' => $countOfStudios];
    }

    /**
     * Формирует условие WHERE для запроса на основе полученных фильтров
     *
     * @param $conditions
     * @return string
     */
    private function makeWhere($conditions)
    {
        if (!$conditions) {
            return '';
        } else {
            $where = '1';

            if (isset($conditions['studioType']) && in_array($conditions['studioType'], ['atelier', 'store'])) {
                $where .= " AND `studio`.`type` = '" . $conditions['studioType'] . "'";
            }

            if (isset($conditions['country']['id']) && (int)$conditions['country']['id'] > 0) {
                $where .= " AND `studio`.`country_id` = " . (int)$conditions['country']['id'];
            }

            if (isset($conditions['city']['id']) && (int)$conditions['city']['id'] > 0) {
                $where .= " AND `studio`.`city_id` = " . (int)$conditions['city']['id'];
            }

            return $where;
        }
    }

    /**
     * Установить лимит выборки товаров
     *
     * @param $limit
     */
    public function setStudiosLimit($limit)
    {
        $this->queryLimit = $limit;
    }

    /**
     * Получить лимит выборки товаров
     *
     * @return int
     */
    public function getStudiosLimit()
    {
        return $this->queryLimit;
    }
}