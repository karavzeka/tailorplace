<?php
/**
 * Страница ателье или магазина
 * @var yii\web\View $this
 * @var app\modules\studio\models\Studio $Studio
 * @var app\modules\good\models\Good[] $Goods
 * @var string $baseUrl
 * @var string $orderBy
 * @var string $direction
 */

use app\assets\AppAsset;
use app\widgets\RatingBlock;
use app\modules\good\models\Photo AS GoodPhoto;
use app\modules\users\models\Photo AS UserPhoto;
use yii\helpers\Url;
use yii\helpers\Html;

$this->registerCssFile('@web/css/studio.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/studio.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = ($Studio->type == 'atelier' ? 'Ателье ' : 'Магазин ') . $Studio->name;
$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::$app->commonTools->metaDescriptionLengthOptimize($this->title . '. ' . $Studio->description)
]);

$finishedOrders = $Studio->finishedOrders();
?>
<main>
    <aside class="float left">
        <div class="person-info">
            <img src="<?= count($Studio->user->photo) > 0 ? $Studio->user->photo[0]->getSrc('_big') : UserPhoto::getNoPhotoSrc() ?>" alt="<?= Html::encode($this->title) ?>">
            <div>
                <div>
                    <div class="icon-circle <?= $Studio->type ?>" data-toggle="tooltip" data-placement="right" title="<?= ucfirst(Yii::t('app', $Studio->type)) ?>"><i></i></div>
                    <p class="name darker">
                        <?= Html::encode($Studio->user->getName()) ?>
                    </p>
                </div>
                <p class="city darker"><?= !empty($Studio->city->name_ru) ? Html::encode($Studio->city->name_ru) : Html::encode($Studio->country->name_ru) ?></p>
            </div>
        </div>
        <div class="rating">
            <div class="icon-line">
                <i class="icon little-star-light"></i>
                <span class="bolder size15">Репутация</span>
                <i class="icon little-star-light"></i>
            </div>
            <div>
                <?= RatingBlock::widget(['reputation' => $Studio->user->sellerReputation(), 'countOfDeals' => $finishedOrders]) ?>
            </div>
            <div class="icon-line size15">
                <i class="icon rouble"></i> <span>Выполнено работ</span> <span class="darker bolder"><?= $finishedOrders ?></span>
            </div>
        </div>
        <?php
        if (Yii::$app->user->id != $Studio->user->id) {
            $linkText = 'Написать сообщение';
            $buttonOptions = ['id' => 'send-message'];

            if (Yii::$app->user->isGuest) {
                $buttonOptions['class'] = 'button disable';
                $insideButton = Html::button($linkText, [
                    'disabled' => 'disabled',
                    'title' => 'Требуется авторизация'
                ]);
            } else {
                $buttonOptions['class'] = 'button yellow';
                $insideButton = Html::a($linkText, Url::toRoute('/cabinet/dialog/' . $Studio->user->id), [
                    'data-type' => 'button',
                    'target' => '_blank'
                ]);
            }
            echo Html::beginTag('div', $buttonOptions);
            echo Html::tag('div', '', ['class' => 'low-layer']);
            echo $insideButton;
            echo Html::endTag('div');
        }
        ?>

        <section>
            <h5 class="big-red-medium">Покупки <?= $Studio->type == 'atelier' ? 'ателье' : 'магазина' ?></h5>
            <ul class="size15">
                <li class="icon-line">
                    <span><i class="icon star"></i><br> </span><!-- здесь спец. невидимый символ (Alt + 255 на Num клавиатуре) -->
                    <span class="quantity-label buyer-reputation">Репутация покупателя</span><span class="bolder darker"><br><?= $Studio->user->buyerReputation() ?>%</span>
                </li>
                <li class="icon-line">
                    <i class="icon purchase"></i>
                    <span class="quantity-label">Количество покупок</span>
                    <span class="bolder darker"><?= $Studio->user->countOfPurchases() ?></span>
                </li>
            </ul>
        </section>

        <section>
            <h5 class="big-red-medium">Категории товаров</h5>
            <ul class="size15 category-list">
                <!-- TODO сделать категории ссылками -->
                <?php
                foreach ($Studio->categoriesOfGoods() as $category) {
                    echo '<li class="icon-line">';
                        echo Html::tag('i', '', ['class' => 'icon ' . $category['css_class']]);
                        echo Html::tag('span', $category['name_ru'], ['class' => 'quantity-label']);
                        echo Html::tag('span', $category['count'], ['class' => 'bolder darker']);
                    echo '</li>';
                }
                ?>
            </ul>
        </section>

        <section>
            <h5 class="big-red-medium">Доставка</h5>
            <ul class="size15">
                <?php
                if (count($Studio->delivery) > 0 || !empty($Studio->custom_delivery)) {
                    foreach ($Studio->delivery as $Delivery) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon ' . $Delivery->css_class]);
                            echo Html::tag('span', $Delivery->name_ru);
                        echo '</li>';
                    }
                    if (isset($Studio->custom_delivery) && !empty($Studio->custom_delivery)) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon other']);
                            echo Html::tag('span', Html::encode($Studio->custom_delivery));
                        echo '</li>';
                    }
                } else {
                    echo '<li class="lighter size15">Не указано</li>';
                }
                ?>
            </ul>
        </section>

        <section>
            <h5 class="big-red-medium">Оплата</h5>
            <ul class="size15">
                <?php
                if (count($Studio->payment) > 0 || !empty($Studio->custom_payment)) {
                    foreach ($Studio->payment as $Payment) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon ' . $Payment->css_class]);
                            echo Html::tag('span', $Payment->name_ru);
                        echo '</li>';
                    }
                    if (isset($Studio->custom_payment) && !empty($Studio->custom_payment)) {
                        echo '<li class="icon-line">';
                            echo Html::tag('i', '', ['class' => 'icon other']);
                            echo Html::tag('span', Html::encode($Studio->custom_payment));
                        echo '</li>';
                    }
                } else {
                    echo '<li class="lighter size15">Не указано</li>';
                }
                ?>
            </ul>
        </section>

        <section>
            <h5 class="big-red-medium">Адрес</h5>
            <p>
                <?= Html::encode($Studio->country->name_ru . (!empty($Studio->city->name_ru) ? (', ' . $Studio->city->name_ru) : '') . (!empty($Studio->address) ? (', ' . Html::encode($Studio->address)) : '')) ?>
                <?= !empty($Studio->phone) ? ('<br>Тел. ' . Yii::$app->numberHelper->numberToPhone($Studio->phone)) : '' ?>
            </p>
        </section>
    </aside>

    <div class="float right">
        <h2><?= Html::encode($Studio->name) ?></h2>
        <div id="slogan">
            <?php
            if ($Studio->slogan) {
                echo Html::tag('h3', Html::encode($Studio->slogan));
            }
            ?>
        </div>
        <div id="social-block">
            <?= \app\widgets\SocialButtons::widget([
                'title' => $Studio->name,
                'description' => $Studio->description ? mb_substr($Studio->description, 0, 300) : null,
                'imageUrl' => count($Studio->user->photo) > 0 ? Url::to($Studio->user->photo[0]->getSrc('_big'), true) : null
            ]) ?>
        </div>
        <div id="description">
            <div class="outer-wrapper">
                <div class="inner-wrapper">
                    <p><?= Html::encode($Studio->description) ?></p>
                </div>
                <!-- <div class="description-footer">
                    <div id="read-all"><span>↓</span> <a href="#description-all" class="dashed-underline">Читать полностью</a></div>
                </div> -->
            </div>
        </div>
        <div class="dashed scissor-right margin-scissor"></div>

        <div id="goods-header">
            <ul class="size16">
                <li class="bold-underline">Все товары</li>
<!--                <li><a href="" class="size16">Рекомендуемые</a></li>-->
            </ul>
            <div class="sort-type">
                <input type="hidden" id="base-url" value="<?= $baseUrl ?>">
                <span class="size16">Сортировать по</span>
                <div class="select">
                    <select name="sort" id="sort-selector">
                        <option value="date_desc" <?= ($orderBy == 'date' && $direction == 'DESC') ? 'selected' : '' ?>>Дате (сначала новые)</option>
                        <option value="date_asc" <?= ($orderBy == 'date' && $direction != 'DESC') ? 'selected' : '' ?>>Дате (сначала старые)</option>
                        <option value="name_asc" <?= ($orderBy == 'name' && $direction != 'DESC') ? 'selected' : '' ?>>Названию А-Я</option>
                        <option value="name_desc" <?= ($orderBy == 'name' && $direction == 'DESC') ? 'selected' : '' ?>>Названию Я-А</option>
                        <option value="price_asc" <?= ($orderBy == 'price' && $direction != 'DESC') ? 'selected' : '' ?>>Возрастанию цены</option>
                        <option value="price_desc" <?= ($orderBy == 'price' && $direction == 'DESC') ? 'selected' : '' ?>>Убыванию цены</option>
                    </select>
                    <div class="select-button">
                        <span class="caret"></span>
                    </div>
                </div>
            </div>
        </div>

        <h4 id="filter-header">Все товары <span>(<?= Yii::t('app', '{n, plural, =0{нет товаров} one{найден # товар} few{найдено # товара} many{найдено # товаров} other{найдено # товаров}}', ['n' => count($Goods)]) ?>)</span></h4>
        <div id="goods-wrapper" class="good-tile without-studio">
            <?php
            if (count($Goods) > 0) {
                $colOfLastElement = 0;
                foreach ($Goods as $Good) {
                    $colOfLastElement = ($colOfLastElement + 1) % 3;
                    echo '<div class="good-instance">';
                        echo '<figure>';
                            $imgHtml = Html::img(count($Good->photo) ? $Good->photo[0]->getSrc('_middle') : GoodPhoto::getNoPhotoSrc('_middle'), [
                                'width' => '188',
                                'height' => '188'
                            ]);
                            echo Html::a($imgHtml, Url::toRoute('/goods/' . $Good->id));
                            echo '<figcaption class="darker">';
                                echo Html::a(Html::encode($Good->name), Url::toRoute('/goods/' . $Good->id), ['class' => 'good-tile-link']);
                            echo '</figcaption>';
                        echo '</figure>';
                        echo '<div class="price">';
                            echo '<span>' . Yii::$app->numberHelper->numberToPrice($Good->price) . '</span><span>руб.</span>';
                        echo '</div>'; // <div class="price">
                    echo '</div>'; // <div class="good-instance">
                    echo ' '; // т.к. выравнивание justify работает при пробелах между словами
                }

                // последнюю строку необходимо дозаполнить пустыми элементами, иначе выравнимание может работать криво
                if ($colOfLastElement != 0) {
                    for ($i = 0; $i < (3 - $colOfLastElement); $i++) {
                        echo '<div class="good-instance empty"></div>';
                    }
                }
            }
            ?>
        </div>
    </div>
    <div class="clear"></div>
</main>