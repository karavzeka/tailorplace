<?php
/**
 * Страница списка ателье и магазинов
 *
 * @var yii\web\View $this
 * @var string $studioList
 * @var string $baseUrl
 * @var string $orderBy
 * @var string $direction
 * @var int $countOfStudios
 * @var array $filterConditions
 * @var bool $showMoreButton
 */

use app\assets\AppAsset;
use yii\helpers\Html;

$this->registerCssFile('@web/css/studios.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/studios.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Ателье и магазины';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Ателье и магазины. Найдите себе индивидуального портного. Если вы ищете материалы для шитья, то поищите среди магазинов.'
]);

$studioTypes = [
    'all' => [
        'name' => 'Все',
        'icon-class' => 'rouble',
    ],
    'atelier' => [
        'name' => 'Ателье',
        'icon-class' => 'atelier',
    ],
    'store' => [
        'name' => 'Магазин',
        'icon-class' => 'store',
    ],
];
if (isset($filterConditions['studioType'])) {
    $studioTypes[$filterConditions['studioType']]['active'] = true;
} else {
    $studioTypes['all']['active'] = true;
}
?>

<main>
    <aside class="float left">
        <section id="studio-section">
            <h3 class="big-red-medium">Категория продавца</h3>
            <ul id="studio-type">
                <?php
                foreach ($studioTypes as $key => $type) {
                    echo '<li class="icon-line">';
                        echo Html::tag('div', '<i></i>', ['class' => 'icon-circle small ' . $type['icon-class']]);
                        $spanClass = $type['active'] ? 'red active' : 'dashed-underline';
                        echo Html::tag('span', $type['name'], [
                            'class' => $spanClass,
                            'data-filter-studio' => $key
                        ]);
                    echo '</li>';
                }
                ?>
<!--                <li class="icon-line">-->
<!--                    <div class="icon-circle rouble small"><i></i></div>-->
<!--                    <a href="" class="dashed-underline">Все</a>-->
<!--                </li>-->
<!--                <li class="icon-line">-->
<!--                    <div class="icon-circle atelier small"><i></i></div>-->
<!--                    <a href="" class="dashed-underline">Ателье</a>-->
<!--                </li>-->
<!--                <li class="icon-line">-->
<!--                    <div class="icon-circle store small"><i></i></div>-->
<!--                    <span class="red">Магазин</span>-->
<!--                </li>-->
            </ul>
        </section>

        <div id="filters-reset">
            <span class="dashed-underline size15">Сбросить фильтры ↓</span>
        </div>

        <section id="country-section">
            <h3 class="big-red-medium">Страна</h3>
            <?= Html::input('text', null, isset($filterConditions['country']['value']) ? $filterConditions['country']['value'] : null, [
                'id' => 'filter-country',
                'class' => 'form-control',
                'data-country-id' => $filterConditions['country']['id']
            ]) ?>
        </section>

        <section id="city-section">
            <h3 class="big-red-medium">Город</h3>
            <?php
            $inputOptions = [
                'id' => 'filter-city',
                'class' => 'form-control',
                'data-city-id' => $filterConditions['city']['id']
            ];
            if (!isset($filterConditions['country']) && !isset($filterConditions['city'])) {
                $inputOptions['disabled'] = 'disabled';
            }
            echo Html::input('text', null, isset($filterConditions['city']['value']) ? $filterConditions['city']['value'] : null, $inputOptions);
            ?>
        </section>

        <!-- TODO добавить фильтрацию по способам доставки и оплаты и по категории товара -->
<!--        <section id="category-section">-->
<!--            <h3 class="big-red-medium">Категория товаров</h3>-->
<!--            <ul class="selected icon-line">-->
<!--                <li>-->
<!--                    <i class="icon jeans"></i> <span>Джинсы</span> <i class="icon delete-red"></i>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <i class="icon indoor"></i> <span>Домашняя одежда</span> <i class="icon delete-red"></i>-->
<!--                </li>-->
<!--            </ul>-->
<!--            <ul class="icon-line">-->
<!--                <li class="active">-->
<!--                    <i class="icon all-goods"></i> <span>Все товары</span>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href=""><i class="icon suit-pants"></i> <span>Брюки</span></a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href=""><i class="icon outerwear"></i> <span>Верхняя одежда</span></a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href=""><i class="icon headdress"></i> <span>Головной убор</span></a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href=""><i class="icon children"></i> <span>Детская одежда</span></a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </section>-->
    </aside>

    <div class="float right">
        <h2>Ателье и магазины</h2>

        <div id="list-header">
            <ul class="size16">
                <li class="bold-underline">Все</li>
                <!-- TODO сделать фильтрацию по рекомендуемым студиям -->
<!--                <li><a href="" class="size16">Рекомендуемые</a></li>-->
            </ul>
            <div class="sort-type">
                <input type="hidden" id="base-url" value="<?= $baseUrl ?>">
                <span class="size16">Сортировать по</span>
                <div class="select">
                    <select id="sort-selector">
                        <option value="date_desc" <?= ($orderBy == 'date' && $direction == 'DESC') ? 'selected' : '' ?>>Дате (сначала новые)</option>
                        <option value="date_asc" <?= ($orderBy == 'date' && $direction != 'DESC') ? 'selected' : '' ?>>Дате (сначала старые)</option>
                        <option value="name_asc" <?= ($orderBy == 'name' && $direction != 'DESC') ? 'selected' : '' ?>>Названию А-Я</option>
                        <option value="name_desc" <?= ($orderBy == 'name' && $direction == 'DESC') ? 'selected' : '' ?>>Названию Я-А</option>
                    </select>
                    <div class="select-button">
                        <span class="caret"></span>
                    </div>
                </div>
            </div>
        </div>

        <div id="count-of-studios" class="size15">
            <?php
            if (isset($filterConditions['studioType']) && $filterConditions['studioType'] == 'atelier') {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного ателье не найдено} one{Найдено # ателье} few{Найдено # ателье} many{Найдено # ателье} other{Найдено # ателье}}', ['n' => $countOfStudios]);
            } elseif (isset($filterConditions['studioType']) && $filterConditions['studioType'] == 'store') {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного магазина не найдено} one{Найден # магазин} few{Найдено # магазина} many{Найдено # магазинов} other{Найдено # магазинов}}', ['n' => $countOfStudios]);
            } else {
                $countText = Yii::t('app', '{n, plural, =0{Ни одного ателье или магазина не найдено} one{Найден # ателье/магазин} few{Найдено # ателье/магазина} many{Найдено # ателье/магазинов} other{Найдено # ателье/магазинов}}', ['n' => $countOfStudios]);
            }
            echo $countText
            ?>
        </div>
        <ul id="studio-wrapper" data-count-of-studios="<?= $countOfStudios ?>">
            <?= $studioList ?>
            <!-- TODO реализовать показ продвинутых студий -->
<!--            <li class="prestige">-->
<!--                <div class="main-info">-->
<!--                    <a href="" class="avatar"><img src="/photos/studio/test-middle.jpg" width="98" height="98"></a>-->
<!--                    <div class="name">-->
<!--                        <div class="icon-line">-->
<!--                            <div class="icon-circle atelier small"><i></i></div> <span><a class="big-red">Brasletiki-spb</a></span>-->
<!--                        </div>-->
<!--                        <p class="italic">Свободная торговая площадка в области портняжного дела площадка в области дела</p>-->
<!--                        TODO добавить миниатюры товаров -->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="additional-info">-->
<!--                    <div class="count">-->
<!--                        <span>Количество товаров:</span>-->
<!--                        <span class="italic">87</span>-->
<!--                    </div>-->
<!--                    <div class="country">-->
<!--                        <span>Страна:</span>-->
<!--                        <span class="italic">Россия</span>-->
<!--                    </div>-->
<!--                    <div class="city">-->
<!--                        <span>Город:</span>-->
<!--                        <span class="italic">Петропавловск-Камчатский</span>-->
<!--                    </div>-->
<!--                    <div class="clear"></div>-->
<!--                </div>-->
<!--            </li>-->
        </ul>
        <div id="show-more">
            <?php if ($showMoreButton) :?>
                <div id="show-more-button" data-part="1">
                    <span class="plus-button"></span>
                    <span>Показать еще</span>
                </div>
            <?php endif ?>
        </div>
    </div>
    <div class="clear"></div>
</main>