<?php
/**
 * Страница админки со списком пользователей
 *
 * @var app\modules\users\models\User $Users[]
 * @var yii\data\Pagination $Pagination
 */

use yii\helpers\Html;
use app\modules\assets\admin\UserListAsset;

UserListAsset::register($this);

$flashMessage = Yii::$app->session->getFlash('userRedacted');
?>

<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1>Пользователи</h1>

        <div class="panel panel-default">
            <div class="panel-heading">Список пользователей</div>
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Логин</th>
                        <th>Email</th>
                        <th>Дата регистрации</th>
                        <th>Студия</th>
                        <th>Параметры</th>
                        <th>Действия</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($Users as $User) {
                        if ($User->hasStudio()) {
                            $studioIcon = '<div class="icon-circle small ' . $User->studio->type . '"><i></i></div>';
                        } else {
                            $studioIcon = '-';
                        }

                        echo '<tr class="user-item-' . $User->id . '">';
                            echo '<td>' . $User->id . '</td>';
                            echo '<td>' . Html::encode($User->login) . '</td>';
                            echo '<td>' . Html::encode($User->email) . '</td>';
                            echo '<td>' . date('d.m.Y', strtotime($User->create_time)) . '</td>';

                            echo '<td class="text-center">' . $studioIcon . '</td>';
                            echo '<td class="text-center params">';
                                if ($User->additional_status == $User::STATUS_BANNED) {
                                    echo '<span class="glyphicon glyphicon-ban-circle text-danger" aria-hidden="true" data-toggle="tooltip" title="Забанен"></span>';
                                }
                            echo '</td>';
                            echo '<td class="text-center">';
                                echo '<a class="btn btn-info btn-xs" type="button" href="/admin/users/redact/' . $User->id . '/" title="Редактировать" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
                                if ($User->additional_status != $User::STATUS_BANNED) {
                                    echo '<span data-toggle="tooltip" class="ban-user" title="Забанить пользователя"><button class="btn btn-danger btn-xs" type="button" data-toggle="modal" data-target="#ban-user-modal" data-user-id="' . $User->id . '" data-user-banned="0"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span></button></span>';
                                } else {
                                    echo '<span data-toggle="tooltip" class="ban-user" title="Снять бан c пользователя"><button class="btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#ban-user-modal" data-user-id="' . $User->id . '" data-user-banned="1"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></button></span>';
                                }
                            echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>

                <?= yii\widgets\LinkPager::widget(['pagination' => $Pagination]) ?>
            </div>
        </div>
    </div>
</div>

<div id="ban-user-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Бан пользователя</h4>
            </div>
            <div class="modal-body">
                <p id="ban-user-modal-placeholder"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>
                <button type="button" id="user-ban-accept" class="btn btn-danger">Да</button>
            </div>
        </div>
    </div>
</div>