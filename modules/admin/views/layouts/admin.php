<?php
/**
 * @var $content string
 */

use yii\helpers\Html;
use app\modules\assets\admin\AdminAsset;

AdminAsset::register($this);

$this->title = empty($this->title) ? 'Tailor place - админка' : $this->title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div id="wrapper">
            <div class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Tp - admin panel</a>
                    </div>
                    <div id="navbar-main" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/admin/">Главная</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="#" data-toggle="dropdown">Управление сайтом <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/admin/users/">Пользователи</a></li>
                                    <li><a href="/admin/news/">Новости</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="/">К сайту</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="content" class="container">
                <?= $content ?>
                <div class="clear"></div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>