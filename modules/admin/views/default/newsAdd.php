<?php
/**
 * Страница создания новости.
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $NewsForm
 * @var string $fileError
 */

use yii\widgets\ActiveForm;

?>
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1>Добавление новости</h1>
        <p>
            <a href="/admin/news/"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> <span>Вернуться к списку новостей</span></a>
        </p>
        <div class="well bs-component">
            <?php
            $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ],
                'fieldConfig' => [
                    'template' => '<div class="col-lg-2">{label}</div><div class="col-lg-10">{input}<div class="help-block"></div></div>'
                ]
            ]);

            echo $form->field($NewsForm, 'title')->textInput([
                'placeholder' => 'Заголовок новости',
            ]);

            $fieldOptions = [];
            if (mb_strlen($fileError) > 0) {
                $fieldOptions = [
                    'template' => '<div class="col-lg-2">{label}</div><div class="col-lg-10">{input}<div class="help-block">' . $fileError . '</div></div>'
                ];
            }
            echo $form->field($NewsForm, 'imageFile', $fieldOptions)->fileInput([
                'class' => 'file-input',
                'title' => 'Выберете изображение',
                'accept' => 'image/*',
            ]);

            echo $form->field($NewsForm, 'text')->textarea([
                'rows' => 8
            ]);

            echo $form->field($NewsForm, 'show')->checkbox();
            ?>
            <div class="form-group">
                <div class="col-lg-2"></div>
                <div class="col-lg-10">
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div> <!-- <div class="col-lg-6"> -->
</div> <!-- <div class="row"> -->