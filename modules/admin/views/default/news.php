<?php
/**
 * Страница админки со списком новостей
 *
 * @var app\models\News $News[]
 */

use app\modules\assets\admin\NewsAsset;
use yii\helpers\Html;

NewsAsset::register($this);

$flashMessage = Yii::$app->session->getFlash('newsRedacted');
?>

<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1>Новости</h1>
        <p>
            <a class="btn" href="/admin/news-add/">Добавить новость</a>
        </p>
        <div class="panel panel-default">
            <div class="panel-heading">Список новостей</div>
            <div class="panel-body">
                <?= mb_strlen($flashMessage) > 0 ? ('<div class="alert alert-success">' . $flashMessage . '</div>') : '' ?>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Заголовок</th>
                            <th>Параметры</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($News as $NewsItem) {
                        echo '<tr id="news-item-' . $NewsItem->id . '">';
                            echo '<td>' . $NewsItem->id . '</td>';
                            echo '<td>' . Html::encode($NewsItem->title) . '</td>';
                            echo '<td class="text-center">';
                                if ($NewsItem->show) {
                                    echo '<span class="glyphicon glyphicon-eye-open text-success" aria-hidden="true" title="Отображается на сайте" data-toggle="tooltip"></span> ';
                                } else {
                                    echo '<span class="glyphicon glyphicon-eye-close text-danger" aria-hidden="true" title="Не отображается на сайте" data-toggle="tooltip"></span> ';
                                }
                                if ($NewsItem->has_img) {
                                    echo '<span class="glyphicon glyphicon-picture text-info" aria-hidden="true" title="Имеется изображение" data-toggle="tooltip"></span> ';
                                }
                            echo '</td>';
                            echo '<td class="text-center">';
                                echo '<a class="btn btn-info btn-xs" type="button" href="/admin/redact-news/' . $NewsItem->id . '/" title="Редактировать" data-toggle="tooltip"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
                                echo '<span data-toggle="tooltip" title="Удалить"><button class="btn btn-danger btn-xs delete-news" type="button" data-toggle="modal" data-target="#delete-news-modal" data-news-id="' . $NewsItem->id . '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></span>';
                                if ($NewsItem->has_img) {
                                    echo '<span data-toggle="tooltip" title="Удалить изображение"><button class="btn btn-danger btn-xs" type="button" data-toggle="modal" data-target="#delete-news-img-modal" data-news-id="' . $NewsItem->id . '"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></button></span>';
                                }
                            echo '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="delete-news-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Уделение новости</h4>
            </div>
            <div class="modal-body">
                <p>Удалить новость #<span id="news-id-modal-placeholder"></span>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="news-delete-accept" class="btn btn-danger">Удалить</button>
            </div>
        </div>
    </div>
</div>

<div id="delete-news-img-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Уделение изображения новости</h4>
            </div>
            <div class="modal-body">
                <p>Удалить изображение у новости #<span id="news-id-img-modal-placeholder"></span>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="news-img-delete-accept" class="btn btn-danger">Удалить</button>
            </div>
        </div>
    </div>
</div>
