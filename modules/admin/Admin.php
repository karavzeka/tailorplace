<?php

namespace app\modules\admin;

use Yii;

class Admin extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->user->can('viewAdminSection') === false) {
                // Если пользователю запрещен просмотр админки, то выдаем 404 ошибку, как будто страницы не существует
                $SiteController = Yii::$app->createController('site');
                echo $SiteController[0]->runAction('404');
                return false;
            } else {
                return true;
            }
        }
    }
}
