<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\News;
use app\modules\admin\models\NewsForm;
use app\controllers\CommonController;

class DefaultController extends CommonController
{
    public $layout = 'admin';

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionNews()
    {
        $News = News::find()->orderBy(['id' => SORT_DESC])->all();

        return $this->render('news', [
            'News' => $News
        ]);
    }

    public function actionRedactNews()
    {
        $newsId = (int)Yii::$app->request->get('newsId');
        if ($newsId == 0) {
            Yii::$app->response->setStatusCode(404);
            return $this->renderContent('<h1>404</h1>');
        }
        $NewsItem = News::findOne($newsId);
        if (!($NewsItem instanceof News)) {
            Yii::$app->response->setStatusCode(404);
            return $this->renderContent('<h1>404</h1>');
        }

        return $this->runAction('news-add');
    }

    /**
     * Создание новости
     *
     * @return string
     * @throws \Exception
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionNewsAdd()
    {
        // TODO добавить поле начало отображения, чтобы можно было делать новость заранее
        $NewsForm = new NewsForm();
        $isNewItem = true;

        $newsId = (int)Yii::$app->request->get('newsId');
        if ($newsId != 0) {
            // Редактирование существующей новости
            $isNewItem = false;
            $NewsItem = News::findOne($newsId);
            if (Yii::$app->request->isGet) {
                // Если был пост, то нам не надо перезатирать запосченные значения
                $NewsForm->title = $NewsItem->title;
                $NewsForm->text = $NewsItem->text;
                $NewsForm->show = $NewsItem->show;
            }
        } else {
            // Новая новость
            $NewsItem = new News();
        }

        $fileError = '';
        if (Yii::$app->request->isPost && $NewsForm->load(Yii::$app->request->post() )) {
            $post = Yii::$app->request->post();
            if ($NewsForm->load($post) && $NewsForm->validate()) {
                $NewsItem->title = $NewsForm->title;
                $NewsItem->text = $NewsForm->text;
                $NewsItem->show = $NewsForm->show;

                $file = \yii\web\UploadedFile::getInstance($NewsForm, 'imageFile');
                if ($file !== null) {
                    $mime = \yii\helpers\BaseFileHelper::getMimeType($file->tempName);
                    if (mb_strpos($mime, 'image') === false) {
                        $NewsForm->addError('imageFile', 'Данный файл не является изображением');
                        $fileError = 'Данный файл не является изображением';
                    }

                    $savedSuccess = false;
                    if (!$fileError) {
                        // Сохранение новости с транзакцией. Если при обработке файла ошибка, то откат
                        $Transaction = Yii::$app->db->beginTransaction();
                        try {
                            $NewsItem->has_img = 1;
                            $savedSuccess = $NewsItem->save();
                            if ($savedSuccess === false) {
                                throw new \Exception();
                            }
                            $newsId = $NewsItem->id;

                            $Image = new \Imagick($file->tempName);
                            if ($Image->getImageHeight() < 150 || $Image->getImageWidth() < 150) {
                                $NewsForm->addError('imageFile', 'Данный файл не является изображением');
                                $fileError = 'Данный файл не является изображением';
                                // прерываем дальнейшую обработку кинув отлавливаемое исключение
                                throw new \ErrorException();
                            }

                            $CropImage = clone $Image;
                            $CropMinImage = clone $Image;
                            if ($Image->getImageHeight() > 2000 || $Image->getImageWidth() > 2000) {
                                $Image->thumbnailImage(2000, 2000, true);
                            }
                            if ($CropImage->getImageHeight() > 378 || $CropImage->getImageWidth() > 378) {
                                $CropImage->cropThumbnailImage(378, 378);
                            }
                            $CropMinImage->cropThumbnailImage(58, 58);

                            $imgDir = Yii::getAlias('@app') . '/web/photos/news/' . $newsId;

                            \yii\helpers\BaseFileHelper::createDirectory($imgDir);
                            $Image->writeImage($imgDir . '/main.jpg');
                            $CropImage->writeImage($imgDir . '/crop.jpg');
                            $CropMinImage->writeImage($imgDir . '/crop_min.jpg');

                            $Transaction->commit();
                            $savedSuccess = true;
                        } catch(\ErrorException $e) {
                            $Transaction->rollBack();
                        } catch(\Exception $e) {
                            $Transaction->rollBack();
                            throw new \Exception($e->getMessage(), $e->getCode(), $e->getPrevious());
                        }
                    }
                } else {
                    // Простое сохранение новости
                    $savedSuccess = $NewsItem->save();
                }

                if ($savedSuccess === true) {
                    $message = $isNewItem ? ('Новость #' . $NewsItem->id . ' создана') : ('Новость #' . $NewsItem->id . ' отредактирована');
                    Yii::$app->session->setFlash('newsRedacted', $message);
                    return $this->redirect(['news']);
                }
            }
        }

        return $this->render('newsAdd', [
            'NewsForm' => $NewsForm,
            'fileError' => $fileError
        ]);
    }

    /**
     * Удаление новости
     */
    public function actionDeleteNewsItem()
    {
        if (Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $newsId = (int)Yii::$app->request->post('newsId');

            if (!($newsId > 0)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $NewsItem = News::findOne($newsId);
            if (!($NewsItem instanceof News)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $res = $NewsItem->delete();
            if ($res) {
                \yii\helpers\BaseFileHelper::removeDirectory(Yii::getAlias('@app') . '/web/photos/news/' . $newsId);
            }

            $response = [
                'status' => 'ok'
            ];

            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Удаление изображения новости
     */
    public function actionDeleteNewsImg()
    {
        if (Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $newsId = (int)Yii::$app->request->post('newsId');

            if (!($newsId > 0)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $NewsItem = News::findOne($newsId);
            if (!($NewsItem instanceof News) || $NewsItem->has_img == 0) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            \yii\helpers\BaseFileHelper::removeDirectory(Yii::getAlias('@app') . '/web/photos/news/' . $newsId);
            $NewsItem->has_img = 0;
            $NewsItem->save();

            $response = [
                'status' => 'ok'
            ];

            $this->sendJsonAnswer($response);
        }
    }
}