<?php

namespace app\modules\admin\controllers;

use Yii;
use app\controllers\CommonController;
use app\modules\users\models\User;

class UserController extends CommonController
{
    public $layout = 'admin';

    public function actionIndex()
    {
        $limit = 20; // Сколько записей отображать на странице

        $UsersQuery = User::find();
        $PaginationQuery = clone $UsersQuery;

        $Pagination = new \yii\data\Pagination([
            'totalCount' => $PaginationQuery->count(),
            'defaultPageSize' => $limit,
        ]);

        $Users = $UsersQuery
            ->orderBy(['id' => SORT_DESC])
            ->offset($Pagination->offset)
            ->limit($Pagination->limit)
            ->all();
        return $this->render('index', [
            'Users' => $Users,
            'Pagination' => $Pagination
        ]);
    }

    /**
     * Обработка AJAX запроса на забанивание/разбанивание пользователей
     */
    public function actionBan()
    {
        if (Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $userId = (int)Yii::$app->request->post('userId');

            if (!($userId > 0)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $User = User::findOne($userId);
            if (!($User instanceof User)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $User->scenario = 'ban';

            // Если забанен, то разбаниваем, иначе забаниваем
            if ($User->additional_status == User::STATUS_BANNED) {
                $User->additional_status = User::STATUS_OK;
                $response['ban'] = 0;
            } else {
                $User->additional_status = User::STATUS_BANNED;
                $response['ban'] = 1;
            }

            if ($User->save()) {
                $response['status'] = 'ok';
            }

            $this->sendJsonAnswer($response);
        }
    }
}
