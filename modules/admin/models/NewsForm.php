<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

class NewsForm extends Model
{
    /**
     * @var string Заголовок новости
     */
    public $title;

    /**
     * @var string Текст новости
     */
    public $text;

    /**
     * @var string Изображение товарв
     */
    public $imageFile;

    /**
     * @var string Флаг, показывающий, отображать ли новость на сайте
     */
    public $show;

    public function rules()
    {
        return [
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'filter', 'filter' => 'strip_tags'],
            ['title', 'required'],
            ['title', 'string', 'min' => 3, 'max' => 255],

            ['text', 'filter', 'filter' => 'trim'],
            ['text', 'filter', 'filter' => function($value) {
                return strip_tags($value, '<a><p><span><b><strong><i><img>');
            }],
            ['text', 'required'],

            ['imageFile', 'file', 'extensions' => 'png, jpg'],

            ['show', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'text' => 'Текст',
            'imageFile' => 'Изображение',
            'show' => 'Показывать новость на сайте?',
        ];
    }
}