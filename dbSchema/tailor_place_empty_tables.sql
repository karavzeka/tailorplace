-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 06 2015 г., 12:24
-- Версия сервера: 5.5.46-0+deb8u1
-- Версия PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tailor_dev`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `basket`
--

DROP TABLE IF EXISTS `basket`;
CREATE TABLE IF NOT EXISTS `basket` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `good_id` int(10) unsigned NOT NULL COMMENT 'Id товара',
  `quantity` smallint(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Кол-во экземпляров',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата добавления товара в корзину'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Корзина заказов пользователей';

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'email, указанный в форме',
  `message` text NOT NULL COMMENT 'Сообщение пользователя',
  `from_authorized_user` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id пользователя, если сообщение отправил авторизованный пользователь',
  `email_registered_to_user` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id пользователя, к котрому привязан email',
  `ip` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'IP с которого выполнен запрос',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания сообщения'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='Сообщения пользователей, отправленные через форму обратной связи';

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

DROP TABLE IF EXISTS `goods`;
CREATE TABLE IF NOT EXISTS `goods` (
`id` int(10) unsigned NOT NULL,
  `studio_id` int(10) unsigned NOT NULL COMMENT 'id студии',
  `name` varchar(70) NOT NULL COMMENT 'Название товара',
  `price` int(10) unsigned NOT NULL COMMENT 'Цена товара умноженная на 100',
  `description` text COMMENT 'Описание товара',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Количество товара (только для магазина)',
  `craft_time` varchar(30) NOT NULL DEFAULT '' COMMENT 'Срок изготовления товара',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания',
  `update_time` timestamp NULL DEFAULT NULL COMMENT 'Дата обновления'
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='Таблица с товарами';

-- --------------------------------------------------------

--
-- Структура таблицы `goods_promotion`
--

DROP TABLE IF EXISTS `goods_promotion`;
CREATE TABLE IF NOT EXISTS `goods_promotion` (
`id` int(10) unsigned NOT NULL,
  `good_id` int(10) unsigned NOT NULL COMMENT 'Id товара',
  `last_promotion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время окончания продвижения товара',
  `end_of_promotion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время окончания продвижения товара'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Времена продвижения студий';

-- --------------------------------------------------------

--
-- Структура таблицы `good_category`
--

DROP TABLE IF EXISTS `good_category`;
CREATE TABLE IF NOT EXISTS `good_category` (
`id` int(10) unsigned NOT NULL,
  `good_id` int(10) unsigned NOT NULL COMMENT 'id товара',
  `category_id` int(10) unsigned NOT NULL COMMENT 'id категории'
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8 COMMENT='Таблица связи товара и категории';

-- --------------------------------------------------------

--
-- Структура таблицы `good_photos`
--

DROP TABLE IF EXISTS `good_photos`;
CREATE TABLE IF NOT EXISTS `good_photos` (
`id` int(10) unsigned NOT NULL,
  `good_id` int(10) unsigned NOT NULL COMMENT 'id товара',
  `file_name` varchar(30) NOT NULL COMMENT 'Имя файла',
  `main` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Это главная фотография товара?',
  `margin_top_part` decimal(4,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'Относительный сдвиг от врхнего края по отношению к меньшей стороне',
  `margin_left_part` decimal(4,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'Относительный сдвиг от левого края по отношению к меньшей стороне',
  `size_part` decimal(4,3) unsigned NOT NULL DEFAULT '1.000' COMMENT 'Относительный размер вырезанной области по отношению к меньшей стороне',
  `real_width` smallint(10) unsigned NOT NULL COMMENT 'Реальная ширина изображения (px)',
  `real_height` smallint(10) unsigned NOT NULL COMMENT 'Реальная высота изображения (px)'
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COMMENT='Фотографии товаров';

-- --------------------------------------------------------

--
-- Структура таблицы `ip_log_common`
--

DROP TABLE IF EXISTS `ip_log_common`;
CREATE TABLE IF NOT EXISTS `ip_log_common` (
`id` int(10) unsigned NOT NULL,
  `action` enum('registration','authorization','create_studio') NOT NULL COMMENT 'Действие, совершенное пользователем',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Id пользователя, если он известен',
  `ip` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'IP пользователя при совершении действия',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания записи'
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='Лог IP''шников при совершении пользователями различных действий';

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
`id` int(10) unsigned NOT NULL,
  `from_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя-отправителя',
  `to_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя-получателя',
  `text` text NOT NULL COMMENT 'Текст сообщения',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время отправления',
  `is_read` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Прочитано ли сообщение?'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='Сообщения пользователей между собой';

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(512) NOT NULL COMMENT 'Заголовок новости',
  `text` text NOT NULL COMMENT 'Текст новости',
  `show` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Показывать новость на сайте?',
  `has_img` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Есть ли изображение у новости?',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания новости'
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='Новости';

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
`id` int(10) unsigned NOT NULL,
  `seller_id` int(10) unsigned NOT NULL COMMENT 'id продавца',
  `buyer_id` int(10) unsigned NOT NULL COMMENT 'id покупателя',
  `seller_evaluation` decimal(3,2) DEFAULT NULL COMMENT 'Какую оценку поставил продавец',
  `buyer_evaluation` decimal(3,2) DEFAULT NULL COMMENT 'Какую оценку поставил покупатель',
  `status_id` int(10) unsigned NOT NULL COMMENT 'id статуса заказа',
  `buyer_phone` varchar(30) NOT NULL DEFAULT '' COMMENT 'Телефон покупателя',
  `buyer_index` varchar(10) NOT NULL DEFAULT '' COMMENT 'Индекс покупателя',
  `buyer_address` varchar(300) NOT NULL DEFAULT '' COMMENT 'Адрес покупателя',
  `comment` text COMMENT 'Комментарий к заказу',
  `delivery_id` int(10) unsigned DEFAULT NULL COMMENT 'Id способа доставки',
  `payment_id` int(10) unsigned DEFAULT NULL COMMENT 'Id способа оплаты',
  `custom_delivery` varchar(250) NOT NULL DEFAULT '' COMMENT 'Каким был свой способ доставки у продавца на момент заказа (если выбран именно он)',
  `custom_payment` varchar(250) NOT NULL DEFAULT '' COMMENT 'Каким был свой способ оплаты у продавца на момент заказа (если выбран именно он)',
  `mark_for_seller` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Заказ помечен для продавца (покупатель что-то изменил)',
  `mark_for_buyer` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Заказ помечен для покупателя (продавец что-то изменил)',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания заказа',
  `close_time` timestamp NULL DEFAULT NULL COMMENT 'Время закрытия заказа',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время последнего обновления заказа'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='Заказы пользователей';

-- --------------------------------------------------------

--
-- Структура таблицы `order_content`
--

DROP TABLE IF EXISTS `order_content`;
CREATE TABLE IF NOT EXISTS `order_content` (
`id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL COMMENT 'id заказа',
  `good_id` int(10) unsigned NOT NULL COMMENT 'id товара',
  `good_name` varchar(70) NOT NULL COMMENT 'Название товара. В случае если у товара сменится, в истории будет прежнее название.',
  `price` int(10) unsigned NOT NULL COMMENT 'Цена товара на момент заказа',
  `quantity` smallint(10) unsigned NOT NULL COMMENT 'Количество товаров в заказе'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COMMENT='Содержимое заказов';

-- --------------------------------------------------------

--
-- Структура таблицы `order_status_history`
--

DROP TABLE IF EXISTS `order_status_history`;
CREATE TABLE IF NOT EXISTS `order_status_history` (
`id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL COMMENT 'Id заказа',
  `status_id` int(10) unsigned NOT NULL COMMENT 'Id статуса заказа',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время изменения статуса'
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COMMENT='История изменения статусов заказов';

-- --------------------------------------------------------

--
-- Структура таблицы `password_recovery`
--

DROP TABLE IF EXISTS `password_recovery`;
CREATE TABLE IF NOT EXISTS `password_recovery` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `secret_key` char(8) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Секретный ключ, по которому определяется, что нужный пользователь зашел на страницу генерации нового пароля',
  `applied` tinyint(1) unsigned NOT NULL COMMENT 'Был ли использован код восстановления пароля?',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время создания заявки',
  `applied_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время применения кода'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица с запросами на восстановление пароля';

-- --------------------------------------------------------

--
-- Структура таблицы `prepared_orders`
--

DROP TABLE IF EXISTS `prepared_orders`;
CREATE TABLE IF NOT EXISTS `prepared_orders` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя, подготавливающего заказ',
  `studio_id` int(10) unsigned NOT NULL COMMENT 'Id студии',
  `delivery_id` int(10) unsigned DEFAULT NULL COMMENT 'Id способа доставки',
  `payment_id` int(10) unsigned DEFAULT NULL COMMENT 'Id способа оплаты',
  `custom_delivery` varchar(250) NOT NULL DEFAULT '' COMMENT 'Каким был свой способ доставки у продавца на момент заказа (если выбран именно он)',
  `custom_payment` varchar(250) NOT NULL DEFAULT '' COMMENT 'Каким был свой способ оплаты у продавца на момент заказа (если выбран именно он)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Предварительно сохраняет способы доставки и оплаты кзаказу у определенной студии';

-- --------------------------------------------------------

--
-- Структура таблицы `registration_activation_keys`
--

DROP TABLE IF EXISTS `registration_activation_keys`;
CREATE TABLE IF NOT EXISTS `registration_activation_keys` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `secret_key` char(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Секретный ключ, для подтверждения пользователем регистрации',
  `applied_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время применения кода'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Секретные ключи для подтверждения регистрации';

-- --------------------------------------------------------

--
-- Структура таблицы `service_buying`
--

DROP TABLE IF EXISTS `service_buying`;
CREATE TABLE IF NOT EXISTS `service_buying` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `service_id` int(10) unsigned NOT NULL COMMENT 'Id услуги',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время заказа услуги'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заказы пользователями услуг';

-- --------------------------------------------------------

--
-- Структура таблицы `service_list`
--

DROP TABLE IF EXISTS `service_list`;
CREATE TABLE IF NOT EXISTS `service_list` (
`id` int(10) unsigned NOT NULL,
  `code` varchar(128) NOT NULL COMMENT 'Буквенный код услуги',
  `description` varchar(255) NOT NULL COMMENT 'Описание услуги'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Список услуг';

-- --------------------------------------------------------

--
-- Структура таблицы `studio`
--

DROP TABLE IF EXISTS `studio`;
CREATE TABLE IF NOT EXISTS `studio` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'id пользователя - хозяина студии',
  `type` enum('atelier','store') NOT NULL COMMENT 'Тип студи (ателье или магазин)',
  `name` varchar(30) NOT NULL COMMENT 'Название студии',
  `slogan` varchar(200) DEFAULT NULL COMMENT 'Слоган студии',
  `description` text COMMENT 'Описание студии',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата обновления',
  `custom_delivery` varchar(250) DEFAULT NULL COMMENT 'Свой способ доставки',
  `custom_payment` varchar(250) DEFAULT NULL COMMENT 'Свой способ оплаты',
  `currency_id` tinyint(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Id валюты студии',
  `country_id` int(10) unsigned NOT NULL COMMENT 'Id страны, где расположена студия',
  `city_id` int(10) unsigned DEFAULT NULL COMMENT 'Id города, где расположена студия'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `studio_delivery`
--

DROP TABLE IF EXISTS `studio_delivery`;
CREATE TABLE IF NOT EXISTS `studio_delivery` (
`id` int(10) unsigned NOT NULL,
  `studio_id` int(10) unsigned NOT NULL COMMENT 'id студии',
  `delivery_id` int(10) unsigned NOT NULL COMMENT 'id способа доставки'
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='Таблица связи студии и способа доставки';

-- --------------------------------------------------------

--
-- Структура таблицы `studio_payment`
--

DROP TABLE IF EXISTS `studio_payment`;
CREATE TABLE IF NOT EXISTS `studio_payment` (
`id` int(10) unsigned NOT NULL,
  `studio_id` int(10) unsigned NOT NULL COMMENT 'id студии',
  `payment_id` int(10) unsigned NOT NULL COMMENT 'id способа оплаты'
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8 COMMENT='Таблица связи студии и способа оплаты';

-- --------------------------------------------------------

--
-- Структура таблицы `studio_promotion`
--

DROP TABLE IF EXISTS `studio_promotion`;
CREATE TABLE IF NOT EXISTS `studio_promotion` (
`id` int(10) unsigned NOT NULL,
  `studio_id` int(10) unsigned NOT NULL COMMENT 'Id студии',
  `last_promotion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Время окончания продвижения студии',
  `end_of_promotion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Время окончания продвижения студии'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Времена продвижения студий';

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `login` varchar(30) NOT NULL COMMENT 'Логин пользователя',
  `email` varchar(100) NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Хэш пароля пользователя',
  `auth_key` binary(32) NOT NULL COMMENT 'Ключ аутентификации пользователя',
  `name` varchar(100) DEFAULT NULL COMMENT 'Имя пользователя (опционально)',
  `additional_status` enum('ok','banned') NOT NULL DEFAULT 'ok' COMMENT 'Дополнительный статус пользователя',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Дата создания',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'Дата обновления',
  `country_id` int(10) unsigned DEFAULT NULL COMMENT 'id страны пользователя',
  `city_id` int(10) unsigned DEFAULT NULL COMMENT 'id города пользователя',
  `address` varchar(500) DEFAULT NULL COMMENT 'Адрес доставки пользователя'
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user_photos`
--

DROP TABLE IF EXISTS `user_photos`;
CREATE TABLE IF NOT EXISTS `user_photos` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL COMMENT 'id пользователя',
  `file_name` varchar(30) NOT NULL COMMENT 'Имя файла',
  `margin_top_part` decimal(4,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'Относительный сдвиг от врхнего края по отношению к меньшей стороне',
  `margin_left_part` decimal(4,3) unsigned NOT NULL DEFAULT '0.000' COMMENT 'Относительный сдвиг от левого края по отношению к меньшей стороне',
  `size_part` decimal(4,3) unsigned NOT NULL DEFAULT '1.000' COMMENT 'Относительный размер вырезанной области по отношению к меньшей стороне',
  `real_width` int(10) unsigned NOT NULL COMMENT 'Реальная ширина изображения (px)',
  `real_height` int(10) unsigned NOT NULL COMMENT 'Реальная высота изображения (px)'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Фотографии товаров';

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
 ADD PRIMARY KEY (`item_name`,`user_id`), ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
 ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `basket`
--
ALTER TABLE `basket`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`good_id`), ADD KEY `good_id` (`good_id`);

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
 ADD PRIMARY KEY (`id`), ADD KEY `studio_id` (`studio_id`), ADD KEY `price` (`price`);

--
-- Индексы таблицы `goods_promotion`
--
ALTER TABLE `goods_promotion`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `studio_id` (`good_id`);

--
-- Индексы таблицы `good_category`
--
ALTER TABLE `good_category`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `unique_relation` (`good_id`,`category_id`), ADD KEY `good_id` (`good_id`), ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `good_photos`
--
ALTER TABLE `good_photos`
 ADD PRIMARY KEY (`id`), ADD KEY `good_id` (`good_id`);

--
-- Индексы таблицы `ip_log_common`
--
ALTER TABLE `ip_log_common`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`), ADD KEY `to_id` (`to_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`id`), ADD KEY `show` (`show`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`id`), ADD KEY `seller_id` (`seller_id`), ADD KEY `buyer_id` (`buyer_id`), ADD KEY `status_id` (`status_id`), ADD KEY `delivery_id` (`delivery_id`), ADD KEY `payment_id` (`payment_id`);

--
-- Индексы таблицы `order_content`
--
ALTER TABLE `order_content`
 ADD PRIMARY KEY (`id`), ADD KEY `order_id` (`order_id`), ADD KEY `good_id` (`good_id`);

--
-- Индексы таблицы `order_status_history`
--
ALTER TABLE `order_status_history`
 ADD PRIMARY KEY (`id`), ADD KEY `order_id` (`order_id`), ADD KEY `status_id` (`status_id`);

--
-- Индексы таблицы `password_recovery`
--
ALTER TABLE `password_recovery`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `secret_key` (`secret_key`), ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `prepared_orders`
--
ALTER TABLE `prepared_orders`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`studio_id`), ADD UNIQUE KEY `delivery_id` (`delivery_id`), ADD KEY `studio_id` (`studio_id`), ADD KEY `payment_id` (`payment_id`);

--
-- Индексы таблицы `registration_activation_keys`
--
ALTER TABLE `registration_activation_keys`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `secret_key` (`secret_key`), ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `service_buying`
--
ALTER TABLE `service_buying`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `service_id` (`service_id`);

--
-- Индексы таблицы `service_list`
--
ALTER TABLE `service_list`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `studio`
--
ALTER TABLE `studio`
 ADD PRIMARY KEY (`id`), ADD KEY `type` (`type`), ADD KEY `user_id` (`user_id`), ADD KEY `country_id` (`country_id`), ADD KEY `city_id` (`city_id`);

--
-- Индексы таблицы `studio_delivery`
--
ALTER TABLE `studio_delivery`
 ADD PRIMARY KEY (`id`), ADD KEY `studio_id` (`studio_id`), ADD KEY `delivery_id` (`delivery_id`);

--
-- Индексы таблицы `studio_payment`
--
ALTER TABLE `studio_payment`
 ADD PRIMARY KEY (`id`), ADD KEY `studio_id` (`studio_id`), ADD KEY `payment_id` (`payment_id`);

--
-- Индексы таблицы `studio_promotion`
--
ALTER TABLE `studio_promotion`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `studio_id` (`studio_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `userscol_UNIQUE` (`login`), ADD UNIQUE KEY `email_UNIQUE` (`email`), ADD KEY `country_id` (`country_id`), ADD KEY `city_id` (`city_id`);

--
-- Индексы таблицы `user_photos`
--
ALTER TABLE `user_photos`
 ADD PRIMARY KEY (`id`), ADD KEY `good_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `basket`
--
ALTER TABLE `basket`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT для таблицы `goods_promotion`
--
ALTER TABLE `goods_promotion`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `good_category`
--
ALTER TABLE `good_category`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT для таблицы `good_photos`
--
ALTER TABLE `good_photos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT для таблицы `ip_log_common`
--
ALTER TABLE `ip_log_common`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `order_content`
--
ALTER TABLE `order_content`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `order_status_history`
--
ALTER TABLE `order_status_history`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `password_recovery`
--
ALTER TABLE `password_recovery`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `prepared_orders`
--
ALTER TABLE `prepared_orders`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `registration_activation_keys`
--
ALTER TABLE `registration_activation_keys`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `service_buying`
--
ALTER TABLE `service_buying`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `service_list`
--
ALTER TABLE `service_list`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `studio`
--
ALTER TABLE `studio`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `studio_delivery`
--
ALTER TABLE `studio_delivery`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT для таблицы `studio_payment`
--
ALTER TABLE `studio_payment`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT для таблицы `studio_promotion`
--
ALTER TABLE `studio_promotion`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `user_photos`
--
ALTER TABLE `user_photos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `basket`
--
ALTER TABLE `basket`
ADD CONSTRAINT `basket_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `basket_ibfk_2` FOREIGN KEY (`good_id`) REFERENCES `goods` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `goods`
--
ALTER TABLE `goods`
ADD CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `goods_promotion`
--
ALTER TABLE `goods_promotion`
ADD CONSTRAINT `goods_promotion_ibfk_1` FOREIGN KEY (`good_id`) REFERENCES `goods` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `good_category`
--
ALTER TABLE `good_category`
ADD CONSTRAINT `good_category_ibfk_1` FOREIGN KEY (`good_id`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `good_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `good_photos`
--
ALTER TABLE `good_photos`
ADD CONSTRAINT `good_photos_ibfk_1` FOREIGN KEY (`good_id`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `order_status_list` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `orders_ibfk_4` FOREIGN KEY (`delivery_id`) REFERENCES `delivery_list` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `orders_ibfk_5` FOREIGN KEY (`payment_id`) REFERENCES `payment_list` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order_content`
--
ALTER TABLE `order_content`
ADD CONSTRAINT `order_content_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `order_content_ibfk_2` FOREIGN KEY (`good_id`) REFERENCES `goods` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order_status_history`
--
ALTER TABLE `order_status_history`
ADD CONSTRAINT `order_status_history_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `order_status_history_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `order_status_list` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `password_recovery`
--
ALTER TABLE `password_recovery`
ADD CONSTRAINT `password_recovery_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `prepared_orders`
--
ALTER TABLE `prepared_orders`
ADD CONSTRAINT `prepared_orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `prepared_orders_ibfk_2` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `prepared_orders_ibfk_3` FOREIGN KEY (`delivery_id`) REFERENCES `delivery_list` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `prepared_orders_ibfk_4` FOREIGN KEY (`payment_id`) REFERENCES `payment_list` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `registration_activation_keys`
--
ALTER TABLE `registration_activation_keys`
ADD CONSTRAINT `registration_activation_keys_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `service_buying`
--
ALTER TABLE `service_buying`
ADD CONSTRAINT `service_buying_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `service_buying_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `service_list` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `studio`
--
ALTER TABLE `studio`
ADD CONSTRAINT `studio_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `studio_ibfk_2` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `studio_ibfk_3` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `studio_delivery`
--
ALTER TABLE `studio_delivery`
ADD CONSTRAINT `studio_delivery_ibfk_1` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `studio_delivery_ibfk_2` FOREIGN KEY (`delivery_id`) REFERENCES `delivery_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `studio_payment`
--
ALTER TABLE `studio_payment`
ADD CONSTRAINT `studio_payment_ibfk_1` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `studio_payment_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `studio_promotion`
--
ALTER TABLE `studio_promotion`
ADD CONSTRAINT `studio_promotion_ibfk_1` FOREIGN KEY (`studio_id`) REFERENCES `studio` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_photos`
--
ALTER TABLE `user_photos`
ADD CONSTRAINT `user_photos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
