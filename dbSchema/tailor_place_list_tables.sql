-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 06 2015 г., 12:31
-- Версия сервера: 5.5.46-0+deb8u1
-- Версия PHP: 5.6.14-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `tailor_dev`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Пользователь, обладающий правами администратора', NULL, NULL, 1441915115, 1441915115),
('atelierOwner', 1, NULL, NULL, NULL, 1441915115, 1441915115),
('createOrder', 2, 'Оформление заказа', NULL, NULL, 1441915115, 1441915115),
('createStudio', 2, 'Создание ателье/магазина', NULL, NULL, 1441915115, 1441915115),
('sendMessage', 2, 'Отправка сообщений', NULL, NULL, 1441915115, 1441915115),
('storeOwner', 1, NULL, NULL, NULL, 1441915115, 1441915115),
('unconfirmed', 1, 'Пользователь еще не подтвердивший свою регистрацию', NULL, NULL, 1441915115, 1441915115),
('user', 1, 'Обычный зарегистрированный пользователь', NULL, NULL, 1441915115, 1441915115),
('viewAdminSection', 2, 'Просмотр раздела админки', NULL, NULL, 1441915115, 1441915115),
('viewCabinet', 2, 'Просмотр личного кабинета', NULL, NULL, 1441915115, 1441915115),
('viewHideNews', 2, 'Просмотр скрытых новостей', NULL, NULL, 1441915115, 1441915115);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('user', 'createOrder'),
('user', 'sendMessage'),
('admin', 'user'),
('atelierOwner', 'user'),
('storeOwner', 'user'),
('admin', 'viewAdminSection'),
('unconfirmed', 'viewCabinet'),
('user', 'viewCabinet'),
('admin', 'viewHideNews');

-- --------------------------------------------------------

--
-- Структура таблицы `category_list`
--

DROP TABLE IF EXISTS `category_list`;
CREATE TABLE IF NOT EXISTS `category_list` (
`id` int(10) unsigned NOT NULL,
  `name_ru` varchar(45) NOT NULL COMMENT 'Русское название',
  `name_en` varchar(45) NOT NULL COMMENT 'Английское название',
  `concretization_ru` varchar(50) DEFAULT NULL COMMENT 'Уточнение русское',
  `concretization_en` varchar(50) DEFAULT NULL COMMENT 'Уточнение аглийское',
  `css_class` varchar(20) NOT NULL COMMENT 'css класс',
  `type` enum('atelier','store','all') NOT NULL COMMENT 'Тип студии, к которому относится классификация товара',
  `sort_position` smallint(10) unsigned NOT NULL COMMENT 'Порядок сортировки'
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='Таблица с категориями товаров';

--
-- Дамп данных таблицы `category_list`
--

INSERT INTO `category_list` (`id`, `name_ru`, `name_en`, `concretization_ru`, `concretization_en`, `css_class`, `type`, `sort_position`) VALUES
(1, 'Брюки', 'Suit pants', NULL, NULL, 'suit-pants', 'atelier', 10),
(2, 'Верхняя одежда', 'Outerwear', NULL, NULL, 'outerwear', 'atelier', 20),
(3, 'Головной убор', 'Headdress', NULL, NULL, 'headdress', 'atelier', 30),
(4, 'Детская одежда', 'Children''s wear', NULL, NULL, 'children', 'atelier', 40),
(5, 'Джинсы', 'Jeans', NULL, NULL, 'jeans', 'atelier', 50),
(6, 'Домашняя одежда', 'Indoor clothing', NULL, NULL, 'indoor', 'atelier', 60),
(7, 'Женская одежда', 'Women''s Clothing', NULL, NULL, 'women', 'atelier', 70),
(8, 'Жилет', 'Vest', NULL, NULL, 'vest', 'atelier', 80),
(9, 'Зимняя одежда', 'Winter clothing', NULL, NULL, 'winter', 'atelier', 90),
(10, 'Изделие из кожи', 'Leather product', NULL, NULL, 'leather', 'atelier', 100),
(11, 'Кардиган', 'Cardigan', NULL, NULL, 'cardigan', 'atelier', 110),
(12, 'Колготки', 'Tights', NULL, NULL, 'tights', 'atelier', 120),
(13, 'Комбинезон', 'Overall', NULL, NULL, 'overall', 'atelier', 130),
(14, 'Костюм', 'Suit', NULL, NULL, 'suit', 'atelier', 140),
(15, 'Кофта', 'Blouse', NULL, NULL, 'blouse', 'atelier', 150),
(16, 'Купальник', 'Swimsuit', NULL, NULL, 'swimsuit', 'atelier', 160),
(17, 'Куртка', 'Jacket', NULL, NULL, 'jacket', 'atelier', 170),
(18, 'Летняя одежда', 'Summer clothing', NULL, NULL, 'summer', 'atelier', 180),
(19, 'Маскарадная одежда', 'Masquerade clothing', NULL, NULL, 'masquerade', 'atelier', 190),
(20, 'Мужская одежда', 'Men''s wear', NULL, NULL, 'men-wear', 'atelier', 200),
(21, 'Национальная одежда', 'National clothes', NULL, NULL, 'national', 'atelier', 210),
(22, 'Нижнее белье', 'Underwear', NULL, NULL, 'underwear', 'atelier', 220),
(23, 'Носки', 'Socks', NULL, NULL, 'socks', 'atelier', 230),
(24, 'Обувь', 'Shoes', NULL, NULL, 'shoes', 'atelier', 240),
(25, 'Осенняя (весенняя) одежда', 'Autumn (spring) clothes', NULL, NULL, 'autumn', 'atelier', 250),
(26, 'Пальто', 'Coat', NULL, NULL, 'coat', 'atelier', 260),
(27, 'Пиджак', 'Blazer', NULL, NULL, 'blazer', 'atelier', 270),
(28, 'Платье', 'Dress', NULL, NULL, 'the-dress', 'atelier', 280),
(29, 'Плащ', 'Cloak', NULL, NULL, 'cloak', 'atelier', 290),
(30, 'Праздничная одежда', 'Festival clothes', NULL, NULL, 'festival', 'atelier', 300),
(31, 'Спецодежда', 'Coveralls', NULL, NULL, 'coveralls', 'atelier', 310),
(32, 'Спортивная одежда', 'Sportswear', NULL, NULL, 'sportswear', 'atelier', 320),
(33, 'Термобелье', 'Thermolinen', NULL, NULL, 'thermolinen', 'atelier', 330),
(34, 'Униформа', 'Uniform', NULL, NULL, 'uniform', 'atelier', 340),
(35, 'Футболка', 'T-shirt', NULL, NULL, 't-shirt', 'atelier', 350),
(36, 'Халат', 'Bathrobe', NULL, NULL, 'bathrobe', 'atelier', 360),
(37, 'Шорты', 'Shorts', NULL, NULL, 'shorts', 'atelier', 370),
(38, 'Штаны', 'Pants', NULL, NULL, 'pants', 'atelier', 380),
(39, 'Шуба', 'Fur coat', NULL, NULL, 'fur-coat', 'atelier', 390),
(40, 'Юбка', 'Skirt', NULL, NULL, 'skirt', 'atelier', 400),
(41, 'Другое', 'Other', NULL, NULL, 'other', 'all', 530),
(42, 'Иглы, булавки', 'Needles, pins', NULL, NULL, 'needles', 'store', 420),
(43, 'Измерительные инструменты', 'Measuring tools', 'сантиметры, линейки, угольники', 'centimeters, rulers, gons', 'centimeters', 'store', 430),
(44, 'Инструменты для разметки', 'Devices for marking', 'мелки', 'crayons', 'marking', 'store', 440),
(45, 'Манекены', 'Mannequins', NULL, NULL, 'mannequins', 'store', 450),
(46, 'Мебель', 'Furniture', 'доски для раскройки, столы', 'board to be cut, tables', 'furniture', 'store', 460),
(47, 'Нитки', 'Threads', NULL, NULL, 'threads', 'store', 470),
(48, 'Отделочные ленты', 'Decorative ribbons', 'тесьма, велкро', 'webbing, velcro', 'ribbons', 'store', 480),
(49, 'Пуговицы, кнопки', 'Buttons', NULL, NULL, 'buttons', 'store', 490),
(50, 'Режущие инструменты', 'Cutting tools', 'ножницы', 'knives, scissors', 'scissors', 'store', 500),
(51, 'Ткани', 'Fabrics', NULL, NULL, 'fabrics', 'store', 510),
(52, 'Электроприборы', 'Appliances', 'швейные машины, утюги', 'sewing machines, irons', 'appliances', 'store', 520),
(53, 'Детали и комплектующие', 'Parts and components', NULL, NULL, 'parts_components', 'store', 415);

-- --------------------------------------------------------

--
-- Структура таблицы `currency`
--

DROP TABLE IF EXISTS `currency`;
CREATE TABLE IF NOT EXISTS `currency` (
`id` tinyint(10) unsigned NOT NULL,
  `code` varchar(3) NOT NULL COMMENT 'Трехзначный код валюты',
  `symbol` varchar(10) NOT NULL COMMENT 'Символ',
  `name_ru` varchar(20) NOT NULL COMMENT 'Русское название',
  `name_en` varchar(20) NOT NULL COMMENT 'Английское название'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `currency`
--

INSERT INTO `currency` (`id`, `code`, `symbol`, `name_ru`, `name_en`) VALUES
(1, 'RUR', '₽', 'Российский рубль', 'Russian Ruble'),
(2, 'USD', '$', 'Доллар США', 'US Dollar'),
(4, 'EUR', '€', 'Евро', 'Euro'),
(5, 'CNY', '¥', 'Юань', 'Yuan Renminbi'),
(6, 'JPY', '¥', 'Иена', 'Yen'),
(7, 'UAH', '₴', 'Гривна', 'Hryvnia'),
(8, 'KZT', '₸', 'Тенге', 'Tenge'),
(9, 'MNT', '₮', 'Тугрик', 'Tugrik');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_list`
--

DROP TABLE IF EXISTS `delivery_list`;
CREATE TABLE IF NOT EXISTS `delivery_list` (
`id` int(10) unsigned NOT NULL,
  `name_ru` varchar(60) NOT NULL COMMENT 'Русское название',
  `name_en` varchar(60) NOT NULL COMMENT 'Английское название',
  `css_class` varchar(30) NOT NULL COMMENT 'css класс'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `delivery_list`
--

INSERT INTO `delivery_list` (`id`, `name_ru`, `name_en`, `css_class`) VALUES
(1, 'Почта', 'Mail', 'mail-delivery'),
(2, 'Курьерская доставка', 'Express delivery', 'express-delivery'),
(3, 'При личной встрече', 'Personal meeting', 'personal-meeting');

-- --------------------------------------------------------

--
-- Структура таблицы `order_status_list`
--

DROP TABLE IF EXISTS `order_status_list`;
CREATE TABLE IF NOT EXISTS `order_status_list` (
`id` int(10) unsigned NOT NULL,
  `name_ru` varchar(50) NOT NULL,
  `name_en` varchar(50) NOT NULL,
  `css_class` varchar(64) NOT NULL COMMENT 'css класс для статуса'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Список статусов заказа';

--
-- Дамп данных таблицы `order_status_list`
--

INSERT INTO `order_status_list` (`id`, `name_ru`, `name_en`, `css_class`) VALUES
(1, 'В ожидании', 'Pending', 'wait'),
(2, 'Завершен', 'Completed', 'complited'),
(3, 'Отклонен', 'Rejected', 'rejected'),
(4, 'Отозван', 'Withdrawn', 'withdrawn'),
(5, 'Принят, ожидает оплаты', 'Adopted and expects payment', 'adopted-wait-paiment'),
(6, 'В работе', 'In work', 'in-work'),
(7, 'Готов, ожидает оплаты', 'Ready and expects payment', 'ready-wait-paiment'),
(8, 'Комплектуется', 'Completed with', 'completed-with'),
(9, 'Отправлен', 'Shipped', 'shipped');

-- --------------------------------------------------------

--
-- Структура таблицы `payment_list`
--

DROP TABLE IF EXISTS `payment_list`;
CREATE TABLE IF NOT EXISTS `payment_list` (
`id` int(10) unsigned NOT NULL,
  `name_ru` varchar(60) NOT NULL COMMENT 'Русское название',
  `name_en` varchar(60) NOT NULL COMMENT 'Английское название',
  `css_class` varchar(30) NOT NULL COMMENT 'css класс'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `payment_list`
--

INSERT INTO `payment_list` (`id`, `name_ru`, `name_en`, `css_class`) VALUES
(1, 'WebMoney', 'WebMoney', 'webmoney'),
(2, 'Яндекс деньги', 'Yandex money', 'yandex-money'),
(3, 'Почтовый перевод', 'Mail transfer', 'mail-transfer'),
(4, 'Банковский перевод', 'Bank transfer', 'bank-transfer'),
(5, 'Наличными при встрече', 'By cash when meeting', 'by-cash-when-meeting'),
(6, 'Qiwi', 'Qiwi', 'qiwi'),
(7, 'PayPal', 'PayPal', 'pay-pal');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
 ADD PRIMARY KEY (`name`), ADD KEY `rule_name` (`rule_name`), ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
 ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Индексы таблицы `category_list`
--
ALTER TABLE `category_list`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `currency`
--
ALTER TABLE `currency`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `delivery_list`
--
ALTER TABLE `delivery_list`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `order_status_list`
--
ALTER TABLE `order_status_list`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `payment_list`
--
ALTER TABLE `payment_list`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category_list`
--
ALTER TABLE `category_list`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT для таблицы `currency`
--
ALTER TABLE `currency`
MODIFY `id` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `delivery_list`
--
ALTER TABLE `delivery_list`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `order_status_list`
--
ALTER TABLE `order_status_list`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `payment_list`
--
ALTER TABLE `payment_list`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
