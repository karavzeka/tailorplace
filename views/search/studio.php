<?php
/**
 * Страница с результатом поиска студий
 * @var yii\web\View $this
 * @var string $studioList
 * @var string $pageTitle
 * @var string $searchAlert
 * @var string $studioType
 * @var bool $showMoreButton
 * @var string $rawQueryString
 * @var int $countOfStudios
 */

use app\assets\AppAsset;
use app\widgets\NewsWidget;
use yii\helpers\Html;

$this->title = $pageTitle;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $pageTitle
]);

$this->registerCssFile('@web/css/search-studio.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/search-studio.js', [
    'depends' => [AppAsset::className()]
]);
?>
<main class="float left">
    <h2><?= $pageTitle ?></h2>
    <p class="size16">Вы искали: "<span id="search-query" class="italic"><?= Html::encode($rawQueryString) ?></span>"</p>
    <div class="sloping-line"></div>
    <?php
    echo '<p class="search-alert">' . $searchAlert . '</p>';

    if ($studioList) {
        echo '<ul id="studio-wrapper" data-count-of-studios="' . $countOfStudios . '" data-studio-type="' . $studioType . '">';
        echo $studioList;
        echo '</ul>';
    }
    ?>
    <div id="show-more">
        <?php if ($showMoreButton) :?>
            <div id="show-more-button" data-part="1">
                <span class="plus-button"></span>
                <span>Показать еще</span>
            </div>
        <?php endif ?>
    </div>
</main>
<aside class="float right">
    <?= NewsWidget::widget() ?>
</aside>