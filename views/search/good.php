<?php
/**
 * Страница с результатом поиска товаров
 * @var yii\web\View $this
 * @var string $goodsList
 * @var string $searchAlert
 * @var bool $showMoreButton
 * @var int $countOfGoods
 * @var string $rawQueryString
 */

use app\assets\AppAsset;
use app\widgets\NewsWidget;
use yii\helpers\Html;

$this->title = 'Результат поиска товаров';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Результат поиска товаров'
]);

$this->registerCssFile('@web/css/search-good.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/search-good.js', [
    'depends' => [AppAsset::className()]
]);
?>
<main class="float left">
    <h2><?= $this->title ?></h2>
    <p class="size16">Вы искали: "<span id="search-query" class="italic"><?= Html::encode($rawQueryString) ?></span>"</p>
    <div class="sloping-line"></div>
    <?php
    echo '<p class="search-alert">' . $searchAlert . '</p>';

    if ($goodsList) {
        echo '<div id="goods-wrapper" class="good-tile"  data-count-of-goods="' . $countOfGoods . '">';
        echo $goodsList;
        echo '</div>';
    }
    ?>
    <div id="show-more">
        <?php if ($showMoreButton) :?>
            <div id="show-more-button" data-part="1">
                <span class="plus-button"></span>
                <span>Показать еще</span>
            </div>
        <?php endif ?>
    </div>
</main>
<aside class="float right">
    <?= NewsWidget::widget() ?>
</aside>