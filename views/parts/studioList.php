<?php
/**
 * Генерирует HTML набора студий
 *
 * @var array $studioData
 * @var bool $fillByEmptyGoods
 */

use app\modules\users\models\Photo AS UserPhoto;
use yii\helpers\Html;
use yii\helpers\Url;

if (count($studioData['studios']) > 0) {
    foreach ($studioData['studios'] as $studio) {
        echo '<li>';
            echo '<div class="main-info">';
                $imgHtml = Html::img(isset($studioData['photos'][$studio['user_id']]) ? $studioData['photos'][$studio['user_id']]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                    'width' => 98,
                    'height' => 98,
                    'alt' => $studio['studio_name']
                ]);
                echo Html::a($imgHtml, Url::toRoute('/studio/' . $studio['studio_id']));
                echo '<div class="name">';
                        echo '<div class="icon-line">';
                            echo Html::tag('div', '<i></i>', ['class' => 'icon-circle small ' . $studio['studio_type']]);
                            echo ' ';
                            echo '<span>';
                                echo Html::a(Html::encode($studio['studio_name']), Url::toRoute('/studio/' . $studio['studio_id']), ['class' => 'big-red']);
                            echo '</span>';
                        echo '</div>'; // <div class="icon-line">
                        echo Html::tag('p', Html::encode($studio['slogan']), ['class' => 'italic']);
                echo '</div>'; // <div class="name">
            echo '</div>'; // <div class="main-info">
            echo '<div class="additional-info">';
                echo '<div class="count">';
                    echo '<span>Количество товаров:</span> <span class="italic">' . $studio['count_of_goods'] . '</span>';
                echo '</div>'; // <div class="count">
                echo '<div class="country">';
                    echo '<span>Страна:</span> <span class="italic">' . $studio['country_name'] . '</span>';
                echo '</div>'; // <div class="country">
                echo '<div class="city">';
                    echo '<span>Город:</span> <span class="italic">' . $studio['city_name'] . '</span>';
                echo '</div>'; // <div class="city">
                echo '<div class="clear"></div>';
            echo '</div>'; // <div class="additional-info">
        echo '</li>';
    }
}