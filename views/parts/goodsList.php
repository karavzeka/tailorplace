<?php
/**
 * Генерирует HTML набора товаров
 *
 * @var array $goodsData
 * @var bool $fillByEmptyGoods
 */

use app\modules\good\models\Photo AS GoodPhoto;
use yii\helpers\Html;
use yii\helpers\Url;

if (count($goodsData['goods']) > 0) {
    foreach ($goodsData['goods'] as $good) {
        echo '<div class="good-instance">';
            echo '<figure>';
                $imgHtml = Html::img(isset($goodsData['photos'][$good['good_id']]) ? $goodsData['photos'][$good['good_id']]->getSrc('_middle') : GoodPhoto::getNoPhotoSrc('_middle'), [
                    'width' => '188',
                    'height' => '188',
                    'alt' => $good['good_name'],
                ]);
                echo Html::a($imgHtml, Url::toRoute('/goods/' . $good['good_id']));
                echo '<figcaption class="darker">';
                    echo Html::a(Html::encode($good['good_name']), Url::toRoute('/goods/' . $good['good_id']), ['class' => 'good-tile-link']);
                echo '</figcaption>';
            echo '</figure>';
            echo '<div class="good-studio">';
                echo '<div>';
                    echo Html::tag('div', '<i></i>', ['class' => 'icon-circle small ' . $good['studio_type']]);
                echo '</div>';
                echo '<span>';
                    echo Html::a(Html::encode($good['studio_name']), Url::toRoute('/studio/' . $good['studio_id']), ['class' => 'italic']);
                echo '</span>';
            echo '</div>'; // <div class="good-studio">
            echo '<div class="price">';
                echo '<span>' . Yii::$app->numberHelper->numberToPrice($good['price']) . '</span><span>руб.</span>';
            echo '</div>'; // <div class="price">
        echo '</div>'; // <div class="good-instance">
        echo ' '; // т.к. выравнивание justify работает при пробелах между словами
    }

    // последнюю строку необходимо дозаполнить пустыми элементами, иначе выравнивание может работать криво
    if ($fillByEmptyGoods) {
        $colOfLastElement = count($goodsData['goods']) % 3;
        if ($colOfLastElement != 0) {
            for ($i = 0; $i < (3 - $colOfLastElement); $i++) {
                echo '<div class="good-instance empty"></div>';
            }
        }
    }
}
?>