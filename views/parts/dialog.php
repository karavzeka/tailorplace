<?php
/**
 * Страница диалога.
 * @var app\modules\users\models\User $User
 * @var app\modules\users\models\User $Interlocutor
 * @var app\models\Message[] $Messages
 */

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\users\models\Photo as UserPhoto;

$opponentUrl = $Interlocutor->hasStudio() ? Url::toRoute('/studio/' . $Interlocutor->studio->id) : Url::toRoute('/simple-user/' . $Interlocutor->id);

date_default_timezone_set('Europe/Moscow');
setlocale(LC_TIME, 'ru_RU.UTF-8');

foreach ($Messages as $Message) {
    $msgTime = strtotime($Message->create_time);
    if ($Message->from_id == $User->id) {
        // это ваше сообщение
        echo '<li class="you">';
            echo '<div class="you-wrapper">';
                echo Html::tag('div', Html::encode($Message->text), ['class' => 'message']);
                echo '<div class="user">';
                    echo '<div class="photo">';
                        echo Html::img($User->photo ? $User->photo[0]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                            'width' => 48,
                            'height' => 48
                        ]);
                        echo Html::tag('div', '<i></i>', [
                            'class' => 'icon-circle small ' . $User->getUserIconClass()
                        ]);
                    echo '</div>'; // <div class="photo">
                    echo '<div class="info">';
                        echo '<div class="name size15">Вы</div>';
                        echo '<time>' . strftime('%e', $msgTime) . ' ' . mb_strtolower(strftime('%B', $msgTime)) . ' ' . strftime('%Y', $msgTime) . '<br>в ' . strftime('%H:%M', $msgTime) . '</time>';
                    echo '</div>'; // <div class="info">
                echo '</div>'; // <div class="user">
            echo '</div>'; // <div class="you-wrapper">
            echo '<div class="clear"></div>';
        echo '</li>'; // <li class="you">
    } else {
        // это сообщение собеседника
        echo '<li class="opponent">';
            echo '<div class="user">';
                echo '<div class="photo">';
                    echo Html::img($Interlocutor->photo ? $Interlocutor->photo[0]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                        'width' => 48,
                        'height' => 48
                    ]);
                    echo Html::tag('div', '<i></i>', [
                        'class' => 'icon-circle small ' . $Interlocutor->getUserIconClass()
                    ]);
                echo '</div>'; // <div class="photo">
                echo '<div class="info">';
                    echo '<div class="name">';
                        echo Html::a(Html::encode($Interlocutor->login), $opponentUrl, [
                            'class' => 'size15 red'
                        ]);
                    echo '</div>'; // <div class="name">
                    echo '<time>' . strftime('%e', $msgTime) . ' ' . mb_strtolower(strftime('%B', $msgTime)) . ' ' . strftime('%Y', $msgTime) . '<br>' . strftime('%H:%M', $msgTime) . '</time>';
                echo '</div>'; // <div class="info">
            echo '</div>'; // <div class="user">
            echo Html::tag('div', Html::encode($Message->text), ['class' => 'message']);
        echo '</li>'; // <li class="opponent">
    }
}
