<?php
/**
 * @var array $lastMsgUserPairs
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\users\models\Photo as UserPhoto;

foreach ($lastMsgUserPairs['users'] as $userWithMessage) {
    $msgTime = strtotime($userWithMessage['message']->create_time);
    echo '<li>';
        echo '<div class="interlocutor">';
            echo '<div class="photo">';
                echo Html::img(isset($lastMsgUserPairs['userPhotos'][$userWithMessage['user']->id]) ? $lastMsgUserPairs['userPhotos'][$userWithMessage['user']->id]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                    'width' => 48,
                    'height' => 48
                ]);
                echo Html::tag('div', '<i></i>', [
                    'class' => 'icon-circle small ' . $userWithMessage['user']->getUserIconClass()
                ]);
            echo '</div>'; // <div class="photo">
            echo '<div class="info">';
                echo Html::beginTag('div', ['class' => 'name']);
                    echo Html::a(Html::encode($userWithMessage['user']->getName()), $userWithMessage['user']->getLinkToPage(), ['class' => 'size15 red']);
                echo Html::endTag('div');
                echo '<time>' . strftime('%e', $msgTime) . ' ' . mb_strtolower(strftime('%B', $msgTime)) . ' ' . strftime('%Y', $msgTime) . '<br>в ' . strftime('%H:%M', $msgTime) . '</time>';
            echo '</div>'; // <div class="info">
        echo '</div>'; // <div class="interlocutor">
        echo Html::beginTag('a', [
            'href' => Url::toRoute('/cabinet/dialog/' . $userWithMessage['user']->id),
            'class' => 'message' . ($userWithMessage['message']->is_read ? '' : ' new')
        ]);
            echo Html::tag('p', $userWithMessage['message']->text);
        echo Html::endTag('a');
    echo '</li>';
}