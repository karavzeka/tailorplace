<p>
    Данное соглашение является дополнением <a href="/help/registration_agreement/">основного пользовательского соглашения</a>,
    и не отменяет не отменяет его правил, только дополниет. Терминология приведена в основном пользовательскм соглашении.
</p>
<p>
    <strong>1. Обязательства Пользователя</strong>
</p>
<p>
    После создания Студии, Портной/Продавец может размещать Объявления, представляюще его услуги. Портной/Продавец
    обязуется поддерживать актуальность информации в размещенных Объявлениях.
</p>
<p>
    При принятии заказа Портной/Продавец несет полную ответственность перед Покупателем за исполнение заказа.
    Tailor Place не принимает участия в исполнении Портными/Продавцами своих обязательств.
</p>
<p>
    Портной/Продавец сам должен договориться с Покупателем о способе и сумме оплаты. Tailor Place не принимает
    участия в денежном обороте и не несет ответственность в случае неполучения Продавцом оплаты за заказ.
</p>