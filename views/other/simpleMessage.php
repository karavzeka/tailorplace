<?php
/**
 * Страница, выводящая пользователю какое-либо сообщение
 * @var yii\web\View $this
 * @var string $message
 */

$this->title = 'Tailor place уведомление';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Страница уведомления пользователя'
]);
?>

<main>
    <div class="panel big-panel" style="margin-top: 30px">
        <div class="panel-body">
            <?= $message ?>
        </div>
    </div>
</main>