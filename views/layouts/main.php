<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\GoodCarousel;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$this->registerJsFile('@web/js/mainSearch.js', [
    'depends' => [AppAsset::className()]
]);

$this->title = empty($this->title) ? 'Tailor place - пошив на заказ, поиск портного, создание ателье' : $this->title;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?php if ((!defined('YII_DEBUG') || \YII_DEBUG !== true) && (!defined('YII_ENV') || \YII_ENV !== 'dev')): ?>
        <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter21054673 = new Ya.Metrika({ id:21054673, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/21054673" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
        <!-- Google Analytics counter -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-35909285-2', 'auto');
            ga('send', 'pageview');

        </script>
        <!-- Google Analytics end -->
    <?php endif ?>
    <div id="wrapper">
        <div id="center-wrapper">
            <header>
                <div id="l-h-spiral"></div>
                <div id="r-h-spiral"></div>
<!-- header-col выстроены в линию, и важно, чтобы не было пробельного символа между тегами -->
                <div class="header-col">
                    <div id="logo">
                        <h1><a href="/">Tailor <span>place</span></a></h1>
                    </div>
                </div><div class="header-col">
                    <p id="site-description"><strong>Свободная торговая площадка в области портняжного дела</strong></p>
<!-- TODO <nav> сделать виджетом, чтобы активную ссылку удобней было ставить -->
                    <nav>
                        <div class="top-line"></div>
                        <ul>
                            <li>
                                <a href="/studio/">Ателье и магазины</a>
                            </li>
                            <li>
                                <a href="/goods/">Товары</a>
                            </li>
<!--                            <li>-->
<!--                                <a href="#">Хочу это</a>-->
<!--                            </li>-->
                        </ul>
                        <div class="bottom-line"></div>
                    </nav>
                    <form id="search" method="post">
                        <input type="hidden" name="request" value="goods">
                        <div class="input-group dropdown-group dropdown-left-group">
                            <div class="input-group-btn">
                                <button data-toggle="dropdown" class="btn btn-default" type="button">Товары <span class="caret"></span></button>
                                <div class="vertical-divider"></div>
                                <ul role="menu" class="dropdown-menu">
                                    <li><span data-search-type="goods">Товары</span></li>
                                    <li class="divider"></li>
                                    <li><span data-search-type="all">Ател./Маг.</span></li>
                                    <li><span data-search-type="atelier">Ателье</span></li>
                                    <li><span data-search-type="store">Магазины</span></li>
                                </ul>
                            </div><!-- /btn-group -->
                            <input type="text" name="query" class="form-control">
                            <button id="search-submit" class="btn btn-default" type="submit"></button>
                        </div>
                    </form>
                </div><div class="header-col">
                    <?php if (Yii::$app->user->isGuest): ?>
<!-- TODO сделать виджетом -->
                    <div id="personal" class="non-authorized">
                        <p class="welcome">Здравствуй гость!</p>
                        <div id="signup-button" class="button yellow">
                            <div class="low-layer"></div>
                            <a data-type="button" href="/signup/" class="icon-line"><i class="icon person-white"></i><span>Регистрация</span></a>
                        </div>
                        <div id="login-button" class="button">
                            <div class="low-layer"></div>
                            <a data-type="button" href="/login/" class="icon-line"><i class="icon login"></i><span>Войти</span></a>
                        </div>
                    </div>
                    <?php else: ?>
                    <div id="personal" class="authorized">
                        <menu>
                            <li class="icon-line"><i class="icon person-green"></i><a href="/cabinet/">Личный кабинет</a><a href="/logout/" id="logout"><i class="icon logout" data-toggle="tooltip" data-placement="top" title="Выход"></i></a></li>
                            <li class="icon-line"><i class="icon order"></i><a href="/cabinet/orders/">Заказы</a> <?= Yii::$app->user->identity->getCountOfMarkOrders() > 0 ? '<span class="red bolder">(' . Yii::$app->user->identity->getCountOfMarkOrders() . ')</span>' : '' ?></li>
                            <li class="icon-line"><i class="icon basket"></i><a href="/cabinet/basket/">Корзина</a> <?= count(Yii::$app->user->identity->basket) ? '<span class="red bolder">(' . count(Yii::$app->user->identity->basket) . ')</span>' : '' ?></li>
                            <li class="icon-line"><i class="icon message"></i><a href="/cabinet/messages/">Сообщения</a><?= Yii::$app->user->identity->newMessages > 0 ? (' <span class="red">(' . Yii::$app->user->identity->newMessages . ')</span>') : '' ?></li>
                        </menu>
                    </div>
                    <?php endif ?>
                    <a href="/help/" class="icon-circle icon-circle-text icon-help" id="main-help"></a>
                </div>
                <div class="dashed scissor-right"></div>
            </header>
            <div id="left-background"></div>
            <div id="right-background"></div>
            <?php
            // На главной странице выведем блок с информацией о сайте
            if ($this->context->id == 'site' && $this->context->action->id == 'index') {
                echo $this->renderFile('@app/views/parts/about-site.php');
            }
            ?>
            <!-- Крусель товаров -->
            <?php echo GoodCarousel::widget(); ?>

            <div id="content">
                <?php
                 if (!($this->context->id == 'site' && $this->context->action->id == 'index')) {
                    echo Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]);
                }
                ?>
                <?= $content ?>
                <div class="clear"></div>
            </div>
        </div>
        <div id="footer-buffer"></div>
    </div>
    <footer>
        <div id="dark-bg"></div>
        <div id="footer-content">
            <div id="mini-logo" class="footer-col">
                <h6>Tailor <span>place</span></h6>
            </div><div class="footer-col description">
                <span>
                    Свободная торговая площадка в области портняжного дела
                </span><br>
                <span>
                    <?= date('Y') ?> г.
                </span>
            </div><div class="footer-col menu">
                <ul>
                    <li><a href="/studio/" class="white">Ателье и магазины</a></li>
                    <li><a href="/goods/" class="white">Товары</a></li>
                </ul>
            </div>
            <div class="contacts">
<!--                <div class="social">
                        <i class="icon vk"></i>
                    </div>-->
                <div class="feedback">
                    <a href="/feedback/" class="white">Обратная связь</a>
                </div>
            </div>
            <a href="/help/" id="footer-help" class="icon-circle icon-circle-text icon-help"></a>
        </div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>