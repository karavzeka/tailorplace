<?php
/**
 * Страница обратной связи
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $Form
 * @var app\models\FeedbackForm $FeedbackForm
 */

use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use app\assets\AppAsset;

$this->registerCssFile('@web/css/feedback.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Обратная связь';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Обратная связь с владельцем сайта'
]);

$successMessage = Yii::$app->session->getFlash('feedbackSuccess');

$fieldTemplate = '<div class="left-col">{label}</div><div class="center-col">{input}<div class="help-block"></div></div>';
?>
<main>
    <div class="panel big-panel">
        <?php
        $Form = ActiveForm::begin([
            'fieldConfig' => [
                'template' => $fieldTemplate
            ]
        ]);
        ?>
        <div class="panel-header">
            <span class="icon-circle icon-circle-text icon-question"></span><h2>Обратная связь</h2>
            <div class="icon-line emails">
                <i class="icon message"></i><span>support@tailor-place.com</span>
            </div>
        </div>
        <div class="panel-body">
            <?php
            if (!empty($successMessage)) {
                echo '<p class="alert success">' . $successMessage . '</p>';
            }
            ?>
            <ul>
                <li>
                    <?= $Form->errorSummary($FeedbackForm, [
                        'header' => ''
                    ])
                    ?>
                </li>
                <li>
                    <?= $Form->field($FeedbackForm, 'email') ?>
                </li>
                <li>
                    <?= $Form->field($FeedbackForm, 'message')->textarea() ?>
                </li>
                <li class="captcha-line">
                    <?= $Form->field($FeedbackForm, 'captcha', [
                        'template' => '<div class="left-col">{label}</div><div class="center-col">{input}</div>'
                    ])->widget(Captcha::className(), [
                        'captchaAction' => '/captcha',
                        'template' => '{input}{image}'
                    ])
                    ?>
                    <div class="right-col"><span>←</span><span>Введите код<br>с картинки</span></div>
                </li>
            </ul>
        </div>
        <div class="panel-footer">
            <div class="button yellow" id="feedback-action">
                <div class="low-layer"></div>
                <button type="submit">Отправить запрос</button>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</main>