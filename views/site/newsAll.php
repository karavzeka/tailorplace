<?php
/**
 * Страница всех новостей.
 * @var yii\web\View $this
 * @var string $news
 * @var int $countOfNews
 * @var int $newsInBatch
 */

use app\assets\AppAsset;

$this->title = 'Tailor place - Новости';

$this->registerCssFile('@web/css/newsAll.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/newsAll.js', [
    'depends' => [AppAsset::className()]
]);

?>
<!-- TODO неплохо было бы сделать фильтр по времени -->
<main>
    <h2>Новости</h2>
    <div id="news-wrapper"><?= $news ?></div> <!-- Не должно быть пробелов -->
    <?php if ($countOfNews > $newsInBatch) : ?>
    <div id="show-more">
        <div data-offset="1" data-max-news="<?= $countOfNews ?>" id="show-more-button">
            <span class="plus-button"></span>
            <span>Показать еще</span>
        </div>
    </div>
    <?php endif ?>
</main>