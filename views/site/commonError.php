<?php
/**
 * Страница обработки любых ошибок
 * @var yii\web\View $this
 * @var int $code
 */

use app\assets\AppAsset;

$this->registerCssFile('@web/css/errors.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Произошла ошибка';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Произошла ошибка'
]);
?>
<main>
    <div class="error-block">
        <h2>Ошибка</h2>
        <div class="code"><?= $code ?></div>
        <div class="error-description">
            <p>Непредвиденная ошибка</p>
        </div>
    </div>
    <p class="description">
        Если ошибка будет повторяться и мешать вам,
        воспользуйтесь формой обратной связи и напишите в службу поддержки.
    </p>
</main>