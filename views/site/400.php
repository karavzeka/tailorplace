<?php
/**
 * Страница 400 ошибки
 * @var yii\web\View $this
 */

use app\assets\AppAsset;

$this->registerCssFile('@web/css/errors.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Ошибочный запрос';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Ошибочный запрос'
]);
?>
<main>
    <div class="error-block">
        <h2>Ошибка</h2>
        <div class="code">400</div>
        <div class="error-description">
            <p>Некорректный запрос</p>
        </div>
    </div>
    <p class="description">Некорректный запрос. Возможно переданы неверные параметры.</p>
</main>