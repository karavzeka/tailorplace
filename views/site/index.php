<?php
/**
 * @var $this yii\web\View
 * @var app\modules\studio\models\Studio[] $PromotedStudios
 * @var app\modules\users\models\Photo[] $StudioPhotos
 * @var array $studioData
 * @var string $randomGoodsList Блок html содержащий случайные товары
 * @var string $lastGoodsList Блок html содержащий последние товары
 * @var app\modules\studio\models\Studio[] $StudiosOfPopularGoods
 */

use app\assets\AppAsset;
use app\widgets\NewsWidget;
use app\modules\users\models\Photo AS UserPhoto;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Сайт Tailor place помогает пользователям найти себе индивидуального портного и подобрать для себя интересную неповторимую одежду. В то же время портные могут создать свое виртуальное ателье и находить новых клиентов.'
]);
$this->registerCssFile('@web/css/index.css', [
    'depends' => [AppAsset::className()]
]);
$this->registerJsFile('@web/js/index.js', [
    'depends' => [AppAsset::className()]
]);
?>
<main class="left float">
    <section id="studio-carousel">
        <h3>Ателье и магазины</h3>
        <h4 class="flag-link">
            <a href="/studio/" class="white">Полный список</a>
        </h4>
        <div id="carousel-help-block">
            <span id="disable-moving-title">[Движение приостановлено]</span>
        </div>
        <?php
        $studioWrapperParams = ['id' => 'studio-carousel-wrapper'];
        if (count($PromotedStudios) > 4) {
            $studioWrapperParams['style'] = 'height: 511px;';
        }
        echo Html::beginTag('div', $studioWrapperParams);
            $counter = 0;
            foreach ($PromotedStudios as $Studio) {
                $counter++;
                echo '<div class="studio-block" data-index="' . $counter . '">';
                    echo '<div class="studio-info">';
                        echo '<div class="photo">';
                            $imgHtml = Html::img(isset($StudioPhotos[$Studio->user_id]) ? $StudioPhotos[$Studio->user_id]->getSrc('_middle') : UserPhoto::getNoPhotoSrc('_middle'), [
                                'width' => 98,
                                'height' => 98,
                                'alt' => $Studio->name,
                            ]);
                            echo Html::a($imgHtml, Url::toRoute('/studio/' . $Studio->id));
                            echo '<div class="icon-circle ' . $Studio->type . '"><i></i></div>';
                        echo '</div>'; // <div class="photo">
                        echo '<div class="info">';
                            echo '<div>';
                                echo Html::a(Html::encode($Studio->name), Url::toRoute('/studio/' . $Studio->id), ['class' => 'big-red']);
                                echo Html::tag('p', Html::encode($Studio->slogan));
                                if (!empty($studioData[$Studio->id]['categories'])) {
                                    echo '<ul class="category">';
                                        foreach ($studioData[$Studio->id]['categories'] as $category) {
                                            echo '<li>';
                                                echo '<span class="lighter">' . $category . '</span>';
                                            echo '</li>';
                                        }
                                    echo '</ul>'; // <ul class="category">
                                }
                            echo '</div>';
                        echo '</div>'; // <div class="info">
                    echo '</div>'; // <div class="studio-info">
                    if (isset($studioData[$Studio->id]['goods']) && is_array($studioData[$Studio->id]['goods']) && count($studioData[$Studio->id]['goods']) > 0) {
                        echo '<div class="some-goods">';
                            foreach ($studioData[$Studio->id]['goods'] as $good) {
                                $goodPhotoImg = Html::img($good['photo_src'], [
                                    'width' => 48,
                                    'height' => 48,
                                    'alt' => $good['name'],
                                    'title' => $good['name'],
                                    'data-toggle' => 'tooltip',
                                ]);
                                echo Html::a($goodPhotoImg, Url::toRoute('/goods/' . $good['id']));
                            }
                        echo '</div>'; // <div class="some-goods">
                    }
                echo '</div>'; // <div class="studio-block">
            }
        ?>
        </div> <!-- <div id="studio-carousel-wrapper"> -->
        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->identity->hasStudio()): ?>
        <div class="how-be-this">
            <span class="darker"><span>↑</span> <a href="/cabinet/services/studio-promotion/">Как сюда попасть?</a></span>
        </div>
        <?php endif ?>
    </section>
    <div class="dashed scissor-right"></div>
    <section id="popular-goods">
        <h3>Случайные товары</h3>
<!--        <h3>Популярные товары</h3>-->
        <div class="flag-link">
            <a href="/goods/" class="white">Полный список</a>
        </div>
        <div class="good-tile">
            <?= $randomGoodsList ?>
        </div>
    </section>
    <div class="dashed scissor-right"></div>
    <section id="new-goods">
        <h3>Новинки</h3>
        <div class="flag-link">
            <a href="/goods/" class="white">Больше новинок!</a>
        </div>
        <div class="good-tile">
            <?= $lastGoodsList ?>
        </div>
    </section>
</main>
<aside class="float right">
    <?= NewsWidget::widget() ?>
    <div id="social-block">
        <?= \app\widgets\SocialButtons::widget() ?>
    </div>
</aside>

