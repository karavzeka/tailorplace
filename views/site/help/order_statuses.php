<p>
    После оформления заказа, его состояние определяется статусом. Сразу после оформления заказа,
    пользователь, сделавший заказ, еще может отменить заказ, а исполнитель может отклонить заказ. В таком
    случае заказ отменяется. Но если исполнитель перевел заказ в иное состояние (т.е. принял заказ),
    то для своего логического завершения заказ должен пройти через последующие состояния, пока заказчик
    не выставит ему статус "Завершен".
</p>
<p>
    В зависимости от текущего статуса заказа, обычно перевести его в следующее состояние может либо
    заказчик, либо исполнитель.
</p>
<p>
    Пример изменения статусов заказа:<br>
    <i>В ожидании → Принят, ожидает оплаты → В работе → Комплектуется → Отправлен → Завершен</i>
</p>
<p>
    В статус "Завершен" всегда переводит заказчик после получения товара на руки.
</p>
<p>
    После завершения заказа обе стороны должны оценить действия друг друга. Исполнитель оценивает
    насколько легко было работать с заказчиком, исправно ли он заплатил. Заказчик оценивает, насколько
    качественно и быстро исполнитель выполнил свои обязанности.
</p>
<p>
    Оценки в итоге влияют на репутацию продавца и покупателя. Подробнее о репутации читать в разделе
    <a href="<?= \yii\helpers\Url::toRoute('/help/reputation/') ?>">репутация</a>.
</p>