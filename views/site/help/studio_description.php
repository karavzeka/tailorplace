<p>
    Виртуальное ателье и виртуальный магазин служат для предоставления их владельцами своих услуг.
    Ателье и магазин различаются только семантически, т.е. типом предлагаемых услуг: ателье предлагает
    услуги по созданию различного вида одежды, магазины предлагают товары, необходимые портным для создания
    этой одежды. В остальном они схожи.
</p>
<p>
    Создать ателье/магазин может любой пользователь. Однако имеется строгая рекомендация не создавать
    ателье/магазин, если вы не собираетесь предоставлять соответствующие услуги.
</p>
<p>
    Пользователь, создавший свое ателье/магазин, может размещать в нем товары, которые будут доступны для
    заказа всем пользователям сайта. Запрещено размещать товары, не соответствующие тематике сайта.
</p>