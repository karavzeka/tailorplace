<p>
    Ниже представлен текст пользовательского соглашения.
</p>
<div class="agreement-text" style="margin-left: 15px; color: #777777">
    <style scoped>
        .agreement-text ul li {
            list-style: square;
            margin-left: 25px;
        }
    </style>
    <?= $this->renderFile('@app/views/parts/registration_agreement.php') ?>
</div>