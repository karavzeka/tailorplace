<p>
    Ниже представлен текст дополнительного пользовательского соглашения, подтверждение которого необходимо для
    создания ателье/магазина.
</p>
<div class="agreement-text" style="margin-left: 15px; color: #777777">
    <style scoped>
        .agreement-text ul li {
            font-size: 14px;
            list-style: circle;
            margin-left: 15px;
        }
    </style>
    <?= $this->renderFile('@app/views/parts/registration_studio_agreement.php') ?>
</div>