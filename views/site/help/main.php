<?php
/**
 * Страница помощи
 * @var yii\web\View $this
 * @var array $helpPagesList
 */

use app\assets\AppAsset;

$this->registerCssFile('@web/css/help.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Помощь';
?>
<main class="left float help-body">
    <h2>Помощь</h2>
    <ul>
        <?php
        $baseUrl = '/help/';
        foreach ($helpPagesList as $item) {
            echo '<li class="help-item">';
            if (isset($item['helpPage'])) {
                echo '<a href="' . $baseUrl . $item['helpPage'] . '/">' . $item['title'] . '</a>';
            } else {
                echo '<span>' . $item['title'] . '</span>';
            }

            if (isset($item['subMenu']) && is_array($item['subMenu']) && count($item['subMenu']) > 0) {
                echo '<ul>';
                foreach ($item['subMenu'] as $subItem) {
                    echo '<li>';
                    echo '<a href="' . $baseUrl . $subItem['helpPage'] . '/">' . $subItem['title'] . '</a>';
                    echo '</li>';
                }
                echo '</ul>';
            }
            echo '</li>';
        }

        ?>
    </ul>
</main>
<aside class="float right">
    <?= \app\widgets\NewsWidget::widget() ?>
</aside>