<?php
/**
 * Страница конкретного раздела помощи
 * @var yii\web\View $this
 * @var string $content
 * @var string $title
 */

use app\assets\AppAsset;

$this->registerCssFile('@web/css/help.css', [
    'depends' => [AppAsset::className()]
]);

$this->title = 'Помощь - ' . $title;
?>
<main class="left float help-body">
    <h2><?= $title ?></h2>
    <?= $content ?>
</main>
<aside class="float right">
    <?= \app\widgets\NewsWidget::widget() ?>
</aside>