<?php
/**
 * Страница конкретной новости.
 * @var yii\web\View $this
 * @var app\models\News $NewsItem
 */

use app\assets\AppAsset;
use yii\helpers\Html;

$this->title = 'Новость. ' . $NewsItem->title;

$this->registerCssFile('@web/css/newsItem.css', [
    'depends' => [AppAsset::className()]
]);

?>

<main class="left float">
    <?php
    echo '<h2>' . Html::encode($NewsItem->title) . '</h2>';
    echo '<time class="news-date" datetime="' . date('Y-m-d', strtotime($NewsItem->create_time)) . '">' . date('d.m.Y', strtotime($NewsItem->create_time)) . '</time>';
    echo '<div id="news-wrapper">';
        if ($NewsItem->has_img) {
            echo '<img src="/photos/news/' . $NewsItem->id . '/crop.jpg">';
        }
        echo $NewsItem->text;
    echo '</div>';
    ?>
</main>
<aside class="float right">
    <?= \app\widgets\NewsWidget::widget() ?>
</aside>