<?php
/**
 * Контроллер осуществляющий сохранение комментариев
 */

namespace app\controllers;

use app\models\Comment;
use Yii;
use \yii\db\ActiveRecord;

class CommentController extends CommonController
{
    /**
     * Возможные типы объектов, доступные для комментирования
     *
     * @var ActiveRecord[]
     */
    private $possibleTypes = [
        Comment::TYPE_GOOD => '\app\modules\good\models\Good',
        Comment::TYPE_STUDIO => '\app\modules\studio\models\Studio',
    ];

    public function actionAdd()
    {
        if (Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error',
                'error' => 'Ошибка. Не удалось сохранить комментарий.'
            ];

            if (\Yii::$app->user->isGuest) {
                Yii::$app->response->setStatusCode(403);
                $this->sendJsonAnswer($response);
                return;
            }

            $objectId = Yii::$app->request->post('objectId');
            $objectType = Yii::$app->request->post('objectType');
            $parentId = Yii::$app->request->post('parentId');
            $text = Yii::$app->request->post('text');
            $text = trim(strip_tags($text, '<br>'));

            // Проверка входных значений
            if (empty($objectId) || !isset($this->possibleTypes[$objectType]) || empty($text)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $User = Yii::$app->user->identity;
            if (empty($User)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            // Проверка, что комментируемый объект существует
            $CommentingObject = $this->getCommentingObject($objectId, $objectType);
            if (!($CommentingObject instanceof ActiveRecord)) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            if ($parentId > 0) {
                // Проверка, что комментарий существует
                if (!(Comment::findOne($parentId) instanceof Comment)) {
                    Yii::$app->response->setStatusCode(400);
                    $this->sendJsonAnswer($response);
                    return;
                }
            }

            // Сохранение комментария
            $Comment = new Comment();
            $Comment->parent_id = $parentId;
            $Comment->object_id = $objectId;
            $Comment->object_type = $objectType;
            $Comment->user_id = $User->getId();
            $Comment->text = $text;

            if ($Comment->save()) {
                $response['status'] = 'ok';
            }

            $this->sendJsonAnswer($response);
            return;
        } else {
            // Если пользователь просто зашел на страницу, то показываем, что такой нет
            $SiteController = Yii::$app->createController('site');
            return $SiteController[0]->runAction('404');
        }
    }

    /**
     * Возвращает ActiveRecord модель комментируемого объекта
     *
     * @param int $objectId
     * @param string $objectType
     * @return null|ActiveRecord
     */
    private function getCommentingObject($objectId, $objectType) {
        return call_user_func([$this->possibleTypes[$objectType], 'findOne'], (int)$objectId);
    }
}