<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Country;
use app\models\City;
use app\models\News;
use app\models\Feedback;
use app\modules\good\models\Good;
use app\modules\good\models\Photo;
use app\modules\studio\models\Studio;
use app\modules\users\models\User;
use app\modules\users\models\Photo AS UserPhoto;
use app\models\FeedbackForm;

class SiteController extends CommonController
{
    public $layout = 'main';

    /**
     * @var int По сколько новостей подгружать на странице новостей
     */
    public $newsInBatch = 15;

    public function actionIndex()
    {
        // Получение списка продвинутых студий
        // сначала берутся продвинутые студии, но если их не набирается, то добираются последними студиями
        $PromotedStudios = Studio::find()
            ->select('
                `studio`.`id` AS `id`,
                `studio`.`user_id` AS `user_id`,
                `studio`.`name` AS `name`,
                `studio`.`slogan` AS `slogan`,
                `studio`.`type` AS `type`')
            ->leftJoin('studio_promotion', '`studio`.`id` = `studio_promotion`.`studio_id`')
            ->orderBy([
                '`studio_promotion`.`last_promotion`' => SORT_DESC,
                '`studio`.`id`' => SORT_DESC
            ])
            ->limit(10)
            ->all();

        $userIds = ArrayHelper::getColumn($PromotedStudios, 'user_id');
        $studioIds = ArrayHelper::getColumn($PromotedStudios, 'id');
        // фотографии студий
        $StudioPhotos = UserPhoto::find()->where([
            'user_id' => $userIds
        ])-> all();
        $StudioPhotos = ArrayHelper::index($StudioPhotos, 'user_id');

        // товары студий с фотографиями и категориями
        $studioData = Yii::$app->cache->get('promotedStudios');
        if (!$studioData) {
            $studioData = [];
            $goodsWithPhoto = Good::find()->select('
                `goods`.`id` AS `good_id`,
                `goods`.`studio_id` AS `studio_id`,
                `goods`.`name` AS `name`,
                `good_photos`.`file_name` AS `file_name`,
            ')
                ->innerJoin('good_photos', '`goods`.`id` = `good_photos`.`good_id`')
                ->where([
                    '`goods`.`studio_id`' => $studioIds,
                    '`good_photos`.`main`' => 1
                ])
                ->asArray()
                ->all();
            if (count($goodsWithPhoto) > 0) {
                $goodIdsToSelectCategories = ArrayHelper::getColumn($goodsWithPhoto, 'good_id');

                $categories = Yii::$app->db->createCommand("
                SELECT
                    `gc`.`category_id` AS `id`,
                    `gc`.`good_id` AS `good_id`,
                    `cl`.`name_ru` AS `name_ru`
                FROM
                    `good_category` AS `gc`
                INNER JOIN
                    `category_list` AS `cl`
                ON
                    `gc`.`category_id` = `cl`.`id`
                WHERE
                    `gc`.`good_id` IN (" . implode(', ', $goodIdsToSelectCategories) . ") AND
                    `cl`.`id` <> 41
            ")->queryAll();


                foreach ($goodsWithPhoto as $goodData) {
                    // можно отобразить не больше 8-ми товаров возле студии
                    if (count($studioData[$goodData['studio_id']]['goods']) < 8) {
                        $studioData[$goodData['studio_id']]['goods'][] = [
                            'id' => $goodData['good_id'],
                            'name' => $goodData['name'],
                            'studio_id' => $goodData['studio_id'],
                            'photo_src' => Photo::getSrcByGoodIdAndPhotoNumber($goodData['good_id'], $goodData['file_name'], '_min')
                        ];
                    }
                    if (!isset($studioData[$goodData['studio_id']]['categories'])) {
                        $studioData[$goodData['studio_id']]['categories'] = [];
                    }
                    // если по студии меньше 3-х категорий, то добавим студии категорию
                    if (count($studioData[$goodData['studio_id']]['categories']) < 3) {
                        foreach ($categories as $category) {
                            if (
                                $category['good_id'] == $goodData['good_id'] &&
                                !isset($studioData[$goodData['studio_id']]['categories'][$category['id']]) &&
                                count($studioData[$goodData['studio_id']]['categories']) < 3
                            ) {
                                $studioData[$goodData['studio_id']]['categories'][$category['id']] = $category['name_ru'];
                            }
                        }
                    }
                }
                Yii::$app->cache->set('promotedStudios', $studioData, 3600);    // Результат кэшируется на час
            }
        }

        // Случайные товары
        // TODO переделать потом на популярные товары
        $GoodControllers = Yii::$app->createController('good/default');
        $GoodController = $GoodControllers[0];

        $randomGoodsData = Yii::$app->cache->get('randomGoodsData');
        if (!$randomGoodsData) {
            $goodIds = Good::find()
                ->select('
                    `goods`.`id` AS `id`
                ')
                ->distinct(true)
                ->innerJoin('good_photos gp', '`goods`.`id` = `gp`.`good_id`')
                ->where('`gp`.`main` = 1')
                ->column();

            // уберем последние 3, т.к. они и так будут выведены
            if (count($goodIds) > 9) {
                array_pop($goodIds);
                array_pop($goodIds);
                array_pop($goodIds);
            }
            if (count($goodIds) > 0) {
                $randomIdKeys = array_rand($goodIds, count($goodIds) < 6 ? count($goodIds) : 6);
            } else {
                $randomIdKeys = [];
            }

            $randomIds = [];
            foreach ($randomIdKeys as $idKey) {
                $randomIds[] = $goodIds[$idKey];
            }

            // Получение случайных товаров
            $wherePart = count($randomIds) > 0 ? '`goods`.`id` IN (' . implode(', ', $randomIds) . ')' : '';
            $randomGoodsData = $GoodController->getGoods($wherePart, null, null, false);

            Yii::$app->cache->set('randomGoodsData', $randomGoodsData, 600);    // Кеширование случайных товаров на 10 мин.
        }

        if (count($randomGoodsData['goods']) > 0) {
            $randomGoodsList = $this->renderPartial('@app/views/parts/goodsList', [
                'goodsData' => $randomGoodsData,
                'fillByEmptyGoods' => true
            ]);
        } else {
            $randomGoodsList = '';
        }

        // Выборка нескольких последних товаров
        $GoodController->setGoodsLimit(3);
        $lastGoodsData = $GoodController->getGoods(null, null, ['good_id' => SORT_DESC], false);
        if (count($lastGoodsData['goods']) > 0) {
            $lastGoodsList = $this->renderPartial('@app/views/parts/goodsList', [
                'goodsData' => $lastGoodsData,
                'fillByEmptyGoods' => true
            ]);
        } else {
            $lastGoodsList = '';
        }

        return $this->render('index', [
            'PromotedStudios' => $PromotedStudios,
            'StudioPhotos' => $StudioPhotos,
            'studioData' => $studioData,
            'randomGoodsList' => $randomGoodsList,
            'lastGoodsList' => $lastGoodsList,
        ]);
    }

    /**
     * Страница со списком новостей
     *
     * @return string
     */
    public function actionNewsAll()
    {
        $countOfNews = News::find()->where(['show' => 1])->count();
        $newsBatch = $this->getNewsBatch();

        return $this->render('newsAll', [
            'news' => $newsBatch,
            'countOfNews' => $countOfNews,
            'newsInBatch' => $this->newsInBatch
        ]);
    }

    /**
     * Подгрузка новостей по Ajax
     */
    public function actionGetNewsAjax()
    {
        if (Yii::$app->request->isAjax) {
            $response = [
                'status' => 'error'
            ];

            $offset = (int)Yii::$app->request->post('offset');
            if ($offset == 0) {
                Yii::$app->response->setStatusCode(400);
                $this->sendJsonAnswer($response);
                return;
            }

            $newsBatch = $this->getNewsBatch($offset);

            $response = [
                'status' => 'ok',
                'html' => $newsBatch
            ];

            $this->sendJsonAnswer($response);
        }
    }

    /**
     * Формирует html с набором новостей
     *
     * @param int $offsetPart
     * @return string
     */
    private function getNewsBatch($offsetPart = 0) {
        $this->addBreadcrumbsItem('Новости');

        $News = News::find()
            ->where(['show' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->offset($offsetPart * $this->newsInBatch)
            ->limit($this->newsInBatch)
            ->all();

        $html = '';
        foreach ($News as $NewsItem) {
            $createTime = strtotime($NewsItem->create_time);
            $newsImg = $NewsItem->has_img ? '<img src="/photos/news/' . $NewsItem->id . '/crop_min.jpg">' : '';
            // для верстки важно, чтобы не было пробелов между тэгами <section>
            $html .= '<section class="news-item">
                    <h3><a href="/news/' . $NewsItem->id . '/">' . $NewsItem->title . '</a></h3>
                    <time class="news-date" datetime="' . date('Y-m-d', $createTime) . '">' . date('d.m.Y', $createTime) . '</time>
                    <p>
                    ' . $newsImg . '
                    ' . (mb_strlen($NewsItem->text) > 120 ? (mb_substr($NewsItem->text, 0, 120) . '…') : $NewsItem->text) . '
                    </p>
                </section>';
        }

        return $html;
    }

    /**
     * Страница конкретной новости
     *
     * @return mixed|string
     * @throws \yii\base\InvalidRouteException
     */
    public function actionNewsItem()
    {
        $newsId = Yii::$app->request->get('newsId');
        $NewsItem = News::findOne($newsId);
        if (!($NewsItem) instanceof News || ($NewsItem->show == 0 && !Yii::$app->user->can('viewHideNews'))) {
            return $this->runAction('404');
        }

        $this->addBreadcrumbsItem(['label' => 'Новости', 'url' => '/news/']);
        $this->addBreadcrumbsItem($NewsItem->title);

        return $this->render('newsItem', [
            'NewsItem' => $NewsItem
        ]);
    }

    /**
     * Страница с формой обратной связи
     *
     * @return string
     */
    public function actionFeedback()
    {
        $this->addBreadcrumbsItem('Обратная связь');

        $FeedbackForm = new FeedbackForm();
        $User = Yii::$app->user->identity;
        if ($User instanceof User) {
            $FeedbackForm->email = $User->email;
        }

        if (Yii::$app->request->isPost && $FeedbackForm->load(Yii::$app->request->post()) && $FeedbackForm->validate()) {
            $Feedback = new Feedback();
            $Feedback->email = $FeedbackForm->email;
            $Feedback->message = $FeedbackForm->message;
            if ($User instanceof User) {
                $Feedback->from_authorized_user = $User->id;
            }
            $UserByEmail = User::find()->where(['email' => $FeedbackForm->email])->one();
            if ($UserByEmail instanceof User) {
                $Feedback->email_registered_to_user = $UserByEmail->id;
            }
            $Feedback->ip = Yii::$app->numberHelper->ipToUnsignedNumber(Yii::$app->request->getUserIP());
            $Feedback->save();

            Yii::$app->mailer->compose('@app/mail/toUs/feedbackNotification', ['FeedbackObject' => $Feedback])
                ->setTo('support@tailor-place.com')
                ->setSubject('Кто-то воспользовался формой обратной связи')
                ->send();

            Yii::$app->session->setFlash('feedbackSuccess', 'Ваше сообщение успешно отправлено. В случае необходимости, администрация сайта свяжется с вами.');
            $FeedbackForm->message = '';
            $FeedbackForm->captcha = '';
        }

        return $this->render('feedback', [
            'FeedbackForm' => $FeedbackForm
        ]);
    }

    public function actionHelp()
    {
        $helpPagesList = [
            [
                'helpPage' => 'about_site',
                'title' => 'О сайте'
            ],
            [
                'title' => 'Регистрация',
                'subMenu' => [
                    [
                        'helpPage' => 'registration',
                        'title' => 'Процесс регистрации'
                    ],
                    [
                        'helpPage' => 'registration_agreement',
                        'title' => 'Пользовательское соглашение'
                    ],
                ]
            ],
            [
                'title' => 'Пароль',
                'subMenu' => [
                    [
                        'helpPage' => 'password_recovery',
                        'title' => 'Восстановление пароля'
                    ],
                    [
                        'helpPage' => 'password_change',
                        'title' => 'Изменение пароля'
                    ],
                ]
            ],
            [
                'title' => 'Ателье/магазин',
                'subMenu' => [
                    [
                        'helpPage' => 'studio_description',
                        'title' => 'Общие положения'
                    ],
                    [
                        'helpPage' => 'registration_studio_agreement',
                        'title' => 'Дополнительное пользовательское соглашение'
                    ],
                    [
                        'helpPage' => 'studio_create',
                        'title' => 'Как создать ателье или магазин'
                    ],
                    [
                        'helpPage' => 'studio_redact',
                        'title' => 'Редактрование ателье/магазина'
                    ],
                    [
                        'helpPage' => 'goods_limit',
                        'title' => 'Лимит товаров'
                    ],
                ]
            ],
            [
                'helpPage' => 'photo_recommendation',
                'title' => 'Рекомендации по добавлению фотографий'
            ],
            [
                'title' => 'Оформление и исполнение заказов',
                'subMenu' => [
                    [
                        'helpPage' => 'create_order',
                        'title' => 'Оформление заказа'
                    ],
                    [
                        'helpPage' => 'order_statuses',
                        'title' => 'Статусы и оценка заказа'
                    ],
                ]
            ],
            [
                'helpPage' => 'reputation',
                'title' => 'Репутация'
            ],
        ];

        $helpPage = Yii::$app->request->get('helpPage');
        if (!empty($helpPage)) {
            // Был запрос конкретного раздела помощи
            // Проверяем, существует ли запрошенный раздел помощи
            $helpPageExists = false;
            $helpPageTitle = '';
            foreach ($helpPagesList as $item) {
                if (isset($item['helpPage']) && $item['helpPage'] == $helpPage) {
                    $helpPageExists = true;
                    $helpPageTitle = $item['title'];
                    break;
                }
                if (isset($item['subMenu']) && is_array($item['subMenu'])) {
                    foreach ($item['subMenu'] as $subItem) {
                        if (isset($subItem['helpPage']) && $subItem['helpPage'] == $helpPage) {
                            $helpPageExists = true;
                            $helpPageTitle = $subItem['title'];
                            break(2);
                        }
                    }
                }
            }

            $contentFile = $this->viewPath . '/help/' . $helpPage . '.php';
            if ($helpPageExists === false || !file_exists($contentFile)) {
                return $this->runAction('404');
            }

            // Раздел найден
            $this->addBreadcrumbsItem(['url' => '/help/', 'label' => 'Помощь']);
            $this->addBreadcrumbsItem($helpPageTitle);

            $content = $this->renderPartial('help/' . $helpPage);

            return $this->render('help/item', [
                'title' => $helpPageTitle,
                'content' => $content
            ]);
        } else {
            // Общий раздел помощи
            $this->addBreadcrumbsItem('Помощь');

            return $this->render('help/main', [
                'helpPagesList' => $helpPagesList
            ]);
        }
    }

    public function actionYouAreBanned()
    {
        return $this->render('//other/simpleMessage', [
            'message' => '<p>Вы были забанены. За подробностями обратитесь в службу поддержки.</p>'
        ]);
    }

    public function actionError()
    {
        $Exception = Yii::$app->getErrorHandler()->exception;
        if ($Exception === null) {
            $Exception = new \yii\web\HttpException(404, Yii::t('yii', 'Страница не найдена'));
        }

        if ($Exception instanceof \yii\web\HttpException) {
            $code = $Exception->statusCode;
        } else {
            $code = $Exception->getCode();
        }

        switch ($code) {
            case 400:
                $answer = $this->render('400');
                break;
            case 403:
                $answer = $this->render('403');
                break;
            case 404:
                $answer = $this->render('404');
                break;
            default:
                Yii::$app->commonTools->sendErrorReport($code, $Exception->getMessage(), debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
                $answer = $this->render('commonError', [
                    'code' => $code,
                ]);
        }
        return $answer;
    }

    public function action400()
    {
        Yii::$app->response->setStatusCode(400);
        return $this->render('400');
    }

    public function action403()
    {
        Yii::$app->response->setStatusCode(403);
        return $this->render('403', [
            'isGuest' => Yii::$app->user->isGuest,
        ]);
    }

    public function action404()
    {
        Yii::$app->response->setStatusCode(404);
        return $this->render('404');
    }

    /**
     * Возвращает список стран в Json формате
     * AJAX метод
     * @return null
     */
    public function actionGetCountryList()
    {
        $input = trim(strip_tags(Yii::$app->request->get('term')));
        $result = Country::getCountryList($input);

        echo Json::encode($result);
    }

    /**
     * Возвращает список городов в Json формате
     * AJAX метод
     * @return null
     */
    public function actionGetCityList()
    {
        $id = (int)Yii::$app->request->post('countryId');
        if (!$id) {
            $id = (int)Yii::$app->request->get('countryId');
        }
        if ($id) {
            $input = trim(strip_tags(Yii::$app->request->get('term')));
            $result = City::getCityList($id, $input);
        } else {
            $result = [];
        }

        echo Json::encode($result);
    }
}
