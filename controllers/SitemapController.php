<?php
/**
 * Контроллер генерации файлов sitemap
 */

namespace app\controllers;

use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class SitemapController extends Controller
{
    /**
     * @var \DOMDocument
     */
    private $Dom;

    /**
     * Генерирует главный индексный sitemap по адресу /sitemap.xml
     */
    public function actionIndex() {
        $this->Dom = new \DOMDocument('1.0', 'UTF-8');
        $Root = $this->Dom->createElement('sitemapindex');
        $Root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->Dom->appendChild($Root);

        $Root->appendChild($this->createSitemapSection(Url::toRoute('/sitemap/common-sitemap', true)));
        $Root->appendChild($this->createSitemapSection(Url::toRoute('/sitemap/studio-sitemap', true)));
        $Root->appendChild($this->createSitemapSection(Url::toRoute('/sitemap/good-sitemap', true)));

        \Yii::$app->response->format = Response::FORMAT_XML;
        echo $this->Dom->saveXML();
    }

    /**
     * Sitemap в основном статических страниц
     */
    public function actionCommonSitemap()
    {
        $this->Dom = new \DOMDocument('1.0', 'UTF-8');
        $Root = $this->Dom->createElement('urlset');
        $Root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->Dom->appendChild($Root);

        $Root->appendChild($this->createUrlSection(Url::home(true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/feedback', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/news', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/about_site/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/registration/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/registration_agreement/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/password_recovery/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/password_change/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/studio_description/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/registration_studio_agreement/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/studio_create/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/studio_redact/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/goods_limit/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/photo_recommendation/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/create_order/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/order_statuses/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/help/reputation/', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/goods', true)));
        $Root->appendChild($this->createUrlSection(Url::toRoute('/studio', true)));

        // Пока новостей не много, пусть будут в общем sitemap'е
        $newsIds = \app\models\News::find()
            ->select(['id'])
            ->where(['show' => 1])
            ->asArray()
            ->column();
        foreach ($newsIds as $newsId) {
            $Root->appendChild($this->createUrlSection(Url::toRoute('/news/' . $newsId, true)));
        }

        \Yii::$app->response->format = Response::FORMAT_XML;
        echo $this->Dom->saveXML();
    }

    /**
     * Sitemap студий
     */
    public function actionStudioSitemap()
    {
        $this->Dom = new \DOMDocument('1.0', 'UTF-8');
        $Root = $this->Dom->createElement('urlset');
        $Root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->Dom->appendChild($Root);

        $studios = \app\modules\studio\models\Studio::find()
            ->select([
                'id',
                'create_time',
                'update_time',
            ])
            ->asArray()
            ->all();

        foreach ($studios as $studio) {
            $lastmod = $studio['update_time'] == '0000-00-00 00:00:00' ? $studio['create_time'] : $studio['update_time'];
            $Root->appendChild($this->createUrlSection(Url::toRoute('/studio/' . $studio['id'], true), $lastmod));
        }

        \Yii::$app->response->format = Response::FORMAT_XML;
        echo $this->Dom->saveXML();
    }

    /**
     * Sitemap товаров
     */
    public function actionGoodSitemap()
    {
        $this->Dom = new \DOMDocument('1.0', 'UTF-8');
        $Root = $this->Dom->createElement('urlset');
        $Root->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $this->Dom->appendChild($Root);

        $goods = \app\modules\good\models\Good::find()
            ->select([
                'id',
                'create_time',
                'update_time',
            ])
            ->asArray()
            ->all();

        foreach ($goods as $good) {
            $lastmod = $good['update_time'] == '0000-00-00 00:00:00' ? $good['create_time'] : $good['update_time'];
            $Root->appendChild($this->createUrlSection(Url::toRoute('/goods/' . $good['id'], true), $lastmod));
        }

        \Yii::$app->response->format = Response::FORMAT_XML;
        echo $this->Dom->saveXML();
    }

    /**
     * Создает DOM секцию sitemap, необходимую индексном сайтмапе
     *
     * @param string $location Url на sitemap
     * @return \DOMElement
     */
    private function createSitemapSection($location)
    {
        $SitemapNode = $this->Dom->createElement('sitemap');
        $LocationNode = $this->Dom->createElement('loc', $location);
        $SitemapNode->appendChild($LocationNode);

        return $SitemapNode;
    }

    /**
     * Создает DOM секцию url для сайтмапа
     *
     * @param string $location Url на страницу сайта
     * @param string|null $lastmod Дата моследней модификации страницы
     * @return \DOMElement
     */
    private function createUrlSection($location, $lastmod = null)
    {
        $UrlNode = $this->Dom->createElement('url');
        $LocationNode = $this->Dom->createElement('loc', $location);
        $UrlNode->appendChild($LocationNode);

        if ($lastmod !== null && strtotime($lastmod) !== false) {
            $LastmodNode = $this->Dom->createElement('lastmod', date('Y-m-d', strtotime($lastmod)));
            $UrlNode->appendChild($LastmodNode);
        }

        return $UrlNode;
    }
}