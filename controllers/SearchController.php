<?php
/**
 * Контроллер управляющий поисковыми запросами
 */

namespace app\controllers;

use Yii;

class SearchController extends CommonController
{
    public $layout = 'main';

    public function actionSearch()
    {
        $queryString = Yii::$app->request->get('q');
        $searchType = Yii::$app->request->get('type');


        switch ($searchType) {
            case 'atelier':
                $resultHtml = $this->generateAtelierAndStoreResult($queryString, 'atelier');
                break;
            case 'store':
                $resultHtml = $this->generateAtelierAndStoreResult($queryString, 'store');
                break;
            case 'goods':
                $resultHtml = $this->generateGoodsResult($queryString);
                break;
            default:
                $resultHtml = $this->generateAtelierAndStoreResult($queryString);
        }

        return $resultHtml;
    }

    /**
     * Выполняет поиск и генерирует HTML с результатом поиска студий
     *
     * @param $queryString
     * @param string $studioType
     * @return string
     */
    private function generateAtelierAndStoreResult($queryString, $studioType = '')
    {
        $rawQueryString = $queryString;
        if (mb_strlen($queryString) <= 2) {
            $searchAlert = 'Длина поискового запроса должна быть минимум 3 символа.';
            $studioList = [];
            $showMoreButton = false;
            $countOfStudios = 0;
        } else {
            $queryString = Yii::$app->db->quoteValue($queryString);
            // т.к. quoteValue() обрамляет кавычками, то в LIKE попадут эти кавычни между процентами и запрос сломается
            // поэтому надо убрать эти кавычки.
            $queryString = mb_substr($queryString, 1, mb_strlen($queryString) - 2);
            $wherePart = "
                (
                    `studio`.`name` LIKE '%" . $queryString . "%' OR
                    `studio`.`slogan` LIKE '%" . $queryString . "%'
                )";

            if ($studioType) {
                $wherePart = "`studio`.`type` = '" . $studioType . "' AND " . $wherePart;
            }

            $StidioControllers = Yii::$app->createController('studio/default');
            $StidioController = $StidioControllers[0];
            $studioData = $StidioController->getStudios($wherePart);

            if (count($studioData['studios']) > 0) {
                $studioList = $this->renderPartial('@app/views/parts/studioList', [
                    'studioData' => $studioData
                ]);
            } else {
                $studioList = '';
            }

            $countOfStudios = $studioData['countOfStudios'];
            $showMoreButton = $countOfStudios > $StidioController->getStudiosLimit();

            if ($studioType == 'atelier') {
                $searchAlert = Yii::t('app', '{n, plural, =0{По вашему запросу ни одного ателье не найдено} one{Найдено # ателье} few{Найдено # ателье} many{Найдено # ателье} other{Найдено # ателье}}', ['n' => $countOfStudios]);
            } elseif ($studioType == 'store') {
                $searchAlert = Yii::t('app', '{n, plural, =0{По вашему запросу ни одного магазина не найдено} one{Найден # магазин} few{Найдено # магазина} many{Найдено # магазинов} other{Найдено # магазинов}}', ['n' => $countOfStudios]);
            } else {
                $searchAlert = Yii::t('app', '{n, plural, =0{По вашему запросу ни одного ателье или магазина не найдено} one{Найдено # ателье/магазин} few{Найдено # ателье/магазина} many{Найдено # ателье/магазинов} other{Найдено # ателье/магазинов}}', ['n' => $countOfStudios]);
            }
        }

        if ($studioType == 'atelier') {
            $searchAlert = $pageTitle = 'Результат поиска ателье';
        } elseif ($studioType == 'store') {
            $pageTitle = 'Результат поиска магазинов';
        } else {
            $pageTitle = 'Результат поиска ателье и магазинов';
        }

        $this->addBreadcrumbsItem(['label' => 'Поиск']);
        $this->addBreadcrumbsItem(['label' => $pageTitle]);

        return $this->render('studio', [
            'pageTitle' => $pageTitle,
            'studioList' => $studioList,
            'searchAlert' => $searchAlert,
            'showMoreButton' => $showMoreButton,
            'rawQueryString' => $rawQueryString,
            'countOfStudios' => $countOfStudios,
        ]);
    }


    /**
     * Выполняет поиск и генерирует HTML с результатом поиска товаров
     *
     * @param $queryString
     * @return string
     */
    private function generateGoodsResult($queryString)
    {
        $rawQueryString = $queryString;
        if (mb_strlen($queryString) <= 2) {
            $searchAlert = 'Длина поискового запроса должна быть минимум 3 символа.';
            $goodsList = [];
            $showMoreButton = false;
            $countOfGoods = 0;
        } else {
            $queryString = Yii::$app->db->quoteValue($queryString);
            // т.к. quoteValue() обрамляет кавычками, то в LIKE попадут эти кавычни между процентами и запрос сломается
            // поэтому надо убрать эти кавычки.
            $queryString = mb_substr($queryString, 1, mb_strlen($queryString) - 2);
            $wherePart = "`goods`.`name` LIKE '%" . $queryString . "%'";

            $GoodControllers = Yii::$app->createController('good/default');
            $GoodController = $GoodControllers[0];
            $goodsData = $GoodController->getGoods($wherePart);

            if (count($goodsData['goods']) > 0) {
                $goodsList = $this->renderPartial('@app/views/parts/goodsList', [
                    'goodsData' => $goodsData,
                    'fillByEmptyGoods' => true
                ]);
            } else {
                $goodsList = '';
            }

            $countOfGoods = $goodsData['countOfGoods'];
            $searchAlert = Yii::t('app', '{n, plural, =0{По вашему запросу ни одного товара не найдено} one{Найден # товар} few{Найдено # товара} many{Найдено # товаров} other{Найдено # товаров}}', ['n' => $countOfGoods]);
            $showMoreButton = $countOfGoods > $GoodController->getGoodsLimit();
        }

        $this->addBreadcrumbsItem(['label' => 'Поиск']);
        $this->addBreadcrumbsItem(['label' => 'Результат поиска товаров']);

        return $this->render('good', [
            'goodsList' => $goodsList,
            'searchAlert' => $searchAlert,
            'showMoreButton' => $showMoreButton,
            'countOfGoods' => $countOfGoods,
            'rawQueryString' => $rawQueryString,
        ]);
    }

    /**
     * Генерация студий при нажатии пользователем кнопки "Показать еще"
     *
     * @return string
     */
    public function actionOnlyStudios()
    {
        if (Yii::$app->request->isAjax) {
            $requestFilters = Yii::$app->request->post('filters', null);

            // поисковый запрос
            $queryString = '';
            if (isset($requestFilters['searchQuery'])) {
                $queryString = $requestFilters['searchQuery'];

                if (mb_strlen($queryString) <= 2) {
                    return '';
                }
            } else {
                return '';
            }

            // получение контроллера студий
            $StudioControllers = Yii::$app->createController('studio/default');
            $StudioController = $StudioControllers[0];

            $offset = 0;
            if (isset($requestFilters['part'])) {
                $offset = (int)$requestFilters['part'] * $StudioController->getStudiosLimit();
            }

            $queryString = Yii::$app->db->quoteValue($queryString);
            $queryString = mb_substr($queryString, 1, mb_strlen($queryString) - 2);
            $wherePart = "
                (
                    `studio`.`name` LIKE '%" . $queryString . "%' OR
                    `studio`.`slogan` LIKE '%" . $queryString . "%'
                )";

            $studioType = '';
            if (isset($requestFilters['studioType'])) {
                if ($requestFilters['studioType'] == 'atelier') {
                    $studioType = 'atelier';
                } elseif ($requestFilters['studioType'] == 'store') {
                    $studioType = 'store';
                }
            }

            if ($studioType) {
                $wherePart = "`studio`.`type` = '" . $studioType . "' AND " . $wherePart;
            }

            $studiosData = $StudioController->getStudios($wherePart, $offset, null, false);

            $html = $this->renderPartial('@app/views/parts/studioList', [
                'studioData' => $studiosData
            ]);

            return json_encode([
                'html' => $html,
                'countOfStudios' => $studiosData['countOfStudios'],
                'studiosOnPage' => count($studiosData['studios']) + $offset
            ]);
        } else {
            return '';
        }
    }

    /**
     * Генерация товаров при нажатии пользователем кнопки "Показать еще"
     *
     * @return string
     */
    public function actionOnlyGoods()
    {
        if (Yii::$app->request->isAjax) {
            $requestFilters = Yii::$app->request->post('filters', null);

            // поисковый запрос
            $queryString = '';
            if (isset($requestFilters['searchQuery'])) {
                $queryString = $requestFilters['searchQuery'];

                if (mb_strlen($queryString) <= 2) {
                    return '';
                }
            } else {
                return '';
            }

            // получение контроллера товаров
            $GoodControllers = Yii::$app->createController('good/default');
            $GoodController = $GoodControllers[0];

            $offset = 0;
            if (isset($requestFilters['part'])) {
                $offset = (int)$requestFilters['part'] * $GoodController->getGoodsLimit();
            }

            $queryString = Yii::$app->db->quoteValue($queryString);
            $queryString = mb_substr($queryString, 1, mb_strlen($queryString) - 2);
            $wherePart = "`goods`.`name` LIKE '%" . $queryString . "%'";

            $goodsData = $GoodController->getGoods($wherePart, $offset, null, false);

            $html = $this->renderPartial('@app/views/parts/goodsList', [
                'goodsData' => $goodsData,
                'fillByEmptyGoods' => false
            ]);

            return json_encode([
                'html' => $html,
                'countOfGoods' => $goodsData['countOfGoods'],
                'goodsOnPage' => count($goodsData['goods']) + $offset
            ]);
        } else {
            return '';
        }
    }
}