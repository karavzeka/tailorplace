<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;

class CommonController extends Controller
{
    public $layout = '@root/views/layouts/main';

    /**
     * Массив экшэнов, которые запрещено сохранять для возврата на предыдущую страницу
     * @var array
     */
    protected $disableReturnToActions = [
        'login',
        'captcha',
        'signup',
    ];
    // TODO заполнить $disableReturnToActions

    public function init()
    {
        $this->on(self::EVENT_BEFORE_ACTION, function ($event) {
            // запоминаем страницу пользователя, чтобы потом отредиректить его обратно с помощью  goBack()
            $request = Yii::$app->getRequest();
            // исключаем страницу авторизации или ajax-запросы
            if (!($request->isAjax || in_array($event->action->id, $this->disableReturnToActions) !== false)) {
                Yii::$app->getUser()->setReturnUrl($request->getUrl());
            }

            // Если пользователь забанен, деавторизуем его
            $User = Yii::$app->user->identity;
            if ($User instanceof \app\modules\users\models\User) {
                $controllerActionPair = Yii::$app->urlManager->parseRequest(Yii::$app->request);
                if ($User->additional_status == $User::STATUS_BANNED && !($controllerActionPair[0] == 'site/you-are-banned' || $controllerActionPair[0] == 'users/default/login')) {
                    Yii::$app->user->logout();
                    Yii::$app->getResponse()->redirect(\yii\helpers\Url::to('/you-are-banned/'));
                }
            }
        });
        $this->on(self::EVENT_AFTER_ACTION, function ($event) {
            $request = Yii::$app->getRequest();
            if (!$request->isAjax) {
                Url::remember('', 'commonReferrer');
            }
        });

        // Регистрация события, которое добавит глобальные js переменные на страницу
        Yii::$app->view->on(\yii\web\View::EVENT_BEFORE_RENDER, function() {
            Yii::$app->view->registerJs('var globalVars = ' . Yii::$app->jsGlobalVar->translateToJs(), \yii\web\View::POS_HEAD);
        });

        // Добавление фавиконки
        $this->view->registerLinkTag([
            'type' => 'image/x-icon',
            'rel' => 'shortcut icon',
            'href' => '/favicon.ico'
        ], 'favicon');
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'app\components\captcha\CaptchaAction',
            ],
        ];
    }

    /**
     * Добавить элемент в виджет Breadcrumbs
     * @param array|string $item
     */
    public function addBreadcrumbsItem($item)
    {
        Yii::$app->view->params['breadcrumbs'][] = $item;
    }

    /**
     * Выставляет нужные заголовки и конвертирует данные в JSON
     *
     * @param array $data Массив с данными для отправки
     */
    protected function sendJsonAnswer(array $data)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->content = json_encode($data);
        Yii::$app->response->send();
    }
}