<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\modules\good\models\Photo;
use app\modules\users\models\Photo as UserPhoto;

class FileController extends Controller
{
    /**
     * Загружает файл во временную директорию
     */
    public function actionUploadTempGoodPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $file = UploadedFile::getInstanceByName('file');
            $mime = BaseFileHelper::getMimeType($file->tempName);
            if (mb_strpos($mime, 'image') === false) {
                $answer['error'] = 'Данный файл не является изображением';
                echo json_encode($answer);
                exit;
            }

            $fileNumber = Yii::$app->request->post('fileNumber');
            $userId = Yii::$app->user->identity->id;
            if (!$userId || !$file || $fileNumber === null) {
                $answer['error'] = 'Произошла ошибка на сервере';
                echo json_encode($answer);
                exit;
            }

            $answer = Yii::$app->fileHelper->uploadTempGoodPhoto($userId, $file, $fileNumber);
            echo json_encode($answer);
        }
    }

    /**
     * Удаляет временную фотографию еще не созданного товара
     */
    public function actionDeleteTempGoodPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $fileNumber = Yii::$app->request->post('fileNumber');
            $userId = Yii::$app->user->identity->id;
            if (!$userId || $fileNumber === null) {
                $answer['error'] = 'Произошла ошибка на сервере';
                echo json_encode($answer);
                exit;
            }

            $answer = Yii::$app->fileHelper->deleteTempGoodPhoto($userId, $fileNumber);
            echo json_encode($answer);
        }
    }

    /**
     * Удаляет фотографию товара. Если это была главная фотография,
     * то назначает новую главную.
     */
    public function actionDeleteGoodPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $photoId = (int)Yii::$app->request->post('photoId');
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $userId = (int)Yii::$app->user->identity->id;
            if ($userId) {
                $Query = new \yii\db\Query();
                $userIdOfPhoto = $Query->select('s.user_id')
                    ->from('good_photos g_p')
                    ->innerJoin('goods g', 'g_p.good_id = g.id')
                    ->innerJoin('studio s', 'g.studio_id = s.id')
                    ->where(['g_p.id' => $photoId])
                    ->scalar();

                if ($userId == (int)$userIdOfPhoto) {
                    $Photo = Photo::findOne($photoId);
                    $isMainPhoto = $Photo->main;
                    $goodId = (int)$Photo->good_id;

                    if ($isMainPhoto) {
                        // назначим первую фотографию товара глвной
                        $Query = new \yii\db\Query();
                        Yii::$app->db->createCommand()
                            ->update(
                                Photo::getTableSchema()->name,
                                ['main' => 1],
                                ['id' => $Query->select('id')
                                    ->from(Photo::getTableSchema()->name)
                                    ->where([
                                        'good_id' => $goodId,
                                        'main' => 0
                                    ])
                                    ->limit(1)
                                    ->scalar()
                                ]
                            )->execute();
                    }

                    $photoDir = Photo::getPhotoDir($Photo->good_id);
                    if (file_exists($photoDir)) {
                        $files = BaseFileHelper::findFiles($photoDir);
                        $partName = $Photo->file_name . '_';
                        foreach ($files as $file) {
                            $file = BaseFileHelper::normalizePath($file);
                            $pathInfo = pathinfo($file);
                            if ($pathInfo['filename'] == $Photo->file_name || mb_strpos($pathInfo['filename'], $partName) !== false) {
                                unlink($file);
                            }
                        }
                    }
                    $delRes = $Photo->delete();

                    $answer['success'] = $delRes;
                } else {
                    $answer['error'] = 'Ошибка при попытке удаления';
                }
            } else {
                $answer['error'] = 'Ошибка при попытке удаления';
            }
            echo json_encode($answer);
        }
    }

    /**
     * Загружает фотографию пользователя во временную директорию
     */
    public function actionUploadTempUserPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $file = UploadedFile::getInstanceByName('file');
            $mime = BaseFileHelper::getMimeType($file->tempName);
            if (mb_strpos($mime, 'image') === false) {
                $answer['error'] = 'Данный файл не является изображением';
                echo json_encode($answer);
                exit;
            }

            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }
            if (!$session->id || !$file) {
                $answer['error'] = 'Произошла ошибка на сервере';
                echo json_encode($answer);
                exit;
            }

            $answer = Yii::$app->fileHelper->uploadTempUserPhoto($session->id, $file);
            echo json_encode($answer);
        }
    }

    /**
     * Удаляет временную фотографию пользователя
     */
    public function actionDeleteTempUserPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $session = Yii::$app->session;
            if (!$session->isActive) {
                $session->open();
            }
            if (!$session->id) {
                $answer['error'] = 'Произошла ошибка на сервере';
                echo json_encode($answer);
                exit;
            }

            $answer = Yii::$app->fileHelper->deleteTempUserPhoto($session->id);
            echo json_encode($answer);
        }
    }

    /**
     * Удаляет фотографию пользователя.
     */
    public function actionDeleteUserPhoto()
    {
        if (Yii::$app->request->isAjax) {
            $photoId = (int)Yii::$app->request->post('photoId');
            $answer = [
                'success' => null,
                'error' => null,
            ];
            $userId = (int)Yii::$app->user->identity->id;
            if ($userId) {
                $PhotoToDelete = UserPhoto::findOne($photoId);

                if ($PhotoToDelete && $PhotoToDelete->user_id == $userId) {
                    $photoDir = UserPhoto::getPhotoDir($PhotoToDelete->user_id);
                    if (file_exists($photoDir)) {
                        $files = BaseFileHelper::findFiles($photoDir);
                        foreach ($files as $file) {
                            $file = BaseFileHelper::normalizePath($file);
                            unlink($file);
                        }
                    }
                    $delRes = $PhotoToDelete->delete();

                    $answer['success'] = $delRes;
                } else {
                    $answer['error'] = 'Ошибка при попытке удаления';
                }
            } else {
                $answer['error'] = 'Ошибка при попытке удаления';
            }
            echo json_encode($answer);
        }
    }
}