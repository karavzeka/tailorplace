<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\modules\users\models\User;

class Feedback extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{feedback}}';
    }

    /**
     * Получить пользователя, отправившего сообщение
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_authorized_user']);
    }
}