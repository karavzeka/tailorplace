<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Модель истории статуса
 * @package app\models
 */
class OrderStatusHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{order_status_history}}';
    }

    /**
     * Получить соответствующую модель статуса
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Order::className(), ['status_id' => 'id']);
    }
}