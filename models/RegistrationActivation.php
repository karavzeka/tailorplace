<?php
namespace app\models;

use yii\db\ActiveRecord;

class RegistrationActivation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{registration_activation_keys}}';
    }

    /**
     * Получить пользователя, оставившего заявку
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}