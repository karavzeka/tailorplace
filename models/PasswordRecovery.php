<?php
/**
 * Заявка на восстановление пароля
 */

namespace app\models;

use app\modules\users\models\User;
use yii\db\ActiveRecord;

class PasswordRecovery extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{password_recovery}}';
    }

    /**
     * Получить пользователя, оставившего заявку
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}