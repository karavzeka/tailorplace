<?php
/**
 * Модель комментариев
 */

namespace app\models;

use app\modules\users\models\User;
use Yii;
use yii\db\ActiveRecord;

class Comment extends ActiveRecord
{
    const TYPE_GOOD = 'good';
    const TYPE_STUDIO = 'studio';

    /**
     * Максимальный отступ
     */
    const MAX_INDENT = 5;

    /**
     * @var self[] Дочерние коментарии
     */
    private $childComments = [];

    /**
     * @var int Шаг отступа комментария от левой границы
     */
    private $indent = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{comments}}';
    }

    /**
     * Получить владельца комментария
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Возвращает дочерние комментарии
     *
     * @return Comment[]
     */
    public function getChilds()
    {
        return $this->childComments;
    }

    /**
     * Увеличивает отступ у комментария и всех его дочерних комментариев
     */
    private function incrementIndent()
    {
        if ($this->indent < self::MAX_INDENT) {
            $this->indent++;
            foreach ($this->childComments as $Comment) {
                $Comment->incrementIndent();
            }
        }
    }

    /**
     * Возвращает отступ
     *
     * @return int
     */
    public function getIndent()
    {
        return $this->indent;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'filter', 'filter' => 'trim'],
            ['text', 'filter', 'filter' => 'strip_tags'],
            ['text', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Комментарий',
        ];
    }

    /**
     * Возвращает комментарии  в древовидной форме.
     * Комментарии содержат информацию о пользователе и его фотографию
     *
     * @param int $objectId Id комментируемого объекта (товара или студии)
     * @param string $objectType Тип объекта
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCommentsWithData($objectId, $objectType)
    {
        /** @var self[] $Comments */
        $Comments = self::find()
            ->with('user.photo')
            ->where([
                'object_id' => $objectId,
                'object_type' => $objectType
            ])
            ->indexBy('id')
            ->all();

        $noTopCommentIds = [];
        foreach ($Comments as $commentId => $Comment) {
            if (!empty($Comment->parent_id) && isset($Comments[$Comment->parent_id])) {
                $Comment->indent = $Comments[$Comment->parent_id]->indent + 1;
                $Comments[$Comment->parent_id]->childComments[] = $Comment;
                $noTopCommentIds[] = $Comment->id;
            }
        }

        // После вложения дочерних комментариев в родительские в массиве надо удалить все дочерние комментарии
        foreach ($noTopCommentIds as $commentId) {
            unset($Comments[$commentId]);
        }

        return $Comments;
    }
}