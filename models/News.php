<?php
/**
 * Модель новости
 * @package app\models
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class News extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'filter', 'filter' => 'trim'],
            ['title', 'required'],

            ['text', 'filter', 'filter' => 'trim'],
            ['text', 'required'],
        ];
    }
}