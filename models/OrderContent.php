<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\modules\good\models\Good;

/**
 * Модель содержимого заказа
 * @package app\models
 */
class OrderContent extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{order_content}}';
    }

    /**
     * Получить товар
     * @return \yii\db\ActiveQuery
     */
    public function getGood()
    {
        return $this->hasOne(Good::className(), ['good_id' => 'id']);
    }
}