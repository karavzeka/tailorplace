<?php

namespace app\models;

use Yii;
use yii\base\Model;

class FeedbackForm extends Model
{
    /**
     * @var string Email, на который будет выслана инструкция по восстановлению пароля
     */
    public $email;

    /**
     * Сообщение
     * @var string
     */
    public $message;

    /**
     * Капча
     * @var string
     */
    public $captcha;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Email - логин
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            // Сообщение
            ['message', 'filter', 'filter' => 'trim'],
            ['message', 'required'],
            // Капча
            ['captcha', 'required'],
            ['captcha', 'captcha', 'captchaAction' => 'common/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'message' => 'Сообщение',
            'captcha' => 'Код-подтверждение',
        ];
    }
}