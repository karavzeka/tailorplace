<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\modules\users\models\User;

class IpLogCommon extends ActiveRecord
{
    const ACTION_REGISTRATION = 'registration';
    const ACTION_AUTHORIZATION = 'authorization';
    const ACTION_CREATE_STUDIO = 'create_studio';

    public static function tableName()
    {
        return '{{ip_log_common}}';
    }

    /**
     * Получить пользователя
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}