<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Модель заказа
 * @package app\models
 */
class Order extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{orders}}';
    }

    /**
     * Получить содержимое заказа
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasMany(OrderContent::className(), ['order_id' => 'id']);
    }

    /**
     * Получить статус заказа
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * Получить способ доставки, выбранный пользователем при оформлении заказа
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * Получить способ оплаты, выбранный пользователем при оформлении заказа
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * Завершен ли заказ?
     * Здесь имеется ввиду, что заказ прошел все стадии и завершился, а не был отменен или отозван.
     *
     * @return bool
     */
    public function isCompleted()
    {
        return ($this->status_id == 2);
    }

    /**
     * Завершен ли заказ?
     * Здесь подразумевается любой конечный статус заказа
     *
     * @return bool
     */
    public function isFinished()
    {
        return (in_array($this->status_id, [2, 3, 4]));
    }

    /**
     * Возвращает в какие статусы можно перенести заказ пользователю
     *
     * @return mixed
     */
    public function getPossibleStatuses()
    {
        $User = Yii::$app->user->identity;
        $userStatus = $User->id == $this->buyer_id ? 'buyer' : 'seller';

        if ($userStatus == 'seller') {
            $possibleStatuses = Yii::$app->params['orderStatusRules'][$this->status_id]['seller'][$User->studio->type];
        } else {
            $possibleStatuses = Yii::$app->params['orderStatusRules'][$this->status_id]['buyer'];
        }

        return $possibleStatuses;
    }

    /**
     * Отрисовывает набор звезд
     *
     * @param float $evaluation Оценка заказа
     * @param string $starSize Размер звезд
     * @return string
     */
    public static function paintEvalStars($evaluation, $starSize = 'big')
    {
        if ($starSize == 'big') {
            $fillStar = '<i class="icon star"></i>';
            $emptyStar = '<i class="icon star-empty"></i>';
        } else {
            $fillStar = '<i class="icon little-star"></i>';
            $emptyStar = '<i class="icon little-star-empty"></i>';
        }
        $starsString = '';
        foreach (Yii::$app->params['evaluationThresholds'] as $threshold) {
            if (floatval($threshold) <= round($evaluation, 1)) {
                $starsString .= $fillStar;
            } else {
                $starsString .= $emptyStar;
            }
        }

        return $starsString;
    }
}