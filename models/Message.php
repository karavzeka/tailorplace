<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\modules\users\models\User;

class Message extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{messages}}';
    }

    /**
     * Получить отправителя
     * @return \yii\db\ActiveQuery
     */
    public function getFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'from_id']);
    }

    /**
     * Получить получателя
     * @return \yii\db\ActiveQuery
     */
    public function getTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //TODO поправить в каких сценариях применять фильтры
        return [
            ['from_id', 'required'],
            ['from_id', 'integer'],

            ['to_id', 'required'],
            ['to_id', 'integer'],

            ['text', 'filter', 'filter' => 'trim'],
            ['text', 'filter', 'filter' => 'strip_tags'],
            ['text', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => 'Сообщение',
        ];
    }
}