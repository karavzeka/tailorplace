<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Модель статуса заказа
 * @package app\models
 */
class OrderStatus extends ActiveRecord
{
    /**
     * Заказ завершен
     */
    const STATUS_COMPLITED = 2;

    /**
     * Заказ отклонен
     */
    const STATUS_REJECTED = 3;

    /**
     * Заказ отозван
     */
    const STATUS_WITHDRAWN = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{order_status_list}}';
    }

    /**
     * Получить заказы
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['id' => 'status_id']);
    }
}